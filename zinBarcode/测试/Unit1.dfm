object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 302
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 168
    Top = 232
    Width = 75
    Height = 25
    Caption = #35774#35745
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object frxReport1: TfrxReport
    Version = '4.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #39044#35774
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41432.794901909720000000
    ReportOptions.LastChange = 41432.794901909720000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 112
    Top = 160
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ZintBarcode1: TfrxZintBarcode
        Left = 40.000000000000000000
        Top = 57.000000000000000000
        Width = 210.000000000000000000
        Height = 182.000000000000000000
        ShowHint = False
        Zoom = 5.000000000000000000
        BarcodeType = tBARCODE_QRCODE
        DataFormat = dfANSI
        BorderWidth = 0
        OutputOptions = []
        FGColor = clBlack
        BGColor = clWhite
        Option1 = -1
        Option2 = 0
        Option3 = 928
        Roatation = r0
        ShowHumanReadableText = True
      end
      object ZintBarcode2: TfrxZintBarcode
        Left = 362.000000000000000000
        Top = 63.000000000000000000
        Width = 138.000000000000000000
        Height = 152.000000000000000000
        ShowHint = False
        Zoom = 1.000000000000000000
        BarcodeType = tBARCODE_PDF417TRUNC
        DataFormat = dfANSI
        BorderWidth = 0
        OutputOptions = []
        FGColor = clBlack
        BGColor = clWhite
        Option1 = 2
        Option2 = 2
        Option3 = 928
        Roatation = r0
        ShowHumanReadableText = True
      end
      object ZintBarcode3: TfrxZintBarcode
        Left = 345.000000000000000000
        Top = 303.000000000000000000
        Width = 120.000000000000000000
        Height = 147.000000000000000000
        ShowHint = False
        Zoom = 5.000000000000000000
        BarcodeType = tBARCODE_DATAMATRIX
        DataFormat = dfANSI
        BorderWidth = 0
        OutputOptions = []
        FGColor = clBlack
        BGColor = clWhite
        Option1 = -1
        Option2 = 0
        Option3 = 928
        Roatation = r0
        ShowHumanReadableText = True
      end
    end
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 232
    Top = 112
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 368
    Top = 104
  end
  object frxOLEObject1: TfrxOLEObject
    Left = 160
    Top = 72
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 32
    Top = 88
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 288
    Top = 208
  end
  object frxDotMatrixExport1: TfrxDotMatrixExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    EscModel = 0
    GraphicFrames = False
    SaveToFile = False
    UseIniSettings = True
    Left = 328
    Top = 176
  end
  object frxGZipCompressor1: TfrxGZipCompressor
    Left = 304
    Top = 272
  end
  object frxChartObject1: TfrxChartObject
    Left = 280
    Top = 48
  end
end
