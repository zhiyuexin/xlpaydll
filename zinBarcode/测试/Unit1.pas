unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ufrxZintBarcode, StdCtrls, Buttons, frxClass, frxDesgn, frxChart,
  frxGZip, frxDMPExport, frxChBox, frxBarcode, frxOLE, frxCross;

type
  TForm1 = class(TForm)
    frxReport1: TfrxReport;
    BitBtn1: TBitBtn;
    frxDesigner1: TfrxDesigner;
    frxCrossObject1: TfrxCrossObject;
    frxOLEObject1: TfrxOLEObject;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxDotMatrixExport1: TfrxDotMatrixExport;
    frxGZipCompressor1: TfrxGZipCompressor;
    frxChartObject1: TfrxChartObject;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
    frxReport1.DesignReport();
end;

end.
