�
 TSERVERFORM1 0r  TPF0TServerForm1ServerForm1Left�Top� Width�HeightCaption
TCP ClientColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight 	TSplitter	Splitter1Left Top� Width�HeightCursorcrVSplitAlignalBottom  TMemoMemo2Left Top� Width�HeightoAlignalBottom
ScrollBarsssBothTabOrder OnExit	Memo2Exit	OnKeyDownMemo2KeyDown  TPanelPanel1Left TopjWidth�Height{AlignalBottomTabOrder TLabelLabel1LeftTopWidth&HeightCaptionAddress  TLabelLabel2LeftTop$WidthHeightCaptionPort(s)  TLabelLabel3LeftTopWidth>HeightCaptionConnections:  TLabellblConCountLeftTop'Width=Height	AlignmenttaCenterAutoSizeCaption---Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabellblCliCountLeftTop7Width=Height	AlignmenttaCenterAutoSizeCaption---Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabellblTotalLeftTopFWidth=Height	AlignmenttaCenterAutoSizeCaption---Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4LeftTop5WidthCHeightCaption
Reconnect:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabellblMemLeftTop	WidthqHeightjAutoSizeCaptionMemory  TLabellblDataLeft�Top
Width}HeightpAutoSizeCaptionData  TLabellblPreCountLeftTopWidth=Height	AlignmenttaCenterAutoSizeCaption---Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TButtonbtnSendLeft� TopYWidthAHeightCaptionSendEnabledTabOrder OnClickbtnSendClick  TButton
btnConnectLeftjTop7WidthAHeightCaptionConnectTabOrderOnClickbtnConnectClick  TEditedAddrLeft<TopWidth� HeightTabOrderText	localhost  TEditedPortLeft<Top Width-HeightTabOrderText8092  	TCheckBoxxRetryOnFailLeftTopTWidth9HeightCaptionOnFailTabOrderOnClickxRetryOnFailClick  TButtonButton1Left TopRWidthEHeightCaptionAuto-createTabOrderVisibleOnClickButton1Click  TButtonButton2LeftiTopYWidthAHeightCaptionCLRTabOrderOnClickButton2Click  	TCheckBoxxMultiThreadLeftpTop$WidthEHeightCaptionThreadsTabOrderOnClickxMultiThreadClick  	TCheckBoxxRetryOnErrorLeftTopEWidth=HeightCaptionOnErrorTabOrder	OnClickxRetryOnErrorClick  	TCheckBoxxRetryOnLostLeftTopcWidth9HeightCaptionOnLostTabOrder
OnClickxRetryOnLostClick  TButtonbtnCloseLeft� Top7WidthAHeightCaption
DisconnectEnabledTabOrderOnClickbtnCloseClick  	TCheckBoxxSilentLeft� Top$Width1HeightCaptionSilentTabOrderOnClickxSilentClick  TButtonbtnMemLeftTop6WidthAHeightCaptionFreeMemTabOrderVisibleOnClickbtnMemClick   TMemoMemo1Left Top Width�Height� AlignalClient
ScrollBarsssBothTabOrderOnExit	Memo2Exit  TTimerTimer1EnabledOnTimerTimer1TimerLeftTTop  TRtcTcpServerServerTimeout.AfterConnectingOnConnectingServerConnectingOnDisconnectingServerDisconnecting	OnConnectServerConnectOnDisconnectServerDisconnectOnExceptionServerExceptionOnClientConnectServerClientConnectOnClientDisconnectServerClientDisconnectOnListenLostServerListenLostOnListenErrorServerListenError	OnRestartServerRestart	OnDataOutServerDataOut
OnDataSentServerDataSentOnDataReceivedServerDataReceivedLeftTop  TRtcTcpClientClientTimeout.AfterConnectingOnConnectingClientConnectingOnDisconnectingClientDisconnecting	OnConnectClientConnectOnDisconnectClientDisconnectOnExceptionClientExceptionOnConnectFailClientConnectFailOnConnectLostClientConnectLostOnConnectErrorClientConnectErrorOnReconnectClientReconnectOnReadyToSendClientReadyToSend	OnDataOutClientDataOut
OnDataSentClientDataSentOnDataReceivedClientDataReceivedLeft0Top   