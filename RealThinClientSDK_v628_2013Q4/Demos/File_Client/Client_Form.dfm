object RtcFileClient: TRtcFileClient
  Left = 308
  Top = 209
  Width = 1024
  Height = 545
  Caption = 'RTC File Client Demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 507
    Align = alClient
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 419
      Top = 0
      Width = 2
      Height = 507
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 419
      Height = 507
      Align = alLeft
      AutoSize = True
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 0
      object Bevel1: TBevel
        Left = 172
        Top = 4
        Width = 161
        Height = 65
        Shape = bsFrame
      end
      object Label7: TLabel
        Left = 4
        Top = 271
        Width = 36
        Height = 13
        Caption = 'Method'
      end
      object Label8: TLabel
        Left = 76
        Top = 271
        Width = 186
        Height = 13
        Caption = 'File Name, without "http://serveraddr/"'
      end
      object Label10: TLabel
        Left = 68
        Top = 291
        Width = 7
        Height = 13
        Caption = '/'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 4
        Top = 311
        Width = 83
        Height = 13
        Caption = 'Query parameters'
      end
      object Label9: TLabel
        Left = 4
        Top = 246
        Width = 56
        Height = 13
        Caption = 'Host Name:'
      end
      object Label3: TLabel
        Left = 4
        Top = 163
        Width = 56
        Height = 13
        Caption = 'Server Port:'
      end
      object Label2: TLabel
        Left = 76
        Top = 163
        Width = 75
        Height = 13
        Caption = 'Server Address:'
      end
      object Label1: TLabel
        Left = 4
        Top = 226
        Width = 60
        Height = 13
        Caption = 'REQUEST'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 4
        Top = 386
        Width = 87
        Height = 13
        Caption = 'Total bytes in+out:'
      end
      object lblDataInOut: TLabel
        Left = 109
        Top = 387
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label13: TLabel
        Left = 4
        Top = 75
        Width = 303
        Height = 13
        Caption = 
          'Proxy Address, with "http://" and port number (empty for default' +
          ')'
      end
      object Label14: TLabel
        Left = 4
        Top = 119
        Width = 77
        Height = 13
        Caption = 'Proxy Username'
      end
      object Label15: TLabel
        Left = 164
        Top = 119
        Width = 75
        Height = 13
        Caption = 'Proxy Password'
      end
      object lblStatus: TLabel
        Left = 4
        Top = 208
        Width = 74
        Height = 13
        Caption = 'Not connected.'
      end
      object Label5: TLabel
        Left = 342
        Top = 60
        Width = 56
        Height = 13
        Caption = 'Reconnect:'
      end
      object eReqMethod: TEdit
        Left = 4
        Top = 286
        Width = 57
        Height = 24
        TabOrder = 9
        Text = 'GET'
      end
      object eReqFileName: TEdit
        Left = 76
        Top = 286
        Width = 249
        Height = 24
        TabOrder = 10
        OnChange = eReqFileNameChange
      end
      object eReqQuery: TEdit
        Left = 4
        Top = 326
        Width = 321
        Height = 24
        TabOrder = 11
      end
      object xSaveToFile: TCheckBox
        Left = 4
        Top = 356
        Width = 82
        Height = 21
        Caption = 'Save to file:'
        TabOrder = 12
      end
      object eFileName: TEdit
        Left = 84
        Top = 356
        Width = 205
        Height = 24
        TabOrder = 13
        Text = 'download\index.htm'
      end
      object btnSaveAs: TButton
        Left = 301
        Top = 356
        Width = 25
        Height = 21
        Caption = '...'
        TabOrder = 14
        OnClick = btnSaveAsClick
      end
      object eReqHost: TEdit
        Left = 76
        Top = 245
        Width = 249
        Height = 24
        TabStop = False
        TabOrder = 8
        Text = 'www.realthinclient.com'
      end
      object ePort: TEdit
        Left = 4
        Top = 180
        Width = 61
        Height = 24
        TabStop = False
        TabOrder = 6
        Text = '80'
        OnChange = ePortChange
      end
      object eAddr: TEdit
        Left = 76
        Top = 180
        Width = 249
        Height = 24
        TabOrder = 7
        Text = 'www.realthinclient.com'
        OnChange = eAddrChange
      end
      object xUseProxy: TCheckBox
        Left = 112
        Top = 29
        Width = 49
        Height = 17
        TabStop = False
        Alignment = taLeftJustify
        Caption = 'Proxy'
        TabOrder = 0
        OnClick = xUseProxyClick
      end
      object xThreads: TCheckBox
        Left = 4
        Top = 29
        Width = 101
        Height = 17
        TabStop = False
        Caption = 'Multi-Threaded'
        TabOrder = 15
        OnClick = xThreadsClick
      end
      object xUseSSL: TCheckBox
        Left = 116
        Top = 9
        Width = 45
        Height = 17
        TabStop = False
        Alignment = taLeftJustify
        Caption = 'SSL'
        TabOrder = 2
        OnClick = xUseSSLClick
      end
      object xAutoConnect: TCheckBox
        Left = 4
        Top = 9
        Width = 89
        Height = 17
        TabStop = False
        Caption = 'Auto-Connect'
        Checked = True
        State = cbChecked
        TabOrder = 16
        OnClick = xAutoConnectClick
      end
      object xWinHTTP: TCheckBox
        Left = 88
        Top = 49
        Width = 73
        Height = 17
        TabStop = False
        Alignment = taLeftJustify
        Caption = 'WinHTTP'
        TabOrder = 1
        OnClick = xWinHTTPClick
      end
      object eProxyAddr: TEdit
        Left = 4
        Top = 92
        Width = 321
        Height = 24
        TabStop = False
        TabOrder = 3
        OnChange = eProxyAddrChange
      end
      object eProxyUsername: TEdit
        Left = 4
        Top = 136
        Width = 149
        Height = 24
        TabStop = False
        TabOrder = 4
        OnChange = eProxyUsernameChange
      end
      object eProxyPassword: TEdit
        Left = 164
        Top = 136
        Width = 161
        Height = 24
        TabStop = False
        TabOrder = 5
        OnChange = eProxyPasswordChange
      end
      object xBlocking: TCheckBox
        Left = 4
        Top = 49
        Width = 69
        Height = 17
        TabStop = False
        Caption = 'Blocking'
        TabOrder = 17
        OnClick = xBlockingClick
      end
      object xCryptPlugin: TCheckBox
        Left = 180
        Top = 9
        Width = 149
        Height = 17
        Caption = 'CryptPlugin (dummy)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 18
        OnClick = xCryptPluginClick
      end
      object xTrustServer: TCheckBox
        Left = 180
        Top = 28
        Width = 141
        Height = 17
        Caption = 'Trust anything received'
        TabOrder = 19
        OnClick = xTrustServerClick
      end
      object xAllowExpired: TCheckBox
        Left = 180
        Top = 48
        Width = 145
        Height = 17
        Caption = 'Allow Expired Certificates'
        TabOrder = 20
        OnClick = xAllowExpiredClick
      end
      object xUseHttp10: TCheckBox
        Left = 76
        Top = 224
        Width = 181
        Height = 17
        Caption = 'Use the old HTTP/1.0 protocol?'
        TabOrder = 21
      end
      object btnConnect: TButton
        Left = 342
        Top = 5
        Width = 73
        Height = 26
        Caption = 'Connect'
        TabOrder = 22
        TabStop = False
        OnClick = btnConnectClick
      end
      object btnDisconnect: TButton
        Left = 342
        Top = 30
        Width = 73
        Height = 26
        Caption = 'Disconnect'
        Enabled = False
        TabOrder = 23
        TabStop = False
        OnClick = btnDisconnectClick
      end
      object btnPost: TButton
        Left = 342
        Top = 145
        Width = 73
        Height = 25
        Caption = 'POST'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 24
        OnClick = btnPostClick
      end
      object btn100Post: TButton
        Left = 342
        Top = 171
        Width = 73
        Height = 25
        Caption = '100 x POST'
        TabOrder = 25
        OnClick = btn100PostClick
      end
      object btnCancelAll: TButton
        Left = 342
        Top = 198
        Width = 73
        Height = 25
        Caption = 'SKIP ALL'
        TabOrder = 26
        OnClick = btnCancelAllClick
      end
      object xReconError: TCheckBox
        Left = 346
        Top = 75
        Width = 65
        Height = 17
        Caption = 'on Error'
        TabOrder = 27
        OnClick = xReconErrorClick
      end
      object xReconFail: TCheckBox
        Left = 346
        Top = 92
        Width = 65
        Height = 16
        Caption = 'on Fail'
        Checked = True
        State = cbChecked
        TabOrder = 28
        OnClick = xReconFailClick
      end
      object xReconLost: TCheckBox
        Left = 346
        Top = 108
        Width = 65
        Height = 17
        Caption = 'on Lost'
        Checked = True
        State = cbChecked
        TabOrder = 29
        OnClick = xReconLostClick
      end
      object xShowWarning: TCheckBox
        Left = 346
        Top = 125
        Width = 69
        Height = 17
        Caption = '>5? Stop!'
        Checked = True
        State = cbChecked
        TabOrder = 30
      end
    end
    object Panel6: TPanel
      Left = 421
      Top = 0
      Width = 587
      Height = 507
      Align = alClient
      TabOrder = 1
      object Splitter2: TSplitter
        Left = 1
        Top = 76
        Width = 585
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 585
        Height = 75
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 585
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label4: TLabel
            Left = 8
            Top = 5
            Width = 68
            Height = 13
            Caption = 'RESPONSE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblCount: TLabel
            Left = 192
            Top = 5
            Width = 55
            Height = 13
            Caption = '0 Received'
          end
          object lblRetry: TLabel
            Left = 96
            Top = 5
            Width = 48
            Height = 13
            Caption = '0 Reposts'
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 21
          Width = 585
          Height = 54
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object eResponseHeader: TMemo
            Left = 2
            Top = 2
            Width = 581
            Height = 50
            Align = alClient
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
      object Panel8: TPanel
        Left = 1
        Top = 79
        Width = 585
        Height = 427
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 585
          Height = 43
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object lblTime: TLabel
            Left = 154
            Top = 5
            Width = 87
            Height = 13
            Caption = 'Time: 0 < 0 < 0 ms'
          end
          object Label6: TLabel
            Left = 8
            Top = 5
            Width = 51
            Height = 13
            Caption = 'Content in:'
          end
          object lblSpeed: TLabel
            Left = 346
            Top = 5
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = '0 KB/s'
          end
          object lblBytes: TLabel
            Left = 64
            Top = 5
            Width = 34
            Height = 13
            Caption = '0 bytes'
          end
          object pBar: TProgressBar
            Left = 8
            Top = 25
            Width = 413
            Height = 16
            TabOrder = 0
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 43
          Width = 585
          Height = 384
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object eResponseBody: TMemo
            Left = 2
            Top = 2
            Width = 581
            Height = 380
            Align = alClient
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
  end
  object Client: TRtcHttpClient
    ServerAddr = 'www.realthinclient.com'
    ServerPort = '80'
    OnConnect = ClientConnect
    OnDisconnect = ClientDisconnect
    OnException = ClientException
    ReconnectOn.ConnectLost = True
    ReconnectOn.ConnectFail = True
    OnConnectFail = ClientConnectFail
    OnConnectError = ClientConnectError
    AutoConnect = True
    OnDataOut = ClientDataOut
    OnDataIn = ClientDataIn
    Left = 204
    Top = 248
  end
  object DataRequest: TRtcDataRequest
    AutoSyncEvents = True
    Client = Client
    OnBeginRequest = DataRequestBeginRequest
    OnRepostCheck = DataRequestRepostCheck
    OnResponseAbort = DataRequestResponseAbort
    OnDataReceived = DataRequestDataReceived
    Left = 260
    Top = 248
  end
  object SaveDialog1: TSaveDialog
    Left = 361
    Top = 234
  end
end
