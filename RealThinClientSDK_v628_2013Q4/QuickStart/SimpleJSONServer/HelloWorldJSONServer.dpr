program HelloWorldJSONServer;

{$include rtcDefs.inc}

uses
  rtcLog,
  Forms,
  Unit5 in 'Unit5.pas' {Form5},
  uDMFileProvider in '..\..\..\XLPay_Service\uDMFileProvider.pas' {File_Provider: TDataModule},
  uDMXLDProvider in '..\..\..\XLPay_Service\uDMXLDProvider.pas' {XLD_Provider: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TFile_Provider, File_Provider);
  Application.CreateForm(TXLD_Provider, XLD_Provider);
  Application.Run;
end.
