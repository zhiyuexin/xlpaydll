unit SrvXLPayService;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  rtcConn, rtcDataSrv, rtcHttpSrv, rtcInfo, rtcTcpSrv, rtcDataCli, rtcHttpCli,

  DB, DBAccess, Uni,
  UniProvider, SQLiteUniProvider, AccessUniProvider,
  SQLServerUniProvider, OracleUniProvider, MySQLUniProvider,
  InterBaseUniProvider, DB2UniProvider, ASEUniProvider,
  AdvantageUniProvider, ODBCUniProvider

  ;

type
  TServiceXLPay = class(TService)
    ServerTcp: TRtcTcpServer;
    ServerHTTPS: TRtcHttpServer;
    ServerHTTP: TRtcHttpServer;
    XLDClient: TRtcHttpClient;
    RtcDataRequest1: TRtcDataRequest;
    ConnLocal: TUniConnection;
    procedure ServerHTTPException(Sender: TRtcConnection; E: Exception);
    procedure ServerHTTPInvalidRequest(Sender: TRtcConnection);
    procedure ServerHTTPListenError(Sender: TRtcConnection; E: Exception);
    procedure ServerHTTPListenLost(Sender: TRtcConnection);
    procedure ServerHTTPListenStart(Sender: TRtcConnection);
    procedure ServerHTTPListenStop(Sender: TRtcConnection);
    procedure ServerHTTPRequestNotAccepted(Sender: TRtcConnection);
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceCreate(Sender: TObject);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    function StartService(): Boolean;
    function StopService(): Boolean;
    function PauseService(): Boolean;
    function ContinueService(): Boolean;
  public
    function GetServiceController: TServiceController; override;
  end;

var
  ServiceXLPay: TServiceXLPay;

implementation

uses Common, Logger, uDMFileProvider, IniFiles;
{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  ServiceXLPay.Controller(CtrlCode);
end;

function TServiceXLPay.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TServiceXLPay.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Started := StartService;
end;

procedure TServiceXLPay.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Stopped := StopService;
end;

procedure TServiceXLPay.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Paused := PauseService;
end;

procedure TServiceXLPay.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  Continued := ContinueService;
end;

procedure TServiceXLPay.ServiceCreate(Sender: TObject);
begin
//
end;

////////////////////////////////////////////////////////////////////////////////
// 启动服务
////////////////////////////////////////////////////////////////////////////////
function TServiceXLPay.StartService: Boolean;
var
  i: Integer;
  ls: TStringList;
begin
  Result := False;
  try
    Log('Starting XingLongPay service...'#13#10 +
      '------------------------------------------------------------'#13#10 +
      '--                                                        --'#13#10 +
      '--                   XingLongPay Service                  --'#13#10 +
      '--                                                        --'#13#10 +
      '--                   Verison: 1.0.0                       --'#13#10 +
      '--                   IssueDate: 2015-04-25                --'#13#10 +
      '--                                                        --'#13#10 +
      '------------------------------------------------------------');

    // 连接本地数据库
    try

      ConnLocal.ProviderName := 'SQLite';
      ConnLocal.Database := 'c:\data.db';
      ConnLocal.Connect;
    except
      on e: Exception do
      begin
        Err(501, 'Connecting to local database exception has occurred.', e);
        Exit;
      end;
    end;
    // 启动HTTP服务
    try
      CreateWebRootDir();
      ServerHTTP.ServerPort := IntToStr(GetHttpPort);
      Log('Starting HttpServer on port ' + ServerHTTP.ServerPort + ' ...');
      with GetFileProvider do
      begin
        ClearIndexPages;
        AddIndexPage('index.html');
        AddIndexPage('index.htm');
        AddIndexPage('default.html');
        AddIndexPage('default.htm');
        ClearHosts;
        Log('loading web virtual directory: * = ' + GetWebRootPath);
        AddHost('* = ' + GetWebRootPath);
        Log('loading web virtual directory: ROOT = ' + GetServicePath);
        AddHost('ROOT = ' + GetServicePath);
        Log('loading web virtual directory: LOG = ' + GetLogPath);
        AddHost('LOG = ' + GetLogPath);

        ls := TStringList.Create;
        try
          with TIniFile.Create(GetConfigPath) do
            try
              ReadSectionValues('WEB_HOSTS', ls);
            finally
              Free;
            end;
          for i := 0 to ls.Count - 1 do
          begin
            Log('loading web virtual directory: ' + ls.Strings[i]);
            AddHost(ls.Strings[i]);
          end;
        finally
          ls.Free;
        end;

        ClearContentTypes;
        //AddContentType('html,zip');
        ServerLink.Server := ServerHTTP;
      end;
      ServerHTTP.Listen;
    except
      on e: Exception do
      begin
        Err(502, 'Starting http service exception has occurred.', e);
        Exit;
      end;
    end;

    //连接XLD Server
    try
      XLDClient.ServerAddr := GetXLDServiceAddr;
      XLDClient.ServerPort := GetXLDServicePort;
      Log('Connecting to xld server at ' + XLDClient.ServerAddr + ' on port ' + XLDClient.ServerPort);
      XLDClient.Connect(True, True);
    except
      on e: Exception do
      begin
        Err(503, 'Connecting xld server exception has occurred.', e);
        Exit;
      end;
    end;

    Log('XingLongPay service has started.');
    Result := True;
  except
    on e: Exception do
    begin
      Err(500, 'Service starting exception has occurred.', e);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 暂停服务
////////////////////////////////////////////////////////////////////////////////
function TServiceXLPay.PauseService: Boolean;
begin
  Result := False;
  try
    Log('Pausing XingLongPay service...');
    ServerHTTP.StopListen;
    XLDClient.Disconnect;

    Log('XingLongPay service has paused.');
    Result := True;
  except
    on e: Exception do
    begin
      Err(510, 'Service pausing exception has occurred.', e);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 继续服务
////////////////////////////////////////////////////////////////////////////////
function TServiceXLPay.ContinueService: Boolean;
begin
  Result := False;
  try
    Log('Continue XingLongPay service...');
    ServerHTTP.Listen;
    XLDClient.Reconnect();


    Log('XingLongPay service has continued.');
    Result := True;
  except
    on e: Exception do
    begin
      Err(520, 'Service continue exception has occurred.', e);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 停止服务
////////////////////////////////////////////////////////////////////////////////
function TServiceXLPay.StopService: Boolean;
begin
  Result := False;
  try
    Log('Stopping XingLongPay service...');
    ServerHTTP.StopListen;
    XLDClient.Disconnect;


    Log('XingLongPay service has stopped.'#13#10#13#10#13#10);
    Result := True;
  except
    on e: Exception do
    begin
      Err(530, 'Service stopping exception has occurred.', e);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 服务安装后添加初始信息
////////////////////////////////////////////////////////////////////////////////
procedure TServiceXLPay.ServiceAfterInstall(Sender: TService);
begin
  WriteServiceInfo();
end;

procedure TServiceXLPay.ServerHTTPException(Sender: TRtcConnection; E: Exception);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPException, E)
  else
    Err(201, 'HttpServer Exception', E);
end;

procedure TServiceXLPay.ServerHTTPInvalidRequest(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPInvalidRequest)
  else
    Log('HttpServer received an invalid Request.');
end;

procedure TServiceXLPay.ServerHTTPListenError(Sender: TRtcConnection; E: Exception);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenError, E)
  else
    Err(201, 'Can not Start the HttpServer on Port ' + Sender.ServerPort, E);
end;

procedure TServiceXLPay.ServerHTTPListenLost(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenLost)
  else
  begin
    Log('HttpServer stopped unexpectedly.');
    // todo... restart
  end;
end;

procedure TServiceXLPay.ServerHTTPListenStart(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenStart)
  else
  begin
    Log('HttpServer is running on port ' + Sender.LocalPort + ' .');
  end;
end;

procedure TServiceXLPay.ServerHTTPListenStop(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenStop)
  else
  begin
    Log('Server stopped by user.');
  end;
end;

procedure TServiceXLPay.ServerHTTPRequestNotAccepted(Sender: TRtcConnection);
var
  Srv: TRtcDataServer absolute Sender;
begin
  Srv.Write('Bad Request: ' + Srv.Request.FileName);
end;

end.
