unit Common;

interface

const
  SERVICE_NAME = 'XingLongPay';  //from service file
  HOST_DIRECTORY = 'WebRoot';
  UPLOAD_DIRECTORY = 'UploadFiles';
  DATA_FILE = 'data.db';
  CONFIG_FILE = 'config.ini';
  DEFAULT_HTTP_PORT = 80;
  DEFAULT_HTTPS_PORT = 443;
  DEFAULT_TCP_PORT = 7000;

function GetGuid(): string;
function GetServicePath: string;
function GetWebRootPath(): string;
function GetUploadPath(): string;
function GetConfigPath(): string;
function GetDataPath(): string;
procedure CreateWebRootDir;
procedure WriteServiceInfo;
function GetServicePort(value: String; default: Integer): Integer;
function GetHttpPort: Integer;
function GetHttpsPort: Integer;
function GetTcpPort: Integer;

function GetXLDServiceAddr(): String;
function GetXLDServicePort(): String;

implementation

uses
  SysUtils, Windows, Classes,
//  UniProvider, SQLiteUniProvider, AccessUniProvider,
//  SQLServerUniProvider, OracleUniProvider, MySQLUniProvider,
//  InterBaseUniProvider, DB2UniProvider, ASEUniProvider,
//  AdvantageUniProvider, ODBCUniProvider,
//  DBAccess, Uni,
  IniFiles, Registry;

function GetGuid(): string;
var
  LTep: TGUID;
begin
  CreateGUID(LTep);
  Result := GUIDToString(LTep);
  Result := StringReplace(Result, '-', '', [rfReplaceAll]);
  Result := Copy(Result, 2, Length(Result) - 2);
end;

function GetServicePath: string;
begin
  Result := ExtractFilePath(ParamStr(0));
end;
function GetWebRootPath(): string;
begin
  Result := GetServicePath + HOST_DIRECTORY + '\';
end;
function GetUploadPath(): string;
begin
  Result := GetWebRootPath + UPLOAD_DIRECTORY + '\';
end;
function GetConfigPath(): string;
begin
  Result := GetWebRootPath + CONFIG_FILE + '\';
end;
function GetDataPath(): string;
begin
  Result := GetWebRootPath + DATA_FILE + '\';
end;
procedure CreateWebRootDir;
var
  dir: String;
begin
  dir := GetWebRootPath;
  if not DirectoryExists(dir) then
  begin
    CreateDir(dir);
  end;
  dir := GetUploadPath;
  if not DirectoryExists(dir) then
  begin
    CreateDir(dir);
  end;
end;

procedure WriteServiceInfo;
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        // ����������Ϣ
        WriteString('Description', 'XingLongPay Service');
        WriteString('ServiceInfo', 'XingLongPay Service');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

function GetServicePort(value: String; default: Integer): Integer;
begin
  Result := default;
  with TIniFile.Create(GetConfigPath) do
  begin
    try
      Result := ReadInteger('SERVICE_PORT', value, Result);
    finally
      Free;
    end;
  end;
end;

function GetHttpPort(): Integer;
begin
  Result := GetServicePort('http', DEFAULT_HTTP_PORT);
end;

function GetHttpsPort(): Integer;
begin
  Result := GetServicePort('https', DEFAULT_HTTPS_PORT);
end;

function GetTcpPort(): Integer;
begin
  Result := GetServicePort('tcp', DEFAULT_TCP_PORT);
end;

function GetXLDServiceAddr(): String;
begin
  Result := '';
  with TIniFile.Create(GetConfigPath) do
  begin
    try
      Result := ReadString('XLD_SERVICE', 'ServerAddr', Result);
    finally
      Free;
    end;
  end;
end;

function GetXLDServicePort(): String;
begin
  Result := '';
  with TIniFile.Create(GetConfigPath) do
  begin
    try
      Result := ReadString('XLD_SERVICE', 'ServerPort', Result);
    finally
      Free;
    end;
  end;
end;

end.
