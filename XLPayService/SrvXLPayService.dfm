object ServiceXLPay: TServiceXLPay
  OldCreateOrder = False
  OnCreate = ServiceCreate
  DisplayName = 'XingLongPay'
  AfterInstall = ServiceAfterInstall
  OnContinue = ServiceContinue
  OnPause = ServicePause
  OnStart = ServiceStart
  OnStop = ServiceStop
  Left = 693
  Top = 427
  Height = 393
  Width = 518
  object ServerTcp: TRtcTcpServer
    MultiThreaded = True
    Left = 159
    Top = 8
  end
  object ServerHTTPS: TRtcHttpServer
    MultiThreaded = True
    Timeout.AfterConnecting = 300
    ServerPort = '443'
    RestartOn.ListenLost = True
    FixupRequest.RemovePrefix = True
    MaxRequestSize = 128000
    MaxHeaderSize = 16000
    Left = 94
    Top = 8
  end
  object ServerHTTP: TRtcHttpServer
    ServerPort = '80'
    OnException = ServerHTTPException
    OnListenStart = ServerHTTPListenStart
    OnListenStop = ServerHTTPListenStop
    OnListenLost = ServerHTTPListenLost
    OnListenError = ServerHTTPListenError
    OnRequestNotAccepted = ServerHTTPRequestNotAccepted
    OnInvalidRequest = ServerHTTPInvalidRequest
    Left = 28
    Top = 8
  end
  object XLDClient: TRtcHttpClient
    AutoConnect = True
    Left = 38
    Top = 105
  end
  object RtcDataRequest1: TRtcDataRequest
    Client = XLDClient
    Left = 121
    Top = 106
  end
  object ConnLocal: TUniConnection
    ProviderName = 'SQLite'
    Left = 69
    Top = 191
  end
end
