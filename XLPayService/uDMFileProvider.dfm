object File_Provider: TFile_Provider
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1129
  Top = 416
  Height = 260
  Width = 480
  object FileProvider: TRtcDataProvider
    Link = ServerLink
    CheckOrder = 900
    OnCheckRequest = FileProviderCheckRequest
    OnDataReceived = FileProviderDataReceived
    OnDataSent = FileProviderDataSent
    OnDisconnect = FileProviderDisconnect
    Left = 309
    Top = 15
  end
  object TimeProvider: TRtcDataProvider
    Link = ServerLink
    CheckOrder = 700
    OnCheckRequest = TimeProviderCheckRequest
    OnDataReceived = TimeProviderDataReceived
    Left = 232
    Top = 15
  end
  object ServerLink: TRtcDataServerLink
    CheckOrder = 1000
    Left = 24
    Top = 15
  end
  object FileInfo: TdcFileInfo
    Left = 376
    Top = 15
  end
  object PageProvider: TRtcDataProvider
    Link = ServerLink
    CheckOrder = 100
    OnCheckRequest = PageProviderCheckRequest
    OnDataReceived = PageProviderDataReceived
    Left = 87
    Top = 17
  end
  object RtcScriptEngine1: TRtcScriptEngine
    FunctionGroup = RtcFunctionGroup
    ScriptOpen = '<?'
    ScriptClose = '?>'
    MaxCodeDepth = 500
    MaxRecursion = 200
    MaxLoopCount = 100000
    Left = 24
    Top = 152
  end
  object RtcFunctionGroup: TRtcFunctionGroup
    Left = 24
    Top = 88
  end
  object RtcFncTime: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'time'
    OnExecute = RtcFncTimeExecute
    Left = 128
    Top = 88
  end
  object RtcFncFileInfo: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'FileInfo'
    OnExecute = RtcFncFileInfoExecute
    Left = 96
    Top = 88
  end
  object RtcFncQuery: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'Q'
    Left = 64
    Top = 87
  end
  object RtcFncNow: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'Now'
    OnExecute = RtcFncNowExecute
    Left = 160
    Top = 88
  end
  object RtcFncDate: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'Date'
    OnExecute = RtcFncDateExecute
    Left = 192
    Top = 88
  end
  object RtcFncFormatDateTime: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'FormatDateTime'
    OnExecute = RtcFncFormatDateTimeExecute
    Left = 224
    Top = 88
  end
  object RtcFncReplace: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'Replace'
    OnExecute = RtcFncReplaceExecute
    Left = 256
    Top = 88
  end
  object RtcFunction1: TRtcFunction
    Group = RtcFunctionGroup
    Left = 392
    Top = 88
  end
  object RtcFncCopy: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'Copy'
    OnExecute = RtcFncCopyExecute
    Left = 288
    Top = 88
  end
  object UploadProvider: TRtcDataProvider
    Link = ServerLink
    CheckOrder = 300
    OnCheckRequest = UploadProviderCheckRequest
    OnDataReceived = UploadProviderDataReceived
    Left = 160
    Top = 16
  end
  object VCLZip1: TVCLZip
    MultiZipInfo.BlockSize = 1457600
    Left = 88
    Top = 152
  end
  object RtcFncExecSQL: TRtcFunction
    Group = RtcFunctionGroup
    FunctionName = 'ExecSQL'
    Left = 328
    Top = 88
  end
end
