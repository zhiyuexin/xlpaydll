unit Logger;

interface

uses
  SysUtils, Windows, Classes;

const
  LOG_FOLDER = 'Log';
  LOGS_LIVE_DAYS = 600;

procedure Log(const text: string; const debug: Boolean = False);
procedure Err(const errCode: Integer; const errMsg: string; const e: Exception; const debug: Boolean = False);
procedure WriteToLog(const ext: string; const text: string);
function GetLogFile(dt: TDateTime): String;
function GetLogPath(): string;


implementation
        
function GetLogPath(): string;
begin
  Result := ExtractFilePath(ParamStr(0)) + LOG_FOLDER + '\';
end;
procedure CreateLogDir;
var
  dir: String;
begin
  dir := GetLogPath;
  if not DirectoryExists(dir) then
  begin
    CreateDir(dir);
  end;
end;

procedure Err(const errCode: Integer; const errMsg: string; const e: Exception; const debug: Boolean = False);
begin
  Log('[ERR:' + IntToStr(errCode) + ']' + errMsg + #13#10 + '->Exception ' + E.ClassName + ':' + E.Message);
end;

procedure Log(const text: string; const debug: Boolean = False);
begin
  if not debug then
  begin
    WriteToLog('log', '[' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
  end;
end;

procedure WriteToLog(const ext: string; const text: string);
var
  MyLogFolder: string;
  procedure Delete_old_logs;
  var
    vdate: TDatetime;
    sr: TSearchRec;
    intFileAge: LongInt;
    myfileage: TDatetime;
  begin
    try
      vdate := Now - LOGS_LIVE_DAYS;
      if FindFirst(MyLogFolder + '\*.' + ext, faAnyFile - faDirectory, sr) = 0 then
        repeat
          intFileAge := FileAge(MyLogFolder + '\' + sr.name);
          if intFileAge > -1 then
          begin
            myfileage := FileDateToDateTime(intFileAge);
            if myfileage < vdate then
            begin
              DeleteFile(pchar(mylogfolder + '\' + sr.name));
            end;
          end;
        until (FindNext(sr) <> 0);
    finally
      SysUtils.FindClose(sr);
    end;
  end;
  procedure File_Append(const fname: string; const Data: string);
  var
    f: integer;
  begin
    f := FileOpen(fname, fmOpenReadWrite + fmShareDenyNone);
    if f < 0 then
    begin
      try
        if LOGS_LIVE_DAYS > 0 then
        begin
          Delete_old_logs;
        end;
      except
        // ignore problems with file deletion
      end;
      f := FileCreate(fname);
      //FileWrite(f, SERVICE_NAME + ' log file'#13#10, Length(SERVICE_NAME + ' log file'#13#10));
    end;
    if f >= 0 then
      try
        if FileSeek(f, 0, 2) >= 0 then
          FileWrite(f, data[1], length(data));
      finally
        FileClose(f);
      end;
  end;
  function GetTempDirectory: String;
  var
    tempFolder: array[0..MAX_PATH] of Char;
  begin
    GetTempPath(MAX_PATH, @tempFolder);
    result := StrPas(tempFolder);
  end;
begin
  MyLogFolder := GetLogPath;
  if not DirectoryExists(MyLogFolder) then
  begin
    if not CreateDir(MyLogFolder) then
    begin
      MyLogFolder := GetTempDirectory;
      if Copy(myLogFolder, length(MyLogFolder), 1) <> '\' then
        MyLogFolder := MyLogFolder + '\';
      MyLogFolder := MyLogFolder + LOG_FOLDER;
      if not DirectoryExists(MyLogFolder) then
        CreateDir(MyLogFolder);
    end;
  end;
  File_Append(myLogFolder + '\' + FormatDateTime('YYYYMMDD.', Now) + ext, text);
end;

function GetLogFile(dt: TDateTime): String;
begin
  Result := GetLogPath + FormatDateTime('YYYYMMDD', dt) + '.log';
end;

end.
 