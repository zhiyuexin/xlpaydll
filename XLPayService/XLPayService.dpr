program XLPayService;

uses
  SvcMgr,
  SrvXLPayService in 'SrvXLPayService.pas' {ServiceXLPay: TService},
  Common in 'Common.pas',
  Logger in 'Logger.pas',
  uDMFileProvider in 'uDMFileProvider.pas' {File_Provider: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TServiceXLPay, ServiceXLPay);
  Application.CreateForm(TFile_Provider, File_Provider);
  Application.Run;
end.
