object Form7: TForm7
  Left = 418
  Top = 712
  Width = 950
  Height = 289
  Caption = 'Form7'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 41
    Width = 934
    Height = 210
    Align = alClient
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 934
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 840
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Login'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 752
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Quest'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Edit1: TEdit
      Left = 9
      Top = 10
      Width = 740
      Height = 21
      TabOrder = 2
    end
  end
  object Edit2: TEdit
    Left = 11
    Top = 217
    Width = 908
    Height = 21
    TabOrder = 2
  end
  object RtcHttpClient1: TRtcHttpClient
    Left = 38
    Top = 104
  end
  object RtcDataRequest1: TRtcDataRequest
    Client = RtcHttpClient1
    OnBeginRequest = RtcDataRequest1BeginRequest
    OnDataReceived = RtcDataRequest1DataReceived
    Left = 90
    Top = 104
  end
  object RtcDataRequest2: TRtcDataRequest
    Client = RtcHttpClient1
    OnBeginRequest = RtcDataRequest2BeginRequest
    OnDataReceived = RtcDataRequest1DataReceived
    Left = 140
    Top = 103
  end
end
