unit Unit7;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rtcInfo, rtcConn, rtcDataCli, rtcHttpCli, StdCtrls, ExtCtrls,
  md5, dateutils;

type
  TForm7 = class(TForm)
    RtcHttpClient1: TRtcHttpClient;
    RtcDataRequest1: TRtcDataRequest;
    Button1: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    RtcDataRequest2: TRtcDataRequest;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RtcDataRequest1BeginRequest(Sender: TRtcConnection);
    procedure RtcDataRequest1DataReceived(Sender: TRtcConnection);
    procedure RtcDataRequest2BeginRequest(Sender: TRtcConnection);
  private
    function DateTimeToUnixDate(const ADate: TDateTime): Longint;
    function getMilliSecond(): String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.Button1Click(Sender: TObject);
begin
  RtcDataRequest1.Request.Method := 'POST'; // Use the "HTTP POST" method
  RtcDataRequest1.Request.FileName := '/xld/j_spring_security_check'; // Set the "FileName" to receive the request
  RtcDataRequest1.Request.Host := RtcHttpClient1.ServerAddr; // Set the "Host" HTTP header
  RtcDataRequest1.Post(); // Post the request to the request queue
end;

procedure TForm7.Button2Click(Sender: TObject);
var
  id, ts, ak, sk, sn: string;
  scan_valid_url: string;
  l: Int64;
begin
  id := '10001';
  ak := 'iowjfsajfowe';
  sk := 'f9dfsd8fhwfw';
  ts := IntToStr(DateTimeToUnixDate(Now) + 60000) + '000';
//  l := Now;
  ts := getMilliSecond;
//  ts := '1429798102505';
//         1429886874
//         40169025
  sn := LowerCase(StrToMD5(id + ak + ts + sk));
  Edit1.Text := '/xlp/pos/jl/hello/whl';
//  Edit2.Text := '{"ak":"' + ak + '","ts":' + ts + ',"sn":"' + sn + '"}';
//  Edit1.Text := '/xlp/pos/jl/posttest';
  scan_valid_url := URL_Encode(Edit1.Text);

  RtcDataRequest2.Request.Method := 'POST';
  RtcDataRequest2.Request.FileName := scan_valid_url;
  RtcDataRequest2.Request.Host := RtcHttpClient1.ServerAddr;
  RtcDataRequest2.Post();

end;

procedure TForm7.FormCreate(Sender: TObject);
begin
//  RtcHttpClient1.ServerAddr := 'o.xlgoo.net';
//  RtcHttpClient1.ServerAddr := 'xld.xinglongstore.com';
  RtcHttpClient1.ServerAddr := 'localhost';
  RtcHttpClient1.ServerPort := '8080';
  RtcHttpClient1.AutoConnect := true;
  Edit1.Text := '/xld/offline/ls_ref_weixin_pay_info.json?comCode=10&posNo=1144421&billId=22222&batchno=3233&amt=11.11&payCode=1234567';

//  Edit1.Text := '/xlp/pos/jl/hello';
end;

procedure TForm7.RtcDataRequest1BeginRequest(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
begin
  Cli.Request.ContentType := 'application/x-www-form-urlencoded'; // important!!!
  // Set all POST variables (in our case, "username" and "pwd")
  Cli.Request.Params.Value['j_username'] := URL_Encode('OPENID_ozeP_jmaakeYTh7YU7YNVdY-UH-M');
  Cli.Request.Params.Value['j_password'] := URL_Encode('');
  Cli.Request.Params.Value['j_logintype'] := URL_Encode('2');
  Cli.Request.Params.Value['j_comcode'] := URL_Encode('1');
  Cli.Request.Params.Value['j_openid'] := URL_Encode('ozeP_jmaakeYTh7YU7YNVdY-UH-M');
  Cli.Request.Params.Value['j_loginTerraceCode'] := URL_Encode('zjxldjt');
  Cli.Request.Params.Value['j_usertype'] := URL_Encode('1');
  // Use the "Write" method to send "Params.Text" ("FORM-URLENCODED" values) to the Server
  Cli.Write(Cli.Request.Params.Text);
end;

procedure TForm7.RtcDataRequest1DataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  mycontent: RtcString;
begin
  // We want to wait for the whole response ...
  if Cli.Response.Done then
  begin
    // Since we have the whole response,
    // we can use a single "Read" call to get the complete content body
    mycontent := Cli.Read;
    mycontent := Utf8Decode(mycontent);
    Memo1.Lines.Text := IntToStr(Cli.Response.StatusCode) + ' ' + Cli.Response.StatusText + #13#10
      + mycontent;
  end;
end;

procedure TForm7.RtcDataRequest2BeginRequest(Sender: TRtcConnection);
var
  id, ts, ak, sk, sn: string;
  scan_valid_url: string;
  l: Int64;
  Cli: TRtcDataClient absolute Sender;
begin
  id := '10001';
  ak := 'iowjfsajfowe';
  sk := 'f9dfsd8fhwfw';
  ts := getMilliSecond;
  sn := LowerCase(StrToMD5(id + ak + ts + sk));

  Cli.Request.ContentType := 'text/html'; // important!!!
  Cli.Request.ContentType := 'application/x-www-form-urlencoded';
  Cli.Request.Params.Value['mybean'] := URL_Encode('{"errcode":"10001"}');
  Cli.Request.Params.Value['ak'] := URL_Encode(ak);
  Cli.Request.Params.Value['ts'] := URL_Encode(ts);
  Cli.Request.Params.Value['sn'] := URL_Encode(sn);

  Cli.Write(Cli.Request.Params.Text);
end;

function TForm7.DateTimeToUnixDate(const ADate: TDateTime): Longint;
const
  cUnixStartDate: TDateTime = 25569.0; // 1970/01/01
begin
  Result := Round((ADate - cUnixStartDate) * 86400);
end;

Function TForm7.GetMilliSecond(): String;
const
  BgnTime: TDateTime = 25569.0; // 1970/01/01
var
  a: Longint;
  b: String;//毫秒数
begin
  a := MinutesBetween(BgnTime, Now);//取分钟差
  a := a * 60;//取秒差
  Randomize;
  b := IntToStr(a + 60000 + Random(1000)) + FormatDateTime('zzz', Now);//将秒差结果后缀加上三个零（等于毫秒数）
//b := IntToStr(a) ;
  Result := b;
end;

end.
