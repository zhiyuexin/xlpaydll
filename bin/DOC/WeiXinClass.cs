﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows.Forms;

namespace Weixing
{
    class WeiXinClass
    {
        [DllImport("xlpay.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall, EntryPoint = "request", ExactSpelling = true)]
        private static extern int request(StringBuilder request_param, StringBuilder return_param);

        private object dllLoad(object param,string classname)//dll调用
        {
            StringBuilder inparam = new StringBuilder();
            MemoryStream ms = new MemoryStream(4096);
            ms.Position = 0;
            new DataContractJsonSerializer(param.GetType()).WriteObject(ms, param);
            string szjson = Encoding.UTF8.GetString(ms.ToArray());
            inparam.Append(szjson);
            StringBuilder outparam = new StringBuilder(4096);
            string str = inparam.ToString();
            request(inparam, outparam);
            char[] rechar = outparam.ToString().Trim().ToArray();
            MemoryStream rems = new MemoryStream(Encoding.UTF8.GetBytes(rechar));//GetEncoding("GB2312")
            return new DataContractJsonSerializer(Type.GetType(classname)).ReadObject(rems);
        }

        public wxpayreparam weixinquest(wxpayparam param)
        {
            //new DataContractJsonSerializer(typeof(wxpayreparam)).ReadObject(rems);
            wxpayreparam reobject = (wxpayreparam)dllLoad(param, "Weixing.wxpayreparam");
            if (!reobject.success)
            {
                string errcode = reobject.err_code;
                string errstring = "";
                switch(errcode)
                { 
                    case "E000":
                        errstring = "系统初始化错误";
                        break;
                    case "E100":
                        errstring = "错误的请求参数";
                        break;
                    case "E101":
                        errstring = "错误的商户编码";
                        break;
                    case "E102":
                        errstring = "错误的支付编码";
                        break;
                    case "E103":
                        errstring = "错误的业务编码";
                        break;
                    case "E104":
                        errstring = "错误的企业编码";
                        break;
                    case "E105":
                        errstring = "错误的设备编号";
                        break;
                    case "E106":
                        errstring = "错误的销售编码";
                        break;
                    case "E107":
                        errstring = "错误的支付序号";
                        break;
                    case "E200":
                        errstring = "初始化错误";
                        break;
                    case "E201":
                        errstring = "监听本机端口失败";
                        break;
                    case "E202":
                        errstring = "连接服务器失败";
                        break;
                    case "E300":
                        errstring = "操作员手动取消交易";
                        break;
                    case "E301":
                        errstring = "获取服务端设置失败";
                        break;
                    case "E302":
                        errstring = "获取查询信息失败";
                        break;
                    case "E303":
                        errstring = "获取退货信息失败";
                        break;
                    case "E401":
                        errstring = "错误的企业编码";
                        break;
                    case "E501":
                        errstring = "连接兴隆支付中心服务器失败";
                        break;
                    case "E502":
                        errstring = "获取商户类型失败";
                        break;
                    case "E503":
                        errstring = "获取支付类型失败";
                        break;
                    case "E504":
                        errstring = "无法找到支付编码";
                        break;
                    case "E505":
                        errstring = "响应xld服务失败";
                        break;
                    case "E506":
                        errstring = "xld 服务无响应";
                        break;
                    case "E507":
                        errstring = "获取支付链接配置失败";
                        break;
                    case "E508":
                        errstring = "获取支付连接失败";
                        break;
                    case "E509":
                        errstring = "无法生成支付链接";
                        break;
                    default:
                        errstring = reobject.err_message.Trim();
                        break;
                }
                reobject.err_message = errstring;
            }
            return reobject;
        }

        public int getMaxBillId(string partnerId,string comCode,string posNo)
        {
            wxBillId item = new wxBillId();
            item.trans_id = "B3";
            item.partner_id = partnerId;
            item.com_code = comCode;
            item.pos_no = posNo;
            wxBillIdRe reobject = (wxBillIdRe)dllLoad(item, "Weixing.wxBillIdRe");
            if(!reobject.success)
            {
                MessageBox.Show(reobject.err_message);
                return -1;
            }
            if (reobject.result == null) reobject.result = "0";
            return int.Parse(reobject.result.Trim());
        }
        public string appRegister(string partnerId, string comCode, string posNo,string terminalId,string terminalSn)
        {
            wxRegister reg = new wxRegister();
            reg.trans_id = "S0";
            reg.partner_id = partnerId;
            reg.com_code = comCode;
            reg.pos_no = posNo;
            reg.terminal_id = terminalId;
            reg.terminal_sn = terminalSn;
            reg.terminal_type = "1";

            wxRegisterRe reobject = (wxRegisterRe)dllLoad(reg, "Weixing.wxRegisterRe");
            if (!reobject.success)
            {
                MessageBox.Show(reobject.err_message);
                return "";
            }
            if(!reobject.registed)
            {
                MessageBox.Show("注册失败");
                return "";
            }
            return reobject.pos_id.Trim();
        }
        public bool appLogin(string pos_id,string userCode,string userPassword)
        {
            wxLogin login = new wxLogin();
            login.trans_id = "S1";
            login.pos_id = pos_id;
            login.logon_type = "1";
            login.operator_id = userCode;
            login.operator_pwd = userPassword;
            wxLoginRe reobject = (wxLoginRe)dllLoad(login, "Weixing.wxLoginRe");
            if (!reobject.success)
            {
                MessageBox.Show(reobject.err_message);
                return false;
            }
            if (!reobject.result)
            {
                MessageBox.Show("注册失败");
                return false;
            }
            return true;


        }


    }
    public class wxpayreparam
    {
        public Boolean success { get; set; }
        public string err_code { get; set; }
        public string err_message { get; set; }
        public string trade_no { get; set; }
        public double? sale_amt { get; set; }
        public string trade_time { get; set; }
    }
    public class wxpayparam
    {
        public string trans_id { get; set; } //业务编码，固定为A1	string
        public string partner_id { get; set; }//商户编码，固定为JL	string
        public string com_code { get; set; }//com_code	门店编码	string
        public string pos_no { get; set; }//pos_no	收款设备编号	string
        public string bill_id { get; set; }//bill_id	销售编码	string	
        public int batch_no { get; set; }//batch_no	支付序号，第几次支付	integer
        public float pay_amt { get; set; }//pay_amt	支付金额(上限)，0不限制,两位小数	float
        public string vip_code { get; set; }//vip_code	会员编码	string	
        public string vip_id { get; set; }//vip_id	会员ID	string	
        public string partner_data { get; set; }//partner_data	商户数据(原封返回)*	string
        public string remark { get; set; }//remark	备注*	string
        public string operator_id { get; set; }//operator_id	操作员ID*	string
        public string pay_code { get; set; }// 支付编码    string	5
        public Boolean need_print { get; set; }// 是否需要打印签购单   boolean	true
        public int print_type { get; set; }// 打印类型（0-驱动打印，1-端口打印。当need_print为true时有效）	integer	0
        public int scan_type { get; set; }// 扫描类型（0-主扫+被扫，1-主扫，2-被扫。当need_print为true时有效）	integer	0
        public string printer_port { get; set; }// 打印端口（当print_type为0时该值为打印机名称，默认打印机请用“default”，为1时该值为端口名称）	string	default
        public string cntt_no { get; set; }//原交易号
        public Boolean cnttno_modified { get; set; }//是否可以修改交易号
        public string report_memo { get; set; }//打印参数
    }
    public class wxBillId //查询单据号参数
    {
        public string trans_id { get; set; } //业务编码，固定为B3 string B3
        public string partner_id { get; set; } //商户编码    string JL
        public string com_code { get; set; }// 门店编码    string	1
        public string pos_no { get; set; }// 收款设备编号  string A101
    }
    public class wxBillIdRe //查询单据号输出参数
    {
        public bool success { get; set; }// 成功标志 boolean true
        public string err_code { get; set; }// 错误码 string	0
        public string err_message { get; set; }// 错误信息    string Err...
        public string result { get; set; }//  最大流水号   string  5554434
    }
    public class wxRegister
    {
        public string trans_id { get; set; }// 业务编码，固定为S0 string S0
        public string partner_id { get; set; }// 商户编码    string JL
        public string com_code { get; set; }// 门店编码    string	1
        public string pos_no { get; set; }// 收款设备编号 string A101
        public string terminal_id { get; set; }//终端标识    string	4jhg476frgtre4gfs
        public string terminal_sn { get; set; }//终端注册序列号 string	945643237545976510
        public string terminal_type { get; set; }//终端类型，1：基于PC系统的销售终端；2：基于WindowsCE系统的手持终端 string	1
    }
    public class wxRegisterRe
    {
        public bool success { get; set; }// 成功标志    boolean	true
        public string err_code { get; set; }// 错误码 string	0
        public string err_message { get; set; }// 错误信息    string Err...
        public string pos_id { get; set; }//  终端ID    string  00090acb652a95048d8bd5411a776dda9a6
        public bool registed { get; set; }//    是否注册成功  string
//        public string result { get; set; }//  终端详细信息 json
    }

    public class wxLogin
    {
        public string trans_id { get; set; }// 业务编码，固定为S1 string S1
        public string pos_id { get; set; }// 终端ID，由注册终端(S0)后得到 string	00090acb652a95048d8bd5411a776dda9a6
        public string logon_type { get; set; }//  登录方式，0：管理员，1：收款员 string	1
        public string operator_id { get; set; }// 操作员ID   string	001
        public string operator_pwd { get; set; }// 操作员密码   string	001

    }
    public class wxLoginRe
    {
        public bool success { get; set; }// 成功标志    boolean	true
        public string err_code { get; set; }// 错误码 string	0
        public string err_message { get; set; }// 错误信息    string Err...
        public bool result { get; set; }//  是否登录成功 boolean	true
    }
}


