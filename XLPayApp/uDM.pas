unit uDM;

interface

uses
  SysUtils, Classes, rtcInfo, rtcConn, rtcDataCli, rtcHttpCli, common, xlpayobject;

type
  TDM = class(TDataModule)
    HttpClient: TRtcHttpClient;
    drTestConnection: TRtcDataRequest;
    procedure DataModuleCreate(Sender: TObject);
    procedure drTestConnectionBeginRequest(Sender: TRtcConnection);
    procedure HttpClientConnect(Sender: TRtcConnection);
    procedure HttpClientConnectError(Sender: TRtcConnection; E: Exception);
    procedure HttpClientConnectFail(Sender: TRtcConnection);
    procedure HttpClientException(Sender: TRtcConnection; E: Exception);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;
  appCfg: TAppConfigure;
  payObj: TXlpayObject;

implementation

uses uFrmLoader, Forms;

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
var
  frmLoader: TfrmLoader;
begin

  appCfg.AppVersion := GetConfig('APPINFO', 'APP_VERSION', '?');
  appCfg.ServerAddr := GetConfig('CONFIGURE', 'SERVER_ADDR', '127.0.0.1');
  appCfg.ServerPort := GetConfig('CONFIGURE', 'SERVER_PORT', '8060');

  if TryIPPort(AppCfg.ServerAddr, StrToInt(AppCfg.ServerPort), 3) then
  begin
    //showmessage('1');
  end;
//
  HttpClient.ServerAddr := appCfg.ServerAddr;
  HttpClient.ServerPort := appCfg.ServerPort;
  HttpClient.AutoConnect := True;
  HttpClient.Connect();

  drTestConnection.Request.Method := 'POST';
  drTestConnection.Request.FileName := URL_Encode('/test.html');
  drTestConnection.Post();
       // 1.注册款机
       // 2.更新程序
  frmLoader := TfrmLoader.Create(nil);
  if frmLoader.ShowModal = 1 then
  begin

  end;
  frmLoader.Free;
end;

procedure TDM.drTestConnectionBeginRequest(Sender: TRtcConnection);
var
  ts: string;
  Cli: TRtcDataClient absolute Sender;
begin
  try
    Cli.Request.ContentType := 'application/x-www-form-urlencoded';
    Cli.Request.Params.Value['ak'] := URL_Encode(APP_AK);
    ts := getMilliSecond;
    Cli.Request.Params.Value['ts'] := URL_Encode(ts);
    Cli.Request.Params.Value['sn'] := URL_Encode(GetAppSn(ts));
//    Cli.Request.Params.Value['com_code'] := URL_Encode(ComCode);
//    Cli.Request.Params.Value['pos_no'] := URL_Encode(PosNo);
//    Cli.Request.Params.Value['bill_id'] := URL_Encode(BillId);
//    Cli.Request.Params.Value['batch_no'] := URL_Encode(BatchNo);
//    Cli.Request.Params.Value['random_id'] := URL_Encode(RandomId);
//    Cli.Request.Params.Value['vip_code'] := URL_Encode(VipCode);
//    Cli.Request.Params.Value['vip_id'] := URL_Encode(VipId);
//    Cli.Request.Params.Value['partner_id'] := URL_Encode(PartnerId);
//    Cli.Request.Params.Value['partner_data'] := URL_Encode(PartnerData);
//    if NeedPrint then
//      Cli.Request.Params.Value['need_print'] := '1'
//    else
//      Cli.Request.Params.Value['need_print'] := '0';
//    Cli.Request.Params.Value['remark'] := URL_Encode(Remark);
//    Cli.Request.Params.Value['operator_id'] := URL_Encode(OperatorId);

    Cli.Write(Cli.Request.Params.Text);


  except
    on E: Exception do
    begin
      ts := '查询信息出错:' + E.Message;
//      MessageBox(Self.Handle, PChar(ts), PChar('错误'), MB_TOPMOST + MB_ICONINFORMATION + MB_SYSTEMMODAL);
    end;
  end;
end;

procedure TDM.HttpClientConnect(Sender: TRtcConnection);
begin
  AppCfg.ServerConnected := True;
end;

procedure TDM.HttpClientConnectError(Sender: TRtcConnection; E: Exception);
begin
  AppCfg.ServerConnected := False;
end;

procedure TDM.HttpClientConnectFail(Sender: TRtcConnection);
begin
  AppCfg.ServerConnected := False;
end;

procedure TDM.HttpClientException(Sender: TRtcConnection; E: Exception);
begin
  AppCfg.ServerConnected := False;
end;

initialization
  appCfg := TAppConfigure.Create();
  payObj := TXlpayObject.Create();

finalization
  appCfg.Free;
  appCfg := nil;
  payObj.Free;
  payObj := nil;

end.
