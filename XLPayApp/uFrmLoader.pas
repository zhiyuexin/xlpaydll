unit uFrmLoader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, common, uDM;

const
  SYS_INFO_TEXT = '系统版本：V{APP_VERSION}  当前服务器：{SERVER_IP}:{SERVER_PORT}';

type
  TfrmLoader = class(TForm)
    lblLoadInfo: TLabel;
    lblSysInfo: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLoader: TfrmLoader;

implementation
{$R *.dfm}
uses
  xlpayobject;

procedure TfrmLoader.FormActivate(Sender: TObject);
var
  RespData: TStringStream;
begin


  while dm.HttpClient.isConnecting do
  begin
    lblLoadInfo.Caption := '正在连接服务器(' + dm.HttpClient.ServerAddr + ':' + dm.HttpClient.ServerPort + ')...';
    Application.ProcessMessages;
  end;
  if AppCfg.ServerConnected then //dm.HttpClient.isConnected then
  begin
    lblLoadInfo.Caption := '服务器连接成功！';
    Application.ProcessMessages;
  end
  else
  begin
    lblLoadInfo.Caption := '服务器连接失败！';
    Application.ProcessMessages;
  end;

end;

procedure TfrmLoader.FormCreate(Sender: TObject);
var
  sysInfo: String;
begin
  sysInfo := SYS_INFO_TEXT;
  sysInfo := StringReplace(sysInfo, '{APP_VERSION}', appCfg.AppVersion, [rfReplaceAll]);
  sysInfo := StringReplace(sysInfo, '{SERVER_IP}', appCfg.ServerAddr, [rfReplaceAll]);
  sysInfo := StringReplace(sysInfo, '{SERVER_PORT}', appCfg.ServerPort, [rfReplaceAll]);
  lblSysInfo.Caption := sysInfo;
  lblSysInfo.Width := Self.Width;
  lblLoadInfo.Width := Self.Width;
end;

end.
