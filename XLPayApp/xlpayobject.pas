unit xlpayobject;

interface

uses
  Classes, SysUtils;

type
  TTransType = (transPay, transRefund, transQuery, transPrint);

  TXlpayObject = class
  private
    f_need_print: Boolean;
    f_amt_modified: Boolean;
    f_pay_amt: Double;
    f_batch_no: Integer;
    f_scan_type: Integer;
    f_sale_tag_limit: Integer;
    f_print_type: Integer;
    f_sale_tag_type: Integer;
    f_printer_port: String;
    f_remark: String;
    f_vip_id: String;
    f_trans_id: TTransType;
    f_sale_tag: String;
    f_operator_id: String;
    f_vip_code: String;
    f_partner_data: String;
    f_pos_no: String;
    f_partner_id: String;
    f_bill_id: String;
    f_com_code: String;
    f_pay_code: String;
    f_cnttno_modified: Boolean;
    f_cntt_no: String;
  public
    property trans_id: TTransType read f_trans_id write f_trans_id;
    property partner_id: String read f_partner_id write f_partner_id;
    property com_code: String read f_com_code write f_com_code;
    property pos_no: String read f_pos_no write f_pos_no;
    property bill_id: String read f_bill_id write f_bill_id;
    property batch_no: Integer read f_batch_no write f_batch_no;
    property pay_code: String read f_pay_code write f_pay_code;
    property pay_amt: Double read f_pay_amt write f_pay_amt;
    property amt_modified: Boolean read f_amt_modified write f_amt_modified;
    property vip_code: String read f_vip_code write f_vip_code;
    property vip_id: String read f_vip_id write f_vip_id;
    property partner_data: String read f_partner_data write f_partner_data;
    property sale_tag_type: Integer read f_sale_tag_type write f_sale_tag_type;
    property sale_tag: String read f_sale_tag write f_sale_tag;
    property sale_tag_limit: Integer read f_sale_tag_limit write f_sale_tag_limit;
    property remark: String read f_remark write f_remark;
    property need_print: Boolean read f_need_print write f_need_print;
    property print_type: Integer read f_print_type write f_print_type;
    property printer_port: String read f_printer_port write f_printer_port;
    property operator_id: String read f_operator_id write f_operator_id;
    property scan_type: Integer read f_scan_type write f_scan_type;
    property cnttno_modified: Boolean read f_cnttno_modified write f_cnttno_modified;
    property cntt_no: String read f_cntt_no write f_cntt_no;

  public function getRequestParam(): String;
  public function loadFromConfigFile(fileName: String): Boolean;
  public function saveToConfigFile(fileName: String): Boolean;
  end;


implementation
uses
  IniFiles;

{ TXlpayObject }

function TXlpayObject.getRequestParam: String;
var
  s_trans_id, s_amt_modified, s_need_print, s_cnttno_modified: String;
begin
  if (trans_id = transPay) then
  begin
    s_trans_id := 'A1';
  end
  else if (trans_id = transRefund) then
  begin
    s_trans_id := 'A2';
  end
  else if (trans_id = transPrint) then
  begin
    s_trans_id := 'B1';
  end
  else if (trans_id = transQuery) then
  begin
    s_trans_id := 'B2';
  end
  else
  begin
    Result := '';
    Exit;
  end;
  if amt_modified then
    s_amt_modified := 'true'
  else
    s_amt_modified := 'false';
  if need_print then
    s_need_print := 'true'
  else
    s_need_print := 'false';
  if cnttno_modified then
    s_cnttno_modified := 'true'
  else
    s_cnttno_modified := 'false';

  Result := '{'#13#10
    + '"trans_id": "' + s_trans_id + '",'#13#10
    + '"partner_id": "' + partner_id + '",'#13#10
    + '"com_code": "' + com_code + '",'#13#10
    + '"pos_no": "' + pos_no + '",'#13#10
    + '"bill_id": "' + bill_id + '",'#13#10
    + '"batch_no": ' + IntToStr(batch_no) + ','#13#10
    + '"pay_code": "' + pay_code + '",'#13#10
    + '"pay_amt": ' + FloatToStr(pay_amt) + ','#13#10
    + '"amt_modified": ' + s_amt_modified + ','#13#10
    + '"vip_code": "' + vip_code + '",'#13#10
    + '"vip_id": "' + vip_id + '",'#13#10
    + '"partner_data": "' + partner_data + '",'#13#10
    + '"sale_tag_type": ' + IntToStr(sale_tag_type) + ','#13#10
    + '"sale_tag": "' + sale_tag + '",'#13#10
    + '"sale_tag_limit": ' + IntToStr(sale_tag_limit) + ','#13#10
    + '"remark": "' + remark + '",'#13#10
    + '"need_print": ' + s_need_print + ','#13#10
    + '"print_type": ' + IntToStr(print_type) + ','#13#10
    + '"printer_port": "' + printer_port + '",'#13#10
    + '"operator_id": "' + operator_id + '",'#13#10
    + '"scan_type": ' + IntToStr(scan_type) + ''#13#10;
  if (trans_id = transRefund) then
  begin
    Result := Result + ',  "cnttno_modified": ' + s_cnttno_modified + ','#13#10
      + '  "cntt_no": "' + cntt_no + '"'#13#10;
  end;
  Result := Result + '}';
end;

function TXlpayObject.loadFromConfigFile(fileName: String): Boolean;
var
  SectionName: String;
begin
  Result := False;
  if (not FileExists(ExtractFilePath(ParamStr(0)) + fileName)) then Exit;
  SectionName := 'CONFIGURE';
  with TIniFile.Create(ExtractFilePath(ParamStr(0)) + fileName) do
  begin
    try
      partner_id := ReadString(SectionName, 'partner_id', '');
      com_code := ReadString(SectionName, 'com_code', '');
      pos_no := ReadString(SectionName, 'pos_no', '');
      pay_code := ReadString(SectionName, 'pay_code', '');
      amt_modified := ReadString(SectionName, 'amt_modified', '0') = '1';
      partner_data := ReadString(SectionName, 'partner_data', '');
      sale_tag_type := ReadInteger(SectionName, 'sale_tag_type', 0);
      sale_tag := ReadString(SectionName, 'sale_tag', '');
      sale_tag_limit := ReadInteger(SectionName, 'sale_tag_limit', 0);
      remark := ReadString(SectionName, 'remark', '');
      need_print := ReadString(SectionName, 'need_print', '0') = '1';
      print_type := ReadInteger(SectionName, 'print_type', 0);
      printer_port := ReadString(SectionName, 'printer_port', '');
      operator_id := ReadString(SectionName, 'operator_id', '');
      scan_type := ReadInteger(SectionName, 'scan_type', 0);
      cnttno_modified := ReadString(SectionName, 'cnttno_modified', '0') = '1';
    finally
      Free;
    end;
  end;
end;

function TXlpayObject.saveToConfigFile(fileName: String): Boolean;
begin

end;

end.

