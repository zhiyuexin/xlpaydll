unit uFrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, common, superobject, xlpayobject, ExtCtrls, ComCtrls, acTrayIcon,
  acClasses, acCaptionButton, acAppAutoRun, LMDCustomComponent,
  LMDGlobalHotKey, acShortcut, StdCtrls;


type
  TfrmMain = class(TForm)
    StatusBar1: TStatusBar;
    pnlMain: TPanel;
    pnlTop: TPanel;
    acAppAutoRun: TacAppAutoRun;
    acTrayIcon: TacTrayIcon;
    hotkeyRestore: TLMDGlobalHotKey;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label7: TLabel;
    Label14: TLabel;
    btn_pay: TButton;
    edt_bill_id: TEdit;
    btn_refund: TButton;
    edt_pay_amt: TEdit;
    edt_batch_no: TEdit;
    edt_vip_code: TEdit;
    edt_pos_no: TEdit;
    edt_partner_data: TEdit;
    edt_operator_id: TEdit;
    edt_need_print: TCheckBox;
    edt_scan_type: TComboBox;
    edt_remark: TEdit;
    edt_printer_port: TEdit;
    edt_print_type: TComboBox;
    edt_pay_code: TEdit;
    btnQuery: TButton;
    btnRePrint: TButton;
    Button1: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure btnQueryClick(Sender: TObject);
    procedure btnRePrintClick(Sender: TObject);
    procedure btn_payClick(Sender: TObject);
    procedure btn_refundClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure hotkeyRestoreKeyDown(Sender: TObject; var Key: Word; Shift:
      TShiftState);
  private
    FPayObject: TXlpayObject;
  private
    property PayObject: TXlpayObject read FPayObject write FPayObject;
    function r(p_pay_param: String): String;
    function doPay(): String; overload;
    function dopay(trans_id: String): String; overload;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}



procedure TfrmMain.FormCreate(Sender: TObject);
//var
//  Shift: TShiftState;
begin      
  // ������ǰ
  SetWindowPos(self.handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);
  // ע���ݼ���Ĭ��F10
  hotkeyRestore.Active := false;
  hotkeyRestore.HotKey := StrToShortCut(GetStringFromIniFile(CFG_FILE, 'CONFIGURE', 'HOT_KEY', 'F10'));// read from config file
  hotkeyRestore.Active := true;
  // ����������
  PayObject := TXlpayObject.Create();
  PayObject.loadFromConfigFile(CFG_FILE);
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  try
    PayObject.Free;
  except

  end;
end;

procedure TfrmMain.btn_payClick(Sender: TObject);
begin
  dopay('A1');
end;

procedure TfrmMain.btn_refundClick(Sender: TObject);
begin
  dopay('A2');
end;

procedure TfrmMain.btnRePrintClick(Sender: TObject);
begin
  dopay('B1');
end;

procedure TfrmMain.btnQueryClick(Sender: TObject);
begin
  dopay('B2');
end;

function TfrmMain.doPay: String;
begin

end;

function TfrmMain.doPay(trans_id: String): String;
var
  s, need_print, amt_modified, cnttno_modified: String;
begin

  amt_modified := 'true';

  need_print := 'true';

  cnttno_modified := 'true';

//  s := '{'#13#10
//    + '  "trans_id": "' + trans_id + '",'#13#10
//    + '  "partner_id": "' + edt_partner_id.Text + '",'#13#10
//    + '  "com_code": "' + edt_com_code.Text + '",'#13#10
//    + '  "pos_no": "' + edt_pos_no.Text + '",'#13#10
//    + '  "bill_id": "' + edt_bill_id.Text + '",'#13#10
//    + '  "batch_no": ' + edt_batch_no.Text + ','#13#10
//    + '  "pay_code": "' + edt_pay_code.Text + '",'#13#10
//    + '  "pay_amt": ' + edt_pay_amt.Text + ','#13#10
//    + '  "amt_modified": ' + amt_modified + ','#13#10
//    + '  "vip_code": "' + edt_vip_code.Text + '",'#13#10
//    + '  "vip_id": "' + edt_vip_id.Text + '",'#13#10
//    + '  "partner_data": "' + edt_partner_data.Text + '",'#13#10
//    + '  "sale_tag_type": ' + IntToStr(edt_sale_tag_type.ItemIndex) + ','#13#10
//    + '  "sale_tag": "' + edt_sale_tag.Text + '",'#13#10
//    + '  "sale_tag_limit": ' + edt_sale_tag_limit.Text + ','#13#10
//    + '  "remark": "' + edt_remark.Text + '",'#13#10
//    + '  "need_print": ' + need_print + ','#13#10
//    + '  "print_type": ' + IntToStr(edt_print_type.ItemIndex) + ','#13#10
//    + '  "printer_port": "' + edt_printer_port.Text + '",'#13#10
//    + '  "operator_id": "' + edt_operator_id.Text + '",'#13#10
//    + '  "scan_type": ' + IntToStr(edt_scan_type.ItemIndex) + ''#13#10;
//  if (trans_id = 'A2') then
//  begin
//    s := s + ',  "cnttno_modified": ' + cnttno_modified + ','#13#10
//      + '  "cntt_no": "' + edt_cntt_no.Text + '"'#13#10;
//  end;
  s := s + '}';
  Result := r(s);
end;

procedure TfrmMain.hotkeyRestoreKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if acTrayIcon.IsMinimized then
  begin
    acTrayIcon.RestoreFromTray;
  end
  else
  begin
    acTrayIcon.MinimizeToTray;
  end;
end;


function TfrmMain.r(p_pay_param: String): String;
type
  Taddc = function(pay_param: PChar; return_param: PChar): Integer; stdcall;
var
  hh: THandle;
  addc: Taddc;
  request_param: PChar;
  return_param: PChar;
begin
  if not FileExists(DLL_FILE) then
  begin
    ShowMessage('file ' + DLL_FILE + ' not found!');
    Exit;
  end;
  hh := LoadLibrary(DLL_FILE);
  Result := '';
  try
    acTrayIcon.MinimizeToTray;
    if hh <> 0 then
    begin
      @addc := GetProcAddress(hh, PChar('request'));
    end;
    if not (@addc = nil) then
    begin
      //request_param := StrAlloc(4096 * SizeOf(Char));
      request_param := PChar(p_pay_param);
      return_param := StrAlloc(4096 * SizeOf(Char));
      addc(request_param, return_param);
      Result := StrPas(return_param);
      StrDispose(return_param);
      //StrDispose(request_param);
    end
    else
    begin
      RaiseLastWin32Error;
    end;
  finally
    FreeLibrary(hh);
    acTrayIcon.RestoreFromTray;
  end;
end;

end.
