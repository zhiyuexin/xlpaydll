unit fFrmConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, superobject, xlpayobject, StdCtrls, ExtCtrls;

type
  TfrmConfig = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    edt_pos_no: TEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    edt_need_print: TCheckBox;
    Label7: TLabel;
    edt_print_type: TComboBox;
    Label12: TLabel;
    edt_printer_port: TEdit;
    Button1: TButton;
    GroupBox4: TGroupBox;
    Label18: TLabel;
    edt_sale_tag_type: TComboBox;
    Label19: TLabel;
    edt_sale_tag_limit: TEdit;
    Label17: TLabel;
    edt_sale_tag: TEdit;
    GroupBox5: TGroupBox;
    edt_cnttno_modified: TCheckBox;
    Label9: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    edt_remark: TEdit;
    edt_partner_data: TEdit;
    edt_operator_id: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    btn_save: TButton;
    Label20: TLabel;
    edt_partner_id: TComboBox;
    Label6: TLabel;
    edt_com_code: TEdit;
    Label14: TLabel;
    edt_pay_code: TEdit;
    Label13: TLabel;
    edt_scan_type: TComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfig: TfrmConfig;

implementation

{$R *.dfm}

end.
