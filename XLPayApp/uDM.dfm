object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 789
  Top = 775
  Height = 209
  Width = 394
  object HttpClient: TRtcHttpClient
    Timeout.AfterConnecting = 5
    Timeout.AfterConnect = 2
    OnConnect = HttpClientConnect
    OnException = HttpClientException
    OnConnectFail = HttpClientConnectFail
    OnConnectError = HttpClientConnectError
    Left = 70
    Top = 36
  end
  object drTestConnection: TRtcDataRequest
    Client = HttpClient
    OnBeginRequest = drTestConnectionBeginRequest
    Left = 153
    Top = 34
  end
end
