object frmLoader: TfrmLoader
  Left = 754
  Top = 511
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'XLPayLoader'
  ClientHeight = 82
  ClientWidth = 381
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblLoadInfo: TLabel
    Left = 1
    Top = 13
    Width = 381
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = #31995#32479#21152#36733#20013#65292#35831#31245#21518#8230#8230
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblSysInfo: TLabel
    Left = 1
    Top = 59
    Width = 381
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = #31995#32479#29256#26412#65306'V1.0.0  '#24403#21069#26381#21153#22120#65306'10.2.27.27:8090'
  end
end
