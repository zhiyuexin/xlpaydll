unit uFrmLogon;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmLogon = class(TForm)
    lblUserCode: TLabel;
    lblBatchNo: TLabel;
    edtUserCode: TEdit;
    btnLogon: TButton;
    btnQuit: TButton;
    edtBatchNo: TEdit;
    procedure btnLogonClick(Sender: TObject);
    procedure btnQuitClick(Sender: TObject);
    procedure edtBatchNoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtUserCodeKeyDown(Sender: TObject; var Key: Word; Shift:
      TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogon: TfrmLogon;

implementation

{$R *.dfm}

//��¼
procedure TfrmLogon.btnLogonClick(Sender: TObject);
begin
  //
end;

//�˳�
procedure TfrmLogon.btnQuitClick(Sender: TObject);
begin
  //
end;

procedure TfrmLogon.edtBatchNoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Key := 0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
end;

procedure TfrmLogon.edtUserCodeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Key := 0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
end;

end.
