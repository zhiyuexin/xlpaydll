program XLPay;

uses
  FastMM4,
  Forms,
  uFrmMain in 'uFrmMain.pas' {frmMain},
  superobject in 'superobject\superobject.pas',
  fFrmConfig in 'fFrmConfig.pas' {frmConfig},
  xlpayobject in 'xlpayobject.pas',
  common in 'common.pas',
  uFrmLoader in 'uFrmLoader.pas' {frmLoader},
  uDM in 'uDM.pas' {DM: TDataModule},
  MD5 in 'MD5.pas',
  uFrmLogon in 'uFrmLogon.pas' {frmLogon};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'xlpay';
  if (Paramcount > 0) then
  begin
    if ((ParamStr(1) = 'config') or (ParamStr(1) = 'cfg')) then
    begin
      Application.CreateForm(TfrmConfig, frmConfig);
  Application.CreateForm(TfrmLogon, frmLogon);
  end
    else
    begin
      Application.CreateForm(TDM, DM);
//      Application.CreateForm(TfrmLoader, frmLoader);
      //Application.CreateForm(TfrmMain, frmMain);
    end;
  end
  else
  begin
    Application.CreateForm(TDM, DM);
//    Application.CreateForm(TfrmLoader, frmLoader);
    //Application.CreateForm(TfrmMain, frmMain);
  end;
  Application.Run;
end.
