unit common;

interface

uses
  Classes, SysUtils, Windows, xlpayobject;

const
  DLL_FILE = 'xlpay.dll';
  CFG_FILE = 'xlpay.cfg';
  APP_AK = 'iowjfsajfowe';
  APP_ID = '10001';
  APP_SK = 'f9dfsd8fhwfw';

type
  TAppConfigure = class
  private
  protected
  public
    AppName: String;
    AppVersion: String;
    ServerAddr: String;
    ServerPort: String;
    ServerConnected:Boolean;
    PosNo: String;
    AppHotKey: TShortCut;
    PrnHotKey: TShortCut;
    WxReportFile: String;
  end;
//var
//  payObj: TXlpayObject;

function ShortCut(Key: Word; Shift: TShiftState): TShortCut;
function StrToShortCut(str: String): TShortCut;
function GetConfig(const SectionName, ConfigName: String; const DefaultValue: String = ''): String;
function GetStringFromIniFile(const fileName, SectionName, ConfigName: String; const DefaultValue: String = ''): String;
procedure SetConfig(const SectionName, ConfigName, Value: String);
procedure SetStringToIniFile(const fileName, SectionName, ConfigName, Value: String);
Function TryIPPort(Const IP: String; Port: Integer; timeOut: Integer): Boolean;
Function GetMilliSecond(): String;
function GetAppSn(ts: string): string;

implementation
uses
  IniFiles, WinSock, DateUtils, MD5;

function ShortCut(Key: Word; Shift: TShiftState): TShortCut;
begin
  Result := 0;
  if WordRec(Key).Hi <> 0 then Exit;
  Result := Key;
  if ssShift in Shift then Inc(Result, scShift);
  if ssCtrl in Shift then Inc(Result, scCtrl);
  if ssAlt in Shift then Inc(Result, scAlt);
end;
function StrToShortCut(str: String): TShortCut;
var
  keyList: TStringList;
  Key: Word;
  Shift: TShiftState;
  function StrToKey(str: String): Word;
  var
    s: String;
    c: Char;
  begin
    Result := 0;
    s := StringReplace(StringReplace(UpperCase(str), 'NUMPAD', '', [rfReplaceAll]), 'PAD', '', [rfReplaceAll]);
    if ((LENGTH(S) = 2) and (CompareStr(s, 'F1') >= 0) and (CompareStr(s, 'F9') <= 0)) or ((LENGTH(S) = 3) and (CompareStr(s, 'F10') >= 0) and (CompareStr(s, 'F12') <= 0)) then
    begin
      s := StringReplace(s, 'F', '', [rfReplaceAll]);
      Result := VK_F1 + (StrToInt(s) - 1);
    end
    else if ((LENGTH(S) = 1) and (CompareStr(s, 'A') >= 0) and (CompareStr(s, 'Z') <= 0)) then
    begin
      c := pChar(s)[0];
      Result := Ord('A') + (Ord(c) - Ord('A'));
    end
    else if ((LENGTH(S) = 1) and (CompareStr(s, '0') >= 0) and (CompareStr(s, '9') <= 0)) then
    begin
      c := pChar(s)[0];
      Result := Ord(c);
    end
    else if (CompareStr(s, 'END') = 0) then
    begin
      Result := VK_END;
    end
    else if (CompareStr(s, 'HOME') = 0) then
    begin
      Result := VK_HOME;
    end
    else if (CompareStr(s, 'LEFT') = 0) then
    begin
      Result := VK_LEFT;
    end
    else if (CompareStr(s, 'UP') = 0) then
    begin
      Result := VK_UP;
    end
    else if (CompareStr(s, 'RIGHT') = 0) then
    begin
      Result := VK_RIGHT;
    end
    else if (CompareStr(s, 'DOWN') = 0) then
    begin
      Result := VK_DOWN;
    end
    else if ((CompareStr(s, 'PRINT') = 0) or (CompareStr(s, 'PRT') = 0)) then
    begin
      Result := VK_PRINT;
    end
    else if ((CompareStr(s, 'INSERT') = 0) or (CompareStr(s, 'INS') = 0)) then
    begin
      Result := VK_INSERT;
    end
    else if ((CompareStr(s, 'DELETE') = 0) or (CompareStr(s, 'DEL') = 0)) then
    begin
      Result := VK_DELETE;
    end
    else if ((CompareStr(s, 'NUMLOCK') = 0) or (CompareStr(s, 'NUM') = 0)) then
    begin
      Result := VK_NUMLOCK;
    end
    else if ((CompareStr(s, 'SCROLL') = 0) or (CompareStr(s, 'SRL') = 0)) then
    begin
      Result := VK_SCROLL;
    end
    else if ((CompareStr(s, 'ESCAPE') = 0) or (CompareStr(s, 'ESC') = 0)) then
    begin
      Result := VK_ESCAPE;
    end
    else if (CompareStr(s, 'VK_MULTIPLY') = 0) then
    begin
      Result := VK_MULTIPLY;
    end
    else if ((CompareStr(s, 'MULTIPLY') = 0) or (CompareStr(s, 'MUL') = 0) or (CompareStr(s, '*') = 0)) then
    begin
      Result := VK_MULTIPLY;
    end
    else if ((CompareStr(s, 'ADD') = 0) or (CompareStr(s, '+') = 0)) then
    begin
      Result := VK_ADD;
    end
    else if ((CompareStr(s, 'SEPARATOR') = 0) or (CompareStr(s, ';') = 0) or (CompareStr(s, 'SEP') = 0)) then
    begin
      Result := VK_SEPARATOR;
    end
    else if ((CompareStr(s, 'SUBTRACT') = 0) or (CompareStr(s, 'SUB') = 0) or (CompareStr(s, '-') = 0)) then
    begin
      Result := VK_SUBTRACT;
    end
    else if ((CompareStr(s, 'DECIMAL') = 0) or (CompareStr(s, 'DEC') = 0) or (CompareStr(s, '.') = 0)) then
    begin
      Result := VK_DECIMAL;
    end
    else if ((CompareStr(s, 'DIVIDE') = 0) or (CompareStr(s, 'DIV') = 0) or (CompareStr(s, '/') = 0)) then
    begin
      Result := VK_DIVIDE;
    end;
  end;
  function StrToShiftState(str: String): TShiftState;
  var
    s: String;
    ShiftState: TShiftState;
  begin
    s := UpperCase(str);
    Result := ShiftState;
    if (CompareStr(s, 'SHIFT') = 0) then
    begin
      Include(Result, ssShift);
    end
    else if (CompareStr(s, 'SFT') = 0) then
    begin
      Include(Result, ssShift);
    end
    else if (CompareStr(s, 'ALT') = 0) then
    begin
      Include(Result, ssAlt);
    end
    else if (CompareStr(s, 'CTRL') = 0) then
    begin
      Include(Result, ssCtrl);
    end
    else if (CompareStr(s, 'CTL') = 0) then
    begin
      Include(Result, ssCtrl);
    end;
  end;
begin
  //s := trim(StringReplace(str, '+', '', [rfReplaceAll]));
  keyList := TStringList.Create;
  key := 0;
  Shift := [];
  try
    ExtractStrings([' ', '+', '-', '_'], [' '], PChar(str), keyList);  //第一个参数是分隔符; 第二个参数是开头被忽略的字符
    if (keyList.Count = 1) then
    begin
      key := StrToKey(keyList.Strings[0]);
    end
    else if (keyList.Count >= 2) then
    begin
      Shift := StrToShiftState(keyList.Strings[0]);
      key := StrToKey(keyList.Strings[1]);
    end;
  finally
    keyList.Free;
  end;
  Result := ShortCut(Key, Shift);
end;


function GetConfig(const SectionName, ConfigName: String; const DefaultValue: String = ''): String;
begin
  Result := GetStringFromIniFile(CFG_FILE, SectionName, ConfigName, DefaultValue);
end;
function GetStringFromIniFile(const fileName, SectionName, ConfigName: String; const DefaultValue: String = ''): String;
begin
  Result := DefaultValue;
  with TIniFile.Create(ExtractFilePath(ParamStr(0)) + fileName) do
  begin
    try
      Result := ReadString(SectionName, ConfigName, Result);
    finally
      Free;
    end;
  end;
end;
procedure SetConfig(const SectionName, ConfigName, Value: String);
begin
  SetStringToIniFile(CFG_FILE, SectionName, ConfigName, Value);
end;
procedure SetStringToIniFile(const fileName, SectionName, ConfigName, Value: String);
begin
  with TIniFile.Create(ExtractFilePath(ParamStr(0)) + fileName) do
  begin
    try
      WriteString(SectionName, ConfigName, Value);
    finally
      Free;
    end;
  end;
end;


Function TryIPPort(Const IP: String; Port: Integer; timeOut: Integer): Boolean;
var
  Sock: TSocket;
  SA: TSockaddr;
  n, ul: integer;
  TV: TTimeVal;
  FDSet: TFDSet;
begin
  FillChar(SA, SizeOf(SA), 0);
  SA.sin_family := AF_INET;
  SA.sin_port := htons(Port);
  SA.sin_addr.S_addr := inet_addr(Pointer(IP));
  Sock := Socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
  Result := Sock <> invalid_socket;
  if Result then
  begin
    n := timeOut * 1000;
    ul := 1;
    if (SetSockopt(Sock, SOL_SOCKET, SO_SNDTIMEO, @n, SizeOf(n)) <> SOCKET_ERROR) and
      (SetSockopt(Sock, SOL_SOCKET, SO_RCVTIMEO, @n, SizeOf(n)) <> SOCKET_ERROR) and
      (ioctlsocket(Sock, FIONBIO, ul) <> SOCKET_ERROR) then begin
      Connect(Sock, SA, SizeOf(SA));
      FD_ZERO(FDSet);
      FD_SET(Sock, FDSet);
      TV.tv_sec := timeOut;
      TV.tv_usec := 0;
      Result := select(0, NIL, @FDSet, NIL, @TV) > 0;

      if Result then
        Result := Send(Sock, SA, 1, 0) = 1;
    end;
    CloseSocket(Sock);
  end;
end;

Function GetMilliSecond(): String;
const
  BgnTime: TDateTime = 25569.0; // 1970/01/01
var
  a: Longint;
  b: String;//毫秒数
begin
  a := MinutesBetween(BgnTime, Now);//取分钟差
  a := a * 60;//取秒差
  Randomize;
  b := IntToStr(a + 60000 + Random(1000)) + FormatDateTime('zzz', Now);//将秒差结果后缀加上三个零（等于毫秒数）
  Result := b;
end;
function GetAppSn(ts: string): string;
begin
  Result := LowerCase(StrToMD5(APP_ID + APP_AK + ts + APP_SK));
end;

end.
