unit uEncrypt;

interface

uses
  SysUtils, Variants, Classes;
const
  EncryptKey: array[0..7] of Byte = ($C4, $3D, $AB, $48, $33, $A7, $F4, $17); //方式1加密用
  SeedA = 518; //方式2加密用
  SeedB = 414; //方式2加密用
  EncrypDefaulttKey = 'dwehllpxhling';    //方式3加密用

function Encrypt(Str: string): string;
function Decrypt(Str: string): string;

function Encrypt1(Str: string): string;
function Decrypt1(Str: string): string;
function Crypt1(str: string; const bEncrypt: boolean): String;

function Encrypt2(s: string; Key: Word): string;
function Decrypt2(s: string; Key: Word): string;
function Crypt2(s: string; Key: Word; const bEncrypt: boolean): String;

function Encrypt3(Src, Key: string): string;
function Decrypt3(Src, Key: string): string;
function Crypt3(Src, Key: string; const bEncrypt: boolean): string;

implementation

function Encrypt(Str: string): string;
begin
  Result := Encrypt1(Str);
//  Result := Encrypt2(Result, 787);
//  Result := Encrypt3(Result, 'xinglong828');
end;
function Decrypt(Str: string): string;
begin
//  Result := Decrypt3(Str, 'xinglong828');
//  Result := Decrypt2(Result, 787);
  Result := Decrypt1(Str);
end;

//********************************************************
//             加密解密                         （方式1）
//********************************************************
function Encrypt1(Str: string): string; //字符加密函數
var
  i, j: Integer;
begin
  Result := '';
  j := 0;
  for i := 1 to Length(Str) do
  begin
    Result := Result + IntToHex(Byte(Str[i]) xor EncryptKey[j], 2);
    j := (j + 1) mod 8;
  end;
end;

function Decrypt1(Str: string): string; //字符解密函數
var
  i, j: Integer;
begin
  Result := '';
  j := 0;
  for i := 1 to Length(Str) div 2 do
  begin
    Result := Result + Char(StrToInt('$' + Copy(Str, i * 2 - 1, 2)) xor EncryptKey[j]);
    j := (j + 1) mod 8;
  end;
end;
function Crypt1(str: string; const bEncrypt: boolean): String;
begin
  if bEncrypt then
    Result := Encrypt1(Str)
  else
    Result := Decrypt1(Str);
end;
//********************************************************
//             加密解密                         （方式2）
//********************************************************
function Encrypt2(s: string; Key: Word): string; //字符加密函數
begin
  Result := Crypt2(s, Key, true);
end;
function Decrypt2(s: string; Key: Word): string; //字符加密函數
begin
  Result := Crypt2(s, Key, false);
end;
function Crypt2(s: string; Key: Word; const bEncrypt: boolean): string;
var
  i: integer;
  ps, pr: ^byte;
begin
  if bEncrypt then
    s := s + #0;
  SetLength(Result, Length(s));
  ps := @s[1];
  pr := @Result[1];
  for i := 1 to length(s) do
  begin
    pr^ := ps^ xor (Key shr 8);
    if bEncrypt then
      Key := (pr^ + Key) * SeedA + SeedB
    else
      Key := (ps^ + Key) * SeedA + SeedB;
    pr := pointer(integer(pr) + 1);
    ps := pointer(integer(ps) + 1);
  end;
end;
//********************************************************
//             加密解密                         （方式3）
//********************************************************
function Encrypt3(Src, Key: string): string; //字符串加密函数
//对字符串加密(Src:源 Key:密匙)  
var KeyLen: Integer;
  KeyPos: Integer;
  offset: Integer;
  dest: string;
  SrcPos: Integer;
  SrcAsc: Integer;
  Range: Integer;
begin
  KeyLen := Length(Key);
  if KeyLen = 0 then key := EncrypDefaulttKey;
  KeyPos := 0;
  Range := 256;
  Randomize;
  offset := Random(Range);
  dest := format('%1.2x', [offset]);
  for SrcPos := 1 to Length(Src) do
  begin
    SrcAsc := (Ord(Src[SrcPos]) + offset) mod 255;
    if KeyPos < KeyLen then
      KeyPos := KeyPos + 1
    else
      KeyPos := 1;
    SrcAsc := SrcAsc xor Ord(Key[KeyPos]);
    dest := dest + format('%1.2x', [SrcAsc]);
    offset := SrcAsc;
  end;
  Result := Dest;
end;

function Decrypt3(Src, Key: string): string; //字符串解密函数
//对字符串解密(Src:源 Key:密匙)  
var KeyLen: Integer;
  KeyPos: Integer;
  offset: Integer;
  dest: string;
  SrcPos: Integer;
  SrcAsc: Integer;
  TmpSrcAsc: Integer;
begin
  KeyLen := Length(Key);
  if KeyLen = 0 then key := EncrypDefaulttKey;
  KeyPos := 0;
  offset := StrToInt('$' + copy(src, 1, 2));
  SrcPos := 3;
  repeat
    SrcAsc := StrToInt('$' + copy(src, SrcPos, 2));
    if KeyPos < KeyLen then
      KeyPos := KeyPos + 1
    else
      KeyPos := 1;
    TmpSrcAsc := SrcAsc xor Ord(Key[KeyPos]);
    if TmpSrcAsc <= offset then
      TmpSrcAsc := 255 + TmpSrcAsc - offset
    else
      TmpSrcAsc := TmpSrcAsc - offset;
    dest := dest + chr(TmpSrcAsc);
    offset := srcAsc;
    SrcPos := SrcPos + 2;
  until SrcPos >= Length(Src);
  Result := Dest;
end;
function Crypt3(Src, Key: string; const bEncrypt: boolean): string;
begin
  if bEncrypt then
    Result := Encrypt3(Src, Key)
  else
    Result := Decrypt3(Src, Key);
end;

end.
 