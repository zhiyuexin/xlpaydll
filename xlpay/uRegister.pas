unit uRegister;

interface

type
  TRegisterInfo = Record
    TerminalSN: String[255];
    TerminalID: String[255];
    PosNo: String[100];
    ComCode: String[100];
    PartnerId: String[100];
    RandomKey: String[255];
    ValidDate: TDateTime;
  end;
  TRegister = class(TObject)
  private
    FTerminalSN: String;
    FTerminalID: String;
    FPosNo: String;
    FComCode: String;
    FPartnerId: String;
    FRandomKey: String;
    FValidDate: TDateTime;
    FRec: TRegisterInfo;
    procedure SetTerminalID(const Value: String);
    procedure SetTerminalSN(const Value: String);
  public
    property TerminalID: String read FTerminalID write SetTerminalID;
    property TerminalSN: String read FTerminalSN write SetTerminalSN;
    property ValidDate: TDateTime read FValidDate write FValidDate;
    property ComCode: String read FComCode write FComCode;
    property PosNo: String read FPosNo write FPosNo;
    property PartnerId: String read FPartnerId write FPartnerId;
    property RandomKey: String read FRandomKey write FRandomKey;
    property Rec: TRegisterInfo read FRec write FRec;

    function WriteToFile(FileName: String): Boolean;
    function ReadFromFile(FileName: String): Boolean;

    function passed(CONST id, sn: String): Boolean;
  end;

implementation

uses uEncrypt;
{ TRegister }

function TRegister.passed(CONST id, sn: String): Boolean;
begin
  FRec.RandomKey := Decrypt(FRec.RandomKey);
  Result := ((FRec.TerminalSN <> '')
    and (FRec.TerminalSN = FRec.RandomKey)
    and (id = FRec.TerminalID)
    and (sn = FRec.TerminalSN));
end;

function TRegister.ReadFromFile(FileName: String): Boolean;
var
  F: File Of TRegisterInfo;
begin
  AssignFile(F, FileName);
  Reset(F);
  try
    Read(F, FRec);
    Result := True;
  finally
    CloseFile(F);
  end;
end;

procedure TRegister.SetTerminalID(const Value: String);
begin
  FTerminalID := Value;
end;

procedure TRegister.SetTerminalSN(const Value: String);
begin
  FTerminalSN := Value;
  FRandomKey := Encrypt(Value);
end;

function TRegister.WriteToFile(FileName: String): Boolean;
var
  F: File Of TRegisterInfo;
begin
  AssignFile(F, FileName);
  Rewrite(F);
  try
    FRec.TerminalSN := FTerminalSN;
    FRec.TerminalID := FTerminalID;
    FRec.PosNo := FPosNo;
    FRec.ComCode := FComCode;
    FRec.PartnerId := FPartnerId;
    FRec.RandomKey := FRandomKey;
    FRec.ValidDate := FValidDate;
    Write(F, FRec);
    Result := True;
  finally
    CloseFile(F);
  end;
end;

end.
 