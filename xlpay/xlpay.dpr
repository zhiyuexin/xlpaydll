library xlpay;

uses
  FastMM4,
  SysUtils,
  Classes,
  Controls,
  Windows,
  ShellAPI,
  superobject,
  uFrmPay in 'uFrmPay.pas' {frmPay},
  ZintInterface in 'ZintInterface.pas',
  MD5 in 'MD5.pas',
  uFrmSoftKey in 'uFrmSoftKey.pas' {frmSoftKey},
  TimerDlg in 'TimerDlg.pas',
  uCommon in 'uCommon.pas',
  uCompress in 'uCompress.pas',
  uEncrypt in 'uEncrypt.pas',
  uRegister in 'uRegister.pas';

{$R *.res}

function request(request_param: PChar; return_value: PChar): Integer; stdcall;
  var
    s: string;
    requestObj: ISuperObject;
    returnObj: ISuperObject;
begin
  // 初始化返回值
  Result := 0;
  try
    s := '{"success":false,"err_code":null,"err_message":null}';
    returnObj := SO(s);
    s := returnObj.AsJSon(True, False);
    StrCopy(return_value, PChar(s));
  except
    on E: Exception do
    begin
      returnObj.B['success'] := false;
      returnObj.S['err_code'] := 'E000';
      returnObj.S['err_message'] := 'system initialize error:' + E.Message;
      s := returnObj.AsJSon(True, False);
      StrCopy(return_value, PChar(s));
      info('返回给调用程序数据[' + IntToStr(strlen(return_value)) + ']: ' + return_value);
      Exit;
    end
  end;
  // 读取请求参数
  try
    requestObj := SO(request_param);
    requestObj.S['trans_id'];
  except
    on E: Exception do
    begin
      returnObj.B['success'] := false;
      returnObj.S['err_code'] := 'E100';
      returnObj.S['err_message'] := 'invalid request param!';//(' + E.Message + ')';
      s := returnObj.AsJSon(True, False);
      StrCopy(return_value, PChar(s));
      info('返回给调用程序数据[' + IntToStr(strlen(return_value)) + ']: ' + return_value);
      Exit;
    end
  end;
  // 处理请求     
  with TfrmPay.Create(nil) do
  begin
    try
      try
        RequestParam := request_param;
        if not RequestValid or not InitValid then
        begin
          StrCopy(return_value, PChar(ReturnValue));
        end
        else
        begin
          if ShowModal = mrOk then
          begin
            Result := 1;
          end;
          StrCopy(return_value, PChar(ReturnValue));
        end;
      except
        on E: Exception do
        begin
          returnObj.B['success'] := false;
          returnObj.S['err_code'] := 'E999';
          returnObj.S['err_message'] := 'system error:' + E.Message;
          s := returnObj.AsJSon(True, False);
          StrCopy(return_value, PChar(s));
        end
      end;
      info('返回给调用程序数据[' + IntToStr(strlen(return_value)) + ']: ' + return_value);
    finally
      Free;
    end;
  end;

end;

exports request;

begin
end.
