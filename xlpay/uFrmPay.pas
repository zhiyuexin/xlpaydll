
unit uFrmPay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,
  uCommon, uRegister,
  superobject, ZintInterface, rtcConn,
  rtcDataCli, rtcHttpCli, rtcInfo, rtcTcpCli, rtcDataSrv, rtcHttpSrv,
  dxmdaset, DB, TimerDlg,
  frxClass, frxExportText, frxDBSet, CPort, frxBarcode, frxDesgn;

const
  APP_AK = 'iowjfsajfowe';
  APP_ID = '10001';
  APP_SK = 'f9dfsd8fhwfw';
//  URL_QUERY = '/query/{pay_code}/{partner_id}/{ext_params}';
//  URL_QUERY_RESULT = '/scan_pay_result/{pay_code}/{partner_id}/{ext_params}';
//  URL_SCAN_PAY = '/scan_pay/{pay_code}/{partner_id}/{ext_params}';
//  URL_REFUND = '/refund/{pay_code}/{partner_id}/{ext_params}';
//  URL_GET_PAY_URL = '/get_pay_url/{pay_code}/{partner_id}';
//  URL_REG = '/sys/reg';
//  URL_LOG_ON = '/sys/logon';
//  URL_LOG_OFF = '/sys/logoff';
//  URL_GET_MAX_BILL_ID = '/qry/getmaxbillid';
//  URL_GET_TRADE_REC = '/qry/gettraderec';
  // 用于双屏程序显示支付二维码
  MDS_WM_SHOWMSG = WM_USER + 10;
  MDS_SHOW_QRCODE_MSG = '{"disp_value":"{qrcode_file}","obj_index":"2","obj_type":"bmp","timeout":"900"}';
  MDS_CLEAR_QRCODE_MSG = '{"obj_index":"2","clear":true}';

type
  TfrmPay = class(TForm)
    pnlTop: TPanel;
    pnlBottom: TPanel;
    pnlMain: TPanel;
    imgQrCode: TImage;
    edtCodeInput: TEdit;
    drScanCode: TRtcDataRequest;
    HttpClient: TRtcHttpClient;
    drGetQrCode: TRtcDataRequest;
    edtPayAmt: TEdit;
    lblPayAmt: TLabel;
    pnlRefund: TPanel;
    edtRefundNo: TEdit;
    Label3: TLabel;
    Button2: TButton;
    pnlSale: TPanel;
    drQuery: TRtcDataRequest;
    drRefund: TRtcDataRequest;
    TimerQuery: TTimer;
    Label1: TLabel;
    edtRefundAmt: TEdit;
    drQueryResult: TRtcDataRequest;
    btnReturnPay: TButton;
    Button1: TButton;
    frxReport: TfrxReport;
    frxSimpleTextExport1: TfrxSimpleTextExport;
    frxDBDataset1: TfrxDBDataset;
    MemData: TdxMemData;
    ComPort: TComPort;
    pnlQueryWait: TPanel;
    lblQueryWaitMsg: TLabel;
    TimerQueryResult: TTimer;
    MemDatabill_content: TStringField;
    MemDatatrade_no: TStringField;
    MemDatapos_no: TStringField;
    MemDatabill_id: TStringField;
    MemDatavip_code: TStringField;
    MemDatasale_amt: TStringField;
    MemDatatrade_time: TStringField;
    frxBarCodeObject1: TfrxBarCodeObject;
    MemDatabill_title: TStringField;
    pnlSaleTag: TPanel;
    Label4: TLabel;
    frxDesigner1: TfrxDesigner;
    edtSaleTag: TComboBox;
    lblSaleTagLimit: TLabel;
    edtSaleTagLimit: TEdit;
    edt_sale_tag_disable: TCheckBox;
    btnSoftKey_refundNo: TButton;
    btnSoftKey_refundAmt: TButton;
    btnSoftKey_payAmt: TButton;
    drReg: TRtcDataRequest;
    drLogon: TRtcDataRequest;
    drLogoff: TRtcDataRequest;
    drCommon: TRtcDataRequest;
    MemDatamemo: TStringField;
    MemDatamemo2: TStringField;
    MemDatamemo3: TStringField;
    MemDatamemo4: TStringField;
    MemDatamemo5: TStringField;
    Edit1: TEdit;
    btnShowQrCode: TButton;
    pnlBottomShowQrCode: TPanel;
    pnlBottomInputCode: TPanel;
    btnReScan: TButton;
    TimerSetFocus: TTimer;
    procedure btnReScanClick(Sender: TObject);
    procedure btnShowQrCodeClick(Sender: TObject);
    procedure btnSoftKey_payAmtClick(Sender: TObject);
    procedure btnSoftKey_refundAmtClick(Sender: TObject);
    procedure btnSoftKey_refundNoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure drGetQrCodeBeginRequest(Sender: TRtcConnection);
    procedure drGetQrCodeDataReceived(Sender: TRtcConnection);
    procedure drBeginRequest(Sender: TRtcConnection);
    procedure drDataReceived(Sender: TRtcConnection);
    procedure drQueryBeginRequest(Sender: TRtcConnection);
    procedure drQueryConnectLost(Sender: TRtcConnection);
    procedure drQueryDataReceived(Sender: TRtcConnection);
    procedure drRefundBeginRequest(Sender: TRtcConnection);
    procedure drRefundDataReceived(Sender: TRtcConnection);
    procedure drRegDataReceived(Sender: TRtcConnection);
    procedure edtCodeInputEnter(Sender: TObject);
    procedure edtCodeInputKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure drScanCodeBeginRequest(Sender: TRtcConnection);
    procedure drScanCodeDataReceived(Sender: TRtcConnection);
    procedure edtCodeInputExit(Sender: TObject);
    procedure edtPayAmtChange(Sender: TObject);
    procedure edtPayAmtKeyPress(Sender: TObject; var Key: Char);
    procedure edtRefundAmtChange(Sender: TObject);
    procedure edtRefundNoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtSaleTagChange(Sender: TObject);
    procedure edtSaleTagLimitChange(Sender: TObject);
    procedure edtSaleTagLimitKeyPress(Sender: TObject; var Key: Char);
    procedure edt_sale_tag_disableClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure HttpClientConnect(Sender: TRtcConnection);
    procedure HttpClientConnectError(Sender: TRtcConnection; E: Exception);
    procedure HttpClientConnectFail(Sender: TRtcConnection);
    procedure HttpClientConnecting(Sender: TRtcConnection);
    procedure HttpClientConnectLost(Sender: TRtcConnection);
    procedure HttpClientDisconnect(Sender: TRtcConnection);
    procedure HttpClientDisconnecting(Sender: TRtcConnection);
    procedure HttpClientException(Sender: TRtcConnection; E: Exception);
    procedure HttpClientInvalidResponse(Sender: TRtcConnection);
    procedure HttpClientReconnect(Sender: TRtcConnection);
//    procedure HttpServerListenError(Sender: TRtcConnection; E: Exception);
//    procedure HttpServerRequestNotAccepted(Sender: TRtcConnection);
    procedure TimerQueryResultTimer(Sender: TObject);
    procedure TimerQueryTimer(Sender: TObject);
    procedure TimerSetFocusTimer(Sender: TObject);
  private
    FResultObj: ISuperObject;
    FComCode: String;
    FBatchNo: String;
    FPosNo: String;
    FVipCode: String;
    FRandomId: String;
    FBillId: String;
    FVipId: String;
    FVipParm: String;
    FRedirectUri: String;
    FPayUrl: String;
    FRequestObj: ISuperObject;
    FRequestStr: String;
    FScanCode: String;
    FRequestValid: Boolean;
    FRequestParam: String;
    FPayAmt: Double;
    FSaleTagLimit: Double;
    FInitValid: Boolean;
    FNeedPrint: Boolean;
    FPartnerData: String;
    FOperatorId: String;
    FRemark: String;
    FPartnerId: String;
    FRequestId: String;
    FQueryUrl: String;
    FPayCode: String;
    FTransId: String;
    FScanType: Integer;
    FPrinterPort: String;
    FPrintType: Integer;
    FTradeNo: String;
//    FPaidAmt: String;
    FSaleAmt: String;
    FTradeTime: String;
    FReConnectTimes: Integer;
    FTradeTag: String;
    FClientSaleTagType: Integer;
    FClientSaleTag: String;
//    QueryResultTime: Integer;
    FScanPayCode: String;
    FAmtModified: Boolean;
    FServerSaleTag: String;
    FSaleTagName: String;
    FClientSaleTagLimit: Double;
    FServerSaleTagConfig: String;
    FRefundNoModified: Boolean;
    FOperatorName: String;

    drQueryRequesting: Boolean;
    FTerminalSn: String;
    FTerminalType: String;
    FTerminalId: String;
    FTerminalCode: String;
    FOperatorPwd: String;
    FLogonType: String;
    FEndTime: String;
    FBgnDate: String;
    FEndDate: String;
    FBgnTime: String;
    FReportMemo5: String;
    FReportMemo: String;
    FReportMemo4: String;
    FReportMemo2: String;
    FReportMemo3: String;
//    FPayType: String;
    FServerPath: String;
    F_URL_QUERY: String;
    F_URL_GET_PAY_URL: String;
    F_URL_REFUND: String;
    F_URL_QUERY_RESULT: String;
    F_URL_LOG_OFF: String;
    F_URL_GET_TRADE_REC: String;
    F_URL_GET_MAX_BILL_ID: String;
    F_URL_REG: String;
    F_URL_SCAN_PAY: String;
    F_URL_LOG_ON: String;
    FIsShowQrCode: Boolean;
    FDeptCode: String;
    FAmtModifiedUserConf: String;
//    FServerSaleTagDetail: TSuperArray;
    function GetReturnValue: string;
    function GetBatchNo: String;
    function GetRandomId: String;
    function GetVipParm: String;
    function GetRedirectUri: String;
    function GetMilliSecond: String;
    procedure SetRequestParam(const Value: String);
    function GetLocalPort: String;
    function GetServerAddr: String;
    function GetServerPort: String;
    procedure SetLocalPort(const Value: String);
    procedure SetServerAddr(const Value: String);
    procedure SetServerPort(const Value: String);
    function GetDataFile: String;
    procedure SetDataFile(const Value: String);
    function GetAppSn(ts: string): string;
    procedure ShowQrCode();
    function GetPayAmt: String;
    procedure SetPayAmt(const Value: String);
    procedure PrintBill;
    procedure Print2Lpt(port, fileName: String);
    procedure Print2Com(port, fileName: String);
    function GetUrlGetPayUrl: String;
    function GetUrlQuery: String;
    function GetUrlQueryResult: String;
    function GetUrlRefund: String;
    function GetUrlScanPay: String;
    function GetReportFileWx: String;
    procedure doQuery;
    procedure doQueryResult;
//    procedure Delay(dwMilliseconds: DWORD);
    function RoundFloat(f: double; i: integer): double;
    procedure SetServerSaleTag(const Value: String);
    function GetSaleTag: String;
    function GetSaleTagLimit: Double;
    procedure SetServerSaleTagPanel(const Value: String);
    function SendQrCode2MDS(msg: String): Integer;
    procedure ShowMsg2MDS(QrCodeBmpFile: String);
    function IsFileInUse(fName: string): boolean;
    procedure AssistKey(var Key: Char);
    procedure showSoftKey(Sender: TEdit);
    function GetExtParams: String;
    procedure doLogoff;
    procedure doLogon;
    procedure doReg;
    function GetUrlLogoff: String;
    function GetUrlLogon: String;
    function GetUrlReg: String;
    function dealUrl(const url: String): String;
    procedure ShowError(errMessage: String; writeLog: Boolean = True);
    procedure ShowErrorAndAbortApp(errMessage: String; returnCode: String = ''; returnMessage: String = ''; success: boolean = False);
    function GetPosID: String;
    procedure SetIsRegisted(const Value: Boolean);
    procedure SetPosID(const Value: String);
    function GetIsRegisted: Boolean;
    function GetTerminalID: String;
    function GetTerminalSN: String;
    procedure SetTerminalID(const Value: String);
    procedure SetTerminalSN(const Value: String);
    function GetTerminalType: String;
    function GetComCode: String;
    procedure SetComCode(const Value: String);
    function GetPosNo: String;
    procedure SetPosNo(const Value: String);
    procedure AbortApp(returnCode: String = ''; returnMessage: String = ''; success: boolean = False);
    function GetPartnerId: String;
    procedure SetPartnerId(const Value: String);
    procedure SendData(Sender: TRtcConnection);
    procedure doCommonQuery(url, waitMsg: String);
    function GetUrlGetMaxBillId: String;
    function GetUrlGetTradeRec: String;
    procedure PostScanCode;
    procedure calcPnlSaleHeight;
    function GetUserDefineNoAskQuit: Boolean;
//    function GetPayType: String;
    function GetServerPath: String;
    procedure SetServerPath(const Value: String);
    function Get_URL_QUERY: String;
    function Get_URL_GET_MAX_BILL_ID: String;
    function Get_URL_GET_PAY_URL: String;
    function Get_URL_GET_TRADE_REC: String;
    function Get_URL_LOG_OFF: String;
    function Get_URL_LOG_ON: String;
    function Get_URL_QUERY_RESULT: String;
    function Get_URL_REFUND: String;
    function Get_URL_REG: String;
    function Get_URL_SCAN_PAY: String;
    procedure doGetQrCode;
    function GetDeptCode: String;
    procedure SetDeptCode(const Value: String);
    function GetAmtModifiedUserConf: String;
    function RptInitStr: String;
    { Private declarations }
  public
    property LocalPort: String read GetLocalPort write SetLocalPort;
    property ServerAddr: String read GetServerAddr write SetServerAddr;
    property ServerPort: String read GetServerPort write SetServerPort;
    property ServerPath: String read GetServerPath write SetServerPath;


    property URL_QUERY: String read Get_URL_QUERY write F_URL_QUERY;
    property URL_QUERY_RESULT: String read Get_URL_QUERY_RESULT write F_URL_QUERY_RESULT;
    property URL_SCAN_PAY: String read Get_URL_SCAN_PAY write F_URL_SCAN_PAY;
    property URL_REFUND: String read Get_URL_REFUND write F_URL_REFUND;
    property URL_GET_PAY_URL: String read Get_URL_GET_PAY_URL write F_URL_GET_PAY_URL;
    property URL_REG: String read Get_URL_REG write F_URL_REG;
    property URL_LOG_ON: String read Get_URL_LOG_ON write F_URL_LOG_ON;
    property URL_LOG_OFF: String read Get_URL_LOG_OFF write F_URL_LOG_OFF;
    property URL_GET_MAX_BILL_ID: String read Get_URL_GET_MAX_BILL_ID write F_URL_GET_MAX_BILL_ID;
    property URL_GET_TRADE_REC: String read Get_URL_GET_TRADE_REC write F_URL_GET_TRADE_REC;

    property DataFile: String read GetDataFile write SetDataFile;
    property PosID: String read GetPosID write SetPosID;
    property IsRegisted: Boolean read GetIsRegisted write SetIsRegisted;
    property TerminalID: String read GetTerminalID write SetTerminalID;
    property TerminalSN: String read GetTerminalSN write SetTerminalSN;
    property ComCode: String read GetComCode write SetComCode;
    property DeptCode: String read GetDeptCode write SetDeptCode;
    property PosNo: String read GetPosNo write SetPosNo;
    property PartnerId: String read GetPartnerId write SetPartnerId;
    property ErpType: String read GetPartnerId write SetPartnerId;

    property RequestParam: String read FRequestParam write SetRequestParam;
    property RequestObj: ISuperObject read FRequestObj write FRequestObj;
    property ResultObj: ISuperObject read FResultObj write FResultObj;
    property ReturnValue: string read GetReturnValue;

    property RequestValid: Boolean read FRequestValid;
    property InitValid: Boolean read FInitValid;

    property ScanCode: String read FScanCode write FScanCode;
    property QueryUrl: String read FQueryUrl write FQueryUrl;

    property UrlQuery: String read GetUrlQuery;
    property UrlQueryResult: String read GetUrlQueryResult;
    property UrlScanPay: String read GetUrlScanPay;
    property UrlRefund: String read GetUrlRefund;
    property UrlGetPayUrl: String read GetUrlGetPayUrl;
    property UrlReg: String read GetUrlReg;
    property UrlLogon: String read GetUrlLogon;
    property UrlLogoff: String read GetUrlLogoff;
    property UrlGetMaxBillId: String read GetUrlGetMaxBillId;
    property UrlGetTradeRec: String read GetUrlGetTradeRec;

    property TransId: String read FTransId write FTransId;
    property RequestId: String read FRequestId write FRequestId;
    property BillId: String read FBillId write FBillId;
    property BatchNo: String read GetBatchNo write FBatchNo;
    property RandomId: String read GetRandomId write FRandomId;
    property PayAmt: String read GetPayAmt write SetPayAmt;
    property AmtModified: Boolean read FAmtModified write FAmtModified;
    property RefundNoModified: Boolean read FRefundNoModified write FRefundNoModified;
    property ScanType: Integer read FScanType write FScanType;
    property PayCode: String read FPayCode write FPayCode;
    property ScanPayCode: String read FScanPayCode write FScanPayCode;
    property IsShowQrCode: Boolean read FIsShowQrCode write FIsShowQrCode;


    property AmtModifiedUserConf: String read GetAmtModifiedUserConf write FAmtModifiedUserConf;

    property VipCode: String read FVipCode write FVipCode;
    property VipId: String read FVipId write FVipId;
    property PartnerData: String read FPartnerData write FPartnerData;
    property NeedPrint: Boolean read FNeedPrint write FNeedPrint;
    property PrintType: Integer read FPrintType write FPrintType;
    property PrinterPort: String read FPrinterPort write FPrinterPort;
    property ClientSaleTagType: Integer read FClientSaleTagType write FClientSaleTagType;
    property ClientSaleTag: String read FClientSaleTag write FClientSaleTag;
    property ClientSaleTagLimit: Double read FClientSaleTagLimit write FClientSaleTagLimit;
    property SaleTagLimit: Double read GetSaleTagLimit write FSaleTagLimit;
    property ServerSaleTag: String read FServerSaleTag write SetServerSaleTag;
    property ServerSaleTagConfig: String read FServerSaleTagConfig write FServerSaleTagConfig;
    property SaleTag: String read GetSaleTag;

    property Remark: String read FRemark write FRemark;
    property OperatorId: String read FOperatorId write FOperatorId;
    property OperatorPwd: String read FOperatorPwd write FOperatorPwd;
    property OperatorName: String read FOperatorName write FOperatorName;
    property LogonType: String read FLogonType write FLogonType;

    property TradeNo: String read FTradeNo write FTradeNo;
    property SaleAmt: String Read FSaleAmt write FSaleAmt;
    property TradeTime: String read FTradeTime write FTradeTime;

//    property PayType: String read GetPayType write FPayType;

    property PayUrl: String read FPayUrl write FPayUrl;
    property RedirectUri: String read GetRedirectUri write FRedirectUri;
    property VipParm: String read GetVipParm write FVipParm;

    property TerminalCode: String read FTerminalCode write FTerminalCode;
    property TerminalType: String read GetTerminalType write FTerminalType;

    property ReportFileWx: String read GetReportFileWx;

    property TradeTag: String read FTradeTag write FTradeTag;

    property ExtParams: String read GetExtParams;
    property BgnTime: String read FBgnTime write FBgnTime;
    property EndTime: String read FEndTime write FEndTime;
    property BgnDate: String read FBgnDate write FBgnDate;
    property EndDate: String read FEndDate write FEndDate;

    property ReportMemo: String read FReportMemo write FReportMemo;
    property ReportMemo2: String read FReportMemo2 write FReportMemo2;
    property ReportMemo3: String read FReportMemo3 write FReportMemo3;
    property ReportMemo4: String read FReportMemo4 write FReportMemo4;
    property ReportMemo5: String read FReportMemo5 write FReportMemo5;

    property ReConnectTimes: Integer read FReConnectTimes write FReConnectTimes;

    property UserDefineNoAskQuit: Boolean read GetUserDefineNoAskQuit;
  end;

var
  frmPay: TfrmPay;
procedure SwitchToThisWindow(hWnd: Thandle; fAltTab: boolean); stdcall; external 'User32.dll';

implementation

uses
  md5, DateUtils, IniFiles, rtcConnProv, Registry, uFrmSoftKey;
{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////
// 窗体创建（初始化）
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.FormCreate(Sender: TObject);
begin
  // 窗口置前
  SetWindowPos(self.handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);
  TimerSetFocusTimer(nil);
  // 是否显示调试信息（支付URL）
  Edit1.Visible := GetConfigValue('DEBUG_MODE', False);
  FInitValid := False;
  FPayAmt := 0;
  ReConnectTimes := 0;

  ResultObj := SO('{'#13#10
    + '  "success": false,'#13#10
    + '  "err_code": null,'#13#10
    + '  "err_message": null,'#13#10
    + '  "trade_no": null,'#13#10
    + '  "sale_amt": null,'#13#10
    + '  "trade_time": null'#13#10
    + '}'
    );

  HttpClient.ServerAddr := ServerAddr;
  HttpClient.ServerPort := ServerPort;
  HttpClient.AutoConnect := True;
  HttpClient.Connect();

  FInitValid := True;

  drQueryRequesting := False;
end;

//procedure TfrmPay.Delay(dwMilliseconds: DWORD);//Longint
//var
//  iStart, iStop: DWORD;
//begin
//  iStart := GetTickCount;
//  repeat
//    iStop := GetTickCount;
//    Application.ProcessMessages;
//  until (iStop - iStart) >= dwMilliseconds;
//end;

////////////////////////////////////////////////////////////////////////////////
// 打印签购单
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.PrintBill();
begin
  if not NeedPrint then Exit;
  if (TransId = 'B2') then  Exit;   //查询
  try
    begin
      if FileExists(ReportFileWx) then
      begin
        frxReport.LoadFromFile(ReportFileWx);
      end;

      frxReport.PrintOptions.Printer := PrinterPort;
      if not MemData.Active then  MemData.Open;
      MemData.First;
      while not MemData.Eof do
      begin
        MemData.Delete;
        MemData.Next;
      end;
      MemData.Insert;
      MemData.FieldByName('trade_no').AsString := TradeNo;
      MemData.FieldByName('pos_no').AsString := PosNo;
      MemData.FieldByName('bill_id').AsString := BillId;
      MemData.FieldByName('vip_code').AsString := VipCode;
      MemData.FieldByName('sale_amt').AsString := SaleAmt;
      MemData.FieldByName('trade_time').AsString := TradeTime;
      MemData.FieldByName('bill_content').AsString := '';
      MemData.FieldByName('memo').AsString := ReportMemo;
      MemData.FieldByName('memo2').AsString := ReportMemo2;
      MemData.FieldByName('memo3').AsString := ReportMemo3;
      MemData.FieldByName('memo4').AsString := ReportMemo4;
      MemData.FieldByName('memo5').AsString := ReportMemo5;
      MemData.Post;
      frxReport.FileName := 'XLPay';
      if frxReport.PrepareReport() then
      begin
        if PrintType = 0 then  //驱动打印
        begin
          frxReport.Print;
        end
        else if PrintType = 1 then   //端口打印
        begin
          frxReport.Export(frxSimpleTextExport1);
          if Copy(PrinterPort, 0, 3) = 'LPT' then
          begin
            Print2Lpt(PrinterPort, frxSimpleTextExport1.FileName);
          end
          else if Copy(PrinterPort, 0, 3) = 'COM' then
          begin
            Print2Com(PrinterPort, frxSimpleTextExport1.FileName);
          end;
        end
        else if PrintType = 2 then   //txt文件打印   
        begin
          frxReport.Export(frxSimpleTextExport1);
        end;
      end;
    end
  except
    on E: Exception do
    begin
      ShowError('打印单据失败！'#13#10 + E.Message);
    end;
  end;
end;

function TfrmPay.RptInitStr(): String;
var
  InitStrType: Integer;
begin
  InitStrType := GetConfigValue('REPORT', 'RPT_INIT_STR', 0);
  If InitStrType = 0 Then Exit
  Else If InitStrType = 1 Then
    Result := char(27) + char(64)
  Else If InitStrType = 2 Then
    Result := char(27) + char(64)
  Else If InitStrType = 3 Then
    Result := char(28) + char(33) + char(3)
  Else If InitStrType = 4 Then
    Result := char(20)
  Else If InitStrType = 5 Then
    Result := char(27) + char(64);
end;

procedure TfrmPay.Print2Lpt(port: String; fileName: String);
Var
  F: TextFile;
  i: Integer;
  initStr: String;
Begin
  try
    AssignFile(F, port);
    Rewrite(F);
    try
      with TStringList.Create do
      begin
        try
          LoadFromFile(fileName);
          If initStr <> '' Then write(F, initStr);
          for i := 0 to Count - 1 do
          begin
            write(F, Strings[i] + #13#10);
          end;

          for i := 0 to 10 do
          begin
            write(F, #13#10);
          end;
        finally
          Free;
        end;
      end;
    finally
      CloseFile(F);
    end;
  Except
    on E: Exception do
    begin
      // 如果LPT1端口不存在，会报错：the specified file not found
      // 有些主板不提供LPT并口，不屏蔽错误，无法收银
    end;
  End;
end;

procedure TfrmPay.Print2Com(port: String; fileName: String);
Var
//  F: TextFile;
  i: Integer;
  initStr: String;
Begin
  try
    ComPort.Port := port;
    ComPort.Connected := True;
    try
      with TStringList.Create do
      begin
        try
          initStr := RptInitStr();
          LoadFromFile(fileName);
          If initStr <> '' Then ComPort.WriteStr(initStr);
          for i := 0 to Count - 1 do
          begin
            ComPort.WriteStr(Strings[i] + #13#10);
          end;

          for i := 0 to 10 do
          begin
            ComPort.WriteStr(#13#10);
          end;
        finally
          Free;
        end;
      end;
    finally
      ComPort.Connected := False;
    end;
  Except
    on E: Exception do
    begin

    end;
  End;
end;

procedure TfrmPay.Button1Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmPay.Button2Click(Sender: TObject);
begin
  if (edtRefundNo.Text = '') then
  begin
    ShowError('请输入原交易单号', False);
    if edtRefundNo.Enabled then edtRefundNo.SetFocus;
    Exit;
  end;
  if (edtRefundAmt.Text = '') then
  begin
    ShowError('请输入退款金额', False);
    if edtRefundAmt.Enabled then edtRefundAmt.SetFocus;
    Exit;
  end;
  try
    if StrToFloat(edtRefundAmt.Text) = 0 then
    begin
      ShowError('请输入退款金额', False);
    end;
  except
    ShowError('输入的退款金额不正确', False);
  end;
  try
    edtRefundNo.Text := TrimLeft(edtRefundNo.Text);
    edtRefundNo.Text := TrimRight(edtRefundNo.Text);
    drRefund.Request.Method := 'POST';
    drRefund.Request.FileName := URL_Encode(UrlRefund);
    drRefund.Request.Host := HttpClient.ServerAddr;
    Button2.Enabled := False;
    drRefund.Post();
    //Button1.SetFocus;
  except
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 请求支付二维码
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.drGetQrCodeBeginRequest(Sender: TRtcConnection);
//var
//  ts: string;
//  Cli: TRtcDataClient absolute Sender;
begin
  if not Sender.inMainThread then
    Sender.Sync(drGetQrCodeBeginRequest)
  else
  begin
    SendData(Sender);
  end;
end;
////////////////////////////////////////////////////////////////////////////////
// 获取支付二维码
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.drGetQrCodeDataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  s: String;
  ReturnInfo, ResultObj: ISuperObject;
begin
  if not Sender.inMainThread then
    Sender.Sync(drGetQrCodeDataReceived)
  else
  begin
    if Cli.Response.Done then
    begin
      if (Cli.Response.StatusCode = 200) then
      begin
        s := Utf8Decode(Cli.Read);
        ReturnInfo := SO(s);
        debug('接收数据从支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][drGetQrCodeDataReceived]: ' + s);
        ResultObj := ReturnInfo.O['result']; // {"success":true,"errCode":"0","errMessage":null,"result":"weixin://wxpay/bizpayurl?pr=PbEvrxA"}

        if ResultObj = nil then
        begin
          ShowErrorAndAbortApp('获取支付二维码信息失败!'#13#10 + '[E301]failed to get server configuration', 'E301', 'failed to get server configuration');
        end;

        if ResultObj.IsType(stString) then
        begin
          ResultObj := SO(ReturnInfo.S['result']);
        end;

        if not ResultObj.B['success'] then
        begin
          ShowErrorAndAbortApp('获取支付二维码信息失败!!'#13#10 + '[' + ResultObj.S['errCode'] + ']' + ResultObj.S['errMessage'], ResultObj.S['errCode'], ResultObj.S['errMessage']);
        end;

        if ComCode = '' then
        begin
          ComCode := ResultObj.S['com_code'];
        end;

        ServerSaleTag := ResultObj.S['server_sale_tag'];

        FPayUrl := ResultObj.S['pay_url'];
        if (FPayUrl = '') then
          FPayUrl := ResultObj.S['result'];
//        showmessage(fpayurl);
        if (FPayUrl <> '') then
        begin
          //ComCode
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{com_code}'), ComCode, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{com_code}', ComCode, [rfReplaceAll, rfIgnoreCase]);
          //PosNo
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{pos_no}'), PosNo, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{pos_no}', PosNo, [rfReplaceAll, rfIgnoreCase]);
          //BillId
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{bill_id}'), BillId, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{bill_id}', BillId, [rfReplaceAll, rfIgnoreCase]);
          //BatchNo
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{batch_no}'), BatchNo, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{batch_no}', BatchNo, [rfReplaceAll, rfIgnoreCase]);
          //RandomId
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{random_id}'), RandomId, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{random_id}', RandomId, [rfReplaceAll, rfIgnoreCase]);
          //VipCode
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{vip_code}'), VipCode, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{vip_code}', VipCode, [rfReplaceAll, rfIgnoreCase]);
          //VipId
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{vip_id}'), VipId, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{vip_id}', VipId, [rfReplaceAll, rfIgnoreCase]);
          //OperatorId
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{operator_id}'), OperatorId, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{operator_id}', OperatorId, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{empl_id}'), OperatorId, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{empl_id}', OperatorId, [rfReplaceAll, rfIgnoreCase]);
          //OperatorPwd
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{operator_pwd}'), OperatorPwd, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{operator_pwd}', OperatorPwd, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{empl_pwd}'), OperatorPwd, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{empl_pwd}', OperatorPwd, [rfReplaceAll, rfIgnoreCase]);
          //OperatorName  
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{operator_name}'), OperatorName, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{operator_name}', OperatorName, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, URL_Encode('{empl_name}'), OperatorName, [rfReplaceAll, rfIgnoreCase]);
          FPayUrl := StringReplace(FPayUrl, '{empl_name}', OperatorName, [rfReplaceAll, rfIgnoreCase]);

          ShowQrCode();
        end
        else
        begin
          imgQrCode.Picture.Bitmap := nil;
          pnlMain.Visible := False;
          pnlBottom.Visible := True;
          pnlBottomShowQrCode.Visible := False;
          pnlMain.Visible := False;
          calcPnlSaleHeight();
          edtCodeInput.SetFocus;
          //ShowErrorAndAbortApp('获取支付二维码信息失败', 'E301', 'failed to get server configuration');
        end;
      end
      else
      begin
        ShowErrorAndAbortApp('获取支付二维码信息出错', 'E301', 'failed to get server configuration, server response status code:' + IntToStr(Cli.Response.StatusCode));
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 轮询
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.TimerQueryTimer(Sender: TObject);
begin
  doQuery();
end;

procedure TFrmPay.doGetQrCode();
begin
  if not IsShowQrCode then Exit;
  imgQrCode.Picture.Bitmap := nil;
  ShowMsg2MDS('');
  if ((PayAmt = '') OR (StrToFloat(PayAmt) = 0)) then Exit;//新版需要请求二维码地址需要金额
  drGetQrCode.Request.Method := 'POST';
  drGetQrCode.Request.FileName := URL_Encode(UrlGetPayUrl);
  drGetQrCode.Request.Host := HttpClient.ServerAddr;
  drGetQrCode.Post();
end;

procedure TfrmPay.doQuery();
begin
  if drQueryRequesting then Exit;
  if PayAmt = '' then Exit;
  try
    drQuery.Request.Method := 'POST';
    drQuery.Request.FileName := URL_Encode(UrlQuery);
    drQuery.Request.Host := HttpClient.ServerAddr;
    drQuery.Post();
    if (TransId = 'B2') then     //查询
    begin
      lblQueryWaitMsg.Caption := lblQueryWaitMsg.Caption + '.';
    end;
  except
    on E: Exception do
    begin
      ShowError('查询信息出错:' + E.Message);
    end;
  end;
end;

procedure TfrmPay.doQueryResult();
begin
  if drQueryRequesting then Exit;
  try
    drQueryResult.Request.Method := 'POST';
    drQueryResult.Request.FileName := URL_Encode(UrlQueryResult);
    drQueryResult.Request.Host := HttpClient.ServerAddr;
    drQueryResult.Post();
  except
    on E: Exception do
    begin
      ShowError('查询信息结果出错:' + E.Message);
    end;
  end;
end;

procedure TfrmPay.TimerQueryResultTimer(Sender: TObject);
begin
  doQueryResult();
//  if (TransId = 'A1') or (TransId = 'A2') or (TransId = 'A3') then
//  begin
//    doQueryResult();
//  end
//  else
//  begin
//    TTimer(Sender).Enabled := False;
//  end;
end;
////////////////////////////////////////////////////////////////////////////////
// 查询请求（轮询）
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.drQueryBeginRequest(Sender: TRtcConnection);
//var
//  ts: string;
//  Cli: TRtcDataClient absolute Sender;
begin
  if drQueryRequesting then Exit;
  if not Sender.inMainThread then
    Sender.Sync(drQueryBeginRequest)
  else
  begin
    drQueryRequesting := True;
    SendData(Sender);
    drQueryRequesting := False;
  end;
end;

procedure TfrmPay.drQueryConnectLost(Sender: TRtcConnection);
begin
  drQueryRequesting := False;
end;

////////////////////////////////////////////////////////////////////////////////
// 查询（轮询）接收
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.drQueryDataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  ReturnInfo: ISuperObject;
  s: String;
  newVersion: Boolean;
begin
  if not Sender.inMainThread then
    Sender.Sync(drQueryDataReceived)
  else
  begin
    if Cli.Response.Done then
    begin
      if (Cli.Response.StatusCode = 200) then
      begin
        s := Utf8Decode(Cli.Read);
        ReturnInfo := SO(s);
        debug('接收数据从支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][drQueryDataReceived]: ' + s);
        ResultObj := SO(ReturnInfo.S['result']);
        debug(ResultObj.AsJSon(true, false));

        newVersion := True;
        if ResultObj.O['result'] <> nil then
          if ResultObj.O['result'].O['PAID_STATUS'] <> nil then
            newVersion := False;
        if newVersion then
        begin
          if ResultObj.B['success'] then
          begin
            FResultObj.B['success'] := ResultObj.B['success'];
            FResultObj.S['err_code'] := ResultObj.S['errCode'];
            FResultObj.S['err_message'] := ResultObj.S['errMessage'];
            FResultObj.I['paid_status'] := 1; //
            FResultObj.S['trade_no'] := ResultObj.O['result'].S['tradeNo'];
            FResultObj.S['pay_no'] := ResultObj.O['result'].S['payNo'];
            TradeNo := FResultObj.S['trade_no'];
            FResultObj.D['sale_amt'] := ResultObj.O['result'].D['payAmt'];
            FResultObj.D['trade_amt'] := ResultObj.O['result'].D['tradeAmt'];
            FResultObj.D['pay_amt'] := ResultObj.O['result'].D['payAmt'];
            SaleAmt := Format('%f', [FResultObj.D['sale_amt']]);
            if ResultObj.O['result'].O['TRADE_COMP_TIME'] <> nil then
              FResultObj.S['trade_time'] := ResultObj.O['result'].S['TRADE_COMP_TIME']
            else
            begin
              s := FResultObj.S['pay_no'];
              FResultObj.S['trade_time'] := copy(s, 6, 4) + '-' + copy(s, 10, 2) + '-' + copy(s, 12, 2) + ' ' +
                copy(s, 14, 2) + ':' + copy(s, 16, 2) + ':' + copy(s, 18, 2);
            end;
            TradeTime := FResultObj.S['trade_time'];

            if ((TransId = 'A1') or (TransId = 'A2') or (TransId = 'A3') or (TransId = 'B1')) then   //消费、撤销、退货  //B1重打印
            begin
              PrintBill();
            end;
            ModalResult := mrOk;
          end
          else
          begin
            if (TransId = 'B1') then   //重印
            begin
              AbortApp(ResultObj.S['errCode'], ResultObj.S['errMessage'], ResultObj.B['success']);
            end
            else if (TransId = 'B2') then  //查询
            begin
              AbortApp(ResultObj.S['errCode'], ResultObj.S['errMessage'], ResultObj.B['success']);
            end;
          end;
        end else begin  // For old version
          if ResultObj.B['success'] then
          begin
            if ResultObj.O['result'] <> nil then
            begin
              if (ResultObj.S['detail'] = '') then // and (ResultObj.O['detail'] = nil) then    //被扫结果
              begin
                info('被扫结果');
                FResultObj.B['success'] := ResultObj.B['success'];
                FResultObj.S['err_code'] := ResultObj.S['errCode'];
                FResultObj.S['err_message'] := ResultObj.S['errMessage'];
                FResultObj.I['paid_status'] := ResultObj.O['result'].I['PAID_STATUS'];
                FResultObj.S['trade_no'] := ResultObj.O['result'].S['TRADE_NO'];
                TradeNo := FResultObj.S['trade_no'];
                FResultObj.D['sale_amt'] := ResultObj.O['result'].D['TRADE_AMT'];
                SaleAmt := Format('%f', [FResultObj.D['sale_amt']]);
                FResultObj.S['trade_time'] := ResultObj.O['result'].S['TRADE_COMP_TIME'];
                TradeTime := FResultObj.S['trade_time'];

                if ((TransId = 'A1') or (TransId = 'A2') or (TransId = 'A3')) then   //消费、撤销、退货
                begin
                  info('TransId:' + TransId);
                //PAID_STATUS 支付状态 0:等待付款;1:支付成功;2:支付失败;3:退还部分款;4:已退款;5:修正退款;6:部分修正退款;
                  if (ResultObj.O['result'].I['PAID_STATUS'] = 1) then
                  begin
                    PrintBill();
                    ModalResult := mrOk;
                  end
                  else
                  if (ResultObj.O['result'].I['PAID_STATUS'] <> 0) then
                  begin
                    AbortApp;
                  end
                  else
                  begin
                    TimerQueryResult.Enabled := True;
                  end;
                end
                else
                if (TransId = 'B1') then   //重打印
                begin
                  if ((ResultObj.O['result'].I['PAID_STATUS'] = 1) or (ResultObj.O['result'].I['PAID_STATUS'] = 3)) then   // 1:支付成功;3:退还部分款
                  begin
                    PrintBill();
                    ModalResult := mrOk;
                  end
                  else
                  begin
                    AbortApp;
                  end;
                end
                else
                if (TransId = 'B2') then   //查询
                begin
                  ModalResult := mrOk;
                end;
              end
              else
              begin   // 主扫结果
                info('主扫结果');
                FResultObj.B['success'] := ResultObj.O['result'].B['success'];
                FResultObj.S['err_code'] := ResultObj.S['errCode'];
                FResultObj.S['err_message'] := ResultObj.S['errMessage'];
                FResultObj.D['sale_amt'] := ResultObj.O['result'].D['tradeAmt'];
                SaleAmt := Format('%f', [FResultObj.D['sale_amt']]);
                FResultObj.S['trade_time'] := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
                TradeTime := FResultObj.S['trade_time'];
                if FResultObj.B['success'] then
                begin
                  FResultObj.S['trade_no'] := ResultObj.O['result'].S['detail'];
                  TradeNo := FResultObj.S['trade_no'];
                  FResultObj.S['trade_time'] := copy(TradeNo, 6, 4) + '-' + copy(TradeNo, 10, 2) + '-' + copy(TradeNo, 12, 2) + ' ' +
                    copy(TradeNo, 14, 2) + ':' + copy(TradeNo, 16, 2) + ':' + copy(TradeNo, 18, 2);
                  PrintBill();
                  ModalResult := mrOk;
                end
                else
                begin
                  ShowError('[' + ResultObj.O['result'].O['detail'].S['tradeStatus'] + ']' + ResultObj.O['result'].O['detail'].S['statusInfo']);
                  ModalResult := mrOk;
                end;
              end;
            end;
          end
          else
          begin
            if (TransId = 'B1') then   //重印
            begin
              AbortApp(ResultObj.S['errCode'], ResultObj.S['errMessage'], ResultObj.B['success']);
            end
            else if (TransId = 'B2') then  //查询
            begin
              AbortApp(ResultObj.S['errCode'], ResultObj.S['errMessage'], ResultObj.B['success']);
            end
            else
            begin
              if ResultObj.B['srvError'] then
              begin
                ShowErrorAndAbortApp('[' + ResultObj.S['errCode'] + ']' + ResultObj.S['errMessage'], ResultObj.S['errCode'], ResultObj.S['errMessage']);
              end;
            end;
          end;
        end;
      end
      else
      begin
        AbortApp('E302', 'failed to get query result');
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// 退货请求
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.drRefundBeginRequest(Sender: TRtcConnection);
//var
//  ts: string;
//  Cli: TRtcDataClient absolute Sender;
begin
  if not Sender.inMainThread then
    Sender.Sync(drRefundBeginRequest)
  else
  begin
    SendData(Sender);
    Button2.Enabled := False;
    Screen.Cursor := crHourGlass;
  end;
end;
////////////////////////////////////////////////////////////////////////////////
// 退货接收  
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.drRefundDataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  ReturnInfo: ISuperObject;
  s: String;
begin
  if not Sender.inMainThread then
    Sender.Sync(drRefundDataReceived)
  else
  begin
    if Cli.Response.Done then
    begin
      Screen.Cursor := crArrow;
      Button2.Enabled := True;
      if (Cli.Response.StatusCode = 200) then
      begin
        s := Utf8Decode(Cli.Read);
        ReturnInfo := SO(s);
        debug('接收数据从支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][drRefundDataReceived]: ' + s);
        ResultObj := SO(ReturnInfo.S['result']);
        debug(ResultObj.AsJSon(true, false));
        FResultObj.B['success'] := ResultObj.B['success'];
        FResultObj.S['err_code'] := ResultObj.S['errCode'];
        FResultObj.S['err_message'] := ResultObj.S['errMessage'];
        if ResultObj.O['result'] <> nil then
        begin
          FResultObj.S['trade_no'] := ResultObj.O['result'].S['tradeNo'];
          FResultObj.S['pay_no'] := ResultObj.O['result'].S['payNo'];
          TradeNo := FResultObj.S['trade_no'];
          FResultObj.D['sale_amt'] := ResultObj.O['result'].D['payAmt'];
          FResultObj.D['refund_amt'] := ResultObj.O['result'].D['payAmt'];
          FResultObj.D['trade_amt'] := ResultObj.O['result'].D['tradeAmt'];
          FResultObj.D['pay_amt'] := ResultObj.O['result'].D['payAmt'];
          FResultObj.S['cntt_no'] := edtRefundNo.Text;
          SaleAmt := Format('%f', [FResultObj.D['sale_amt']]);
          FResultObj.S['trade_time'] := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
          FResultObj.S['trade_time'] := copy(TradeNo, 6, 4) + '-' + copy(TradeNo, 10, 2) + '-' + copy(TradeNo, 12, 2) + ' ' +
            copy(TradeNo, 14, 2) + ':' + copy(TradeNo, 16, 2) + ':' + copy(TradeNo, 18, 2);
          TradeTime := FResultObj.S['trade_time'];
        end;

        if ResultObj.B['success'] then
        begin
          PrintBill();
          ModalResult := mrOk;
        end
        else
        begin
          if ResultObj.B['srvError'] then
          begin
            ShowErrorAndAbortApp('[' + ResultObj.S['errCode'] + ']' + ResultObj.S['errMessage'], ResultObj.S['errCode'], ResultObj.S['errMessage']);
          end
          else
          begin
            ShowError('[' + FResultObj.S['errCode'] + ']' + FResultObj.S['errMessage']);
          end;
        end;
      end
      else
      begin
        AbortApp('E303', 'failed to get refund result, server response status code:' + IntToStr(Cli.Response.StatusCode));
      end;
    end;
  end;
end;

procedure TfrmPay.edtCodeInputEnter(Sender: TObject);
var
  edt: TEdit absolute Sender;
begin
  edt.Clear;
  btnReScan.Visible := False;
end;

procedure TfrmPay.edtCodeInputKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  edt: TEdit absolute Sender;
begin
  if (Key = 13) and (edt.Text <> '') then
  begin
    PostScanCode();
  end
  else if ((edt.Text = 'q') or (edt.Text = 'Q')) then
  begin
    edt.Clear;
    btnReturnPay.Click;
  end
  else if (pnlBottomShowQrCode.Visible and ((edt.Text = 'r') or (edt.Text = 'R'))) then
  begin
    edt.Clear;
    btnShowQrCode.Click;
  end
  else if (edtPayAmt.Enabled and ((edt.Text = 'z') or (edt.Text = 'Z'))) then
  begin
    edt.Clear;
    btnShowQrCode.Click;
  end
  else if (pnlSaleTag.Visible and ((edt.Text = 'y') or (edt.Text = 'Y'))) then
  begin
    edt.Clear;
    btnShowQrCode.Click;
  end;
end;

procedure TfrmPay.PostScanCode();
begin
  ScanPayCode := edtCodeInput.Text;
  edtCodeInput.Enabled := False;
  imgQrCode.Visible := False;
  //imgQrCode.Picture.Bitmap := nil;
  // 提交扫描码数据
  drScanCode.Request.Method := 'POST';
  drScanCode.Request.FileName := URL_Encode(UrlScanPay);
  drScanCode.Request.Host := HttpClient.ServerAddr;
  drScanCode.Post();
  btnReScan.Visible := True;
end;

procedure TfrmPay.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := (ModalResult = mrOK) or (ModalResult = mrAbort);
  if CanClose then
  begin
  end
  else
  begin
    if (UserDefineNoAskQuit) then
    begin
      AbortApp('E300', 'user canceled');
      CanClose := True;
    end
    else
    begin
      if (MessageBox(Self.Handle, PChar('是否退出?'), PChar('退出确认'), 292) = mrYES) then
      begin
        AbortApp('E300', 'user canceled');
        CanClose := True;
      end;
    end;
  end;
end;

procedure TfrmPay.FormResize(Sender: TObject);
var
  width, height, space: Integer;
begin
  space := 20;
  width := pnlMain.Width - space * 2;
  height := pnlMain.Height - space * 2;
  if (width > height) then width := height;
  if (width > PnlMain.Height) then width := PnlMain.Height;
  imgQrCode.width := width;
  imgQrCode.Height := width;
  imgQrCode.Left := (pnlMain.Width - width) div 2;
  imgQrCode.Top := (pnlMain.Height - width) div 2;

  edtCodeInput.Width := PnlBottom.Width;
end;

function TfrmPay.GetReturnValue: string;
begin
  Result := ResultObj.AsJSon(True, False);
end;

function TfrmPay.GetBatchNo: String;
begin
  Result := FBatchNo;
end;

function TfrmPay.GetRandomId: String;
begin
  Result := FRandomId;
end;

function TfrmPay.GetVipParm: String;
begin
  if ((VipCode <> '') and (VipId <> '')) then
  begin
    Result := FVipParm;
    Result := StringReplace(Result, '{vip_code}', VipCode, [rfReplaceAll, rfIgnoreCase]);
    Result := StringReplace(Result, '{vip_id}', VipId, [rfReplaceAll, rfIgnoreCase]);
  end
  else
  begin
    Result := '';
  end;
end;

function TfrmPay.GetRedirectUri: String;
begin
  Result := FRedirectUri;
  Result := StringReplace(Result, '{web_token}', 'b0167845f08a618e2281b2c4de943cd2', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{v_type}', '100', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{wx_code}', 'lnxldzsw', [rfReplaceAll, rfIgnoreCase]);

  Result := StringReplace(Result, '{com_code}', ComCode, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{pos_no}', PosNo, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{bill_id}', BillId, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{batch_no}', BatchNo, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{random_id}', RandomId, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{pay_amt}', PayAmt, [rfReplaceAll, rfIgnoreCase]);

  Result := StringReplace(Result, '{operator_id}', OperatorId, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{operator_pwd}', OperatorPwd, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{operator_name}', OperatorName, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{empl_id}', OperatorId, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{empl_pwd}', OperatorPwd, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{empl_name}', OperatorName, [rfReplaceAll, rfIgnoreCase]);
  Result := URL_Encode(Result);
end;

procedure TfrmPay.drScanCodeBeginRequest(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(drScanCodeBeginRequest)
  else
  begin
    SendData(Sender);
  end;
end;

function TfrmPay.GetAppSn(ts: string): string;
begin
  Result := LowerCase(StrToMD5(APP_ID + APP_AK + ts + APP_SK));
end;

procedure TfrmPay.drScanCodeDataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  ReturnInfo: ISuperObject;
  s: String;
begin
  if not Sender.inMainThread then
    Sender.Sync(drScanCodeDataReceived)
  else
  begin
    if Cli.Response.Done then
    begin
      if (Cli.Response.StatusCode = 200) then
      begin
        s := Utf8Decode(Cli.Read);
        ReturnInfo := SO(s);
        debug('接收数据从支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][drScanCodeDataReceived]: ' + s);
        //ResultObj := SO(ReturnInfo.AsString);
        ResultObj := SO(ReturnInfo.S['result']);
        debug(ResultObj.AsJSon(true, false));
        FResultObj.B['success'] := ResultObj.B['success'];
        FResultObj.S['err_code'] := ResultObj.S['errCode'];
        FResultObj.S['err_message'] := ResultObj.S['errMessage'];

        if ResultObj.B['success'] then
        begin
          TimerQueryResult.Interval := 2000;
          TimerQueryResult.Enabled := True;
          // 断业务逻辑，服务器是否接受请求
          edtCodeInput.Text := '信息已接收，正在等待服务器处理...';
          btnReturnPay.SetFocus;
        end
        else
        begin
          s := copy(FResultObj.S['err_message'], 0, pos('-', FResultObj.S['err_message']) - 1);
          if (s = 'NOTENOUGH') then //微信：NOTENOUGH-余额不足
          begin
            edtCodeInput.Text := '(X)余额不足，请重新扫描';
            ShowError('余额不足，请重新扫描', False);
            edtCodeInput.Enabled := True;
            edtCodeInput.SetFocus;
          end
          else if (s = 'USERPAYING') then //微信：USERPAYING-需要用户输入支付密码
          begin
            edtCodeInput.Text := '等待用户输入支付密码...';
          end
          else
          begin
            //ShowErrorAndAbortApp('扫描支付失败'#13#10'[' + FResultObj.S['err_code'] + ']' + FResultObj.S['err_message'], 'E510', 'failed to scan pay code');
            ShowError('扫描支付失败'#13#10'[' + FResultObj.S['err_code'] + ']' + FResultObj.S['err_message'], False);
            btnReScan.Click;
          end;
        end;
      end
      else //to do...
      begin
        edtCodeInput.Text := '发送信息失败，请重新扫描';
        edtCodeInput.Enabled := True;
      end;
    end;
  end;
end;

procedure TfrmPay.edtCodeInputExit(Sender: TObject);
var
  edt: TEdit absolute Sender;
begin
  if edt.Text = '' then
    edt.Text := '(X)点击扫描顾客支付二维码(条码)';
end;

procedure TfrmPay.edtPayAmtChange(Sender: TObject);
var
  edt: TEdit absolute Sender;
  NewPayAmt: Double;
begin
  if (edt.Text <> '') and (FPayAmt > 0) then
  begin
    NewPayAmt := StrToFloat(edt.Text);
    if (NewPayAmt > FPayAmt) then
    begin
      edt.Text := FloatToStr(FPayAmt);
      edt.SelStart := Length(edt.Text);
    end;
  end;
  //新版需要重新请求二维码地址
  doGetQrCode(); //ShowQrCode();
end;

procedure TfrmPay.edtPayAmtKeyPress(Sender: TObject; var Key: Char);
var
  edt: TEdit absolute Sender;
begin
  AssistKey(Key);
  if not (Key in ['0'..'9', '.', #08, '-']) then
  begin
    Key := #0;
    if (pnlBottom.Visible and edtCodeInput.Enabled and ((Key = 'x') or (Key = 'X'))) then
    begin
      edtCodeInput.SetFocus;
    end;
  end
  else
  begin
    if (key = '-') then
    begin
      if ((TransId = 'A2') or (TransId = 'A3')) then
      begin
        if (Pos('-', edt.Text) > 0) or (edt.SelStart <> 0) then
        begin
          key := #0;
        end;
      end
      else
      begin
        key := #0;
      end;
    end
    else
    if (key = '.') then
    begin
      if (Pos('.', edt.Text) > 0) then
      begin
        key := #0;
      end;
    end
    else
    begin
      if (Pos('.', edt.Text) > 0) then
        if (edt.SelLength = 0) and (key <> #08) and (Pos('.', edt.Text) <= edt.SelStart) and (Pos('.', edt.Text) <= length(edt.Text) - 2) then
        begin
          key := #0;
        end;
    end;
  end;
end;

procedure TfrmPay.edtRefundAmtChange(Sender: TObject);
var
  edt: TEdit absolute Sender;
  NewPayAmt: Double;
begin
  if (edt.Text <> '') and (edt.Text <> '-') and (FPayAmt <> 0) then
  begin
    NewPayAmt := StrToFloat(edt.Text);
    if (TransId = 'A1') then
    begin
      if (NewPayAmt > FPayAmt) then
      begin
        edt.Text := FloatToStr(FPayAmt);
        edt.SelStart := Length(edt.Text);
      end;
    end
    else
    begin
      if (Abs(NewPayAmt) > Abs(FPayAmt)) then
      begin
        if (NewPayAmt > 0) then
          edt.Text := FloatToStr(FPayAmt)
        else
          edt.Text := '-' + FloatToStr(Abs(FPayAmt));
        edt.SelStart := Length(edt.Text);
      end;

    end;
  end;
end;

procedure TfrmPay.edtSaleTagChange(Sender: TObject);
var
  edt: TComboBox absolute Sender;
  obj: ISuperObject;
  ServerSaleTagDetail: TSuperArray;
begin
  if edt.Text = '' then
  begin
    edtSaleTagLimit.Text := FloatToStr(ClientSaleTagLimit);
    doGetQrCode();
    exit;
  end;
  if edt.Items.IndexOf(edt.Text) = -1 then
  begin
    edtSaleTagLimit.Text := FloatToStr(ClientSaleTagLimit);
    doGetQrCode();
    exit;
  end;

  obj := SO(ServerSaleTag);
  edtSaleTagLimit.Text := FloatToStr(ClientSaleTagLimit);
  if obj <> nil then
  begin
    ServerSaleTagDetail := obj.A['detail'];
    if ServerSaleTagDetail <> nil then
      if ServerSaleTagDetail.Length > 0 then
      begin
        edtSaleTagLimit.Text := ServerSaleTagDetail[edt.ItemIndex].S['discountlimit'];
      end;
  end;
  doGetQrCode();
end;

procedure TfrmPay.edtSaleTagLimitChange(Sender: TObject);
var
  edt: TEdit absolute Sender;
begin
  doGetQrCode();
end;

procedure TfrmPay.edtSaleTagLimitKeyPress(Sender: TObject; var Key: Char);
var
  edt: TEdit absolute Sender;
begin
  if not (Key in ['0'..'9', '.', #08]) then
  begin
    Key := #0;
  end
  else
  begin
    if (key = '.') then
    begin
      if (Pos('.', edt.Text) > 0) then
      begin
        key := #0;
      end;
    end
    else
    begin
      if (Pos('.', edt.Text) > 0) then
        if (edt.SelLength = 0) and (key <> #08) and (Pos('.', edt.Text) <= edt.SelStart) and (Pos('.', edt.Text) <= length(edt.Text) - 2) then
        begin
          key := #0;
        end;
    end;
  end;
end;

procedure TfrmPay.edt_sale_tag_disableClick(Sender: TObject);
begin
  if edt_sale_tag_disable.Checked then
  begin
    edtSaleTag.Enabled := False;
    edtSaleTag.ItemIndex := -1;
  end
  else
  begin
    edtSaleTag.Enabled := True;
    if edtSaleTag.Items.Count > 0 then
      edtSaleTag.ItemIndex := 0;
  end;
end;

procedure TfrmPay.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    ShowMsg2MDS('');
    HttpClient.Disconnect;
    //HttpClient.DisconnectNow();
    HttpClient.Free;
  except
  end;
end;

procedure TfrmPay.AssistKey(var Key: Char);
begin
  if (key = 'x') OR (key = 'X') then     //x 88, 120
  begin
    key := #0;
    if edtCodeInput.Enabled then
    begin
      edtCodeInput.SetFocus;
    end;
  end
  else
  if (key = 'y') OR (key = 'Y') then      //y  89, 121
  begin
    key := #0;
    if edtSaleTag.Enabled then
    begin
      edtSaleTag.SetFocus;
      edtSaleTag.DroppedDown := True;
    end;
  end
  else
  if (key = 'z') OR (key = 'Z') then      //z   90, 122
  begin
    key := #0;
    if edtPayAmt.Enabled then
    begin
      edtPayAmt.SetFocus;
    end;
  end
  else
  if (key = 's') OR (key = 'S') then     //s  83, 115
  begin
    key := #0;
    if edtRefundNo.Enabled then
    begin
      edtRefundNo.SetFocus;
    end;
  end
  else
  if (key = 't') OR (key = 'T') then     //t  84, 116
  begin
    key := #0;
    if edtRefundAmt.Enabled then
    begin
      edtRefundAmt.SetFocus;
    end;
  end
  else
  if (key = 'o') OR (key = 'O') then     //o  79, 111
  begin
    key := #0;
    Button2Click(nil);
  end
  else
  if (key = 'q') OR (key = 'Q') then      //q  81, 113
  begin
    key := #0;
    ModalResult := mrCancel;
  end;
end;

procedure TfrmPay.btnSoftKey_payAmtClick(Sender: TObject);
begin
  showSoftKey(edtPayAmt);
end;

procedure TfrmPay.btnSoftKey_refundAmtClick(Sender: TObject);
begin
  showSoftKey(edtRefundAmt);
end;

procedure TfrmPay.btnSoftKey_refundNoClick(Sender: TObject);
begin
  showSoftKey(edtRefundNo);
end;

procedure TfrmPay.showSoftKey(Sender: TEdit);
var
  frmSoftKey: TfrmSoftKey;
begin
  frmSoftKey := TfrmSoftKey.Create(nil);
  try
    frmSoftKey.edtNo.Text := Sender.Text;
    if frmSoftKey.ShowModal = mrOk then
    begin
      Sender.Text := frmSoftKey.edtNo.Text;
    end;
  finally
    frmSoftKey.Free;
  end;
end;

procedure TfrmPay.edtRefundNoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = 13) and (edtRefundNo.Text <> '') then
  begin
    Button2Click(nil);
  end;
end;

Function TfrmPay.GetMilliSecond(): String;
const
  BgnTime: TDateTime = 25569.0; // 1970/01/01
var
  a: Longint;
  b: String;//毫秒数
begin
  a := MinutesBetween(BgnTime, Now);//取分钟差
  a := a * 60;//取秒差
  Randomize;
  b := IntToStr(a + 60000 + Random(1000)) + FormatDateTime('zzz', Now);//将秒差结果后缀加上三个零（等于毫秒数）
  Result := b;
end;

procedure TfrmPay.HttpClientConnectError(Sender: TRtcConnection; E: Exception);
begin
  ReConnectTimes := ReConnectTimes + 1;
  if ReConnectTimes < MAX_RECONNECT then Exit;
  ShowErrorAndAbortApp('连接服务器出错', 'E202', 'failed to connect to server');
end;

procedure TfrmPay.HttpClientConnect(Sender: TRtcConnection);
begin
  Self.Caption := '';
end;

procedure TfrmPay.HttpClientConnectFail(Sender: TRtcConnection);
begin
  Self.Caption := '服务连接失败！';
end;

procedure TfrmPay.HttpClientConnecting(Sender: TRtcConnection);
begin
  Self.Caption := '服务连接中...';
end;

procedure TfrmPay.HttpClientConnectLost(Sender: TRtcConnection);
begin
  //Self.Caption := '服务连接已丢失';
end;

procedure TfrmPay.HttpClientDisconnecting(Sender: TRtcConnection);
begin
  Self.Caption := '断开服务...';
end;

procedure TfrmPay.HttpClientDisconnect(Sender: TRtcConnection);
begin
  Self.Caption := '服务已断开';
end;

procedure TfrmPay.HttpClientException(Sender: TRtcConnection; E: Exception);
begin
  //ShowError('服务连接出错:[' + IntToStr(e.HelpContext) + ']' + e.Message);
  ShowErrorAndAbortApp('连接服务器失败#13#10[' + IntToStr(e.HelpContext) + ']' + e.Message, 'E202', 'failed to connect to server');
end;

procedure TfrmPay.HttpClientInvalidResponse(Sender: TRtcConnection);
begin
  Self.Caption := '服务通讯被拒绝！';
end;

procedure TfrmPay.HttpClientReconnect(Sender: TRtcConnection);
begin
  Self.Caption := '重新连接服务...';
end;

//procedure TfrmPay.HttpServerRequestNotAccepted(Sender: TRtcConnection);
//var
//  Srv: TRtcDataServer absolute Sender;
//begin
//  Srv.Response.Status(404, Srv.Request.FileName + ' not found.');
//  Srv.Write('Bad Request: ' + Srv.Request.FileName);
//end;

//procedure TfrmPay.HttpServerListenError(Sender: TRtcConnection; E: Exception);
//begin
//  AbortApp('E201', 'invalid listen port:' + LocalPort);
//end;

function TfrmPay.GetUserDefineNoAskQuit: Boolean;
begin
  Result := GetConfigValue('QUIT_WITH_NO_ASK', True);
end;

function TfrmPay.GetLocalPort: String;
begin
  Result := GetConfigValue('LOCAL_PORT', '7090');
end;

function TfrmPay.GetServerAddr: String;
begin
  Result := GetConfigValue('SERVER_ADDR', '');
end;

function TfrmPay.GetServerPort: String;
begin
  Result := GetConfigValue('SERVER_PORT', '');
end;

procedure TfrmPay.SetLocalPort(const Value: String);
begin
  SetConfigValue('LOCAL_PORT', Value);
end;

procedure TfrmPay.SetServerAddr(const Value: String);
begin
  SetConfigValue('SERVER_ADDR', Value);
end;

procedure TfrmPay.SetServerPort(const Value: String);
begin
  SetConfigValue('SERVER_PORT', Value);
end;

function TfrmPay.GetServerPath: String;
begin
  if (FServerPath = '') then
  begin
    FServerPath := GetConfigValue('SERVER_PATH', '/xlp/srv/');
    if (copy(FServerPath, 1, 1) <> '/') then FServerPath := '/' + FServerPath;
    if (copy(FServerPath, length(FServerPath), 1) <> '/') then FServerPath := FServerPath + '/';
  end;
  Result := FServerPath;
end;

procedure TfrmPay.SetServerPath(const Value: String);
begin
  FServerPath := Value;
  SetConfigValue('SERVER_PATH', Value);
end;

function TfrmPay.GetDataFile: String;
begin
  Result := GetConfigValue('DATA_FILE', 'xlpay.data');
end;

procedure TfrmPay.SetDataFile(const Value: String);
begin
  SetConfigValue('DATA_FILE', Value);
end;

function TfrmPay.GetPosID: String;
begin
  Result := GetConfigValue('POS_ID', '');
end;

procedure TfrmPay.SetIsRegisted(const Value: Boolean);
begin
  SetConfigValue('REGISTED', Value);
end;

procedure TfrmPay.SetPosID(const Value: String);
begin
  SetConfigValue('POS_ID', Value);
end;

function TfrmPay.GetIsRegisted: Boolean;
begin
  Result := GetConfigValue('REGISTED', False);
end;

function TfrmPay.GetTerminalID: String;
begin
  if FTerminalId = '' then
    FTerminalId := GetConfigValue('TERMINAL_ID', '');
  Result := FTerminalId;
end;

function TfrmPay.GetTerminalSN: String;
begin
  if FTerminalSn = '' then
    FTerminalSn := GetConfigValue('TERMINAL_SN', '');
  Result := FTerminalSn;
end;

procedure TfrmPay.SetTerminalID(const Value: String);
begin
  FTerminalId := Value;
  SetConfigValue('TERMINAL_ID', Value);
end;

procedure TfrmPay.SetTerminalSN(const Value: String);
begin
  FTerminalSn := Value;
  SetConfigValue('TERMINAL_SN', Value);
end;

function TfrmPay.GetTerminalType: String;
begin
  if FTerminalType = '' then
    FTerminalType := GetConfigValue('TERMINAL_TYPE', '1');
  Result := FTerminalType;
end;

function TfrmPay.GetComCode: String;
begin
  if FComCode = '' then
    FComCode := GetConfigValue('COM_CODE', '');
  Result := FComCode;
end;

procedure TfrmPay.SetComCode(const Value: String);
begin
  FComCode := Value;
  if (GetConfigValue('COM_CODE', '') <> Value) then
  begin
    SetConfigValue('COM_CODE', Value);
  end;
end;


function TfrmPay.GetDeptCode: String;
begin
  if FDeptCode = '' then
    FDeptCode := GetConfigValue('DEPT_CODE', '');
  Result := FDeptCode;
end;

procedure TfrmPay.SetDeptCode(const Value: String);
begin
  FDeptCode := Value;
  if (GetConfigValue('DEPT_CODE', '') <> Value) then
  begin
    SetConfigValue('DEPT_CODE', Value);
  end;
end;

function TfrmPay.GetPosNo: String;
begin
  if FPosNo = '' then
    FPosNo := GetConfigValue('POS_NO', '');
  Result := FPosNo;
end;

procedure TfrmPay.SetPosNo(const Value: String);
begin
  FPosNo := Value;
  if (GetConfigValue('POS_NO', '') <> Value) then
  begin
    SetConfigValue('POS_NO', Value);
  end;
end;

function TfrmPay.GetPartnerId: String;
begin
  if FPartnerId = '' then
    FPartnerId := GetConfigValue('PARTNER_ID', '');
  Result := UpperCase(FPartnerId);
end;

procedure TfrmPay.SetPartnerId(const Value: String);
begin
  FPartnerId := Value;
  if (GetConfigValue('PARTNER_ID', '') <> Value) then
  begin
    SetConfigValue('PARTNER_ID', FPartnerId);
  end;
end;

function TfrmPay.GetPayAmt: String;
begin
  Result := edtPayAmt.Text;
end;

procedure TfrmPay.SetPayAmt(const Value: String);
begin
  try
    FPayAmt := StrToFloat(Value);
    FPayAmt := RoundFloat(FPayAmt, 2);
    edtPayAmt.Text := FloatToStr(FPayAmt);
    edtRefundAmt.Text := FloatToStr(FPayAmt);
  except
    FPayAmt := 0;
    edtPayAmt.Text := '';
    edtRefundAmt.Text := '';
  end;
end;

//function TfrmPay.GetPayType: String;
//begin
//  if (FPayType = '') then FPayType := 'weixin';
//  Result := FPayType;
//end;

function TfrmPay.Get_URL_QUERY: String;
begin
  F_URL_QUERY := GetConfigValue('URL_QUERY', 'query/{pay_code}/{partner_id}/{ext_params}');
  Result := ServerPath + F_URL_QUERY;
end;

function TfrmPay.Get_URL_GET_MAX_BILL_ID: String;
begin
  F_URL_GET_MAX_BILL_ID := GetConfigValue('URL_GET_MAX_BILL_ID', 'qry/getmaxbillid');
  Result := ServerPath + F_URL_GET_MAX_BILL_ID;
end;

function TfrmPay.Get_URL_GET_PAY_URL: String;
begin
  F_URL_GET_PAY_URL := GetConfigValue('URL_GET_PAY_URL', 'get_pay_url/{pay_code}/{partner_id}/{ext_params}');
  Result := ServerPath + F_URL_GET_PAY_URL;
end;

function TfrmPay.Get_URL_GET_TRADE_REC: String;
begin
  F_URL_GET_TRADE_REC := GetConfigValue('URL_GET_TRADE_REC', 'qry/gettraderec');
  Result := ServerPath + F_URL_GET_TRADE_REC;
end;

function TfrmPay.Get_URL_LOG_OFF: String;
begin
  F_URL_LOG_OFF := GetConfigValue('URL_LOG_OFF', 'sys/logoff');
  Result := ServerPath + F_URL_LOG_OFF;
end;

function TfrmPay.Get_URL_LOG_ON: String;
begin
  F_URL_LOG_ON := GetConfigValue('URL_LOG_ON', 'sys/logon');
  Result := ServerPath + F_URL_LOG_ON;
end;

function TfrmPay.Get_URL_QUERY_RESULT: String;
begin
  F_URL_QUERY_RESULT := GetConfigValue('URL_QUERY_RESULT', 'scan_pay_result/{pay_code}/{partner_id}/{ext_params}');
  Result := ServerPath + F_URL_QUERY_RESULT;
end;

function TfrmPay.Get_URL_REFUND: String;
begin
  F_URL_REFUND := GetConfigValue('URL_REFUND', 'refund/{pay_code}/{partner_id}/{ext_params}');
  Result := ServerPath + F_URL_REFUND;
end;

function TfrmPay.Get_URL_REG: String;
begin
  F_URL_REG := GetConfigValue('URL_REG', 'sys/reg');
  Result := ServerPath + F_URL_REG;
end;

function TfrmPay.Get_URL_SCAN_PAY: String;
begin
  F_URL_SCAN_PAY := GetConfigValue('URL_SCAN_PAY', 'scan_pay/{pay_code}/{partner_id}/{ext_params}');
  Result := ServerPath + F_URL_SCAN_PAY;
end;

function TfrmPay.RoundFloat(f: double; i: integer): double;
var
  s: string;
  ef: extended;
begin
  s := '#.' + StringOfChar('0', i);
  ef := StrToFloat(FloatToStr(f));//防止浮点运算的误差
  result := StrToFloat(FormatFloat(s, ef));
end;


////////////////////////////////////////////////////////////////////////////////
// 显示二维码
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.ShowQrCode();
var
  FSymbol: PZSymbol;
  bmp: TBitmap;
  code, amt, bmpFile, sale_tag: String;
  createQrcodeFile: Boolean;
begin
  if not IsShowQrCode then Exit;
  imgQrCode.Picture.Bitmap := nil;
  ShowMsg2MDS('');
  if (PayUrl = '') or (PayAmt = '') then Exit;
  amt := StringReplace(PayAmt, '.', '%2E', [rfReplaceAll, rfIgnoreCase]);

  code := StringReplace(PayUrl, URL_Encode('{pay_amt}'), amt, [rfReplaceAll, rfIgnoreCase]);
  code := StringReplace(code, '{pay_amt}', amt, [rfReplaceAll, rfIgnoreCase]);
  sale_tag := SaleTag;
  code := StringReplace(code, URL_Encode('{sale_tag}'), sale_tag, [rfReplaceAll, rfIgnoreCase]);
  code := StringReplace(code, '{sale_tag}', sale_tag, [rfReplaceAll, rfIgnoreCase]);
  code := StringReplace(code, URL_Encode('{sale_tag_limit}'), FloatToStr(SaleTagLimit), [rfReplaceAll, rfIgnoreCase]);
  code := StringReplace(code, '{sale_tag_limit}', FloatToStr(SaleTagLimit), [rfReplaceAll, rfIgnoreCase]);

  Edit1.Text := code;
  FSymbol := ZBarcode_Create;
  bmp := TBitmap.Create;
  try
    if not pnlMain.Visible then
    begin
      pnlMain.Visible := True;
      calcPnlSaleHeight();
    end;
    FSymbol.symbology := BARCODE_QRCODE;
    FSymbol.scale := 4;
    FSymbol.option_1 := 5;
    FSymbol.option_2 := 1;

    ZBarcode_Encode_and_Buffer(FSymbol, PChar(code), Length(code), 0);
    ZBarcodeToBitmap(FSymbol, imgQrCode.Picture.Bitmap);
    bmp.Height := 400;//imgQrCode.Picture.Graphic.Height * 2 + 50;
    if sale_tag <> '' then  bmp.Height := bmp.Height + 10;
    bmp.Width := 400;//imgQrCode.Picture.Graphic.Width * 2 + 40;
    //bmp.Canvas.Draw(20, 30, imgQrCode.Picture.Graphic);
    bmp.Canvas.StretchDraw(Rect(30, 35, 370, 375), imgQrCode.Picture.Graphic);
    bmp.Canvas.Font.Size := 14;
    bmp.Canvas.Font.Style := [fsBold];
    amt := FormatFloat('0.00', StrToFloat(PayAmt));
    bmp.Canvas.TextOut(120, 5, '收款金额: ' + amt);
    if sale_tag <> '' then
      bmp.Canvas.TextOut(100, 377, '优惠券: ' + FSaleTagName);     //imgQrCode.Picture.Graphic.Height + 33

    bmpFile := GetAppPath + 'qrcode.bmp';
    bmpFile := StringReplace(bmpFile, '\', '/', [rfReplaceAll]);
    while IsFileInUse(bmpFile) do
    begin
      bmpFile := bmpFile + '.bmp';
    end;
    createQrcodeFile := False;
    while not createQrcodeFile do
      try
        bmp.SaveToFile(bmpFile);
        createQrcodeFile := True;
      except
        createQrcodeFile := False;
        while IsFileInUse(bmpFile) do
        begin
          bmpFile := bmpFile + '.bmp';
        end;
      end;
    ShowMsg2MDS(bmpFile);
  finally
    bmp.Free;
    ZBarcode_Delete(FSymbol);
    FSymbol := nil;
  end;
//scale  放大倍数
//option_1 依赖于symbology，不同编码有不同意义    //二维码:容错级别
//option_2 依赖于symbology，不同编码有不同意义    //二维码:版本，决定图片大小
//symbology  编码类型，在手册上有描述，如二维码是58
//output_options 有无边框之类的控制
//show_hrt 在图片上显示可读信息，如果设置为1，则text必须含有内容。
//text 图片上显示的文字信息，配合show_hrt使用。
//input_mode 决定编码格式，二进制流，还是ASCII，亦或是UTF8。
end;

function TfrmPay.IsFileInUse(fName: string): boolean;
var
  HFileRes: HFILE;
begin
  Result := False;
  if not FileExists(fName) then //如果文件不存在 判断文件是否存在
    exit;
  HFileRes := CreateFile(pchar(fName), GENERIC_READ or GENERIC_WRITE,
    0 {this is the trick!}, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  Result := (HFileRes = INVALID_HANDLE_VALUE);
  if not Result then
    CloseHandle(HFileRes);
end;

procedure TfrmPay.ShowMsg2MDS(QrCodeBmpFile: String);
begin
  if QrCodeBmpFile <> '' then
    SendQrCode2MDS(StringReplace(MDS_SHOW_QRCODE_MSG, '{qrcode_file}', QrCodeBmpFile, [rfReplaceAll, rfIgnoreCase]))
  else
    SendQrCode2MDS(MDS_CLEAR_QRCODE_MSG);;
end;

function TfrmPay.SendQrCode2MDS(msg: String): Integer;
var
  hwins: DWORD;
  ARegistry: TRegistry;
begin
  Result := 0;
  hwins := FindWindow(PChar('TfrmMDS'), nil);
  if hwins > 0 then
  begin
    Result := 1;
    ARegistry := TRegistry.Create;
    try
      with ARegistry do
      begin
        RootKey := HKEY_LOCAL_MACHINE;
        if OpenKey('Software\HappyFamily\MDS', true) then
        begin
          WriteString('msg', msg);
        end;
        CloseKey;//关闭主键
      end;
    finally
      ARegistry.Destroy;//释放内存
    end;
    PostMessage(hwins, MDS_WM_SHOWMSG, 0, 0);
  end;
end;

function TfrmPay.GetUrlGetPayUrl: String;
begin
  Result := dealUrl(URL_GET_PAY_URL);
end;

function TfrmPay.GetUrlQuery: String;
begin
  Result := dealUrl(URL_QUERY);
end;

function TfrmPay.GetUrlQueryResult: String;
begin
  Result := dealUrl(URL_QUERY_RESULT);
end;

function TfrmPay.GetUrlRefund: String;
begin
  Result := dealUrl(URL_REFUND);
end;

function TfrmPay.GetUrlScanPay: String;
begin
  Result := dealUrl(URL_SCAN_PAY);
end;

function TfrmPay.GetUrlLogoff: String;
begin
  Result := dealUrl(URL_LOG_OFF);
end;

function TfrmPay.GetUrlLogon: String;
begin
  Result := dealUrl(URL_LOG_ON);
end;

function TfrmPay.GetUrlReg: String;
begin
  Result := dealUrl(URL_REG);
end;

function TfrmPay.GetUrlGetMaxBillId: String;
begin
  Result := dealUrl(URL_GET_MAX_BILL_ID);
end;

function TfrmPay.GetUrlGetTradeRec: String;
begin
  Result := dealUrl(URL_GET_TRADE_REC);
end;

function TfrmPay.GetAmtModifiedUserConf: String;
begin
  if (FAmtModifiedUserConf = '') then
    FAmtModifiedUserConf := GetUserConfigValue('AMT_MODIFIED', 'BY_PARAM');
  Result := FAmtModifiedUserConf;
end;

function TfrmPay.dealUrl(const url: String): String;
begin
  Result := StringReplace(url, '{pay_code}', PayCode, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{partner_id}', PartnerId, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '{ext_params}', GetExtParams, [rfReplaceAll, rfIgnoreCase]);
end;

function TfrmPay.GetReportFileWx: String;
begin
  Result := GetConfigValue('REPORT', 'WX', 'xlpay_wx.fr3');
end;

procedure TfrmPay.SetServerSaleTag(const Value: String);
var
  obj: ISuperObject;
begin
  obj := SO(Value);
  ServerSaleTagConfig := obj.S['config'];

  if ServerSaleTagConfig = '0' then //禁用Sale_Tag销售标识
  begin
    pnlSaleTag.Visible := False;
  end
  else if ServerSaleTagConfig = '1' then //使用客户端设置，如果客户端没有传入值，则使用服务端配置
  begin
    pnlSaleTag.Visible := True;
    if obj.B['client_revisable'] then
    begin
      edtSaleTag.Style := csDropDown;
      edtSaleTagLimit.Enabled := True;
    end;
    if ClientSaleTag <> '' then
    begin
      edtSaleTag.Clear;
      edtSaleTag.Items.Add(ClientSaleTag);
      edtSaleTag.ItemIndex := 0;
      edtSaleTagLimit.Text := FloatToStr(ClientSaleTagLimit);
    end
    else
    begin
      SetServerSaleTagPanel(Value);
    end;
  end
  else if ServerSaleTagConfig = '2' then //仅使用服务端配置
  begin
    if (obj.A['detail'].Length = 0) then
    begin
      ServerSaleTagConfig := '0';
      pnlSaleTag.Visible := False;
    end
    else
    begin
      pnlSaleTag.Visible := True;
      if obj.B['client_revisable'] then
      begin
        edtSaleTag.Style := csDropDown;
        edtSaleTagLimit.Enabled := True;
      end;
      SetServerSaleTagPanel(Value);
    end;
  end
  else
  begin
    ServerSaleTagConfig := '0'; // 默认禁用Sale_Tag销售标识
    pnlSaleTag.Visible := False;
  end;
  calcPnlSaleHeight();
  FServerSaleTag := Value;
end;

procedure TfrmPay.SetServerSaleTagPanel(const Value: String);
var
  obj: ISuperObject;
  ServerSaleTagDetail: TSuperArray;
  i: Integer;
begin
  obj := SO(Value);
  if obj = nil then Exit;
  ServerSaleTagDetail := obj.A['detail'];
  if ServerSaleTagDetail = nil then Exit;
  if ServerSaleTagDetail.Length = 0 then Exit;
  //ShowMessage(obj.AsJSon(true, false));
  edtSaleTag.Clear;
  for i := 0 to ServerSaleTagDetail.Length - 1 do
  begin
    if ServerSaleTagDetail[i].S['code_value'] <> '' then
      edtSaleTag.Items.Add(ServerSaleTagDetail[i].S['code'] + ' (' + ServerSaleTagDetail[i].S['code_value'] + ')')
    else
      edtSaleTag.Items.Add(ServerSaleTagDetail[i].S['code']);
  end;
  edtSaleTagLimit.Text := ServerSaleTagDetail[0].S['discountlimit'];
  edtSaleTag.ItemIndex := 0;
end;

function TfrmPay.GetSaleTag: String;
var
  obj: ISuperObject;
  ServerSaleTagDetail: TSuperArray;
  ItemIndex: Integer;
begin
  Result := '';
  FSaleTagName := '';
  if edt_sale_tag_disable.Checked then    //客户端禁用
  begin
    Exit;
  end;
  if edtSaleTag.Text = '' then  // 未输入值
  begin
    Exit;
  end;
  if ServerSaleTagConfig = '0' then      //服务端禁用
  begin
    Exit;
  end
  else
  if ServerSaleTagConfig = '1' then //使用客户端设置，如果客户端没有传入值，则使用服务端配置
  begin
    if ClientSaleTag <> '' then
    begin
      FSaleTagName := ClientSaleTag;
      Result := ClientSaleTag;
      Exit;
    end;
  end;
  ItemIndex := edtSaleTag.Items.IndexOf(edtSaleTag.Text);
  if ItemIndex = -1 then
  begin
    FSaleTagName := edtSaleTag.Text;
    Result := edtSaleTag.Text;
  end
  else
  begin
    obj := SO(ServerSaleTag);
    if obj <> nil then
    begin
      ServerSaleTagDetail := obj.A['detail'];
      if ServerSaleTagDetail <> nil then
        if ServerSaleTagDetail.Length > 0 then
        begin
          FSaleTagName := ServerSaleTagDetail[ItemIndex].S['code'];
          Result := ServerSaleTagDetail[ItemIndex].S['code_value'];
        end;
    end;
  end;
end;

function TfrmPay.GetSaleTagLimit: Double;
begin
  if edtSaleTagLimit.Text = '' then
  begin
    Result := 0;
  end
  else
  begin
    FSaleTagLimit := StrToFloat(edtSaleTagLimit.Text);
    FSaleTagLimit := RoundFloat(FSaleTagLimit, 2);
    Result := FSaleTagLimit;
  end;
end;

function TfrmPay.GetExtParams: String;
begin
  Result := 'com_code=' + ComCode +
    ',operator_id=' + OperatorId +
    ',operator_name=' + OperatorName;
end;

procedure TfrmPay.doReg();
begin
  try
    ReConnectTimes := MAX_RECONNECT;
    drReg.Request.Method := 'POST';
    drReg.Request.FileName := URL_Encode(UrlReg);
    drReg.Request.Host := HttpClient.ServerAddr;
    drReg.Post();
    lblQueryWaitMsg.Caption := lblQueryWaitMsg.Caption + '.';
  except
    on E: Exception do
    begin
      ShowError('注册终端出错:' + E.Message);
    end;
  end;
end;

procedure TfrmPay.doLogon();
begin
  try
    ReConnectTimes := MAX_RECONNECT;
    drLogon.Request.Method := 'POST';
    drLogon.Request.FileName := URL_Encode(UrlLogon);
    drLogon.Request.Host := HttpClient.ServerAddr;
    drLogon.Post();
    lblQueryWaitMsg.Caption := lblQueryWaitMsg.Caption + '.';
  except
    on E: Exception do
    begin
      ShowError('登录终端出错:' + E.Message);
    end;
  end;
end;

procedure TfrmPay.doLogoff();
begin
  try
    ReConnectTimes := MAX_RECONNECT;
    drLogoff.Request.Method := 'POST';
    drLogoff.Request.FileName := URL_Encode(UrlLogoff);
    drLogoff.Request.Host := HttpClient.ServerAddr;
    drLogoff.Post();
    lblQueryWaitMsg.Caption := lblQueryWaitMsg.Caption + '.';
  except
    on E: Exception do
    begin
      ShowError('登录终端出错:' + E.Message);
    end;
  end;
end;

procedure TfrmPay.doCommonQuery(url: String; waitMsg: String);
begin
  try
    pnlQueryWait.Visible := True;
    lblQueryWaitMsg.Caption := waitMsg + '...';
    Application.ProcessMessages;
    //showmessage(URL_Encode(url));
    ReConnectTimes := MAX_RECONNECT;
    drCommon.Request.Method := 'POST';
    drCommon.Request.FileName := URL_Encode(url);
    drCommon.Request.Host := HttpClient.ServerAddr;
    drCommon.Post();
  except
    on E: Exception do
    begin
      ShowError('提交数据出错:' + E.Message);
    end;
  end;
end;

procedure TfrmPay.drRegDataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  ReturnInfo: ISuperObject;
  s: String;
  reg: TRegister;
begin
  if not Sender.inMainThread then
    Sender.Sync(drRegDataReceived)
  else
  begin
    if Cli.Response.Done then
    begin
      Screen.Cursor := crArrow;
      if (Cli.Response.StatusCode = 200) then
      begin
        s := Utf8Decode(Cli.Read);
        ReturnInfo := SO(s);
        debug('接收数据从支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][drRegDataReceived]: ' + s);
        ResultObj := SO(ReturnInfo.S['result']);
        debug(ResultObj.AsJSon(true, false));
        FResultObj.B['success'] := ResultObj.B['success'];
        FResultObj.S['err_code'] := ResultObj.S['errCode'];
        FResultObj.S['err_message'] := ResultObj.S['errMessage'];
        if ResultObj.O['result'] <> nil then
        begin
          FResultObj.B['registed'] := ResultObj.O['result'].B['isRegisted'];
          FResultObj.S['pos_id'] := ResultObj.O['result'].S['id'];
          PosID := FResultObj.S['pos_id'];
          IsRegisted := FResultObj.B['registed'];
        end;

        if ResultObj.B['success'] then
        begin
          reg := TRegister.Create;
          try
            reg.TerminalID := TerminalID;
            reg.TerminalSN := TerminalSN;
            reg.ComCode := ComCode;
            reg.PosNo := PosNo;
            reg.PartnerId := PartnerId;
            reg.WriteToFile(GetAppPath + 'reg.dat');
          finally
            reg.Free;
          end;
          ModalResult := mrOk;
        end
        else
        begin
          if ResultObj.B['srvError'] then
          begin
            ShowErrorAndAbortApp('[' + ResultObj.S['errCode'] + ']' + ResultObj.S['errMessage']);
          end
          else
          begin
            ShowErrorAndAbortApp('[' + FResultObj.S['errCode'] + ']' + FResultObj.S['errMessage'], FResultObj.S['errCode'], FResultObj.S['errMessage']);
          end;
        end;
      end
      else
      begin
        AbortApp('E303', 'failed to get reg result, server response status code:' + IntToStr(Cli.Response.StatusCode));
      end;
    end;
  end;
end;

procedure TfrmPay.calcPnlSaleHeight();
var
  pnlSaleHeight: Integer;
begin
  pnlSaleHeight := 0;
  if (pnlTop.Visible) then pnlSaleHeight := pnlSaleHeight + pnlTop.Height;
  if (pnlSaleTag.Visible) then pnlSaleHeight := pnlSaleHeight + pnlSaleTag.Height;
  if (pnlMain.Visible) then pnlSaleHeight := pnlSaleHeight + pnlMain.Width;
  if (pnlBottom.Visible) then pnlSaleHeight := pnlSaleHeight + pnlBottom.Height;
  pnlSale.Height := pnlSaleHeight;
  Self.AutoSize := False;
  pnlSale.Height := pnlSaleHeight;
  Self.AutoSize := True;
  Self.Top := (Screen.Height - Self.Height) div 2;
end;

///////////////////////////////////////////////////////////////////////
//        通用显示错误函数                                           //
///////////////////////////////////////////////////////////////////////
procedure TfrmPay.ShowError(errMessage: String; writeLog: Boolean = True);
begin
  if (writeLog) then
  begin
    error(errMessage);
  end;
  SetDlgAutoClose();
  MessageBox(Self.Handle, PChar(errMessage), PChar('错误'), MB_TOPMOST + MB_ICONINFORMATION + MB_SYSTEMMODAL);
end;

procedure TfrmPay.AbortApp(returnCode: String = ''; returnMessage: String = ''; success: boolean = False);
begin
  FResultObj.B['success'] := success;
  FResultObj.S['err_code'] := returnCode;
  FResultObj.S['err_message'] := returnMessage;
  if ((not success) and (returnCode <> 'E300')) then
  begin
    error('[' + returnCode + '] ' + returnMessage);
  end;
  ModalResult := mrAbort;
end;

//重新扫描（主扫）
procedure TfrmPay.btnReScanClick(Sender: TObject);
begin
  imgQrCode.Visible := pnlMain.Visible;
  edtCodeInput.Enabled := True;
  edtCodeInput.SetFocus;
end;

procedure TfrmPay.btnShowQrCodeClick(Sender: TObject);
begin
  IsShowQrCode := True;
  imgQrCode.Visible := True;
  pnlBottomShowQrCode.Visible := False;
  doGetQrCode();
  if (pnlBottom.Visible and edtCodeInput.Enabled) then
  begin
    edtCodeInput.SetFocus;
  end;
end;

procedure TfrmPay.ShowErrorAndAbortApp(errMessage: String; returnCode: String = ''; returnMessage: String = ''; success: boolean = False);
begin
  ShowError(errMessage + #13#10'[' + returnCode + ']' + returnMessage, False);
  AbortApp(returnCode, returnMessage, success);
end;

///////////////////////////////////////////////////////////////////////
//        通用数据发送处理函数（不支持并发）                         //
///////////////////////////////////////////////////////////////////////
procedure TfrmPay.drBeginRequest(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(drBeginRequest)
  else
  begin
    SendData(Sender);
  end;
end;
///////////////////////////////////////////////////////////////////////
//        通用数据接收处理函数（不支持并发）                         //
///////////////////////////////////////////////////////////////////////
procedure TfrmPay.drDataReceived(Sender: TRtcConnection);
var
  Cli: TRtcDataClient absolute Sender;
  ReturnInfo: ISuperObject;
  s: String;
begin
  if not Sender.inMainThread then
    Sender.Sync(drDataReceived)
  else
  begin
    if Cli.Response.Done then
    begin
      Screen.Cursor := crArrow;
      if (Cli.Response.StatusCode = 200) then
      begin
        s := Utf8Decode(Cli.Read);
        ReturnInfo := SO(s);
        debug('接收数据从支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][drDataReceived]: ' + s);
        ResultObj := SO(ReturnInfo.S['result']);
        info('接收支付节点服务器数据[' + Sender.PeerAddr + ':' + Sender.PeerPort + ']: ' + ResultObj.AsJSon(true, false));
        debug(ResultObj.AsJSon(true, false));
        FResultObj.B['success'] := ResultObj.B['success'];
        FResultObj.S['err_code'] := ResultObj.S['errCode'];
        FResultObj.S['err_message'] := ResultObj.S['errMessage'];
        if ResultObj.O['result'] <> nil then
        begin
        end;

        if ResultObj.B['success'] then
        begin
          ModalResult := mrOk;
        end
        else
        begin
          if ResultObj.B['srvError'] then
          begin
            ShowErrorAndAbortApp('[' + ResultObj.S['errCode'] + ']' + ResultObj.S['errMessage']);
          end
          else
          begin
            ShowErrorAndAbortApp('[' + FResultObj.S['errCode'] + ']' + FResultObj.S['errMessage'], FResultObj.S['errCode'], FResultObj.S['errMessage']);
          end;
        end;
      end
      else
      begin
        AbortApp('E303', 'failed to get server result, server response status code:' + IntToStr(Cli.Response.StatusCode));
      end;
    end;
  end;
end;

procedure TfrmPay.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (pnlBottom.Visible and edtCodeInput.Enabled and ((Key = 'x') or (Key = 'X'))) then
  begin
    edtCodeInput.SetFocus;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//               设置请求参数                                                 //
////////////////////////////////////////////////////////////////////////////////
procedure TfrmPay.SetRequestParam(const Value: String);
begin
  FRequestStr := Value;
  FRequestValid := False;
  try
    info('接收到请求:' + Value);
    RequestObj := SO(Value);
    Self.AutoSize := True;
    // showmessage(RequestObj.AsJSon(true, false));
    TransId := UpperCase(RequestObj.S['trans_id']);
    if (TransId = '') then
    begin
      AbortApp('E103', 'invalid param trans_id');
      Exit;
    end;
    if (TransId = 'RD') then  //report design 报表设计
    begin
      if FileExists(ReportFileWx) then
      begin
        frxReport.LoadFromFile(ReportFileWx);
      end;
      info('进入报表设计器');
      frxReport.DesignReport();
      Exit;
    end;
    if (RequestObj.S['partner_id'] <> '') then
      PartnerId := RequestObj.S['partner_id'];//UpperCase();
    if (PartnerId = '') then
    begin
      AbortApp('E101', 'invalid param partner_id');
      Exit;
    end;
    if ((TransId <> 'S0')
      and ((UpperCase(PartnerId) <> 'JL')
      and (UpperCase(PartnerId) <> 'BN')
      and (UpperCase(PartnerId) <> 'LS')
      and (UpperCase(PartnerId) <> 'LSCP'))) then //非注册
    begin
      with TRegister.Create do
      begin
        try
          if not FileExists(GetAppPath + 'reg.dat') then
          begin
            AbortApp('E197', 'invalid register file');
            Exit;
          end;
          try
            ReadFromFile(GetAppPath + 'reg.dat');
          except
            AbortApp('E198', 'bad register file');
            Exit;
          end;
          if not passed(Self.TerminalID, Self.TerminalSN) then
          begin
            AbortApp('E199', 'invalid register info, ' + TerminalSN + ',' + RandomKey);
            Exit;
          end;
        finally
          free;
        end;
      end;
    end;

    if (RequestObj.S['com_code'] <> '') then
      ComCode := RequestObj.S['com_code'];
    if (RequestObj.S['dept_code'] <> '') then
      DeptCode := RequestObj.S['dept_code'];
    if (RequestObj.S['pos_no'] <> '') then
      PosNo := RequestObj.S['pos_no'];
    TerminalCode := RequestObj.S['terminal_code'];
    if (RequestObj.S['terminal_type'] <> '') then
      TerminalType := RequestObj.S['terminal_type'];
    if (RequestObj.S['terminal_id'] <> '') then
      TerminalID := RequestObj.S['terminal_id'];
    if (RequestObj.S['terminal_sn'] <> '') then
      TerminalSN := RequestObj.S['terminal_sn'];

//    PayType := RequestObj.S['pay_type'];

    PartnerData := RequestObj.S['partner_data'];
    FResultObj.S['partner_data'] := PartnerData; //数据返回，暂不处理
    Remark := RequestObj.S['remark'];
    OperatorId := RequestObj.S['operator_id'];
    OperatorPwd := RequestObj.S['operator_pwd'];
    OperatorName := RequestObj.S['operator_name'];
    LogonType := RequestObj.S['logon_type'];
    BgnTime := RequestObj.S['begin_time'];
    EndTime := RequestObj.S['end_time'];
    BgnDate := RequestObj.S['begin_date'];
    EndDate := RequestObj.S['end_date'];

    ReportMemo := RequestObj.S['report_memo'];
    ReportMemo2 := RequestObj.S['report_memo2'];
    ReportMemo3 := RequestObj.S['report_memo3'];
    ReportMemo4 := RequestObj.S['report_memo4'];
    ReportMemo5 := RequestObj.S['report_memo5'];
//    if (ComCode = '') then
//    begin
//      FResultObj.B['success'] := false;
//      FResultObj.S['err_code'] := 'E104';
//      FResultObj.S['err_message'] := 'invalid param com_code';
//      ModalResult := mrAbort;
//      Exit;
//    end;

    PayCode := RequestObj.s['pay_code'];
    if (PayCode = '') then
      PayCode := RequestObj.s['partner_pay_code'];

    //if ((copy(TransId, 1, 1) = 'A') OR (copy(TransId, 1, 1) = 'B')) then //交易业务
    if (TransId = 'A1') OR (TransId = 'A2') OR (TransId = 'B1') OR (TransId = 'B2') then
    begin
      if (PayCode = '') then
      begin
        AbortApp('E102', 'invalid param pay_code');
        Exit;
      end;
      if (PosNo = '') then
      begin
        AbortApp('E105', 'invalid param pos_no');
        Exit;
      end;
      BillId := RequestObj.S['bill_id'];
      if (BillId = '') then
      begin
        AbortApp('E106', 'invalid param bill_id');
        Exit;
      end;
      BatchNo := RequestObj.S['batch_no'];
      if (BatchNo = '') then
      begin
        AbortApp('E107', 'invalid param batch_no');
        Exit;
      end;
      try // 验证batch_no是否为数字
//      RandomId := IntToStr(StrToInt(BatchNo) * 100);
        Randomize;
        RandomId := IntToStr(Random(StrToInt(BatchNo) * 10000));
      except
        begin
          AbortApp('E107', 'invalid param batch_no');
          Exit;
        end;
      end;

      PayAmt := RequestObj.S['pay_amt'];
      if (PayAmt = '') then
      begin
        PayAmt := '0';
      end;
      // 是否可以修改交易金额
      if (FPayAmt = 0) then
      begin
        AmtModified := True;
      end
      else
      begin
        if (SameText(AmtModifiedUserConf, 'TRUE')) then
        begin
          AmtModified := True;
        end
        else if (SameText(AmtModifiedUserConf, 'FALSE')) then
        begin
          AmtModified := False;
        end
        else
        begin
          if (requestObj.S['amt_modified'] = '') then
          begin
            if (SameText(AmtModifiedUserConf, 'BY_PARAM') OR SameText(AmtModifiedUserConf, 'DEFAULT_FALSE')) then
            begin
              AmtModified := False;
            end
            else if (SameText(AmtModifiedUserConf, 'BY_PARAM')) then
            begin
              AmtModified := True;
            end
            else
            begin
              AmtModified := True; // default, request by qmg
            end;
          end
          else
            AmtModified := requestObj.B['amt_modified'];
        end;
      end;
//      if (requestObj.S['amt_modified'] = '') then
//      begin
//        AmtModified := True; // False or (FPayAmt = 0)     // default True , qmg...
//      end
//      else
//        AmtModified := requestObj.B['amt_modified'] or (FPayAmt = 0);

      edtRefundAmt.Enabled := AmtModified;
      edtPayAmt.Enabled := AmtModified;
      btnSoftKey_payAmt.Visible := AmtModified;
      btnSoftKey_refundAmt.Visible := AmtModified;

      ScanType := RequestObj.I['scan_type'];
      ScanPayCode := RequestObj.S['scan_code'];
      VipCode := RequestObj.S['vip_code'];
      VipId := RequestObj.S['vip_id'];

      ClientSaleTag := RequestObj.S['sale_tag'];
      ClientSaleTagLimit := RequestObj.D['sale_tag_limit'];

      if (requestObj.S['need_print'] = '') then
        NeedPrint := True
      else
        NeedPrint := requestObj.B['need_print'];
      PrintType := RequestObj.I['print_type'];
      PrinterPort := UpperCase(RequestObj.S['printer_port']);
      if NeedPrint then
      begin
        if PrintType = 0 then  //驱动打印
        begin
          if PrinterPort = '' then PrinterPort := 'default';
        end
        else                   //端口打印
        begin
          if PrinterPort = '' then NeedPrint := False;// PrinterPort := 'LPT1';
        end;
      end;

      TradeTag := RequestObj.S['trade_tag'];
    end
    else if (copy(TransId, 1, 1) = 'S') then //系统业务(注册终端，登录，注销)
    begin
      if (TerminalCode = '') then TerminalCode := PosNo;
    end;

    if (TransId = 'A1') then  //消费
    begin
      info('消费');
      PayAmt := RequestObj.S['pay_amt'];
      pnlSale.Visible := True;
      // 是否显示二维码
      IsShowQrCode := ((ScanType = 0) OR (ScanType = 2)) and (ScanPayCode = '');
      pnlBottomShowQrCode.Visible := not IsShowQrCode;
      pnlMain.Visible := IsShowQrCode;

      if (ScanType = 2) then//2 - 被扫
      begin
        pnlBottom.Visible := False;
      end;
      if (ScanPayCode <> '') then  //扫描条码，当传值时，scan_type设置无效（视为1主扫）
      begin
        pnlMain.Visible := False;
        pnlBottom.Visible := True;
        edtCodeInput.Text := ScanPayCode;
        btnReturnPay.SetFocus;
        PostScanCode();
      end;
      try
        if (PayAmt = '') then
          edtPayAmt.SetFocus
        else if (pnlSaleTag.Visible) then
          edtSaleTag.SetFocus
        else if (pnlBottom.Visible) then
          edtCodeInput.SetFocus;
      except
      end;

      calcPnlSaleHeight();

      if IsShowQrCode then    // 非主扫 从服务端请求二维码参数
      begin
        doGetQrCode;
      end;
      doQuery();
      //QueryResultTime := 0;
      TimerQuery.Enabled := true;
    end
    else if ((TransId = 'A2') or (TransId = 'A3')) then     //撤销 退货
    begin
      info('退货/撤销');
      pnlRefund.Visible := True;
      if (RequestObj.S['cntt_no'] <> '') then
        edtRefundNo.Text := RequestObj.S['cntt_no']
      else if (ScanPayCode <> '') then
        edtRefundNo.Text := ScanPayCode;
      if (requestObj.S['cnttno_modified'] = '') then
        RefundNoModified := False
      else
        RefundNoModified := requestObj.B['cnttno_modified'];
      RefundNoModified := RefundNoModified or (RequestObj.S['cntt_no'] = '');
      edtRefundNo.Enabled := RefundNoModified;
      btnSoftKey_refundNo.Visible := RefundNoModified;
      if (edtRefundNo.Text = '') then
        edtRefundNo.SetFocus
      else if (edtRefundAmt.Text = '') OR (StrToFloat(edtRefundAmt.Text) = 0) then
        edtRefundAmt.SetFocus
      else
        Button2.SetFocus;
      doQuery();
      //QueryResultTime := 0;
      //TimerQuery.Enabled := true;
    end
    else if (TransId = 'A3') then     //退货
    begin
    end
    else if (TransId = 'B1') then     //重打印
    begin
      info('重打印');
      pnlQueryWait.Visible := True;
      NeedPrint := True;
      doQuery();
      //QueryResultTime := 0;
      TimerQuery.Enabled := true;
    end
    else if (TransId = 'B2') then     //查询交易
    begin
      info('查询交易');
      lblQueryWaitMsg.Caption := '';
      pnlQueryWait.Visible := True;
      NeedPrint := False;
//      Delay(50);
//      lblQueryWaitMsg.Caption := '单据查询';
//      doQuery();
//      Delay(100);
      //QueryResultTime := 0;
      TimerQuery.Enabled := true;
      lblQueryWaitMsg.Caption := '查询中,请稍后...';
      doQuery();
    end
    else if (TransId = 'B3') then     //查询最大流水号
    begin
      info('查询最大流水号');
      doCommonQuery(UrlGetMaxBillId, '查询中,请稍后');
    end
    else if (TransId = 'B4') then     //查询交易流水记录
    begin
      info('查询交易流水记录');
      doCommonQuery(UrlGetTradeRec, '查询中,请稍后');
    end
    else if (TransId = 'S0') then     //注册终端
    begin
      info('注册终端');
      lblQueryWaitMsg.Caption := '注册中,请稍后...';
      pnlQueryWait.Visible := True;
      NeedPrint := False;
      Application.ProcessMessages;
      doReg();
    end
    else if (TransId = 'S1') then     //登录终端
    begin
      info('登录终端');
      lblQueryWaitMsg.Caption := '登录中,请稍后...';
      pnlQueryWait.Visible := True;
      NeedPrint := False;
      Application.ProcessMessages;
      doLogon();
    end
    else if (TransId = 'S2') then     //注销终端
    begin
      info('注销终端');
      lblQueryWaitMsg.Caption := '注销中,请稍后...';
      pnlQueryWait.Visible := True;
      NeedPrint := False;
      Application.ProcessMessages;
      doLogoff();
    end
    else
    begin
      AbortApp('E103', 'invalid param trans_id');
      Exit;
    end;
    Self.Left := (Screen.Width - Self.Width) div 2;
    FRequestValid := True;
  except
    on E: Exception do
    begin
      AbortApp('E100', 'invalid request param: ' + e.Message);
    end;
  end;
end;

///////////////////////////////////////////////////////////////////////
//        发送数据（通用函数）                                       //
///////////////////////////////////////////////////////////////////////
procedure TfrmPay.SendData(Sender: TRtcConnection);
var
  ts: string;
  Cli: TRtcDataClient absolute Sender;
  function AnsiToWide(const S: AnsiString): WideString;
  var
    len: integer;
    ws: WideString;
  begin
    Result := '';
    if (Length(S) = 0) then
      exit;
    len := MultiByteToWideChar(CP_ACP, 0, PChar(s), -1, nil, 0);
    SetLength(ws, len);
    MultiByteToWideChar(CP_ACP, 0, PChar(s), -1, PWideChar(ws), len);
    Result := ws;
  end;
  function WideToUTF8(const WS: WideString): UTF8String;
  var
    len: integer;
    us: UTF8String;
  begin
    Result := '';
    if (Length(WS) = 0) then
      exit;
    len := WideCharToMultiByte(CP_UTF8, 0, PWideChar(WS), -1, nil, 0, nil, nil);
    SetLength(us, len);
    WideCharToMultiByte(CP_UTF8, 0, PWideChar(WS), -1, PChar(us), len, nil, nil);
    Result := us;
  end;
  function AnsiToUTF8(const S: AnsiString): UTF8String;
  begin
    Result := WideToUTF8(AnsiToWide(S));
  end;
begin
  if not Sender.inMainThread then
    Sender.Sync(SendData)
  else
  begin
    try
      Cli.Request.ContentType := 'application/x-www-form-urlencoded';
      Cli.Request.Params.Value['ak'] := APP_AK;
      ts := getMilliSecond;
      Cli.Request.Params.Value['ts'] := ts;
      Cli.Request.Params.Value['sn'] := GetAppSn(ts);

      Cli.Request.Params.Value['com_code'] := ComCode;
      Cli.Request.Params.Value['dept_code'] := DeptCode;
      Cli.Request.Params.Value['terminal_code'] := TerminalCode;
      Cli.Request.Params.Value['terminal_type'] := TerminalType;
      Cli.Request.Params.Value['terminal_id'] := TerminalId;
      Cli.Request.Params.Value['terminal_sn'] := terminalSn;
      Cli.Request.Params.Value['partner_id'] := PartnerId;
      Cli.Request.Params.Value['partner_pay_code'] := PayCode;
      Cli.Request.Params.Value['erp_type'] := PartnerId;
      Cli.Request.Params.Value['pos_no'] := PosNo;
      Cli.Request.Params.Value['pos_id'] := PosId;
//    Cli.Request.Params.Value['pay_type'] := URL_Encode(PayType);

      Cli.Request.Params.Value['operator_id'] := OperatorId;
      Cli.Request.Params.Value['operator_pwd'] := OperatorPwd;
      Cli.Request.Params.Value['logon_type'] := LogonType;
      Cli.Request.Params.Value['operator_name'] := OperatorName;

      Cli.Request.Params.Value['begin_time'] := BgnTime;
      Cli.Request.Params.Value['end_time'] := EndTime;
      Cli.Request.Params.Value['begin_date'] := BgnDate;
      Cli.Request.Params.Value['end_date'] := EndDate;

      Cli.Request.Params.Value['bill_id'] := BillId;
      Cli.Request.Params.Value['batch_no'] := BatchNo;
      Cli.Request.Params.Value['random_id'] := RandomId;
      Cli.Request.Params.Value['pay_amt'] := PayAmt;
      Cli.Request.Params.Value['pay_code'] := ScanPayCode;
      Cli.Request.Params.Value['scan_code'] := ScanPayCode;
      Cli.Request.Params.Value['sale_tag'] := SaleTag;
      Cli.Request.Params.Value['sale_tag_limit'] := FloatToStr(SaleTagLimit);
      if NeedPrint then
        Cli.Request.Params.Value['need_print'] := '1'
      else
        Cli.Request.Params.Value['need_print'] := '0';

      Cli.Request.Params.Value['vip_id'] := VipId;
      Cli.Request.Params.Value['vip_code'] := VipCode;

      Cli.Request.Params.Value['partner_data'] := PartnerData;
      Cli.Request.Params.Value['remark'] := Remark;

      if (TransId = 'A1') then  //消费
      begin
      end
      else if (TransId = 'A2') then     //退货
      begin
        Cli.Request.Params.Value['cntt_no'] := edtRefundNo.Text;
        if StrToFloat(edtRefundAmt.Text) > 0 then
          Cli.Request.Params.Value['pay_amt'] := '-' + edtRefundAmt.Text
        else
          Cli.Request.Params.Value['pay_amt'] := edtRefundAmt.Text;
      end
      else if (TransId = 'B1') then     //重打印
      begin
      end
      else if (TransId = 'B2') then     //查询
      begin
      end
      else if (TransId = 'B3') then     //最大流水号查询
      begin
      end
      else if (TransId = 'B4') then     //交易流水记录查询
      begin
      end
      else if (TransId = 'S0') then     //注册终端
      begin
      end
      else if (TransId = 'S1') then     //登录终端
      begin
      end
      else if (TransId = 'S2') then     //注销终端
      begin
      end
      else
      begin
      end;
      info('发送数据到支付节点服务器[' + TransId + '][' + Sender.PeerAddr + ':' + Sender.PeerPort + '][' + Cli.Request.FileName + ']: ' + Cli.Request.Params.Text);
      Cli.Write(Cli.Request.Params.Text);
    except
      on E: Exception do
      begin
        ShowError('数据发送失败:' + E.Message);
      end;
    end;
  end;
end;

procedure TfrmPay.TimerSetFocusTimer(Sender: TObject);
var
  oldPoint: TPOINT;
  h, h2: THandle;
begin
  h := GetForegroundWindow();
  h2 := FindWindow('#32770', '错误');
  if (h2 > 0) then
  begin
    if (h = h2) then exit;
    SwitchToThisWindow(h2, True);
    exit;
  end;
  h2 := FindWindow('#32770', '退出确认');
  if (h2 > 0) then
  begin
    if (h = h2) then exit;
    SwitchToThisWindow(h2, True);
    exit;
  end;
  h2 := FindWindow('TfrmSoftKey', 'frmSoftKey');
  if (h2 > 0) then
  begin
    if (h = h2) then exit;
    SwitchToThisWindow(h2, True);
    exit;
  end;

  if (h <> Self.Handle) then
    if ((TransId = 'A1') OR (TransId = 'A2')) then   //消费 或 退货
      try
//    h := FindWindow(nil, PChar(Self.Caption));
        SwitchToThisWindow(Self.Handle, True);
        h := GetForegroundWindow();
        if (h <> Self.Handle) then
        begin
          GetCursorPos(oldPoint); //保存当前鼠标位置
          SetCursorPos(Self.Left + 30, Self.Top + 30);
          mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
          SetCursorPos(oldPoint.X, oldPoint.Y);
        end;
      except
      end;
end;


//finalization

end.
