unit uFrmSoftKey;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmSoftKey = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btn7: TButton;
    btn8: TButton;
    btn9: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btnDot: TButton;
    btn0: TButton;
    btnOK: TButton;
    edtNo: TEdit;
    btnBack: TButton;
    procedure btnBackClick(Sender: TObject);
    procedure btnNumClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSoftKey: TfrmSoftKey;

implementation

{$R *.dfm}

procedure TfrmSoftKey.btnBackClick(Sender: TObject);
begin
  edtNo.Text := copy(edtNo.Text, 1, length(edtNo.Text) - 1);
end;

procedure TfrmSoftKey.btnNumClick(Sender: TObject);
var
  edtNum: TButton absolute Sender;
begin
  edtNo.Text := edtNo.Text + edtNum.Caption;
end;

end.
