unit uCommon;

interface

uses
  SysUtils, Windows, Classes;

CONST
  APP_NAME = 'XLPay_DLL';
  CONFIG_FILE = 'xlpay.cfg';
  DEFAULT_CONFIG_SECTION = 'CONFIGURE';
  USER_CONFIG_SECTION = 'USERCONF';

VAR
  system_config: TStringList;

function GetAppPath: string;
function GetLogFolder: string;

function GetConfigValue(const SectionName: String; const CfgName: String; const DefaultValue: String; ForceReadFormConfigFile: Boolean = False): String; overload;
function GetConfigValue(const SectionName: String; const CfgName: String; const DefaultValue: Integer; ForceReadFormConfigFile: Boolean = False): Integer; overload;
function GetConfigValue(const SectionName: String; const CfgName: String; const DefaultValue: Boolean): Boolean; overload;
function GetConfigValue(const CfgName: String; const DefaultValue: String): String; overload;
function GetConfigValue(const CfgName: String; const DefaultValue: Integer): Integer; overload;
function GetConfigValue(const CfgName: String; const DefaultValue: Boolean): Boolean; overload;
function GetUserConfigValue(const CfgName: String; const DefaultValue: String): String;

procedure SetConfigValue(const SectionName: String; const CfgName: String; const Value: String); overload;
procedure SetConfigValue(const SectionName: String; const CfgName: String; const Value: Integer); overload;
procedure SetConfigValue(const SectionName: String; const CfgName: String; const Value: Boolean); overload;
procedure SetConfigValue(const CfgName: String; const Value: String); overload;
procedure SetConfigValue(const CfgName: String; const Value: Integer); overload;
procedure SetConfigValue(const CfgName: String; const Value: Boolean); overload;

procedure WriteToLog(const ext: string; const text: string);
procedure Log(const text: string; logLevel: Integer = 2);
procedure debug(const text: string);
procedure info(const text: string);
procedure warn(const text: string);
procedure error(const text: string);
procedure fatal(const text: string);

function GetLogFile(dt: TDateTime): String;

function LOG_FOLDER: string;
function LOGS_LIVE_DAYS: Integer;
function LOG_LEVEL: Integer;
function MAX_RECONNECT: Integer;
function MAX_QUERYRESULT_TIME: Integer;

implementation
uses
  Registry, IniFiles;

function GetAppPath: string;
begin
  Result := ExtractFilePath(ParamStr(0));
end;
function GetLogFolder: string;
begin
  Result := GetAppPath + LOG_FOLDER;
end;
procedure Log(const text: string; logLevel: Integer = 2);
begin
  if (LOG_LEVEL = 0) then Exit;
  if (logLevel < LOG_LEVEL) then Exit;
  if (logLevel = 1) then debug(text)
  else if (logLevel = 2) then info(text)
  else if (logLevel = 3) then warn(text)
  else if (logLevel = 4) then error(text)
  else if (logLevel = 5) then fatal(text);
end;

procedure debug(const text: string);
begin
  if (1 < LOG_LEVEL) then Exit;
  WriteToLog('log', '[DEBUG][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
end;

procedure info(const text: string);
begin
  if (2 < LOG_LEVEL) then Exit;
  WriteToLog('log', '[INFO][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
end;

procedure warn(const text: string);
begin
  if (3 < LOG_LEVEL) then Exit;
  WriteToLog('log', '[WARN][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
end;

procedure error(const text: string);
begin
  if (4 < LOG_LEVEL) then Exit;
  WriteToLog('log', '[ERROR][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
  WriteToLog('err', '[ERROR][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
end;

procedure fatal(const text: string);
begin
  if (5 < LOG_LEVEL) then Exit;
  WriteToLog('log', '[FATAL][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
  WriteToLog('err', '[FATAL][' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
end;

procedure WriteToLog(const ext: string; const text: string);
var
  MyLogFolder: string;
  procedure Delete_old_logs;
  var
    vdate: TDatetime;
    sr: TSearchRec;
    intFileAge: LongInt;
    myfileage: TDatetime;
  begin
    try
      vdate := Now - LOGS_LIVE_DAYS;
      if FindFirst(MyLogFolder + '\*.' + ext, faAnyFile - faDirectory, sr) = 0 then
        repeat
          intFileAge := FileAge(MyLogFolder + '\' + sr.name);
          if intFileAge > -1 then
          begin
            myfileage := FileDateToDateTime(intFileAge);
            if myfileage < vdate then
            begin
              DeleteFile(pchar(mylogfolder + '\' + sr.name));
            end;
          end;
        until (FindNext(sr) <> 0);
    finally
      SysUtils.FindClose(sr);
    end;
  end;
  procedure File_Append(const fname: string; const Data: string);
  var
    f: integer;
  begin
    f := FileOpen(fname, fmOpenReadWrite + fmShareDenyNone);
    if f < 0 then
    begin
      try
        if LOGS_LIVE_DAYS > 0 then
        begin
          Delete_old_logs;
        end;
      except
        // ignore problems with file deletion
      end;
      f := FileCreate(fname);
      //FileWrite(f, SERVICE_NAME + ' log file'#13#10, Length(SERVICE_NAME + ' log file'#13#10));
    end;
    if f >= 0 then
      try
        if FileSeek(f, 0, 2) >= 0 then
          FileWrite(f, data[1], length(data));
      finally
        FileClose(f);
      end;
  end;
  function GetTempDirectory: String;
  var
    tempFolder: array[0..MAX_PATH] of Char;
  begin
    GetTempPath(MAX_PATH, @tempFolder);
    result := StrPas(tempFolder);
  end;
begin
  if MyLogFolder = '' then
  begin
    MyLogFolder := GetAppPath;
    if Copy(MyLogFolder, length(MyLogFolder), 1) <> '\' then
      MyLogFolder := MyLogFolder + '\';

    if LOG_FOLDER <> '' then
    begin
      MyLogFolder := MyLogFolder + LOG_FOLDER;

      if not DirectoryExists(MyLogFolder) then
        if not CreateDir(MyLogFolder) then
        begin
          MyLogFolder := GetTempDirectory;
          if Copy(myLogFolder, length(MyLogFolder), 1) <> '\' then
            MyLogFolder := MyLogFolder + '\';
          MyLogFolder := MyLogFolder + LOG_FOLDER;
          if not DirectoryExists(MyLogFolder) then
            CreateDir(MyLogFolder);
        end;
    end;
  end;
  File_Append(myLogFolder + '\' + copy(ExtractFileName(ParamStr(0)), 1, length(ExtractFileName(ParamStr(0))) - 3) + FormatDateTime('YYYYMMDD.', Now) + ext, text);
end;

function GetLogFile(dt: TDateTime): String;
begin
  Result := GetAppPath + LOG_FOLDER + '\' + copy(ExtractFileName(ParamStr(0)), 1, length(ExtractFileName(ParamStr(0))) - 3) + FormatDateTime('YYYYMMDD', dt) + '.log';
end;

function LoadSystemConfig(ForceLoad: Boolean = False): TStringList;
var
  i, j: Integer;
  Sections, SectionValues: TStringList;
begin
  if system_config <> nil then
  begin
    if ForceLoad then
    begin
      system_config.Free;
    end
    else
    begin
      Result := system_config;
      Exit;
    end;
  end;
  system_config := TStringList.Create;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    Sections := TStringList.Create;
    try
      ReadSections(Sections);
      for i := 0 to Sections.Count - 1 do
      begin
        SectionValues := TStringList.Create;
        try
          ReadSectionValues(Sections.Strings[i], SectionValues);
          for j := 0 to SectionValues.Count - 1 do
          begin
            system_config.Values[Sections.Strings[i] + '_' + SectionValues.Names[j]] := SectionValues.ValueFromIndex[j];
          end;
        finally
          SectionValues.Free;
        end;
      end;
    finally
      Sections.Free;
      Free;
    end;
  end;
  Result := system_config;
end;

function GetConfigValue(const SectionName: String; const CfgName: String; const DefaultValue: String; ForceReadFormConfigFile: Boolean = False): String;
var
  value: String;
begin
  Result := DefaultValue;
  if (system_config = nil) then  LoadSystemConfig();
  if (system_config <> nil) then
  begin
    value := system_config.Values[SectionName + '_' + CfgName];
    if value <> '' then
    begin
      Result := value;
      Exit;
    end;
  end;
  if (not ForceReadFormConfigFile) then Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      Result := ReadString(SectionName, CfgName, Result);
    finally
      Free;
    end;
  end;
end;

function GetConfigValue(const CfgName: String; const DefaultValue: String): String;
begin
  Result := GetConfigValue(DEFAULT_CONFIG_SECTION, CfgName, DefaultValue);
end;

function GetUserConfigValue(const CfgName: String; const DefaultValue: String): String;
begin
  Result := GetConfigValue(USER_CONFIG_SECTION, CfgName, DefaultValue);
end;

function GetConfigValue(const SectionName: String; const CfgName: String; const DefaultValue: Integer; ForceReadFormConfigFile: Boolean = False): Integer;
var
  value: String;
begin
  Result := DefaultValue;
  if (system_config = nil) then  LoadSystemConfig();
  if (system_config <> nil) then
  begin
    value := system_config.Values[SectionName + '_' + CfgName];
    if value <> '' then
    begin
      try
        Result := StrToInt(value);
        Exit;
      except
      end;
    end;
  end;
  if (not ForceReadFormConfigFile) then Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      Result := ReadInteger(SectionName, CfgName, Result);
    finally
      Free;
    end;
  end;
end;

function GetConfigValue(const CfgName: String; const DefaultValue: Integer): Integer;
begin
  Result := GetConfigValue(DEFAULT_CONFIG_SECTION, CfgName, DefaultValue);
end;

function GetConfigValue(const SectionName: String; const CfgName: String; const DefaultValue: Boolean): Boolean;
var
  value: Integer;
begin
  if DefaultValue then
    value := 1
  else
    value := 0;
  value := GetConfigValue(SectionName, CfgName, value);
  if value = 1 then
    Result := True
  else
    Result := False;
end;

function GetConfigValue(const CfgName: String; const DefaultValue: Boolean): Boolean;
begin
  Result := GetConfigValue(DEFAULT_CONFIG_SECTION, CfgName, DefaultValue);
end;

procedure SetConfigValue(const SectionName: String; const CfgName: String; const Value: String);
begin
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      WriteString(SectionName, CfgName, Value);
    finally
      Free;
    end;
  end;
end;

procedure SetConfigValue(const CfgName: String; const Value: String);
begin
  SetConfigValue(DEFAULT_CONFIG_SECTION, CfgName, Value);
end;

procedure SetConfigValue(const SectionName: String; const CfgName: String; const Value: Integer);
begin
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      WriteInteger(SectionName, CfgName, Value);
    finally
      Free;
    end;
  end;
end;

procedure SetConfigValue(const CfgName: String; const Value: Integer);
begin
  SetConfigValue(DEFAULT_CONFIG_SECTION, CfgName, Value);
end;

procedure SetConfigValue(const SectionName: String; const CfgName: String; const Value: Boolean);
begin
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      if Value then
        WriteInteger(SectionName, CfgName, 1)
      else
        WriteInteger(SectionName, CfgName, 0);
    finally
      Free;
    end;
  end;
end;

procedure SetConfigValue(const CfgName: String; const Value: Boolean);
begin
  SetConfigValue(DEFAULT_CONFIG_SECTION, CfgName, Value);
end;

function LOG_FOLDER: string;
begin
  Result := GetConfigValue('LOG_FOLDER', 'LOG');
//  with TRegistry.Create do
//  begin
//    try
//      RootKey := HKEY_LOCAL_MACHINE;
//      if OpenKey('SOFTWARE\XinglongStore\' + APP_NAME, False) then
//      begin
//        if ValueExists('LogFolder') then
//          Result := ReadString('LogFolder')
//        else
//          WriteString('LogFolder', Result);
//      end;
//      CloseKey;
//    finally
//      Free;
//    end;
//  end;
end;

function LOGS_LIVE_DAYS: Integer;
begin
  Result := GetConfigValue('LOGS_LIVE_DAYS', 0);
end;

function LOG_LEVEL: Integer;
begin
  Result := GetConfigValue('LOG_LEVEL', 3);
end;

function MAX_RECONNECT: Integer;
begin
  Result := GetConfigValue('MAX_RECONNECT', 3);
end;

function MAX_QUERYRESULT_TIME: Integer;
begin
  Result := GetConfigValue('MAX_QUERYRESULT_TIME', 3);
end;

end.
