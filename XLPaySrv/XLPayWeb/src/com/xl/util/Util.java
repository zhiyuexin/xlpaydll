package com.xl.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.CharacterIterator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xl.pay.srv.GetPayUrl;
import com.xl.pay.srv.PayParam;

public class Util {
	private static Logger logger = LoggerFactory.getLogger(GetPayUrl.class);

	// 属性对象
	private static ListProperties ENV_Properties = null;

	// 用户密码加密属性
	public static final String PASSWORD_HASH_ALGORITHM = "password.hashAlgorithm";
	public static final String PASSWORD_HASH_ENCODING = "password.hashEncoding";
	public static final String PASSWORD_HASH_CHARSET = "password.hashCharset";

	// Factory类型
	public static final String EJB3_FACTORY = "ejb3.factory.type";
	public static final String EJB3_FACTORY_BLAZEDS = "BlazeDS";
	public static final String EJB3_FACTORY_GENERIC_TYPE = "ejb3.factory.generic.type";

	// 动态委派
	public static final String EJB3_DELEGATE_FACTORY = "ejb3.delegate.factory";
	
	public static boolean isReg = false;
	public static String srvId = "";

	/**
	 * 直接输出消息到控制台
	 * 
	 * @param msg
	 */
	static final public void output(String msg) {
		System.out.println(msg);
	}

	/**
	 * 根据属性文件生成全局属性对象
	 * 
	 * @param propsFile
	 * @throws FileNotFoundException
	 */
	static final public void loadProperties(ListProperties propsFile) {
		ENV_Properties = propsFile;
	}
	static final public ListProperties getProperties() {
		return ENV_Properties;
	}

	/**
	 * Method getProperty.
	 * 
	 * @param name
	 * @return value
	 */
	static final public String getProperty(String name) {
		String value = "";
		try {
			if (ENV_Properties == null) {
				return null;
			}
			value = ENV_Properties.getProperty(name);
		} catch (Exception e) {
			output("Util.getProperty(): Exception: " + e);
		}
		return (value);
	}

	/**
	 * Method readTokens.
	 * 
	 * @param text
	 * @param token
	 * @return list
	 */
	public static String[] readTokens(String text, String token) {
		StringTokenizer parser = new StringTokenizer(text, token);
		int numTokens = parser.countTokens();
		String[] list = new String[numTokens];
		for (int i = 0; i < numTokens; i++) {
			list[i] = parser.nextToken();
		}
		return list;
	}

	/**
	 * Method getProperties.
	 * 
	 * @param name
	 * @return values
	 */
	public static String[] getProperties(String name) {
		String[] values = { "" };
		try {
			if (ENV_Properties == null) {
				return null;
			}
			values = ENV_Properties.getProperties(name);
			// output("Util.getProperties: property (" + name + ") -> " +
			// values.toString());
			// for (Enumeration e = PBW_Properties.propertyNames() ;
			// e.hasMoreElements() ;) {
			// debug((String)e.nextElement());
			// }
		} catch (Exception e) {
			output("Util.getProperties(): Exception: " + e);
		}
		return (values);
	}

	/**
	 * 检查字符串是否包含在数组中
	 * 
	 * @param strs
	 * @param s
	 * @return
	 */
	public static boolean isContains(String[] strs, String s) {
		if (strs == null || s == null)
			return false;
		List<String> l = Arrays.asList(strs);
		return l.contains(s);
	}

	public static void ObjectToFile(Object obj, String filename) {
		try {
			@SuppressWarnings("resource")
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
			    filename));
			oos.writeObject(obj);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Object FileToObject(String filename) {
		try {
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
			    filename));
			return ois.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map getSerializableProperties(Object o) {
		Map m = new HashMap();
		Field[] fields = o.getClass().getFields();
		try {
			for (Field field : fields) {
				field.setAccessible(true);
				Object value = field.get(o);
				if (value instanceof Serializable)
					m.put(field.getName(), value);
			}
			return m;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, String> doExtParams(String extParams) {
		Map<String, String> result = new HashMap<String, String>();
		if (extParams != null && !extParams.isEmpty()){
			String[] params = extParams.split(",");
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null && !params[i].isEmpty()){
					String[] values = params[i].split("=");
					if (values != null && values.length>1) {
						result.put(values[0], values[1]);
					}
				}
			}
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public static String replaceUrl(String url,  Map... parameterMaps) {
		if (url == null) return "";
		// 同义词转换
		String result = url
				.replace("{erp_type}","{partner_id}")
//				.replace("{erp_type}","{partner_id}")
//				.replace("{erp_type}","{partner_id}")
				.replace("{empl_id}","{operator_id}")
				.replace("{empl_name}","{operator_name}");
		Pattern pattern = Pattern.compile("\\{\\w+\\}");
		// 依次替换参数
		for (Map parameterMap: parameterMaps){
			if (parameterMap == null || parameterMap.size() == 0) continue;
			Matcher matcher = pattern.matcher(result);
			while (matcher.find()) {
				String s1 = matcher.group();
				String s2 = s1.replaceAll("[\\{,\\}]", "");
				if (!isEmpty(s2) && parameterMap.get(s2) != null) {
					if(parameterMap.get(s2) instanceof String[]){
			            String[] values = (String[])parameterMap.get(s2);
			            s2 = "";
			            for(int i=0;i<values.length;i++){
								s2 += values[i] + ",";
			            }
			            s2 = s2.substring(0, s2.length()-1);
			        }else{
			            s2 = parameterMap.get(s2).toString();
			        }
				}else {
					s2 = "";
				}
				if (!isEmpty(s2)) result = result.replace(s1, s2);
				//System.out.println("replace " + s1 + " to " +s2 + "=>" + result);
			}
		}
		// 替换系统定义参数
		Matcher matcher = pattern.matcher(result);
		while (matcher.find()) {
			String s1 = matcher.group();
			String s2 = s1.replaceAll("[\\{,\\}]", "");
			if (!isEmpty(s2)) {
	            s2 = getProperty(s2);
			}else {
				s2 = "";
			}
			if (!isEmpty(s2)) result = result.replace(s1, s2);
			//System.out.println("replace " + s1 + " to " +s2 + "=>" + result);
		}
		// 最后替换掉未找到参数
		result = result.replaceAll("\\{\\w+\\}", "");
		return result;
	}
	

	public static String replaceUrlBySysProperty(String url) {
		if (url == null) return "";
		String result = url;
		Pattern pattern = Pattern.compile("\\{\\w+\\}");
		// 替换系统定义参数
		Matcher matcher = pattern.matcher(result);
		while (matcher.find()) {
			String s1 = matcher.group();
			String s2 = s1.replaceAll("[\\{,\\}]", "");
			if (!isEmpty(s2)) {
	            s2 = getProperty(s2);
			}else {
				s2 = "";
			}
			if (!isEmpty(s2)) result = result.replace(s1, s2);
		}
		return result;
	}
	
	public static String addParamsToUrl(String url,  Map<String, Object>... parameterMaps) {
		if (url == null) url = "";
		// 先替换
		String result = replaceUrl(url, parameterMaps);
		StringBuilder queryString = new StringBuilder();
		// 依次替换参数
		for (Map<String, Object> parameterMap: parameterMaps){
			if (parameterMap == null) continue;
			for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
				if (entry.getKey() != null) {
					queryString.append("&");
					queryString.append(entry.getKey());
					queryString.append("=");
					if(entry.getValue() instanceof String[]){
			            String[] values = (String[])entry.getValue();
			            String s = "";
			            for(int j=0;j<values.length;j++){
							s += (j==0?"":",") + values[j];
			            }
						queryString.append(s);
			        }else{
						queryString.append(entry.getValue());
			        }
				}
			}
		}
		if (result.indexOf('?') != -1){
			result = result + queryString.toString();
		}else{
			result = result + "?1=1" + queryString.toString();
		}
		return result;
	}

	/*
	 * 判断是否为空
	 */
	public static Boolean isEmpty(Object o){
		if (o instanceof String){
			return o == null || o.toString().trim().equals("");
		}else{
			return o == null;
		}
	}
	/*
	 * 前后增加单引号
	 */
	public static String quoteStr(String s){
		if (isEmpty(s))
			return "''";
		else
			return "'" + s + "'";
	}
	public static String parseString(Object obj){
		return parseStr(obj, "");
	}
	public static String parseStr(Object obj){
		return parseStr(obj, null);
	}
	public static String parseStr(Object obj, String defaultValue){
		return isEmpty(obj) ? defaultValue : obj.toString();
	}
	public static Integer parseInteger(Object obj){
		return parseInteger(obj, null);
	}
	public static Integer parseInteger(Object obj, Integer defaultValue){
		if (obj instanceof Integer){
			return (Integer) obj;
		}else{
			if (isEmpty(obj)){
				return defaultValue;
			}else{
				return Integer.parseInt(obj.toString());
			}
		}
	}
	public static Double parseDbl(Object obj){
		return parseDbl(obj, null);
	}
	public static Double parseDbl(Object obj, Double defaultValue){
		return parseDouble(obj, defaultValue);
	}
	public static Double parseDouble(Object obj){
		return parseDouble(obj, null);
	}
	public static Double parseDouble(Object obj, Double defaultValue){
		if (obj instanceof Double){
			return (Double) obj;
		}else{
			if (isEmpty(obj)){
				return defaultValue;
			}else{
				return Double.parseDouble(obj.toString());
			}
		}
	}
	public static BigDecimal parseBigDecimal(Object obj){
		return parseBigDecimal(obj, null);
	}
	public static BigDecimal parseBigDecimal(Object obj, BigDecimal defaultValue){
		if (obj instanceof BigDecimal){
			return (BigDecimal) obj;
		}else{
			if (isEmpty(obj)){
				return defaultValue;
			}else{
				return BigDecimal.valueOf(Double.parseDouble(obj.toString()));
			}
		}
	}
	public static Long parseLong(Object obj){
		return parseLong(obj, null);
	}
	public static Long parseLong(Object obj, Long defaultValue){
		if (obj instanceof Long){
			return (Long) obj;
		}else{
			if (isEmpty(obj)){
				return defaultValue;
			}else{
				return Long.parseLong(obj.toString());
			}
		}
	}
	public static boolean parseBoolean(Object obj){
		if (obj instanceof Boolean){
			return (Boolean) obj;
		}else{
			return parseString(obj).equalsIgnoreCase("true") || parseString(obj).equalsIgnoreCase("1");
		}
	}

	public static boolean parseBoolean(Object obj, boolean defaultValue){
		if (isEmpty(obj)) return defaultValue;
		if (obj instanceof Boolean){
			return (Boolean) obj;
		}else{
			return parseString(obj).equalsIgnoreCase("true") || parseString(obj).equalsIgnoreCase("1");
		}
	}
	
	public static Date parseDate(Long obj){
		if (isEmpty(obj)) return null;
		return new Date(obj);
	}
	public static Date parseDate(Object obj){
		return parseDate(obj, "yyyy-MM-dd HH:mm:ss");
	}
	public static Date parseDate(Object obj, String formatStr){
		if (isEmpty(obj)) return null;
		if (obj instanceof oracle.sql.TIMESTAMP){
			return parseDate((oracle.sql.TIMESTAMP)obj, formatStr);
		}
		try {
			SimpleDateFormat ft = new SimpleDateFormat(formatStr);
			return ft.parse(parseString(obj));
		}catch(Exception e){
			return null;
		}
	}
	public static Date parseDate(oracle.sql.TIMESTAMP obj){
		return parseDate(obj, "yyyy-MM-dd HH:mm:ss");
	}
	public static Date parseDate(oracle.sql.TIMESTAMP obj, String formatStr){
		DateFormat sdf = new SimpleDateFormat(formatStr);
		Timestamp tt;
		try {
			tt = obj.timestampValue();
			return sdf.parse(sdf.format(tt));
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
    public static String lPad (String stringToPad, String padder, int size) {
        if (padder == null || padder.length() == 0) {
        	return stringToPad;
        }
        StringBuffer strb = new StringBuffer(stringToPad);
        StringCharacterIterator sci  = new StringCharacterIterator(padder);
        while (strb.length() < (size - stringToPad.length())) {
        	for (char ch = sci.first(); ch != CharacterIterator.DONE ; ch = sci.next()) {
        	    if (strb.length() <  size - stringToPad.length()) {
        	    	strb.insert(strb.length(),String.valueOf(ch));
        	    }
    	    }
        }
        return strb.append(stringToPad).toString();
    }
    public static String rPad (String stringToPad, String padder, int size) {
        if (padder == null || padder.length() == 0) {
        	return stringToPad;
        }
        StringBuffer strb = new StringBuffer(stringToPad);
        StringCharacterIterator sci  = new StringCharacterIterator(padder);
        while (strb.length() < size) {
            for (char ch = sci.first(); ch != CharacterIterator.DONE ; ch = sci.next()) {
                if (strb.length() < size) {
                    strb.append(String.valueOf(ch));
                }
            }
        }
        return strb.toString();
    }

	/**
	 * 
	 * toBean:map转bean<br/>
	 *
	 * @version 0.1
	 * @author 王宏亮
	 * @param type
	 * @param map
	 * @return
	 * @throws IntrospectionException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @since JDK 1.7
	 */
    public static final Object toBean(Class<?> type, Map<String, ? extends Object> map)   
            throws IntrospectionException, IllegalAccessException,  InstantiationException, InvocationTargetException {  
        BeanInfo beanInfo = Introspector.getBeanInfo(type);  
        Object obj = type.newInstance();  
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();  
        for (int i = 0; i< propertyDescriptors.length; i++) {  
            PropertyDescriptor descriptor = propertyDescriptors[i];  
            String propertyName = descriptor.getName();  
            if (map.containsKey(propertyName)) {  
                Object value = map.get(propertyName);  
                Object[] args = new Object[1];  
                args[0] = value;  
                descriptor.getWriteMethod().invoke(obj, args);  
            }  
        }  
        return obj;  
    }  
	/**
	 * 
	 * toMap:bean转map<br/>
	 *
	 * @version 0.1
	 * @author 王宏亮
	 * @param bean
	 * @return
	 * @throws IntrospectionException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @since JDK 1.7
	 */
    public static final Map<String, Object> toMap(Object bean)  
            throws IntrospectionException, IllegalAccessException, InvocationTargetException {  
        Map<String, Object> returnMap = new HashMap<String, Object>();  
        BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());  
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();  
        for (int i = 0; i< propertyDescriptors.length; i++) {  
            PropertyDescriptor descriptor = propertyDescriptors[i];  
            String propertyName = descriptor.getName();  
            if (!propertyName.equals("class")) {  
                Method readMethod = descriptor.getReadMethod();  
                Object result = readMethod.invoke(bean, new Object[0]);  
                if (result != null) {  
                    returnMap.put(propertyName, result);  
                } else {  
                    returnMap.put(propertyName, "");  
                }  
            }  
        }  
        return returnMap;  
    }
    public static String getLocalMac(InetAddress ia) throws SocketException {
		//获取网卡，获取地址
	    if (ia == null) {
			try {
				ia = InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return null;
			}
    	}
		byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
//		System.out.println("mac数组长度："+mac.length);
		StringBuffer sb = new StringBuffer("");
		for(int i=0; i<mac.length; i++) {
//			if(i!=0) {
//				sb.append("-");
//			}
			//字节转换为整数
			int temp = mac[i]&0xff;
			String str = Integer.toHexString(temp);
//			System.out.println("每8位:"+str);
			if(str.length()==1) {
				sb.append("0"+str);
			}else {
				sb.append(str);
			}
		}
//		System.out.println("本机MAC地址:"+sb.toString().toUpperCase());
		return sb.toString();
	}
	public static String getSysUrl(String urlName) {
		String url = getServerUrl(urlName); //Util.getProperty("server") + Util.getProperty(urlName);
    	return replaceUrlBySysProperty(url);
    }
	public static String getServerUrl(String urlName) {
		String server = Util.getProperty("server");
		String result = Util.getProperty(urlName);
		if (result == null && server != null){
			result = urlName;
		}
		if (server !=null && result != null && result.indexOf(server) == -1){
			if (!server.substring(server.length()-1).equals("/") && !result.substring(0, 1).equals("/")){
				result = server + "/" + result;
			}else{
				result = server + result;
			}
		}
		return result;
    }
    public static String parseUrlQueryString(Map<String, String> argParams) {
		StringBuffer queryString = new StringBuffer();
		if (argParams != null) {
			int i = 0;
			for (Map.Entry<String, String> entry : argParams.entrySet()) {
				if (entry.getKey() != null) {
					//queryString.append((i==0?"":"&") + entry.getKey() + "=" + entry.getValue()==null?"":entry.getValue());
					if (i>0) queryString.append("&");
					queryString.append(entry.getKey());
					queryString.append("=");
					queryString.append(entry.getValue());
					i++;
				}
			}
		}
		return queryString.toString();
    }
    
    public static String getServerResult(String query_url) {
		logger.debug("request:{}", query_url);
		String result = "";
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		HttpGet httpget = new HttpGet(query_url);
		RequestConfig requestConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(5000)
				.setConnectTimeout(5000)
				.setSocketTimeout(5000).build();
		httpget.setConfig(requestConfig);

		CloseableHttpResponse response;
		try {
			response = httpclient.execute(httpget);
			int responseCode = response.getStatusLine().getStatusCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, "UTF-8");
				logger.debug("response:{}", result);
			}else{
				logger.error("srvError:true, errCode:E505,errMessage:xld server response error, responseCode is " + responseCode);
				return "{\"srvError\":true, \"success\":false, \"errCode\":\"E505\", \"errMessage\":\"xld server response error, responseCode is " + responseCode + "\"}";
			}
		} catch (IOException e) {
			logger.error("srvError:true, success:false,errCode:E501,errMessage:failed to connnect to xld server." + e.getMessage() +"\"}");
			return "{\"srvError\":true, \"success\":false,\"errCode\":\"E501\",\"errMessage\":\"failed to connnect to xld server." + e.getMessage() +"\"}";
		} finally {
			httpget.releaseConnection();
		}
		if (result == null || result.isEmpty()){
			logger.error("srvError:true, errCode:E506,errMessage:xld server not response");
			return "{\"srvError\":true, \"success\":false, \"errCode\":\"E506\", \"errMessage\":\"xld server not response\"}";
		}
		return result;
    }
    
    @SuppressWarnings("rawtypes")
    public static String getServerResponseStringByParam(String payCode, String erpType, String getType, String extParams, Map extMapParams) {
		// 判断payType和erpType的合法性
    	PayParam payParam = new PayParam(payCode,erpType);
		if (!payParam.success){
			return "{\"srvError\":true, \"success\":false,\"errCode\":\"" + payParam.errCode + "\",\"errMessage\":\"" + payParam.errMessage + "\"}";
		}
		String payType = payParam.payType;
		erpType = payParam.erpType;
		// 目前支持直微信支付
		if(PayParam.isSupportPay(payType)){
			// 获取URL格式（含须提交数据）
			String query_url = Util.getProperty(payType + ".pay.url." + getType);
			if (Util.isEmpty(query_url)){
				query_url = Util.getServerUrl("url." + getType);
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("{erp_type}", erpType);
			params.put("{partner_id}", erpType);
			// 替换字符（生成提交数据字符串）
			query_url = Util.replaceUrl(query_url, params, Util.doExtParams(extParams), extMapParams);
			logger.info(query_url);
			// 把QueryString转换为Map（为了POST，GET方法中文有乱码）
			Map<String, Object> m = queryStringToMap(query_url);
		    int i = query_url.indexOf('?');
		    if (i != -1){
		    	query_url = query_url.substring(0,i);
		    }
			return doPost(query_url, m, "UTF-8");
//			return Util.getServerResponseString(query_url);
		}else{
			return "{\"srvError\":true, \"success\":false,\"errCode\":\"E503\",\"errMessage\":\"invalid param payType\"}";
		}
    }

    @SuppressWarnings("rawtypes")
    public static String getServerResponseStringByParam(String query_url, String extParams, Map extMapParams) {
		// 替换字符（生成提交数据字符串）
    	logger.info(query_url);
		query_url = Util.replaceUrl(query_url, null, Util.doExtParams(extParams), extMapParams);
    	logger.info(query_url);
		// 把QueryString转换为Map（为了POST，GET方法中文有乱码）
		Map<String, Object> m = queryStringToMap(query_url);
	    int i = query_url.indexOf('?');
	    if (i != -1){
	    	query_url = query_url.substring(0,i);
	    }
		return doPost(query_url, m, "UTF-8");
    }

	public static Map<String, Object> queryStringToMap(String queryString) {  
	    if (queryString == null || queryString.equals("")) {  
	        return null;  
	    }
	    int i = queryString.indexOf('?');
	    if (i != -1){
	    	queryString = queryString.substring(i+1);
	    }
//	    System.out.println("-------------->" + queryString);
	    Map<String, Object> map = new HashMap<String, Object>();  
	    String[] text = queryString.split("&"); // 转换为数组  
	    for (String str : text) {  
	        String[] keyText = str.split("="); // 转换key与value的数组  
	        String key = keyText[0]; // key  
	        String value = keyText.length>1?keyText[1]:""; // value  
	        map.put(key, value);  
//		    System.out.println("------------->>" + key + "=" + value);
	    }  
	    return map;  
	}

    public static String getServerResponseString(String query_url) {
		logger.info("request:{}", query_url);
		StringBuffer result = new StringBuffer();
		try {
			// 建立连接
			URL url = new URL(query_url);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			// //设置连接属性
			httpConn.setDoOutput(true);// 使用 URL 连接进行输出
			httpConn.setDoInput(true);// 使用 URL 连接进行输入
			httpConn.setUseCaches(false);// Post 请求不能使用缓存
			httpConn.setRequestMethod("GET");// 设置URL请求方法
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setConnectTimeout(60000);//设置连接超时  
			//如果在建立连接之前超时期满，则会引发一个 java.net.SocketTimeoutException。超时时间为零表示无穷大超时。  
			httpConn.setReadTimeout(60000);//设置读取超时  
			// 获得响应状态
			int responseCode = httpConn.getResponseCode();
			if (HttpURLConnection.HTTP_OK == responseCode) {// 连接成功
				BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "utf-8"));
				String line;
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				reader.close();
			}else{
				logger.error("srvError:true, errCode:E505,errMessage:xld server response error, responseCode is " + responseCode);
				return "{\"srvError\":true, \"success\":false, \"errCode\":\"E505\", \"errMessage\":\"xld server response error, responseCode is " + responseCode + "\"}";
			}
			httpConn.disconnect();
		} catch (Exception ex) {

			logger.error("srvError:true, success:false,errCode:E501,errMessage:failed to connnect to xld server");
			return "{\"srvError\":true, \"success\":false,\"errCode\":\"E501\",\"errMessage\":\"failed to connnect to xld server\"}";
		}
		if (result.toString().isEmpty()){
			logger.error("srvError:true, errCode:E506,errMessage:xld server not response");
			return "{\"srvError\":true, \"success\":false, \"errCode\":\"E506\", \"errMessage\":\"xld server not response\"}";
		}
		return result.toString();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map<String, Object> unionMapParams(Map... parameterMaps) {
    	Map<String, Object> result = new HashMap<String, Object>();
		for (Map parameterMap: parameterMaps){
			if (parameterMap == null || parameterMap.size() == 0) continue;
				result.putAll(parameterMap);
		}
    	return result;
    }
    
	public static String doPost(String url, Map<String, Object> params, String charset) {
		logger.info("request:{}", url);
		String result = "";
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httppost = new HttpPost(url);
		if (params != null) {
			List<NameValuePair> postData = new ArrayList<NameValuePair>();
			for (Map.Entry<String, Object> entry : params.entrySet()) {
				NameValuePair p = new BasicNameValuePair(entry.getKey(), Util.parseStr(entry.getValue()));
				postData.add(p);
			}

			HttpEntity en = null;
			try {
				en = new UrlEncodedFormEntity(postData, charset);
			} catch (UnsupportedEncodingException e) {
			}
			httppost.setEntity(en);
		}

		CloseableHttpResponse response;
		try {
			response = httpClient.execute(httppost);
			int responseCode = response.getStatusLine().getStatusCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, charset);//"UTF-8");
				logger.info("response:{}", result);
			}else{
				logger.error("srvError:true, errCode:E505,errMessage:xld server response error, responseCode is " + responseCode);
				return "{\"srvError\":true, \"success\":false, \"errCode\":\"E505\", \"errMessage\":\"xld server response error, responseCode is " + responseCode + "\"}";
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("srvError:true, success:false,errCode:E501,errMessage:failed to connnect to xld server." + e.getMessage() +"\"}");
			return "{\"srvError\":true, \"success\":false,\"errCode\":\"E501\",\"errMessage\":\"failed to connnect to xld server." + e.getMessage() +"\"}";
		} finally {
			httppost.releaseConnection();
		}
		if (result == null || result.isEmpty()){
			logger.error("srvError:true, errCode:E506,errMessage:xld server not response");
			return "{\"srvError\":true, \"success\":false, \"errCode\":\"E506\", \"errMessage\":\"xld server not response\"}";
		}
		return result;
	}
	public static void main(String[] args) {
//		Pattern pattern = Pattern.compile("\\{\\w+\\}");
//		String s = "t.json?erpType={partner_id}&comCode={com_code}&amt={pay_amt}&cnttTradeNo={cntt_no}&posNo={pos_no}&billId={bill_id}&batchno={batch_no}";
//		Matcher matcher = pattern.matcher(s);
//		while (matcher.find()) {
//			System.out.println(matcher.group());
//			System.out.println("Match   at positions " + matcher.start() + "-" + (matcher.end() - 1));
//		}
		String fullPath = "/query/{payType}/{erpType}/{ext_params}";
		String uri = "/query/5/operator_id%3D101,operator_name%3D101";
		String pattern_str = fullPath.replaceAll("\\{\\w+\\}","[0-9,a-zA-Z%_=]+");
		System.out.println(pattern_str);
		pattern_str = "^" + pattern_str.replace("/", "[\\/]");
		System.out.println(pattern_str);
		Pattern p = Pattern.compile(pattern_str);
		Matcher m = p.matcher(uri); 
		System.out.println("m.matches():" + m.matches());
	}
}
