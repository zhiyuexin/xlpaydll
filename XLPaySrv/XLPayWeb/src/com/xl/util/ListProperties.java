package com.xl.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author samcland
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/**
 * Utility class.
 */
@SuppressWarnings("unchecked")
public class ListProperties extends Properties {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Hashtable<String, Vector<String>> listProps = null;

	private Logger logger = LoggerFactory.getLogger(ListProperties.class);

	/*
	 * Method load
	 * 
	 * @param inStream
	 */

	@SuppressWarnings("rawtypes")
	public void load(InputStream inStream) throws IOException {
		try {
			// Parse property file, remove comments, blank lines, and combine
			// continued lines.
			String propFile = "";
			BufferedReader inputLine = new BufferedReader(
					new InputStreamReader(inStream));
			String line = inputLine.readLine();
			boolean lineContinue = false;
			while (line != null) {
//				System.out.println("ListProperties.load - Line read: " + line);
				line = line.trim();
				String currLine = "";
				if (line.startsWith("#")) {
					// Skipping comment
				} else if (line.startsWith("!")) {
					// Skipping comment
				} else if (line.equals("")) {
					// Skipping blank lines
				} else {
					if (!lineContinue) {
						currLine = line;
					} else {
						// This is a continuation line. Add to previous line.
						currLine += line;
					}
					// Must be a property line
					if (line.endsWith("\\")) {
						// Next line is continued from the current one.
						lineContinue = true;
					} else {
						// The current line is completed. Parse the property.
						propFile += currLine + "\n";
						currLine = "";
						lineContinue = false;
					}
				}
				line = inputLine.readLine();
			}
			// Load Properties
			listProps = new Hashtable<String, Vector<String>>();
			// Now parse the Properties to create an array
			String[] props = readTokens(propFile, "\n");
			for (int index = 0; index < props.length; index++) {
//				logger.info("ListProperties.load() - props[{}] = {}", index,
//						props[index]);
				// Parse the line to get the key,value pair
				String[] val = readTokens(props[index], "=", 1);
				if (!val[0].equals("")) {
					if (this.containsKey(val[0])) {
						// Previous key,value was already created.
						// Need an array
						Vector<String> currList = (Vector) listProps
								.get(val[0]);
						if ((currList == null) || currList.isEmpty()) {
							currList = new Vector<String>();
							String prevVal = this.getProperty(val[0]);
							currList.addElement(prevVal);
						}
						currList.addElement(val[1]);
						listProps.put(val[0], currList);
					}
					this.setProperty(val[0], val[1]);
				}
			}
		} catch (Exception e) {
			logger.error("ListProperties.load(): Exception: ", e);
		}
	}

	/**
	 * Method readTokens.
	 * 
	 * @param text
	 * @param token
	 * @return list
	 */
	public String[] readTokens(String text, String token) {
		StringTokenizer parser = new StringTokenizer(text, token);
		int numTokens = parser.countTokens();
		String[] list = new String[numTokens];
		for (int i = 0; i < numTokens; i++) {
			list[i] = parser.nextToken();
		}
		return list;
	}
	
	public String[] readTokens(String text, String token, int num) {
		StringTokenizer parser = new StringTokenizer(text, token);
		String[] list = new String[num+1];
		int len = 0;
		for (int i = 0; i < num; i++) {
			list[i] = parser.nextToken();
			len += list[i].length();
			len += token.length();
		}
		list[num] = text.substring(len);
		return list;
	}

	/**
	 * Method getProperties.
	 * 
	 * @param name
	 * @return values
	 */
	@SuppressWarnings("rawtypes")
	public String[] getProperties(String name) {
		String[] values = { "" };
		try {
			String value = this.getProperty(name);
//			logger.debug("ListProperties.getProperties: property ({}) -> {}",
//					name, value);
			if (listProps.containsKey(name)) {
				Vector list = (Vector) listProps.get(name);
				values = new String[list.size()];
				for (int index = 0; index < list.size(); index++) {
					values[index] = (String) list.elementAt(index);
				}
			} else {
				values[0] = value;
			}
		} catch (Exception e) {
			logger.error("ListProperties.getProperties(): Exception: ", e);
		}
		return (values);
	}
}
