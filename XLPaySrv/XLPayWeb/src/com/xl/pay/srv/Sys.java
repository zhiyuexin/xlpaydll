package com.xl.pay.srv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xl.pay.srv.vo.ResInfo;
import com.xl.util.ListProperties;
import com.xl.util.Util;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.PathParam;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;
/*
 * 系统
 */
@WebServlet
@Path("/sys")
public class Sys {
	private static Logger logger = LoggerFactory.getLogger(Sys.class);
	public static String SystemConfigFile = "";
	
	@ContextResource
	ServletRequest request;
	
	@POST
	@Path("/reg")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String reg() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return Util.getServerResponseStringByParam(Util.getSysUrl("url.reg_pos"), extParams, request.getRequest().getParameterMap());
	}

	@POST
	@Path("/logon")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String logon() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return Util.getServerResponseStringByParam(Util.getSysUrl("url.log_on"), extParams, request.getRequest().getParameterMap());
	}

	@POST
	@Path("/logoff")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String logoff() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return Util.getServerResponseStringByParam(Util.getSysUrl("url.log_off"), extParams, request.getRequest().getParameterMap());
	}

	
	@POST
	@Path("/getmaxbillid")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String getMaxBillId() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return Util.getServerResponseStringByParam(Util.getSysUrl("url.max_bill_id"), extParams, request.getRequest().getParameterMap());
	}
	
	
	@POST
	@Path("/getdate")
	@Produces(MediaType.TEXT_PLAIN)
	public String getdate() {
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
		return sFormat.format(new Date());
	}

	@POST
	@Path("/getsysinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public ListProperties getSysInfo() {
		ListProperties props = Util.getProperties();
		return props;
	}
	
	@POST
	@Path("/getterminalsn/{terminalId}/{userCode}/{userPwd}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.TEXT_PLAIN)
	public String getPayTerminalSn(@PathParam("terminalId") String terminalId,@PathParam("userCode") String userCode,@PathParam("userPwd") String userPwd) {
		String extParams = "terminal_id=" + terminalId + ",user_code=" + userCode + ",user_pwd=" + userPwd;
		return Util.getServerResponseStringByParam(Util.getSysUrl("url.get_terminal_sn"), extParams, request.getRequest().getParameterMap());
	}
	/**
	 * 加载系统配置
	 */
	@Path("/reloadconfig")
	@Produces(MediaType.TEXT_PLAIN)
	public void loadSystemConfig(){
		if ("".equals(SystemConfigFile)) {
			logger.error("未指定系统配置文件");
			return;
		}
		
		logger.info("加载服务配置信息");
		ListProperties props = new ListProperties();
		try {
			String path = this.getClass().getClassLoader().getResource("")+ "/" + SystemConfigFile;
			path = path.substring(6);
			logger.info("开始加载系统配置\"{}\"", path);
			File file = new File(path);
			
			props.load(new FileInputStream(file));
			Util.loadProperties(props);
			
			// 注册URL
			if (Util.isEmpty(props.getProperty("terminal.reg_url"))) {
				props.setProperty("terminal.reg_url", "offline_pay/reg_pay_terminal_srv.json"); // 默认获取企业配置信息URL
			}
			// 服务器配置URL
			if (Util.isEmpty(props.getProperty("url.get_com_cfg"))) {
				props.setProperty("url.get_com_cfg", "offline_pay/get_com_cfg.json?comCode={com_code}&srvId={srv_id}"); // 默认获取企业配置信息URL
			}
			// 服务器类型
			if (Util.isEmpty(props.getProperty("terminal.type"))) {
				props.setProperty("terminal.type", "1"); // 默认获取企业配置信息URL
			}
			// 二次处理URL（前面加上Server地址）
			if (!Util.isEmpty(props.getProperty("wx.pay.url.query"))) { // 查询
				props.setProperty("wx.pay.url.query",Util.getSysUrl("wx.pay.url.query")); 
			}
			if (!Util.isEmpty(props.getProperty("wx.pay.url.scan"))) { // 主扫（扫描顾客手机）提交
				props.setProperty("wx.pay.url.scan",Util.getSysUrl("wx.pay.url.scan")); 
			}
			if (!Util.isEmpty(props.getProperty("wx.pay.url.scanResult"))) { // 主扫（结果）查询
				props.setProperty("wx.pay.url.scanResult",Util.getSysUrl("wx.pay.url.scanResult"));
			}
			if (!Util.isEmpty(props.getProperty("wx.pay.url.refund"))) { //退货
				props.setProperty("wx.pay.url.refund",Util.getSysUrl("wx.pay.url.refund"));
			}
			// 注册
			Sys.sysreg();
			//获取服务器端配置
			Boolean use_server_cfg = Util.parseBoolean(props.getProperty("use_server_cfg"), true);
			if (use_server_cfg){
				Sys.loadServerConfig();
			}else{
				logger.info("从本地配置获取信息，若想从服务器获取配置信息请修改use_server_cfg配置项。");
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

	/**
	 * 注册服务器
	 */
	@Path("/regsrv")
	@Produces(MediaType.TEXT_PLAIN)
	public static void sysreg() {
		Util.isReg = false;
		InetAddress ia = null;
		String terminal_id = null;
		Map<String, Object> params = null;
		try {
			ListProperties props = Util.getProperties();
			String reg_url = Util.getSysUrl("terminal.reg_url");	
			logger.info("注册:{}", reg_url);
			// 生成注册信息
			ia = InetAddress.getLocalHost();
			terminal_id = Util.getLocalMac(ia);
			params = new HashMap<String, Object>();
			params.put("terminal_id", terminal_id);
			params.put("terminal_sn", props.getProperty("terminal.sn"));
			params.put("terminal_code", props.getProperty("terminal.code"));
			params.put("terminal_name", props.getProperty("terminal.name"));
			params.put("terminal_type", props.getProperty("terminal.type"));
			params.put("com_code", props.getProperty("com_code"));
			params.put("ip_addr", ia==null?"":ia.getHostAddress());
			params.put("port", "");
			// 提交信息注册
			String str = Util.doPost(reg_url, params, "UTF-8");
			logger.info("返回信息:{}", str);
			ResInfo reg_info = JSON.parseObject(str, ResInfo.class);
			if (!reg_info.getSuccess()) {
				logger.error("注册失败:{}", str);
			}
			JSONObject reg_result = reg_info.getResult();
			if (reg_result != null) {
				Util.isReg = Util.parseBoolean(reg_result.get("isRegisted"));
			}
			if (!Util.isReg) {
				logger.error("该服务器未注册，请与管理员联系，本机注册ID为{}，请正确设置注册序列号（terminal.sn）信息后再启动服务。", terminal_id);		
			}else{
				Util.srvId = Util.parseStr(reg_result.get("id"));
				props.setProperty("srv_id", Util.srvId);
				Util.loadProperties(props);
				logger.info("注册成功！SrvId:" + Util.srvId);
			}
		} catch (UnknownHostException e) {
			Util.isReg = false;
			e.printStackTrace();
			logger.error("注册失败:{}", e.getMessage());
			return;
		} catch (SocketException e) {
			Util.isReg = false;
			e.printStackTrace();
			logger.error("注册失败:{}", e.getMessage());
			return;
		}catch(Exception e){
			Util.isReg = false;
			e.printStackTrace();
			logger.error("注册失败:{}", e.getMessage());
		}
	}
	/**
	 * 从服务端加载门店配置信息
	 */
	@Path("/reloadserverconfig")
	@Produces(MediaType.TEXT_PLAIN)
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static void loadServerConfig() {
		ListProperties props = Util.getProperties();
		String com_code = props.getProperty("com_code");
		if(Util.isEmpty(com_code)){
			logger.error("获取的企业编码(com_code错误!)请设置企业编码后从新启动客户端!");
			return;
		}

		String get_com_cfg_url = Util.getSysUrl("url.get_com_cfg");
		if(Util.isEmpty(get_com_cfg_url)){
			logger.error("请设置正确的获取配置企业配置信息的URL地址(url.get_com_cfg)!");
			return;
		}
		logger.info("连接服务器获取信息:{}", get_com_cfg_url);
		StringBuffer com_cfg = new StringBuffer();
		try {
			// 建立连接
			URL url = new URL(get_com_cfg_url);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			// 设置连接属性
			httpConn.setDoOutput(true);// 使用 URL 连接进行输出
			httpConn.setDoInput(true);// 使用 URL 连接进行输入
			httpConn.setUseCaches(false);// Post 请求不能使用缓存
			httpConn.setRequestMethod("GET");// 设置URL请求方法
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setConnectTimeout(60000);//设置连接超时  
			//如果在建立连接之前超时期满，则会引发一个 java.net.SocketTimeoutException。超时时间为零表示无穷大超时。  
			httpConn.setReadTimeout(60000);//设置读取超时  
			// 获得响应状态
			int responseCode = httpConn.getResponseCode();
			if (HttpURLConnection.HTTP_OK == responseCode) {// 连接成功
				//
				BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "utf-8"));
				String line;

				while ((line = reader.readLine()) != null) {
					com_cfg.append(line);
				}
				reader.close();
			}else{
				logger.error("连接服务器失败,responseCode:{}", responseCode);
			}
			// connection.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("获取到服务器配置出错:{}", ex.toString());
		}
		//
		if (!com_cfg.toString().isEmpty()){
			Map<String, Object> cfg = JSONObject.parseObject(com_cfg.toString(), HashMap.class);
			Boolean success = (Boolean) cfg.get("success");
			if (success){
				cfg = (Map<String, Object>) cfg.get("result");

				for (Map.Entry<String, Object> entry : cfg.entrySet()) {
					if (entry.getKey() != null && entry.getKey().indexOf("url.") != -1){
						props.setProperty(entry.getKey(), Util.getServerUrl(Util.parseString(entry.getValue())));
					}else{
						props.setProperty(entry.getKey(), Util.parseString(entry.getValue()));
					}
				    System.out.println(entry.getKey() + " = " + entry.getValue());  
				}
				if (cfg.get("wx_cfg") != null) {
					Map wx_cfg = (Map) cfg.get("wx_cfg");
					props.setProperty("wx.pay.vtype", wx_cfg.get("v_type").toString());
					props.setProperty("wx.pay.appid", wx_cfg.get("app_id").toString());
					props.setProperty("wx.pay.webtoken", wx_cfg.get("web_token").toString());
					props.setProperty("wx.pay.url", wx_cfg.get("pay_url").toString());
					props.setProperty("wx.pay.url.redirectUri", wx_cfg.get("redirect_uri").toString());
				}
				Util.loadProperties(props);
			}else{
				String errMsg = "[" + cfg.get("errCode").toString() + "]" + cfg.get("errMessage").toString();
				logger.error("获取服务器获取配置信息失败：" + errMsg);
			}
		}
	}
}
