package com.xl.pay.srv;

import com.xl.util.Util;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;
/*
 * 查询
 */
@WebServlet
@Path("/qry")
public class Qry {
	//private static Logger logger = LoggerFactory.getLogger(Qry.class);
	@ContextResource
	ServletRequest request;
	/**
	 * 查询款机终端的最大流水
	 * 	trans_id	业务编码，固定为B3
	 * 	partner_id	商户编码
	 * 	com_code	门店编码
	 * 	pos_no		收款设备编号
	 */
	@POST
	@Path("/getmaxbillid")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String getMaxBillId() {
		return Util.getServerResponseStringByParam(Util.getServerUrl("url.max_bill_id"), null, request.getRequest().getParameterMap());
	}
	/**
	 * 
	 * 
	 */
	@POST
	@Path("/gettraderec")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String getTradeRec() {
		return Util.getServerResponseStringByParam(Util.getServerUrl("url.get_trade_rec"), null, request.getRequest().getParameterMap());
	}
}
