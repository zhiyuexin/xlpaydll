package com.xl.pay.srv;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;



import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xl.util.Util;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.PathParam;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;


@WebServlet
@Path("/get_pay_url")
/*
 * 获取支付被扫二维码代码
 */
public class GetPayUrl {
	
	private static Logger logger = LoggerFactory.getLogger(GetPayUrl.class);
	@ContextResource
	ServletRequest request;

	@POST
	@Path("/{payCode}/{erpType}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.APPLICATION_JSON)
	@NeedToken
	public Map<String,Object> get_pay_url(@PathParam("payCode") String payCode,@PathParam("erpType") String erpType) {
		return get_pay_url(payCode, erpType, null);
	}
	
	@POST
	@Path("/{payCode}/{erpType}/{extParams}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.APPLICATION_JSON)
	@NeedToken
	public Map<String,Object> get_pay_url(@PathParam("payCode") String payCode,@PathParam("erpType") String erpType,@PathParam("extParams") String extParams) {

		Map<String,Object> result = new HashMap<String, Object>();
		String pay_url = null, //支付链接
				app_id = null, 
				web_token = null,//微信支付加密参数
				v_type = null,
				redirect_uri = null,
				login_terrace_code = null;
		
		PayParam payParam = new PayParam(payCode,erpType);
		if (!payParam.success){
			result.put("srvError", true);
			result.put("success", false);
			result.put("errCode", payParam.errCode);
			result.put("errMessage", payParam.errMessage);
			return result;
		}
		String payType = payParam.payType;
		erpType = payParam.erpType;
		try {
			if (payType.equalsIgnoreCase("wx")){//判断payType是否为wx
				pay_url = Util.getProperty(payType + ".pay.url");
				app_id = Util.getProperty(payType + ".pay.appid");
				web_token = Util.getProperty(payType + ".pay.webtoken");
				v_type = Util.getProperty(payType + ".pay.vtype");
				redirect_uri = Util.getProperty(payType + ".pay.url.redirectUri");
				login_terrace_code = Util.getProperty(payType + ".pay.loginTerraceCode");
				if (pay_url==null || app_id==null || web_token==null || v_type==null || redirect_uri==null){
					result.put("srvError", true);
					result.put("success", false);
					result.put("errCode", "E507");
					result.put("errMessage", "invalid configuration of " + payType + " pay url");
					return result;
				}
				if (redirect_uri!=null){
					redirect_uri = redirect_uri
							.replace("{web_token}", web_token==null?"":web_token)
							.replace("{v_type}", v_type==null?"":v_type)
							.replace("{erp_type}", erpType==null?"":erpType)
							.replace("{partner_id}", erpType==null?"":erpType)
							.replace("{login_terrace_code}", login_terrace_code==null?"":login_terrace_code);

					redirect_uri = URLEncoder.encode(redirect_uri,"utf-8").replace(".", "%2E");
				}
				pay_url = pay_url
						.replace("{app_id}", app_id==null?"":app_id)
						.replace("{redirect_uri}", redirect_uri==null?"":redirect_uri);
				result.put("srvError", false);
				result.put("success", true);
				result.put("pay_url", pay_url);
				GetSaleTag serverSaleTag =  new GetSaleTag();
				result.put("server_sale_tag", serverSaleTag.getSaleTag());
			 	return result;
			}else{
				result.put("srvError", true);
				result.put("success", false);
				result.put("errCode", "E507");
				result.put("errMessage", "invalid configuration of " + payType + " pay url");
				return result;
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("srvError:true, success:false,errCode:E508,errMessage:failed to generate pay url}");
			result.put("srvError", true);
			result.put("success", false);
			result.put("errCode", "E508");
			result.put("errMessage", "failed to generate pay url");
			return result;
		}
	}
}
