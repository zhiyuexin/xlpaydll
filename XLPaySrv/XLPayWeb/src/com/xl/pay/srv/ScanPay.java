package com.xl.pay.srv;

import com.xl.util.Util;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.PathParam;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;
@WebServlet
@Path("/scan_pay")


/*
 * ��ɨ-ɨ�˿ͣ�api��
 */
public class ScanPay {
//	private static Logger logger = LoggerFactory.getLogger(ScanPay.class);
	@ContextResource
	ServletRequest request;
	
	@POST
	@Path("/{payCode}/{erpType}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String scan_pay(@PathParam("payCode") String payCode,@PathParam("erpType") String erpType) {
		return scan_pay(payCode, erpType, null);
	}
	
	@POST
	@Path("/{payCode}/{erpType}/{extParams}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String scan_pay(@PathParam("payCode") String payCode,@PathParam("erpType") String erpType,@PathParam("extParams") String extParams) {
		return Util.getServerResponseStringByParam(payCode, erpType, "scan", extParams, request.getRequest().getParameterMap());
	}
}
