package com.xl.pay.srv;

import com.xl.util.Util;

public class PayParam {
//	private static Logger logger = LoggerFactory.getLogger(PayParam.class);
	public static String[] support_pay={"wx","zfb"};
	public String payCode;
	public String payType;
	public String erpType;
	public String errCode;
	public String errMessage;
	public Boolean success;
	/*
	 * 处理支付编码和商户编码payType,erpType（partner_id）
	 * 将用户的支付编码（pay_code）翻译成对应的支付方式（pay_type）
	 */
	public PayParam(String payCode, String erpType){
		this.payCode = payCode;
		this.erpType = erpType; //partner_id
		this.success = false;
				
		if (erpType==null || erpType.equals("")){
			this.errCode = "E502";
			this.errMessage = "invalid param erpType(partner_id)";
			return;
		}
		if (payCode==null || payCode.equals("")){
			this.errCode = "E503";
			this.errMessage = "invalid param payCode";
			return;
		}
		
		Boolean findProp = false;//查询状态标记
		for(int i=0;i<support_pay.length;i++){
			//遍历数组查询配置文件中是否匹配	wx.pay.jl.code
			String propValue = Util.getProperty(support_pay[i] + ".pay." + erpType.toLowerCase() + ".code");
			if (propValue!=null && !propValue.equals("")){
				propValue = "," + propValue + ",";
				if (propValue.indexOf(","+payCode+",")!=-1){//查找支付编码 
					findProp = true;
					this.payType = support_pay[i];
					break;
				}
			}
		}
		if (!findProp){
			this.errCode = "E504";
			this.errMessage = "DoParam : failed to find configuration of pay_code";
			return;
		}
		this.success = true;
	}
	
	public static boolean isSupportPay(String payType) {
		for(int i=0; i<support_pay.length; i++){
			if (payType.equalsIgnoreCase(support_pay[i])) {
				return true;
			}
		}
		return false;
	}
}
