package com.xl.pay.srv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.xl.util.Util;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
/*
 * 获取服务端商品折扣
 */
@WebServlet
@Path("/get_sale_tag")
public class GetSaleTag {
	@POST
	@Path("")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.APPLICATION_JSON)
	@NeedToken
	public Map<String,Object> getSaleTag(){
		Map<String,Object> result = new HashMap<String, Object>();
		List<Map<String,Object>> detail_list = new ArrayList<Map<String, Object>>();
		
		String  sale_tag_detail_discountlimit = null,
				default_sale_tag_detail_discountlimit = null,
				sale_tag_detail_code = null,
				propKey=null;
		String[] sale_tag_value = null;
		
		String config = Util.getProperty("sale_tag.config");
		String sale_tag_client_revisable = Util.getProperty("sale_tag.client_revisable");
		result.put("config", config==null?0:config);
		result.put("client_revisable",sale_tag_client_revisable==null?false:sale_tag_client_revisable.equalsIgnoreCase("true"));
		String stdc = Util.getProperty("sale_tag.detail.code");//a,b,xx,c
		
		if (stdc==null || stdc.equals("")){
			result.put("detail", detail_list);
			return result;
		}
		
		sale_tag_value = stdc.split(",");

		default_sale_tag_detail_discountlimit = Util.getProperty("sale_tag.detail.discountlimit");
		if(default_sale_tag_detail_discountlimit == null || default_sale_tag_detail_discountlimit.equals("") ){
			default_sale_tag_detail_discountlimit = "0";
		}
		for(int i=0;i<sale_tag_value.length;i++){
			propKey = sale_tag_value[i].toString();
			sale_tag_detail_code = Util.getProperty("sale_tag.detail.code."+propKey);
			sale_tag_detail_discountlimit = Util.getProperty("sale_tag.detail.discountlimit."+propKey);

			if(sale_tag_detail_code == null){
				sale_tag_detail_code = propKey;
			}
			if(sale_tag_detail_discountlimit == null || sale_tag_detail_discountlimit.equals("") ){
				sale_tag_detail_discountlimit = default_sale_tag_detail_discountlimit;
			}
			Map<String,Object> detail = new HashMap<String, Object>();
			detail.put("code",propKey);
			detail.put("code_value", sale_tag_detail_code);
			detail.put("discountlimit",sale_tag_detail_discountlimit);

			detail_list.add(detail);
		}

		result.put("detail", detail_list);
		
		return result;
	}
	
}
