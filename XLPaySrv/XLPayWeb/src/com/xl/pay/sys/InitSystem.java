package com.xl.pay.sys;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xl.pay.srv.Query;
import com.xl.pay.srv.Sys;

/**
 * Servlet implementation class InitSystem
 */
public class InitSystem extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(Query.class);
	
	@Override
	public void init() throws ServletException {
        super.init();
        logger.info("系统启动");
        
        Sys.SystemConfigFile = getInitParameter("SystemConfigFile");
        Sys s = new Sys();
        s.loadSystemConfig();
    }
    
}
