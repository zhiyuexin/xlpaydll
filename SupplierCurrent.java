package com.xl.scm.query;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xl.exception.ExceptionCode;
import com.xl.exception.LoggableEJBException;
import com.xl.scm.query.i.SupplierCurrentLocal;
import com.xl.scm.query.i.SupplierCurrentRemote;
import com.xl.sys.PrincipalHelperLocal;
import com.xl.util.EncryptUtil;
import com.xl.util.HttpInvoker;
import com.xl.util.Util;
import com.xl.util.wrapper.MapObject;

/**
 * Session Bean implementation class SupplierCurrent
 */
@SuppressWarnings("rawtypes")
@Stateless
public class SupplierCurrent implements SupplierCurrentRemote,
		SupplierCurrentLocal {

	private Logger logger = LoggerFactory.getLogger(SupplierCurrent.class);

	@EJB
	private PrincipalHelperLocal ph;

	public static void main(String[] args) {
		// List<Long> reqids = new ArrayList<Long>();
		// reqids.add(1L);
		// reqids.add(2L);
		// try {
		// System.out.println(JSONObject.toJSONString(reqids));
		//
		// List<Map> result = new ArrayList<Map>();
		// String s =
		// "{\"message\":\"ok\",\"result\":[{\"oaopername\":\"李海萌\",\"oastatus\":\"归档\",\"oaworkflow\":[{\"lastname\":\"李海萌\",\"nodename\":\"1.培训专员发起\",\"operatetime\":\"2015-01-04 18:23:29\",\"receivetime\":\"2015-01-04 18:22:03\",\"status\":\"已提交\",\"workflowname\":\"部经理资格认证报名流程\"},{\"lastname\":\"朱丽红\",\"nodename\":\"2.人力资源处主任审批\",\"operatetime\":\"2015-01-04 18:26:40\",\"receivetime\":\"2015-01-04 18:23:29\",\"status\":\"已提交\",\"workflowname\":\"部经理资格认证报名流程\"},{\"lastname\":\"贾建辉\",\"nodename\":\"3.企业管理经理审批\",\"operatetime\":\"2015-01-04 21:51:02\",\"receivetime\":\"2015-01-04 18:26:40\",\"status\":\"已提交\",\"workflowname\":\"部经理资格认证报名流程\"},{\"lastname\":\"张伟\",\"nodename\":\"4.商学院专员审核\",\"operatetime\":\"2015-01-05 10:10:44\",\"receivetime\":\"2015-01-04 21:51:02\",\"status\":\"已提交\",\"workflowname\":\"部经理资格认证报名流程\"},{\"lastname\":\"李海萌\",\"nodename\":\"5.归档至申请人\",\"operatetime\":\"2015-01-05 10:27:41\",\"receivetime\":\"2015-01-05 10:10:44\",\"status\":\"归档\",\"workflowname\":\"部经理资格认证报名流程\"}],\"requestid\":86766},{\"oaopername\":\"张洪瑜\",\"oastatus\":\"公司财务结算科人员审批\",\"oaworkflow\":[{\"lastname\":\"张月梅\",\"nodename\":\"1.门店收银主管申请\",\"operatetime\":\"2015-01-05 08:36:50\",\"receivetime\":\"2015-01-05 08:36:50\",\"status\":\"已提交\",\"workflowname\":\"门店平帐申请流程-超市\"},{\"lastname\":\"张洪瑜\",\"nodename\":\"2.公司财务结算科主管审批\",\"operatetime\":\" \",\"receivetime\":\"2015-01-05 08:36:50\",\"status\":\"未查看\",\"workflowname\":\"门店平帐申请流程-超市\"}],\"requestid\":86780}],\"status\":\"0\"}";
		//
		// JSONObject o = JSONObject.parseObject(s);
		// System.out.println(o);
		// JSONArray arr = o.getJSONArray("result");
		// for (int i = 0; i < arr.size(); i++) {
		// result.add(arr.getJSONObject(i));
		// }
		// System.out.println(result);
		// String json = JSONObject.toJSONString(result);
		// System.out.println("ff: " + json);
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }
		// String uri =
		// "http://10.2.27.81:9080/rws/oa/flowlist/[86766,86780]/{\"ak\":\"i8fi0Erfv2311I0P\",\"sn\":\"6a7f0b24c5fbc63db4ff028463fd0eaf\",\"ts\":1428051895252}";
		// try {
		// Map m = new HashMap<String, String>();
		// m.put("ts", "123");
		// // uri = URLEncoder.encode(uri, "UTF-8");
		// String s = HttpInvoker.doPost(uri);
		// System.out.println(s);
		// } catch (UnsupportedEncodingException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}

	@Override
	public List getOpenAccount(Map para) {
		List<Map> result = new ArrayList<Map>();

		try {
			MapObject args = new MapObject(para);
			String comcode = (String) args.obtain("comcode");
			String supids = (String) args.get("supid");
			if (supids == null)
				supids = (String) args.get("supids");

			if (supids == null)
				supids = ph.getExtCallerSupplierIds(comcode);

			if (supids == null || supids.trim().equals(""))
				throw new LoggableEJBException(ExceptionCode.KEY_MISMATCHING,
						"未指定参数供应商ID (supid)");

			logger.debug("获取[{}]-{} 的未足售卖率单据", comcode, supids);

			// http://10.2.26.47:9080/rws/iq
			StringBuffer url = new StringBuffer(
					Util.getProperty("ws.internal.iq.url"));
			// /open_in_bill/025/570/{"ak":"akey","sn":"sn_code","ts":xxxxx}
			url.append("/open_in_bill/");
			url.append(comcode);
			url.append("/");
			url.append(supids);
			url.append("/{\"ak\":\"");
			url.append(Util.getProperty("ws.internal.iq.ak"));
			url.append("\",\"sn\":\"");
			Long ts = Calendar.getInstance().getTimeInMillis();
			url.append(EncryptUtil.MD5Encode(Util
					.getProperty("ws.internal.iq.id")
					+ Util.getProperty("ws.internal.iq.ak")
					+ ts
					+ Util.getProperty("ws.internal.iq.sk")));
			url.append("\",\"ts\":");
			url.append(ts.toString());
			url.append("}");
			logger.debug("发送IQ Webservice请求: {}", url.toString());

			// POST
			String s = HttpInvoker.doPost(url.toString());
			logger.debug("接收响应串: {}", s);

			JSONObject json = JSONObject.parseObject(s);
			if (!"0".equals(json.getString("status"))) {
				// 错误
				throw new LoggableEJBException(ExceptionCode.REMOTE_EXCEPTION,
						json.getString("message"));
			} else {
				// 成功
				JSONArray arr = json.getJSONArray("result");
				for (int i = 0; i < arr.size(); i++) {
					result.add(arr.getJSONObject(i));
				}
			}
			return result;
		} catch (Exception e) {
			if (e instanceof LoggableEJBException)
				throw (LoggableEJBException) e;
			throw new LoggableEJBException(ExceptionCode.EJB_EXCEPTION, e);
		}
	}

	@Override
	public List getRefundList(Map para) {
		List<Map> result = new ArrayList<Map>();

		try {
			MapObject args = new MapObject(para);
			String comcode = (String) args.obtain("comcode");
			Number billid = (Number) args.get("billid");

			logger.debug("获取[{}]-结算单({})包含的返厂记录", comcode, billid);

			// http://10.2.26.47:9080/rws/iq
			StringBuffer url = new StringBuffer(
					Util.getProperty("ws.internal.iq.url"));
			// /open_in_bill/025/570/{"ak":"akey","sn":"sn_code","ts":xxxxx}
			url.append("/refund/list/");
			url.append(comcode);
			url.append("/");
			url.append(billid.toString());
			url.append("/{\"ak\":\"");
			url.append(Util.getProperty("ws.internal.iq.ak"));
			url.append("\",\"sn\":\"");
			Long ts = Calendar.getInstance().getTimeInMillis();
			url.append(EncryptUtil.MD5Encode(Util
					.getProperty("ws.internal.iq.id")
					+ Util.getProperty("ws.internal.iq.ak")
					+ ts
					+ Util.getProperty("ws.internal.iq.sk")));
			url.append("\",\"ts\":");
			url.append(ts.toString());
			url.append("}");
			logger.debug("发送IQ Webservice请求: {}", url.toString());

			// POST
			String s = HttpInvoker.doPost(url.toString());
			logger.debug("接收响应串: {}", s);

			JSONObject json = JSONObject.parseObject(s);
			if (!"0".equals(json.getString("status"))) {
				// 错误
				throw new LoggableEJBException(ExceptionCode.REMOTE_EXCEPTION,
						json.getString("message"));
			} else {
				// 成功
				JSONArray arr = json.getJSONArray("result");
				for (int i = 0; i < arr.size(); i++) {
					result.add(arr.getJSONObject(i));
				}
			}
			return result;
		} catch (Exception e) {
			if (e instanceof LoggableEJBException)
				throw (LoggableEJBException) e;
			throw new LoggableEJBException(ExceptionCode.EJB_EXCEPTION, e);
		}
	}
}
