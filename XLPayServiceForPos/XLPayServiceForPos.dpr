program XLPayServiceForPos;

uses
  Forms,
  FrmMain in 'FrmMain.pas' {XLPayService},
  DmMain in 'DmMain.pas' {DM: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TXLPayService, XLPayService);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
