object DM: TDM
  OldCreateOrder = False
  Left = 407
  Top = 212
  Height = 332
  Width = 476
  object ConnLocal: TUniConnection
    ProviderName = 'SQLite'
    Left = 37
    Top = 191
  end
  object HttpServer: TRtcHttpServer
    ServerPort = '7890'
    Left = 40
    Top = 145
  end
  object DataProvider1: TRtcDataProvider
    Server = HttpServer
    OnCheckRequest = DataProvider1CheckRequest
    OnDataReceived = DataProvider1DataReceived
    Left = 118
    Top = 146
  end
  object RtcDataRequest1: TRtcDataRequest
    Client = RtcHttpClient1
    Left = 129
    Top = 94
  end
  object RtcHttpClient1: TRtcHttpClient
    Left = 38
    Top = 104
  end
end
