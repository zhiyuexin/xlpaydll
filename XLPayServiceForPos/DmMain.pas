unit DmMain;

interface

uses
  SysUtils, Classes, rtcTcpSrv, rtcConn, rtcDataCli, rtcHttpCli,
  rtcDataSrv, rtcInfo, rtcHttpSrv, DB, DBAccess, Uni;

type
  TDM = class(TDataModule)
    ConnLocal: TUniConnection;
    HttpServer: TRtcHttpServer;
    DataProvider1: TRtcDataProvider;
    RtcDataRequest1: TRtcDataRequest;
    RtcHttpClient1: TRtcHttpClient;
    procedure DataProvider1CheckRequest(Sender: TRtcConnection);
    procedure DataProvider1DataReceived(Sender: TRtcConnection);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{$R *.dfm}

procedure TDM.DataProvider1CheckRequest(Sender: TRtcConnection);
var
  Srv: TRtcDataServer absolute Sender;
begin
  if Srv.Request.FileName = '/test' then
  begin
    Srv.Accept;
  end
  else
  begin
    Srv.Response.Status(404, 'File not found.');
    Srv.Write(Srv.Request.FileName + ' not found.');
  end;
end;

procedure TDM.DataProvider1DataReceived(Sender: TRtcConnection);
var
  Srv: TRtcDataServer absolute Sender;
  RevContent: RtcString;
begin
  if Srv.Request.Complete then
  begin
    RevContent := Srv.Read;

    Srv.Response['Content-Type'] := 'application/json; charset=UTF-8';
    Srv.Response['Server'] := 'XingLongPay Pos Server';
    Srv.Response['Date'] := DateTime2Str(Now);

    Srv.Response.Status(500, 'err');
    Srv.Write('error');
  end;
end;

end.
