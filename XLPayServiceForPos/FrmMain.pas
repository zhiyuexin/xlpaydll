unit FrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RzTray, Menus, ExtCtrls, ComCtrls, StdCtrls, DmMain, rtcInfo,
  rtcConn, rtcTcpSrv;

type
  TXLPayService = class(TForm)
    TrayIcon: TRzTrayIcon;
    PopupMenuTray: TPopupMenu;
    NAbout: TMenuItem;
    N2: TMenuItem;
    NShowWindow: TMenuItem;
    NHideWindow: TMenuItem;
    NQuit: TMenuItem;
    pnlTop: TPanel;
    pnlBody: TPanel;
    StatusBar1: TStatusBar;
    pnlLeft: TPanel;
    pnlMain: TPanel;
    Splitter1: TSplitter;
    edtInfo: TMemo;
    TcpServer: TRtcTcpServer;
    procedure FormCreate(Sender: TObject);
    procedure NAboutClick(Sender: TObject);
    procedure NHideWindowClick(Sender: TObject);
    procedure NQuitClick(Sender: TObject);
    procedure NShowWindowClick(Sender: TObject);
    procedure TcpServerClientConnect(Sender: TRtcConnection);
    procedure TcpServerClientDisconnect(Sender: TRtcConnection);
    procedure TcpServerListenStart(Sender: TRtcConnection);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  XLPayService: TXLPayService;

implementation

{$R *.dfm}

procedure TXLPayService.FormCreate(Sender: TObject);
begin
  edtInfo.Clear;
  TcpServer.ServerPort := '7890';
  TcpServer.Listen();
end;

// ���̲˵�������
procedure TXLPayService.NAboutClick(Sender: TObject);
begin
  ShowMessage('XLPayServiceForPos');
end;

// ���̲˵������ش���
procedure TXLPayService.NHideWindowClick(Sender: TObject);
begin
  TrayIcon.MinimizeApp;
end;

// ���̲˵����˳�
procedure TXLPayService.NQuitClick(Sender: TObject);
begin
  Close();
end;

// ���̲˵�����ʾ����
procedure TXLPayService.NShowWindowClick(Sender: TObject);
begin
  TrayIcon.RestoreApp;
end;

procedure TXLPayService.TcpServerClientConnect(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(TcpServerClientConnect)
  else
    edtInfo.Lines.Add('TcpServerClientConnect');
end;

procedure TXLPayService.TcpServerClientDisconnect(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(TcpServerClientDisconnect)
  else
    edtInfo.Lines.Add('TcpServerClientDisconnect');
end;

procedure TXLPayService.TcpServerListenStart(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(TcpServerListenStart)
  else
    edtInfo.Lines.Add('TcpServerListenStart');
end;

end.
