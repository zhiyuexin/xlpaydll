object XLPayService: TXLPayService
  Left = 1040
  Top = 497
  Width = 660
  Height = 401
  Caption = 'XLPayServiceForPos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 644
    Height = 55
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
  end
  object pnlBody: TPanel
    Left = 0
    Top = 55
    Width = 644
    Height = 289
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 149
      Top = 0
      Height = 289
    end
    object pnlLeft: TPanel
      Left = 0
      Top = 0
      Width = 149
      Height = 289
      Align = alLeft
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
    end
    object pnlMain: TPanel
      Left = 152
      Top = 0
      Width = 492
      Height = 289
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 1
      object edtInfo: TMemo
        Left = 2
        Top = 2
        Width = 488
        Height = 285
        Align = alClient
        Lines.Strings = (
          'edtInfo')
        TabOrder = 0
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 344
    Width = 644
    Height = 19
    Panels = <>
  end
  object TrayIcon: TRzTrayIcon
    HideOnStartup = True
    Hint = 'XLPayServiceForPos'
    PopupMenu = PopupMenuTray
    Left = 33
    Top = 283
  end
  object PopupMenuTray: TPopupMenu
    Left = 74
    Top = 284
    object NAbout: TMenuItem
      Caption = #20851#20110'(&A)'
      OnClick = NAboutClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object NShowWindow: TMenuItem
      Caption = #26174#31034#31383#20307'(&S)'
      OnClick = NShowWindowClick
    end
    object NHideWindow: TMenuItem
      Caption = #38544#34255#31383#20307'(&H)'
      OnClick = NHideWindowClick
    end
    object NQuit: TMenuItem
      Caption = #36864#20986'(&Q)'
      OnClick = NQuitClick
    end
  end
  object TcpServer: TRtcTcpServer
    ServerPort = '7890'
    OnClientConnect = TcpServerClientConnect
    OnClientDisconnect = TcpServerClientDisconnect
    OnListenStart = TcpServerListenStart
    Left = 26
    Top = 71
  end
end
