program ReportDesigner;

uses
  Windows, SysUtils;

const
DLL_FILE = 'xlpay.dll';

procedure r(p_pay_param: String);
  type
    Taddc = function(pay_param: PChar; return_param: PChar): Integer; stdcall;
  var
    hh: THandle;
    addc: Taddc;
    request_param: PChar;
    return_param: PChar;
begin
  if not FileExists(DLL_FILE) then
  begin
    Exit;
  end;
  hh := LoadLibrary(DLL_FILE);
  try
    if hh <> 0 then
    begin
      @addc := GetProcAddress(hh, PChar('request'));
    end;
    if not (@addc = nil) then
    begin
      request_param := PChar(p_pay_param);
      return_param := StrAlloc(4096 * SizeOf(Char));
      addc(request_param, return_param);
      StrDispose(return_param);
    end
    else
    begin
      RaiseLastWin32Error;
    end;
  finally
    FreeLibrary(hh);
  end;
end;

begin
  r('{"trans_id": "RD"}');
end.
