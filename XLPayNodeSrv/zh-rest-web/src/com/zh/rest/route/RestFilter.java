package com.zh.rest.route;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.zh.annotation.rest.ApplicationPath;
import com.zh.exception.ApplicationException;
import com.zh.exception.ExceptionMessage;
import com.zh.exception.LoggableEJBException;
import com.zh.exception.RestfulException;
import com.zh.rest.resource.ResourceConfig;
import com.zh.rest.resource.ResourceManager;
import com.zh.rest.resource.WSResult;

public abstract class RestFilter implements Filter {

	protected Logger LOGGER = LoggerFactory.getLogger(RestFilter.class);

	protected static final String CHARACTER_ENCODING = "UTF-8";
	protected static final String REQUEST_URI_CHARACTER = "gb2312";

	protected static final String PARAM_NAME_APPLICATIONS = "Applications";

	protected String domain = null;

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		domain = fConfig.getInitParameter("domain");
		LOGGER.info("RestFilter domain: {}", domain);

		String apps = fConfig.getInitParameter(PARAM_NAME_APPLICATIONS);
		LOGGER.debug("PARAM_NAME_APPLICATIONS: {}", apps);

		try {
			if (apps != null && apps.length() > 0) {
				String[] appArray = apps.split(",");
				for (String app : appArray) {
					registApplication(app.trim());
				}
			}
		} catch (InstantiationException e) {
			e.printStackTrace(System.out);
		} catch (IllegalAccessException e) {
			e.printStackTrace(System.out);
		} catch (ClassNotFoundException e) {
			e.printStackTrace(System.out);
		} catch (RestfulException e) {
			e.printStackTrace(System.out);
		}
	}

	private void registApplication(String app) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException, RestfulException {
		Class<?> clazz = Thread.currentThread().getContextClassLoader()
				.loadClass(app);

		if (clazz.isAnnotationPresent(ApplicationPath.class)) {
			ResourceConfig rc = (ResourceConfig) clazz.newInstance();
			LOGGER.debug("注册RESTful应用: {}，@Path=(\"{}\")", rc.getClass()
					.getName(), rc.getApplicationPath());
			ResourceManager.newApplication(rc);
		}
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	protected void setHeaderDomain(HttpServletResponse response) {
		if (domain != null) {
			response.addHeader("Access-Control-Allow-Origin", domain);
			response.addHeader("Access-Control-Max-Age", new Integer(
					60 * 60 * 24).toString());
			response.addHeader("Access-Control-Allow-Methods",
					"GET, POST, PUT, DELETE, OPTIONS, HEAD");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "no-cache");// 浏览器和缓存服务器都不应该缓存页面信息
			response.addHeader("Cache-Control", "no-store");// 请求和响应的信息都不应该被存储在对方的磁盘系统中；
			response.addHeader("Cache-Control", "must-revalidate");

			// response.addHeader("Access-Control-Allow-Credentials", "true");
			// response
			// .addHeader(
			// "Access-Control-Request-Headers",
			// "Authorization, User-Agent, If-Modified-Since, Cache-Control, Content-Type, Referer, Accept-Encoding, Cookie, Connection, Content-Length, Host, Referer");
			// response
			// .addHeader(
			// "Access-Control-Allow-Headers",
			// "Authorization, User-Agent, If-Modified-Since, Cache-Control, Content-Type, Referer, Accept-Encoding, Cookie, Connection, Content-Length, Host, Referer");
		}
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		LOGGER.debug("Request.Encodeing:{}", request.getCharacterEncoding());
		LOGGER.debug("Response.Encodeing:{}", response.getCharacterEncoding());

		if (!(request instanceof HttpServletRequest)
				|| !(response instanceof HttpServletResponse)) {
			throw new ServletException(
					"Rest doesn't support non-HTTP request or response.");
		}

		this.setHeaderDomain((HttpServletResponse) response);

		// 设置响应字符集UTF-8
		response.setCharacterEncoding(RestFilter.CHARACTER_ENCODING);

		HttpServletRequest req = (HttpServletRequest) request;
		LOGGER.debug("REST方法: {} - URI:{}", req.getMethod(), req.getRequestURI());

		// 请求路径，去掉Web上下文根路径
		String restPath = req.getRequestURI().substring(
				req.getContextPath().length());

		// 请求串转化UTF-8
		restPath = URLDecoder
				.decode(restPath, RestFilter.REQUEST_URI_CHARACTER);
		LOGGER.debug("REST请求串: {}", restPath);

		Handler handle = null;
		WSResult result = new WSResult();

		try {
			handle = Handler.handle(restPath, req);
			// pass the request along the filter chain
			if (handle == null) {
				LOGGER.debug("没有REST资源，走正常Web Servlet");
				chain.doFilter(request, response);
				return;
			}

			// 终结请求, 完全由过滤器分配方法.
			result.setResult(handle.action());
			if (result.getResult() != null) {
				LOGGER.debug(result.getResult().toString().length() > 200 ? result
						.getResult().toString().substring(0, 200)
						: result.getResult().toString());
			}
			result.setStatus("0");
			result.setMessage("ok");

		} catch (Throwable e) {
			if (e instanceof LoggableEJBException) {
				LoggableEJBException le = (LoggableEJBException) e;
				if (le.getCause() instanceof ApplicationException) {
					ApplicationException ae = (ApplicationException) le
							.getCause();
					if (!ae.getLogged()) {
						ae.setLogged(true);
						LOGGER.error("加载系统核心模块失败：", ae);
					}
					result.setStatus(ae.getError().getExceptionCode());
				} else {
					if (!le.getLogged()) {
						le.setLogged(true);
						LOGGER.error("加载系统核心模块失败：", le);
					}
					result.setStatus(le.getError().getExceptionCode());
				}
			} else if (e instanceof ApplicationException) {
				ApplicationException ae = (ApplicationException) e;
				if (!ae.getLogged()) {
					ae.setLogged(true);
					LOGGER.error("加载系统核心模块失败：", ae);
				}
				result.setStatus(ae.getError().getExceptionCode());
			} else {
				LOGGER.error("加载系统核心模块失败：", e);
				result.setStatus(ExceptionMessage.APPLICATION_EXCEPTION
						.toString());
			}
			result.setMessage(e.getMessage());
		}
		
		PrintWriter out = response.getWriter();
		try {
			out.print(JSONObject.toJSONString(result, SerializerFeature.WriteMapNullValue));
		} finally {
			out.close();
		}
	}
}
