package com.zh.rest.route;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zh.annotation.rest.RolesAllow;
import com.zh.annotation.rest.RolesExclude;
import com.zh.exception.AccessDeniedException;
import com.zh.exception.ApplicationException;
import com.zh.rest.inject.Injectable;
import com.zh.rest.resource.Resource;
import com.zh.rest.resource.ResourceConfig;
import com.zh.rest.resource.ResourceManager;
import com.zh.rest.resource.ServletRequest;

public class Handler {

	private Logger LOGGER = LoggerFactory.getLogger(Handler.class);

	ResourceConfig application;
	Resource resource;
	String uri;
	ServletRequest request;

	private Handler(ResourceConfig application, Resource resource, String uri,
			HttpServletRequest request) throws AccessDeniedException {
		this.application = application;
		this.resource = resource;
		this.uri = uri;
		this.request = new ServletRequest(request);

		this.checkPermission();
	}

	/**
	 * 获取URI对应的Handler
	 * 
	 * @param uri
	 * @param request
	 * @return
	 * @throws AccessDeniedException
	 */
	public static Handler handle(String uri, HttpServletRequest request)
			throws AccessDeniedException {

		ResourceConfig app = ResourceManager.findApplication(uri);
		if (app == null)
			return null;

		Resource r = ResourceManager.findResource(uri, app);
		if (r == null)
			return null;

		// 检查ConfigPath访问配置
		if (!app.allowRemoteAccess(request))
			throw new AccessDeniedException();

		if (!app.checkToken(r, request))
			throw new AccessDeniedException();

		return new Handler(app, r, uri, request);
	}

	/**
	 * 执行资源方法，并根据生产类型返回数据。
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws ApplicationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public Object action() throws IllegalArgumentException,
			IllegalAccessException, ApplicationException,
			InstantiationException {
		LOGGER.info("执行资源{}.{}：{}", resource.getResourceClass().getName(),
				resource.getResourceMethod().getName(), uri);
		LOGGER.debug("Principal is {}", this.request.getRequest()
				.getUserPrincipal());

		// 去掉应用根路径
		uri = uri.substring(application.getApplicationPath().length());

		if (!uri.startsWith("/"))
			uri = "/" + uri;

		// 再去掉Class路径
		uri = uri.substring(resource.getClassRootPath().length());

		// 执行仅剩下MethodQueryString
		return Injectable.injectAndRun(this.resource, uri, request);
	}

	/**
	 * 检查角色许可
	 * 
	 * @throws AccessDeniedException
	 */
	private void checkPermission() throws AccessDeniedException {

		boolean havePermission = false;

		/**
		 * 首先判断方法是否有排除的角色。
		 */
		if (resource.getResourceMethod()
				.isAnnotationPresent(RolesExclude.class)) {

			havePermission = true;

			String[] excludes = resource.getResourceMethod()
					.getAnnotation(RolesExclude.class).value();

			if (request.containsRoles(excludes))
				throw new AccessDeniedException();
		}

		/**
		 * 再读取方法的角色许可，因为方法的角色许可覆盖类的。
		 */
		if (resource.getResourceMethod().isAnnotationPresent(RolesAllow.class)) {
			// 定义了角色许可
			havePermission = true;

			String[] allows = resource.getResourceMethod()
					.getAnnotation(RolesAllow.class).value();

			// 只要方法特意声明了允许匿名访问就可以
			if (this.allowAnonymous(allows))
				return;

			// Session中的用户信息包括定义的角色许可
			if (request.containsRoles(allows))
				return;
		}

		/**
		 * 判断是否有默认的排除角色。
		 */
		if (resource.getResourceClass().isAnnotationPresent(RolesExclude.class)) {
			havePermission = true;

			String[] excludes = resource.getResourceClass()
					.getAnnotation(RolesExclude.class).value();

			if (request.containsRoles(excludes))
				throw new AccessDeniedException();
		}

		/**
		 * 最后判断类是否有默认的角色许可。
		 */
		if (resource.getResourceClass().isAnnotationPresent(RolesAllow.class)) {
			// 定义了默认角色许可
			havePermission = true;

			String[] allows = resource.getResourceClass()
					.getAnnotation(RolesAllow.class).value();

			if (request.containsRoles(allows))
				return;
		}

		/**
		 * 如果设置了角色许可或默认角色许可，那么角色并不匹配所以拒绝访问。<br/>
		 * 如果没有设置任何角色许可，则说明资源允许匿名访问。
		 */
		if (havePermission)
			throw new AccessDeniedException();
	}

	/**
	 * 判断方法是否可以匿名访问，用来在方法上覆盖类的角色许可。
	 * 
	 * @param allows
	 * @return
	 */
	private boolean allowAnonymous(String... allows) {
		if (allows == null)
			return false;

		for (String s : allows) {
			if ("Anonymous".equals(s))
				return true;
		}
		return false;
	}
}
