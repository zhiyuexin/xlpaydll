package com.zh.rest.inject;

import java.lang.reflect.Field;

//import javax.ejb.EJB;

import com.zh.annotation.rest.ContextResource;
//import com.zh.ejb.EjbFactory;
import com.zh.exception.ApplicationException;
import com.zh.rest.resource.RunnableResource;

public class Injectable {

	/**
	 * 运行URI请求，并且在这之前将指定的上下文资源注入到实例中
	 * 
	 * @param resource
	 *            可运行的资源
	 * @param uri
	 *            请求串
	 * @param resources
	 *            需要注入的资源对象
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ApplicationException
	 */
	public static Object injectAndRun(RunnableResource resource, String uri,
			Object... resources) throws IllegalArgumentException,
			IllegalAccessException, InstantiationException,
			ApplicationException {

		injectContextResource(resource.getResourceObject(), resources);
		return resource.run(uri);
	};

	/**
	 * 将指定的上下文资源注入到实例中
	 * 
	 * @param resources
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws ApplicationException
	 */
	private static void injectContextResource(Object instance,
			Object... resources) throws IllegalArgumentException,
			IllegalAccessException, ApplicationException {

		if (instance == null || resources == null || resources.length == 0)
			return;

		Field[] fields = instance.getClass().getDeclaredFields();
		for (Field field : fields) {
			/**
			 * 支持直接注入EJB
			 */
//			if (field.isAnnotationPresent(EJB.class)) {
//				String jndi = "ejblocal:" + field.getType().getName();
//				field.setAccessible(true);
//				field.set(instance, EjbFactory.lookup(jndi, field.getType()));
//				continue;
//			}
			/**
			 * 也可以注入指定的上下文资源，例如Servlet请求、Session等
			 */
			if (field.isAnnotationPresent(ContextResource.class)) {
				for (Object o : resources) {
					if (o != null && o.getClass().equals(field.getType())) {
						field.setAccessible(true);
						field.set(instance, o);
						break;
					}
				}
			}
		}
	}
}
