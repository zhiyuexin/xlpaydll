package com.zh.rest.resource;

import java.lang.reflect.Method;

import com.zh.exception.RestfulException;

public interface RunnableResource {

	/**
	 * 执行资源
	 * 
	 * @param url
	 * @return
	 * @throws RestfulException
	 */
	public Object run(String url) throws RestfulException;

	/**
	 * 获得资源对象的类型
	 * 
	 * @return
	 */
	public Class<?> getResourceClass();

	/**
	 * 获取资源方法
	 * 
	 * @return
	 */
	public Method getResourceMethod();

//	/**
//	 * 设置资源对象
//	 */
//	public void setResourceObject(Object obj);

	/**
	 * 获取资源对象
	 * 
	 * @return
	 */
	public Object getResourceObject();

}
