package com.zh.rest.resource;

public class WSToken {
	public String ak;
	public String sn;
	public Long ts;

	@Override
	public String toString() {
		return "WSToken [ak=" + ak + ", sn=" + sn + ", ts=" + ts + "]";
	}

}
