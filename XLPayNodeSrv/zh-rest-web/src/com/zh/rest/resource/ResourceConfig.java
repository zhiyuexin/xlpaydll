package com.zh.rest.resource;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zh.annotation.rest.ApplicationPath;
import com.zh.annotation.rest.ConfigPath;
import com.zh.exception.AccessDeniedException;
import com.zh.exception.RestfulException;
import com.zh.util.file.ScanClassUtil;
import com.zh.util.xml.XmlUtil;

/**
 * 可以选择加入注释ApplicationPath，即root起点
 * 
 * @author ZhangHai
 *
 */
// @ApplicationPath("/root")
// @ConfigPath("com/zh/rest/resource/Test-Config.xml")
public abstract class ResourceConfig {

	private static Logger LOGGER = LoggerFactory
			.getLogger(ResourceConfig.class);

	private Configuration config;

	private String appPath;

	private Map<Class<?>, Set<Resource>> resources = new HashMap<Class<?>, Set<Resource>>();

	/**
	 * 校验令牌是否允许访问
	 * 
	 * @param token
	 * @param resource
	 * @return
	 * @throws AccessDeniedException
	 */
	public boolean checkToken(Resource resource, HttpServletRequest request)
			throws AccessDeniedException {

		// 资源应用及资源没有访问限制
		if (config == null || !resource.isNeedToken())
			return true;
		
		WSToken token = new WSToken();
		token.ak = request.getParameter("ak");
		token.sn = request.getParameter("sn");
		try {
			token.ts = Long.parseLong(request.getParameter("ts"));
		} catch (Exception e) {
			token.ts = null;
		}
		LOGGER.debug("请求TOKEN：ak={}, sn={}, ts={}", token.ak, token.sn,
				token.ts);

		// 必须包含令牌信息，并且令牌必须合法
		if (token != null && config.hasAk(token.ak)) {
			// sn与ts必须传入
			if (token.sn == null || token.ts == null
					|| !token.sn.equals(config.getSn(token.ak, token.ts))) {

				LOGGER.error("请求SN非法：Method={}, Node-Id={}, Node-Name={}",
						resource.getFullPath(), config.getId(token.ak),
						config.getName(token.ak));
				return false;
			}

			if (config.getTsLimit(token.ak) != null
					&& token.ts + config.getTsLimit(token.ak) * 1000 < Calendar
							.getInstance().getTimeInMillis()) {

				LOGGER.error("请求已过期：Method={}, Node-Id={}, Node-Name={}",
						resource.getFullPath(), config.getId(token.ak),
						config.getName(token.ak));
				throw new AccessDeniedException("请求已过期");
			}
			return true;
		}

		return false;
	}

	/**
	 * 验证外部请求WebService的远程请求IP是否合法
	 * 
	 * @param req
	 * @return
	 */
	public boolean allowRemoteAccess(HttpServletRequest req) {
		if (config == null)
			return true;

		LOGGER.debug("请求地址{}, 主机{}, 端口{}.", req.getRemoteAddr(),
				req.getRemoteHost(), req.getRemotePort());

		if (!config.isIpAllowed(req.getRemoteHost(), req.getRemotePort() + ""))
			return false;

		return true;
	}

	private Resource findClassResource(String uri) {
		Iterator<Class<?>> i = resources.keySet().iterator();
		while (i.hasNext()) {
			Class<?> clazz = i.next();
			Set<Resource> sr = resources.get(clazz);

			if (sr.isEmpty())
				continue;

			Resource first = sr.iterator().next();

			if (first.isClassPath(uri)) {
				return findResource(uri, clazz);
			}
		}
		return null;
	}

	private Resource findResource(String uri, Class<?> clazz) {
		Set<Resource> sr = resources.get(clazz);
		Iterator<Resource> i = sr.iterator();
		while (i.hasNext()) {
			Resource r = i.next();
			if (r.isHandle(uri))
				return r;
		}
		return null;
	}

	/**
	 * 根据请求路径查找并返回需要相应请求的资源
	 * 
	 * @param uri
	 *            <code>/ClassRootPath/MethodQueryString</code>
	 * @return
	 */
	public Resource findResource(String uri) {
		if (uri == null)
			return null;

		if (!uri.startsWith("/"))
			uri = "/" + uri;

		LOGGER.debug("根据URI定位资源：{}", uri);///refund/5/BN/operator_id=101,operator_name=101

		return findClassResource(uri);
	}

	/**
	 * 初始化ApplicationPath路径，注册资源类及其内部方法。保证Path是以/开头的，并确保应用根路径只能有一个/
	 * 
	 * @throws RestfulException
	 */
	public ResourceConfig() throws RestfulException {
		if (!getClass().isAnnotationPresent(ApplicationPath.class)) {
			appPath = "/";
		} else {
			appPath = getClass().getAnnotation(ApplicationPath.class).value();
			if (appPath == null || appPath.equals(""))
				appPath = "/";
			// 去掉多余的/
			if (appPath.indexOf("/", 1) > 0) {
				appPath = appPath.substring(0, appPath.indexOf("/", 1));
			}
		}

		loadConfig();

		init();
	}

	/**
	 * 加载对开放为WebService的Application配置的文件并缓存
	 * 
	 * @throws RestfulException
	 */
	private void loadConfig() throws RestfulException {
		if (getClass().isAnnotationPresent(ConfigPath.class)) {
			try {
				config = new Configuration(getClass().getAnnotation(
						ConfigPath.class).value());
			} catch (DocumentException e) {
				throw new RestfulException(e);
			}
		} else
			config = null;
	}

	/**
	 * 继承这个类，并在初始化方法中注册你的WebServlet
	 */
	public abstract void init() throws RestfulException;

	/**
	 * 注册指定包中的WebServlet组件类，可以用逗号指定多个包。
	 * 
	 * @param basePackage
	 * @throws RestfulException
	 */
	public void register(final String basePackage) throws RestfulException {
		if (basePackage.indexOf(",") > 0) {
			String packageNameArr[] = basePackage.split(",");
			for (String packageName : packageNameArr) {
				registerPackage(packageName);
			}
		} else {
			registerPackage(basePackage);
		}
	}

	private void registerPackage(String packageName) throws RestfulException {
		Set<Class<?>> setClasses = ScanClassUtil.getClasses(packageName);
		for (Class<?> clazz : setClasses) {
			register(clazz);
		}
	}

	/**
	 * 注册一个WebServlet组件类
	 * 
	 * @param componentClass
	 * @throws RestfulException
	 */
	public void register(final Class<?> componentClass) throws RestfulException {
		Method[] m = componentClass.getDeclaredMethods();
		for (Method i : m) {
			register(i);
		}
	}

	private void register(Resource resource) {
		if (resource == null)
			return;

		Set<Resource> resources = this.resources.get(resource
				.getResourceClass());
		if (resources != null) {
			if (resources.contains(resource))
				resources.remove(resource);
			resources.add(resource);
		} else {
			resources = new HashSet<Resource>();
			resources.add(resource);
			this.resources.put(resource.getResourceClass(), resources);
		}
	}

	private void register(Method method) throws RestfulException {
		register(Resource.register(method/*, this*/));
	}

	/**
	 * 是否默认资源应用
	 * 
	 * @return
	 */
	public boolean defaultApplication() {
		return "/".equals(appPath) || "".equals(appPath) || appPath == null;
	}

	/**
	 * 获取资源根路径
	 * 
	 * @return
	 */
	public String getApplicationPath() {
		return appPath == null ? "/" : (appPath.equals("") ? "/" : appPath);
	}

	private static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}

	public static void main(String[] args) {
		long l = Calendar.getInstance().getTimeInMillis() + 60000;
		System.out.println(l);
		System.out.println(MD5("10003" + "i8fi0Erfv2311I0P" + l
				+ "942ddv7Z9W21apcC"));
		System.out.println(MD5("iiop1123"));
	}

	/**
	 * 对资源的辅助配置XML文件的缓存，主要用来控制WebService的访问权限与签名握手。
	 * 
	 * @author ZhangHai
	 *
	 */
	public class Configuration {

		private List<String> allowIp;

		private Map<String, AllowNode> allowNode;

		public Configuration(String configXmlPath) throws DocumentException {

			XmlUtil xml = new XmlUtil(Thread.currentThread()
					.getContextClassLoader().getResourceAsStream(configXmlPath));

			List<Element> elements = xml
					.getElements("assembly-descriptor.security-permission.allow-ip");
			if (elements != null) {
				allowIp = new ArrayList<String>();

				for (Element i : elements) {
					allowIp.add(i.getText());
				}
			}

			elements = xml
					.getElements("assembly-descriptor.security-permission.allow-node");
			if (elements != null) {
				allowNode = new HashMap<String, ResourceConfig.AllowNode>();

				for (Element i : elements) {
					AllowNode node = new AllowNode();
					node.nodeId = i.attributeValue("id");
					node.nodeName = i.attributeValue("name");
					node.ak = i.attributeValue("ak");
					node.sk = i.attributeValue("sk");
					try {
						node.ts = Long.parseLong(i.attributeValue("ts"));
					} catch (NumberFormatException e) {
						node.ts = null;
					}
					allowNode.put(node.ak, node);
				}
			}
		}

		public boolean isIpAllowed(String ip, String port) {
			if (allowIp == null)
				return true;

			for (String i : allowIp) {
				if (i.indexOf(":") > 0) {
					// 限制了端口
					if (i.equals(ip + ":" + port))
						return true;
				} else {
					// 仅限制IP
					if (i.equals(ip))
						return true;
				}
			}
			return false;
		}

		public boolean hasAk(String ak) {
			if (allowNode == null)
				return false;

			return allowNode.containsKey(ak);
		}

		public String getId(String ak) {
			if (allowNode == null || !hasAk(ak))
				return null;
			return allowNode.get(ak).nodeId;
		}

		/**
		 * 实时计算SN
		 * 
		 * @param ak
		 * @param ts
		 * @return
		 */
		public String getSn(String ak, Long ts) {
			if (allowNode == null || !hasAk(ak))
				return null;
			AllowNode node = allowNode.get(ak);
			return MD5(node.nodeId + node.ak + ts + node.sk);
		}

		public String getName(String ak) {
			if (allowNode == null || !hasAk(ak))
				return null;
			return allowNode.get(ak).nodeName;
		}

		/**
		 * 获得时间戳限时有效时间
		 * 
		 * @param ak
		 * @return
		 */
		public Long getTsLimit(String ak) {
			if (allowNode == null || !hasAk(ak))
				return null;
			return allowNode.get(ak).ts;
		}
	}

	private static class AllowNode {
		public String nodeId;
		public String nodeName;
		public String ak;
		public String sk;
		// public String sn;
		public Long ts;
	}
}
