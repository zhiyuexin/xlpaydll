package com.zh.rest.resource;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zh.security.jaas.JaasLogin;

public class ServletRequest {

	private HttpServletRequest request;

	@SuppressWarnings("unused")
	private ServletRequest() {
	};

	public ServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpSession getSession() {
		return request == null ? null : request.getSession();
	}

	/**
	 * 判断当前Session中是否拥有指定的角色
	 * 
	 * @param roles
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean containsRoles(String... roles) {
		if (request == null || roles == null || request.getSession() == null)
			return false;

		List<String> callerRoles = (List<String>) request.getSession()
				.getAttribute(JaasLogin.SESSION_CALLER_ROLES);
		if (callerRoles == null)
			return false;

		for (String s : callerRoles) {
			for (String s1 : roles) {
				if (s != null && s.equals(s1))
					return true;
			}
		}

		return false;
	}

	public Object getCallerPrincipal() {
		if (request == null || request.getSession() == null)
			return null;

		return request.getSession().getAttribute(JaasLogin.SESSION_CALLER);
	}

	@SuppressWarnings("unchecked")
	public List<String> getCallerRoles() {
		if (request == null || request.getSession() == null)
			return null;

		return (List<String>) request.getSession().getAttribute(
				JaasLogin.SESSION_CALLER_ROLES);
	}
}
