package com.zh.rest.resource;

public class WSResult {

	private String status;
	private String message;
	private Object result;

	@Override
	public String toString() {
		return "WSResult [status=" + status + ", message=" + message
				+ ", result=" + result + "]";
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}
