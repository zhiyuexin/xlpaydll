package com.zh.rest.resource;

import java.util.HashMap;
import java.util.Map;

public final class ResourceManager {

	private static final ResourceManager instance = new ResourceManager();

	private Map<String, ResourceConfig> applications = new HashMap<String, ResourceConfig>();

	private ResourceManager() {
	}

	public static ResourceManager getInstance() {
		return instance;
	}

	/**
	 * 注册一个新的ServletApplication应用
	 * 
	 * @param app
	 */
	public static void newApplication(ResourceConfig app) {
		if (app != null) {
			instance.applications.put(app.getApplicationPath(), app);
		}
	}

	/**
	 * 找到匹配路径的资源<br/>
	 * <b>注意：应用的资源根路径不能包含两个以上（含）的斜杠"/"</b>
	 * 
	 * @param uri
	 * @return
	 */
	public static Resource findResource(String uri) {
		ResourceConfig rc = findApplication(uri);
		return rc == null ? null : rc.findResource(uri);
	}

	public static Resource findResource(String uri, ResourceConfig app) {
		return app == null ? null : app.findResource(uri.substring(app
				.getApplicationPath().length()));
	}

	public static ResourceConfig findApplication(String uri) {
		ResourceConfig rc = null;

		Resource.LOGGER.debug("根据URI定位Application: uri={}", uri);
		if (uri == null)
			rc = getDefaultApplication();
		else {
			// 找第二个/
			int pos = uri.indexOf("/", 1);
			String path = null;
			if (pos > 0)
				path = uri.substring(0, pos);
			else
				path = uri;

			rc = instance.applications.get(path);
			if (rc == null)
				rc = getDefaultApplication();
		}
		Resource.LOGGER.debug("定位到应用{}", rc.getClass().getName());

		return rc;
	}

	/**
	 * 返回默认的资源应用，既根路径为"/"的资源，如果不存在则返回null。
	 * 
	 * @return
	 */
	public static ResourceConfig getDefaultApplication() {
		return instance.applications.get("/");
	}
}
