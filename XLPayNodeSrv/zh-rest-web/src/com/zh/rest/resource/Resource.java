package com.zh.rest.resource;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.PathParam;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
//import com.zh.ejb.EjbFactory;
import com.zh.exception.AccessDeniedException;
import com.zh.exception.ApplicationException;
import com.zh.exception.ExceptionMessage;
import com.zh.exception.RestfulException;
import com.zh.util.StringUtil;

/**
 * REST请求的方法资源，用以解析方法的REST注释，并对实际传递的参数进行解析以调用其对应的方法。
 * 
 * @author ZhangHai
 *
 */
public class Resource implements RunnableResource {

	static Logger LOGGER = LoggerFactory.getLogger(Resource.class);

//	private ResourceConfig application;

	/**
	 * 方法的定义类上注释的<code>@Path</code><br/>
	 * <code>/WebContext/ApplicatoinPath<b><i>/ClassRootPath</i></b>/MethodQueryString</code>
	 */
	private String classRootPath;

	/**
	 * 方法上注释的请求路径<br/>
	 * <code>/WebContext/ApplicatoinPath/ClassRootPath<b><i>/MethodQueryString</i></b></code>
	 */
	private String methodPath;

	/**
	 * 包含类Path的请求路径<br/>
	 * <code>/WebContext/ApplicatoinPath<b><i>/ClassRootPath/MethodQueryString</i></b></code>
	 */
	private String fullPath;

	/**
	 * 请求方法 GET, POST，其他的再说
	 */
	private String requestMethod;

	/**
	 * 是否需要令牌来访问资源
	 */
	private boolean needToken;

	/**
	 * 资源类
	 */
	private Class<?> resourceClass;

	/**
	 * 资源对象
	 */
	private Object resource;

	/**
	 * 资源方法
	 */
	private Method resourceMethod;

	/**
	 * 方法的参数
	 */
	private Map<Integer, Resource.Parameter> params;

	private MediaType[] consumes;

	private MediaType[] produces;

	/**
	 * 参数间的固定分隔字符，用来判断实际参数。如果参数处于首尾位置，则会在队列中多分隔出一个空串以表示其是首尾位置的参数。<br/>
	 * <br/>
	 * 例如：方法的路径是<code>/hello/{name}:{sex}</code><br/>
	 * 那么隔离字符列表就是<code><b>/hello/</b></code>和<b>:</b>以及最后的空串。<br/>
	 */
	private List<String> seperators;

	private Resource() {
	}

	/**
	 * 通过方法创建资源
	 * 
	 * @param method
	 * @throws NamingException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ApplicationException
	 */
	private Resource(Method method)
			throws IllegalAccessException, InstantiationException,
			ApplicationException {
		resourceClass = method.getDeclaringClass();
		resourceMethod = method;
//		application = app;

		/**
		 * 类上的主路径，必须以/开头，并保证不以/结尾
		 */
		classRootPath = resourceClass.getAnnotation(Path.class).value();
		if (classRootPath == null)
			classRootPath = "/";
		else if (!classRootPath.startsWith("/"))
			classRootPath = "/" + classRootPath;

		if (classRootPath.length() > 1 && classRootPath.endsWith("/"))
			classRootPath = classRootPath.substring(0,
					classRootPath.length() - 1);

		/**
		 * 方法的请求串，必须以/开头，默认的方法除外，默认的方法以空串表示。
		 */
		methodPath = resourceMethod.getAnnotation(Path.class).value();
		if (methodPath == null)
			methodPath = "";
		if (methodPath.length() > 0 && !methodPath.startsWith("/"))
			methodPath = "/" + methodPath;

		/**
		 * 拼全路径
		 */
		if (classRootPath.endsWith("/") && methodPath.startsWith("/"))
			fullPath = classRootPath + methodPath.substring(1);
		else
			fullPath = classRootPath + methodPath;

		if (resourceMethod.isAnnotationPresent(POST.class))
			requestMethod = "POST";
		else
			requestMethod = "GET";

		needToken = resourceMethod.isAnnotationPresent(NeedToken.class);

		// 取得参数列表{*}
		List<String> parms = StringUtil.getMatched(methodPath, "(\\{[^\\}]+})");
		if (parms.size() > 0) {
			params = new HashMap<Integer, Resource.Parameter>();

			for (String parm : parms) {
				Parameter p = new Parameter(parm, resourceMethod);
				if (p.getIndex() > 0)
					params.put(p.getIndex(), p);
			}
		}

		/**
		 * 消费类型与生产类型
		 */
		if (resourceMethod.isAnnotationPresent(Consumes.class)) {
			consumes = resourceMethod.getAnnotation(Consumes.class).value();
		} else {
			if (params != null) {
				consumes = new MediaType[params.size()];
				for (int i = 0; i < consumes.length; i++)
					consumes[i] = MediaType.TEXT_PLAIN;
			}
		}

		if (resourceMethod.isAnnotationPresent(Produces.class)) {
			produces = resourceMethod.getAnnotation(Produces.class).value();
		} else {
			produces = new MediaType[1];
			produces[0] = MediaType.TEXT_PLAIN;
		}

		// 参数间隔
		if (params != null) {
			seperators = StringUtil.separator(parms, methodPath);
		}

		resource = newResourceObject();
	}

	/**
	 * 注册方法：此方法的定义类必须被注释为<code>@WebServlet</code>，并指定<code>@Path</code>
	 * 路径。方法本身也要指定<code>@Path</code>路径。否则注册将失败！
	 * 
	 * @param method
	 *            方法
	 * @return 包装好的方法资源
	 * @throws RestfulException
	 */
	public static Resource register(Method method/*, ResourceConfig app*/)
			throws RestfulException {
		if (!method.getDeclaringClass().isAnnotationPresent(WebServlet.class)
				|| !method.getDeclaringClass().isAnnotationPresent(Path.class))
			return null;
		// throw new RestfulException(ExceptionMessage.RESTFUL_EXCEPTION,
		// "资源注册失败 - 类" + resourceClass.getName() + "没有定义@WebServlet或@Path");
		if (!method.isAnnotationPresent(Path.class))
			return null;
		// throw new RestfulException(ExceptionMessage.RESTFUL_EXCEPTION,
		// "资源注册失败 - 方法" + resourceMethod.getName() + "没有定义@Path");

		try {
			return new Resource(method);
		} catch (IllegalAccessException e) {
			throw new RestfulException(ExceptionMessage.RESTFUL_EXCEPTION,
					"资源注册失败：资源对象类型\"" + method.getDeclaringClass().getName()
							+ "\"的构造函数不可见");
		} catch (InstantiationException e) {
			throw new RestfulException(ExceptionMessage.RESTFUL_EXCEPTION,
					"资源注册失败：资源类\"" + method.getDeclaringClass().getName()
							+ "\"实例化失败", e);
		} catch (ApplicationException e) {
			throw new RestfulException(ExceptionMessage.RESTFUL_EXCEPTION,
					"资源注册失败：JNDI Naming Exception.", e);
		}
	}

	/**
	 * 执行资源，并根据生产类型返回数据。
	 * 
	 * @param uri
	 *            MethodQueryString
	 * @return
	 * @throws RestfulException
	 */
	@Override
	public Object run(String uri) throws RestfulException {

		try {
			Object[] args = getParameters(uri);
			LOGGER.debug("执行{}.{}({})", resourceClass.getName(),
					resourceMethod.getName(), args);
			if (resource == null)
				resource = newResourceObject();

			LOGGER.debug("ResourceClass intance: {}", resource);
			Object result = resourceMethod.invoke(resource, args);
			if (result == null)
				return null;

			if (produces == null || produces.length == 0) {
				produces = new MediaType[1];
				produces[0] = MediaType.TEXT_PLAIN;
			}

			switch (produces[0]) {
			case APPLICATION_JSON:
				// return JSONObject.toJSONString(result);
				return result;
			default: // TEXT_PLAIN
				return result.toString();
			}
		} catch (InstantiationException e) {
			e.printStackTrace(System.out);
			throw new RestfulException(ExceptionMessage.RELECT_EXCEPTION,
					"资源类\"" + resourceClass.getName() + "\"实例化失败", e);
		} catch (IllegalAccessException e) {
			e.printStackTrace(System.out);
			throw new RestfulException(ExceptionMessage.RELECT_EXCEPTION,
					"禁止访问: " + resourceClass.getName() + "."
							+ resourceMethod.getName(), e);
		} catch (IllegalArgumentException e) {
			e.printStackTrace(System.out);
			throw new RestfulException(ExceptionMessage.RELECT_EXCEPTION,
					"参数错误: " + resourceClass.getName() + "."
							+ resourceMethod.getName(), e);
		} catch (InvocationTargetException e) {
			e.printStackTrace(System.out);
			throw new RestfulException(ExceptionMessage.RELECT_EXCEPTION,
					"调用目标异常: " + resourceClass.getName() + "."
							+ resourceMethod.getName(), e);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new RestfulException(ExceptionMessage.RELECT_EXCEPTION, e);
		}
	}

	@Override
	public Class<?> getResourceClass() {
		return resourceClass;
	}

	@Override
	public Object getResourceObject() {
		return this.resource;
	};

	private Object newResourceObject() throws IllegalAccessException,
			InstantiationException, ApplicationException {
		if (resource != null)
			return resource;

		if (resourceClass.isInterface()) {
			String jndi = "ejblocal:" + resourceClass.getName();
			LOGGER.info("初始化EJB Local - {}", jndi);
			// resource = EjbFactory.lookup(jndi, resourceClass);
		} else {
			resource = resourceClass.newInstance();
		}
		return resource;
	}

	/**
	 * 根据MethodQueryString解析解析出参数<br/>
	 * <code>/MethodQueryString</code>
	 * 
	 * @param uri
	 * @return
	 * @throws RestfulException
	 * @throws AccessDeniedException
	 */
	private Object[] getParameters(String uri) throws RestfulException,
			AccessDeniedException {
		if (!uri.startsWith("/"))
			uri = "/" + uri;

		if (params == null || params.size() == 0)
			return null;

		List<String> arguments = StringUtil.separator(seperators, uri);

		List<Object> result = new ArrayList<Object>();
		int p = 1;
		for (int i = 0; i < consumes.length; i++) {
			if (p <= params.size()) {
				Parameter parm = params.get(p++);

				if (arguments.size() <= i) {
					// 实际的参数不足
					throw new RestfulException(
							ExceptionMessage.ARGUMENTS_MISMATCHING, "实际参数不足："
									+ arguments);
				}
				result.add(parm.consume(arguments.get(i), consumes[i]));
			}
		}

		return result.toArray();
	}

//	public Object[] getParameter4Text(String uri) throws RestfulException,
//			AccessDeniedException {
//		if (!uri.startsWith("/"))
//			uri = "/" + uri;
//
//		if (params == null || params.size() == 0)
//			return null;
//
//		List<String> arguments = StringUtil.separator(seperators, uri);
//
//		List<Object> result = new ArrayList<Object>();
//		int p = 1;
//		for (int i = 0; i < consumes.length; i++) {
//			if (p <= params.size()) {
//				Parameter parm = params.get(p++);
//
//				if (arguments.size() <= i) {
//					// 实际的参数不足
//					throw new RestfulException(
//							ExceptionMessage.ARGUMENTS_MISMATCHING, "实际参数不足："
//									+ arguments);
//				}
//				result.add(parm.consume(arguments.get(i), consumes[i]));
//			}
//		}
//
//		return result.toArray();
//	}

	/**
	 * 判断指定的请求地址是否就是此资源
	 * 
	 * @param uri
	 * @return
	 */
	public boolean isHandle(String uri) {
		// 判断是否是类路径
		if (!isClassPath(uri))
			return false;

		// 本资源没有参数，则直接比较
		if (this.params == null || this.params.size() == 0)
			return fullPath.equals(uri);

		// 有参数则先判断头/test/user/pass
		int pos = fullPath.indexOf(this.params.get(1).getName());
		if (!uri.startsWith(fullPath.substring(0, pos)))
			return false;
		
		String s1, s2;
		s1 = fullPath.substring(pos);
		s2 = uri.substring(pos);
		
		return StringUtil.findStrCount(s1, "/") == StringUtil.findStrCount(s2, "/");
	}
	
//	public static void main(String args[]) {
//		Test t = new Test();
//		try {
//			Method m = t.getClass().getDeclaredMethod("test1", String.class, String.class, String.class);
//			Resource r = register(m);
//			boolean b = r.isHandle("/refund/5/BN/operator_id=101,operator_name=101");
//			System.out.println(b);
//		} catch (Exception e) {
//			e.printStackTrace();;
//		}
//	}

	/**
	 * 访问资源是否需要令牌
	 * 
	 * @return
	 */
	public boolean isNeedToken() {
		return needToken;
	}

	/**
	 * 判断是否此类的资源
	 * 
	 * @param uri
	 * @return
	 */
	public boolean isClassPath(String uri) {
		if (classRootPath.equals(uri))
			return true;

		if (classRootPath.endsWith("/"))
			return uri.startsWith(classRootPath);
		else
			return uri.startsWith(classRootPath + "/");
	}

	/**
	 * 包含类Path的请求路径<br/>
	 * <code>/WebContext/ApplicatoinPath<b>/ClassRootPath/MethodQueryString</b></code>
	 * 
	 * @return
	 */
	public String getFullPath() {
		return fullPath;
	}

	/**
	 * 方法上注释的请求路径<br/>
	 * <code>/WebContext/ApplicatoinPath/ClassRootPath<b>/MethodQueryString</b></code>
	 * 
	 * @return
	 */
	public String getMethodPath() {
		return methodPath;
	}

	/**
	 * 请求方法 GET, POST，其他的再说
	 * 
	 * @return
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * 方法的定义类上注释的<code>@Path</code><br/>
	 * <code>/WebContext/ApplicatoinPath<b>/ClassRootPath</b>/MethodQueryString</code>
	 * 
	 * @return
	 */
	public String getClassRootPath() {
		return classRootPath;
	}

	@Override
	public Method getResourceMethod() {
		return resourceMethod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((classRootPath == null) ? 0 : classRootPath.hashCode());
		result = prime * result
				+ ((fullPath == null) ? 0 : fullPath.hashCode());
		result = prime * result
				+ ((methodPath == null) ? 0 : methodPath.hashCode());
		result = prime * result
				+ ((requestMethod == null) ? 0 : requestMethod.hashCode());
		result = prime * result
				+ ((resourceMethod == null) ? 0 : resourceMethod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resource other = (Resource) obj;
		if (classRootPath == null) {
			if (other.classRootPath != null)
				return false;
		} else if (!classRootPath.equals(other.classRootPath))
			return false;
		if (fullPath == null) {
			if (other.fullPath != null)
				return false;
		} else if (!fullPath.equals(other.fullPath))
			return false;
		if (methodPath == null) {
			if (other.methodPath != null)
				return false;
		} else if (!methodPath.equals(other.methodPath))
			return false;
		if (requestMethod == null) {
			if (other.requestMethod != null)
				return false;
		} else if (!requestMethod.equals(other.requestMethod))
			return false;
		if (resourceClass == null) {
			if (other.resourceClass != null)
				return false;
		} else if (!resourceClass.equals(other.resourceClass))
			return false;
		if (resourceMethod == null) {
			if (other.resourceMethod != null)
				return false;
		} else if (!resourceMethod.equals(other.resourceMethod))
			return false;
		return true;
	}

	final class Parameter {
		private int index = 0;
		private String name = null;
		private Class<?> type = null;

		@SuppressWarnings("unused")
		private Parameter() {
		}

		/**
		 * 根据消费类型消费这个参数
		 * 
		 * @param value
		 * @return
		 * @throws RestfulException
		 */
		public Object consume(String value, MediaType type)
				throws RestfulException {
			Object result = null;
			switch (type) {
			case APPLICATION_JSON:
				try {
					try {
						result = JSONObject.parseObject(value, this.type);
					} catch (ClassCastException e) {
						// 尝试解析为List
						result = JSONArray.parseArray(value, this.type);
					}
				} catch (ClassCastException e) {
					System.out.println("非Json格式参数，则转为文本");
					result = value;
				}
				break;
			default: // TEXT_PLAIN
				result = value;
			}
			return result;
		}

		public Parameter(String paramName, Method method) {
			// Annotation[参数][参数的注释]
			Annotation[][] paramAnnos = method.getParameterAnnotations();
			// 循环参数
			for (int i = 0; i < paramAnnos.length; i++) {
				Annotation[] annos = paramAnnos[i];
				// 循环参数注释类
				for (Annotation anno : annos) {
					if (anno instanceof PathParam) {
						if (("{" + ((PathParam) anno).value() + "}")
								.equals(paramName)) {
							index = i + 1;
							name = paramName;
							type = method.getParameterTypes()[i];
							return;
						}
					}
				}
			}
		}

		public int getIndex() {
			return index;
		}

		public String getName() {
			return name;
		}

		public Class<?> getType() {
			return type;
		}

		private Resource getOuterType() {
			return Resource.this;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + index;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Parameter other = (Parameter) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (index != other.index)
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			return true;
		}
	}

	// public static void main(String[] args) throws RestfulException {
	// Method[] m = OaService.class.getDeclaredMethods();
	//
	// Resource r = Resource.register(m[0], null);
	// Parameter parm = r.new Parameter("{ids}", m[0]);
	// Object o = parm.consume("[123,122]", MediaType.APPLICATION_JSON);
	//
	//
	// // Class clazz = m[0].getParameterTypes()[0];
	// // System.out.println(clazz.getSimpleName());
	// //
	// // Object o = JSONObject.parseArray("[12,23]", Double.class);
	//
	// // List<Double> l = new ArrayList<Double>();
	// // l.add(11d);
	// // l.add(22d);
	// // String s = JSONObject.toJSONString(l);
	//
	// System.out.println(o);
	//
	// // JSONObject.parseObject(arguments.get(i),
	// // WSToken.class);
	// }

}
