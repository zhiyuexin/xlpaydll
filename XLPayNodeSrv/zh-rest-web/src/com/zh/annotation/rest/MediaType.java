package com.zh.annotation.rest;

public enum MediaType {

	TEXT_PLAIN, APPLICATION_JSON
}
