package com.zh.exception;

public class AccessDeniedException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4174291175160582231L;
	
	// Ĭ��
	protected ExceptionMessage errorMess = ExceptionMessage.ACCESS_DENIED;

	public AccessDeniedException() {
		this(ExceptionMessage.ACCESS_DENIED);
	}

	public AccessDeniedException(Throwable cause) {
		super(cause);
	}

	public AccessDeniedException(String errMessage) {
		super(errMessage);
	}

	public AccessDeniedException(ExceptionMessage mess) {
		super(mess);
	}

	public AccessDeniedException(String errMessage, Throwable cause) {
		super(errMessage, cause);
	}

	public AccessDeniedException(ExceptionMessage error, Throwable cause) {
		super(error, cause);
	}

	public AccessDeniedException(ExceptionMessage error, String errMessage) {
		super(error, errMessage);
	}

	public AccessDeniedException(ExceptionMessage error, String errMessage,
			Throwable cause) {
		super(error, errMessage, cause);
	}
}
