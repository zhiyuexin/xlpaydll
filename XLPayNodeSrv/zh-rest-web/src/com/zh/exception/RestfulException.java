package com.zh.exception;

public class RestfulException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3270564400193621966L;
	
	// Ĭ��
	protected ExceptionMessage errorMess = ExceptionMessage.RESTFUL_EXCEPTION;

	public RestfulException() {
		super("");
	}

	public RestfulException(Throwable cause) {
		super(cause);
	}

	public RestfulException(String errMessage) {
		super(errMessage);
	}

	public RestfulException(ExceptionMessage mess) {
		super(mess);
	}

	public RestfulException(String errMessage, Throwable cause) {
		super(errMessage, cause);
	}

	public RestfulException(ExceptionMessage error, Throwable cause) {
		super(error, cause);
	}

	public RestfulException(ExceptionMessage error, String errMessage) {
		super(error, errMessage);
	}

	public RestfulException(ExceptionMessage error, String errMessage,
			Throwable cause) {
		super(error, errMessage, cause);
	}
}
