package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class AdminLogin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"zh-CN\">\r\n");
      out.write("  <head>\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("    <title>Node Server Runtime Configuration</title>\r\n");
      out.write("\r\n");
      out.write("    <!-- Bootstrap -->\r\n");
      out.write("    <link href=\"bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->\r\n");
      out.write("    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->\r\n");
      out.write("    <!--[if lt IE 9]>\r\n");
      out.write("      <script src=\"resources/js/html5shiv.min.js\"></script>\r\n");
      out.write("      <script src=\"resources/js/respond.min.js\"></script>\r\n");
      out.write("    <![endif]-->\r\n");
      out.write("\r\n");
      out.write("    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\r\n");
      out.write("    <script src=\"resources/js/jquery.min.js\"></script>\r\n");
      out.write("    <!-- Include all compiled plugins (below), or include individual files as needed -->\r\n");
      out.write("    <script src=\"bootstrap/js/bootstrap.min.js\"></script>\r\n");
      out.write("    <script src=\"resources/js/jsonFormater.js\" type=\"text/javascript\"></script>\r\n");
      out.write("    <link href=\"resources/css/jsonFormater.css\" type=\"text/css\" rel=\"stylesheet\"/>\r\n");
      out.write("  </head>\r\n");
      out.write("  <body>\r\n");
      out.write("\t<form class=\"form-horizontal\">\r\n");
      out.write("\t  <div class=\"form-group\">\r\n");
      out.write("\t    <label for=\"username\" class=\"col-sm-2 control-label\">username</label>\r\n");
      out.write("\t    <div class=\"col-sm-10\">\r\n");
      out.write("\t      <input type=\"text\" class=\"form-control\" id=\"username\" placeholder=\"username\">\r\n");
      out.write("\t    </div>\r\n");
      out.write("\t  </div>\r\n");
      out.write("\t  <div class=\"form-group\">\r\n");
      out.write("\t    <label for=\"password\" class=\"col-sm-2 control-label\">password</label>\r\n");
      out.write("\t    <div class=\"col-sm-10\">\r\n");
      out.write("\t      <input type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"password\">\r\n");
      out.write("\t    </div>\r\n");
      out.write("\t  </div>\r\n");
      out.write("\t  <div class=\"form-group\">\r\n");
      out.write("\t    <div class=\"col-sm-offset-2 col-sm-10\">\r\n");
      out.write("\t      <button type=\"button\" onclick=\"javascript:login();\" class=\"btn btn-default\">Sign in</button>\r\n");
      out.write("\t    </div>\r\n");
      out.write("\t  </div>\r\n");
      out.write("\t</form>\r\n");
      out.write("\t\r\n");
      out.write("\t<div id=\"msg\" style=\"display: none\" class=\"alert alert-warning alert-dismissible\" role=\"alert\">\t  \r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<script>\t\r\n");
      out.write("\t\tvar login = function() {\r\n");
      out.write("\t\t\tvar username = $('#username').val();\r\n");
      out.write("\t\t\tvar password = $('#password').val();\r\n");
      out.write("\t\t\tif (username == null || username == '') {\r\n");
      out.write("    \t\t\t$('#msg').css('display','block');\r\n");
      out.write("    \t\t\t$('#msg').html(\"<strong>Wrong: </strong> please your login name and password !\");\r\n");
      out.write("    \t\t\treturn;\r\n");
      out.write("        \t}\r\n");
      out.write("\t\t\tif (password == null) password = '';\r\n");
      out.write("\t\t    $.ajax({\r\n");
      out.write("\t\t        type: \"POST\",\r\n");
      out.write("\t\t        url: \"sys/adminLogin\",\r\n");
      out.write("\t\t        data: {\r\n");
      out.write("\t\t        \tusername: username,\r\n");
      out.write("\t\t        \tpassword: password\r\n");
      out.write("\t\t        },\r\n");
      out.write("\t\t        dataType: \"json\",\r\n");
      out.write("\t\t        success: function success(data, textStatus, jqXHR) {\r\n");
      out.write("\t\t\t\t\tconsole.log(\"result\", data.result, data);\r\n");
      out.write("\t        \t\tif (data.result) {\r\n");
      out.write("\t        \t\t\t$('#msg').css('display','none');\r\n");
      out.write("\t        \t\t\twindow.location.href = \"SystemConfig.jsp\";\r\n");
      out.write("\t        \t\t}else{\r\n");
      out.write("\t        \t\t\t$('#msg').css('display','block');\r\n");
      out.write("\t        \t\t\t$('#msg').html(\"<strong>login refused!</strong> wrong username or password!\");\r\n");
      out.write("\t\t        \t}\r\n");
      out.write("\t\t        }});\r\n");
      out.write("\t\t};\r\n");
      out.write("\t</script>\r\n");
      out.write("  </body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
