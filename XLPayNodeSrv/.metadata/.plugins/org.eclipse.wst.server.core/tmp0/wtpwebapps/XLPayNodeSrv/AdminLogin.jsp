<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Node Server Runtime Configuration</title>

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="resources/js/html5shiv.min.js"></script>
      <script src="resources/js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="resources/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="resources/js/jsonFormater.js" type="text/javascript"></script>
    <link href="resources/css/jsonFormater.css" type="text/css" rel="stylesheet"/>
  </head>
  <body>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="username" class="col-sm-2 control-label">username</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="username" placeholder="username">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="password" class="col-sm-2 control-label">password</label>
	    <div class="col-sm-10">
	      <input type="password" class="form-control" id="password" placeholder="password">
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" onclick="javascript:login();" class="btn btn-default">Sign in</button>
	    </div>
	  </div>
	</form>
	
	<div id="msg" style="display: none" class="alert alert-warning alert-dismissible" role="alert">	  
	</div>
	
	<script>	
		var login = function() {
			var username = $('#username').val();
			var password = $('#password').val();
			if (username == null || username == '') {
    			$('#msg').css('display','block');
    			$('#msg').html("<strong>Wrong: </strong> please your login name and password !");
    			return;
        	}
			if (password == null) password = '';
		    $.ajax({
		        type: "POST",
		        url: "sys/adminLogin",
		        data: {
		        	username: username,
		        	password: password
		        },
		        dataType: "json",
		        success: function success(data, textStatus, jqXHR) {
					console.log("result", data.result, data);
	        		if (data.result) {
	        			$('#msg').css('display','none');
	        			window.location.href = "SystemConfig.jsp";
	        		}else{
	        			$('#msg').css('display','block');
	        			$('#msg').html("<strong>login refused!</strong> wrong username or password!");
		        	}
		        }});
		};
	</script>
  </body>
</html>

