<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Node Server Runtime Configuration</title>

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="resources/js/html5shiv.min.js"></script>
      <script src="resources/js/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="resources/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="resources/js/jsonFormater.js" type="text/javascript"></script>
    <link href="resources/css/jsonFormater.css" type="text/css" rel="stylesheet"/>
  </head>
<%
Boolean AdminLogin = false;
if (session.getAttribute("AdminLogin") != null) {
	AdminLogin = (Boolean)session.getAttribute("AdminLogin");
}
if (!AdminLogin) {
	response.sendRedirect("AdminLogin.jsp");
}
%>
  <body>
  	<ul class="nav nav-tabs">
	  <li id="liRuntimeConfiguration" role="presentation" class="active"><a href="javascript:ShowRuntimeConfig();">Runtime Configuration</a></li>
	  <li id="liSystemConfiguration" role="presentation" class=""><a href="javascript:ShowSystemConfig();">System Configuration</a></li>
	  <li id="liSystemConfiguration" role="presentation" class=""><a href="javascript:LogOff();">Log Off</a></li>
	  <!-- <li id="liRegisterKeyFile" role="presentation" class=""><a href="javascript:ShowSystemRegisterKeyFile();">System RegisterKeyFile</a></li> -->
	</ul>
	
	<div id="pnlRuntimeConfiguration" class="panel panel-default">
	  <div class="panel-body">
	    <h2 id="config_title1"></h2>
	    <h3 id="config_title2"></h3>
	    <div id="runtime_config_content" class="Canvas"></div>
	    <br />
	    <a class="btn btn-lg btn-success" href="javascript:reloadRuntimeConfig();" role="button">reload runtime configuration</a>											
	  </div>
	</div>
	
	<div id="pnlSystemConfiguration" style="display: none" class="panel panel-default">
	  <div class="panel-body">
	    <textarea id=system_config_content class="form-control" placeholder="" rows="20"></textarea>
	    <br />
	    <a class="btn btn-lg btn-success" href="javascript:setSystemConfig();" role="button">set system configuration</a>										
	  </div>
	</div>
	
	<div id="pnlRegisterKeyFile" style="display: none" class="panel panel-default">
		<form id="file_upload_id" name="file_upload_name" action="sys/uploadFile" method="post" enctype="multipart/form-data">  
	        <div><input type="file" name="file_upload"/></div>  
	        <div><input type="submit" value="submit"/></div>  
	    </form>
	</div>
	
	<div class="container">
	    <div class="modal" id="msgWin" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-head">
	                    <a href="#" class="close" data-dismiss="modal">&times; &nbsp;&nbsp;</a>
	                    <h4 id="msgWin_title">&nbsp;&nbsp;warn</h4>
	                </div>
	                <div class="modal-body" id="msgWin_content">
	                </div>
	                <div class="modal-footer">
	                    <button type="button" onclick="javascript:closeMsgDialog();" class="btn btn-primary">OK</button>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

    <script>    
    var format = function (json) {
        var options = {
            dom: '#runtime_config_content',
            isCollapsible: true,
            quoteKeys: true,
            tabSize: 2
        };
        window.jf = new JsonFormater(options);
        jf.doFormat(json);
    };

    var reloadRuntimeConfig = function() {
    	loadRuntimeConfig(true);
    };

    var loadRuntimeConfig = function(reload) {
        var title = "XLPay Node Server Runtime Configuration";
    	$('#config_title1').html(title);
    	$('#config_title2').html("loading ...");
    	$('#runtime_config_content').html("");
        $.ajax({
            type: "POST",
            url: reload?"sys/reloadconfig":"sys/getsysinfo",
            data: null ,
            dataType: "json",
            success: function success(data, textStatus, jqXHR) {
    			console.log("result", data.result);
            	if (data.status == 0) {
            		var code = (data.result['properties']==null?'':(data.result['properties']['terminal.code']==null?'':data.result['properties']['terminal.code'])) + '&nbsp;&nbsp;';
            		var name = data.result['properties']==null?'':(data.result['properties']['terminal.name']==null?'':data.result['properties']['terminal.name']);
            		$('#config_title1').html(code + title + (reload?" (reloaded)":""));
            		$('#config_title2').html(name + (data.result['registered']?"":" <span style='color:red'>未注册</span>"));
                	format(data.result);
            	}else{
            		$('#config_title2').html((reload?"re":"") + "load config failed");
            	}
            }});
    };
    
    var loadSystemConfig = function() {
        $.ajax({
            type: "POST",
            url: "sys/getSystemConfigFileContent",
            data: null ,
            dataType: "json",
            success: function success(data, textStatus, jqXHR) {
    			console.log("result", data);
            	if (data.status == 0) {
            		$('#system_config_content').val(data.result);
            	}else{
            		$('#system_config_content').val("error");
            	}
            }});
    };
    
    var setSystemConfig = function() {
    	var sysconf = $('#system_config_content').val();
        $.ajax({
            type: "POST",
            url: "sys/setSystemConfigFileContent",
            data: {
            	sysconf: sysconf
            },
            dataType: "json",
            success: function success(data, textStatus, jqXHR) {
    			console.log("result", data);
            	if (data.status == 0) {
            		$('#system_config_content').val(data.result);
            		showMsg('reset system configuration successful !<br/>please reload runtime configuration manually!');
            	}else{
            		$('#system_config_content').val("error");
            	}
            }});
    };

    var ShowRuntimeConfig = function(forceload) {
    	if ($('#runtime_config_content').html() == '' || forceload) {
    		loadRuntimeConfig();
    	}
    	$('#pnlRuntimeConfiguration').css('display','block');
    	$('#liRuntimeConfiguration').attr("class", "active");
    	$('#pnlSystemConfiguration').css('display','none');
    	$('#liSystemConfiguration').attr("class", "");
    };
    var ShowSystemConfig = function(forceload) {
    	if ($('#system_config_content').val() == '' || forceload) {
    		loadSystemConfig();
    	}
    	$('#pnlRuntimeConfiguration').css('display','none');
    	$('#liRuntimeConfiguration').attr("class", "");
    	$('#pnlSystemConfiguration').css('display','block');
    	$('#liSystemConfiguration').attr("class", "active");
    };
    var ShowSystemRegisterKeyFile = function() {

    	$('#pnlRegisterKeyFile').css('display','block');
    	$('#liRegisterKeyFile').attr("class", "active");
    	
    	$('#pnlRuntimeConfiguration').css('display','none');
    	$('#liRuntimeConfiguration').attr("class", "");
    	$('#pnlSystemConfiguration').css('display','none');
    	$('#liSystemConfiguration').attr("class", "");
    }
    loadRuntimeConfig(false);
    
    var showMsg = function(msg) {
		$('#msgWin_content').html(msg);
        $("#msgWin").modal("show");
        //setTimeout(function(){$("#msgWin").modal("hide")},20000);
    }
    var closeMsgDialog = function() {
    	$("#msgWin").modal("hide");
    }
    var LogOff = function() {
	    $.ajax({
	        type: "POST",
	        url: "sys/adminLogout",
	        data: null,
	        dataType: "json",
	        success: function success(data, textStatus, jqXHR) {
        		window.location.href = "AdminLogin.jsp";
	        }});
    }
    </script>
  </body>
</html>