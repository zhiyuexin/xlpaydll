package com.xl.xlpay.nodesrv.app;

import com.zh.annotation.rest.ApplicationPath;
import com.zh.annotation.rest.ConfigPath;
import com.zh.exception.RestfulException;
import com.zh.rest.resource.ResourceConfig;

@ApplicationPath("/")

@ConfigPath("service-config.xml")
public class app extends ResourceConfig {

	public app() throws RestfulException {
		super();
	}

	@Override
	public void init() throws RestfulException {
		register("com.xl.xlpay.nodesrv.srv");
	}

	public static void main(String[] args) {
	}
}
