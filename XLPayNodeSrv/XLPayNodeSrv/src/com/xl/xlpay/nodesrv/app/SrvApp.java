package com.xl.xlpay.nodesrv.app;

import java.util.Calendar;

import com.zh.annotation.rest.ApplicationPath;
import com.zh.annotation.rest.ConfigPath;
import com.zh.exception.RestfulException;
import com.zh.rest.resource.ResourceConfig;

@ApplicationPath("/srv")

@ConfigPath("service-config.xml")
public class SrvApp extends ResourceConfig {

	public SrvApp() throws RestfulException {
		super();
	}

	@Override
	public void init() throws RestfulException {
		register("com.xl.xlpay.nodesrv.srv");
	}

	// ��������ʱ����token
	public static void main(String[] args) {
		long l = Calendar.getInstance().getTimeInMillis() + 60000;
		System.out.println(l);
		System.out.println(MD5("10001" + "iowjfsajfowe" + l + "f9dfsd8fhwfw"));
	}

	private static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
}
