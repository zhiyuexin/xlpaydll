package com.xl.xlpay.nodesrv.srv;

import com.xl.xlpay.nodesrv.util.Util;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;
/*
 * 查询
 */
@WebServlet
@Path("/qry")
public class Qry {
	@ContextResource
	ServletRequest request;
	/**
	 * 查询款机终端的最大流水号
	 * 	trans_id	业务编码，固定为B3
	 * 	partner_id	商户编码
	 * 	com_code	门店编码
	 * 	pos_no		收款设备编号
	 */
	@POST
	@Path("/getmaxbillid")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String getMaxBillId() {
		return Sys.doAPI("getPayTerminalMaxBillId", null, Util.requestParamerterToMap(request.getRequest()));
	}
	/**
	 * 查询流水记录
	 */
	@POST
	@Path("/gettraderec")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String getTradeRec() {
		return Sys.doAPI("getPayTerminalTradeRec", null, Util.requestParamerterToMap(request.getRequest()));
	}
}
