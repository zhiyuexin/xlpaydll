package com.xl.xlpay.nodesrv.srv.vo;

import java.util.HashMap;
import java.util.Map;

public class ParnterPayCode {
	private String partnerId;
	private String payCode;
	private String comCode;
	private String payAccNo;
	private String sysNo;
	private String payType;
	public Map<String, String> toMap() {
		Map<String, String> result = new HashMap<String, String>();
		result.put("partner_id", partnerId);
		result.put("pay_code", payCode);
		result.put("com_code", comCode);
		result.put("pay_acc_no", payAccNo);
		result.put("sys_no", sysNo);
		result.put("pay_type", payType);
		return result;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPayCode() {
		return payCode;
	}
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	public String getComCode() {
		return comCode;
	}
	public void setComCode(String comCode) {
		this.comCode = comCode;
	}
	public String getPayAccNo() {
		return payAccNo;
	}
	public void setPayAccNo(String payAccNo) {
		this.payAccNo = payAccNo;
	}
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
}
