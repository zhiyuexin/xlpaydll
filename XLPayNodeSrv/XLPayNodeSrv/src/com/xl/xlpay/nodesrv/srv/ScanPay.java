package com.xl.xlpay.nodesrv.srv;

import com.xl.xlpay.nodesrv.util.Util;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.PathParam;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;
@WebServlet
@Path("/scan_pay")


/*
 * 主扫-扫顾客手机条码支付
 */
public class ScanPay {
	private String apiName = "scanPay";
	
	@ContextResource
	ServletRequest request;
	
	@POST
	@Path("/{payCode}/{erpType}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String doApi(@PathParam("payCode") String payCode,@PathParam("erpType") String erpType) {
		return doApi(payCode, erpType, null);
	}
	
	@POST
	@Path("/{payCode}/{erpType}/{extParams}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String doApi(@PathParam("payCode") String payCode,@PathParam("erpType") String erpType,@PathParam("extParams") String extParams) {
		return Sys.systemRuntimeParam==null?null:Sys.systemRuntimeParam.getServerResponseStringByParam(payCode, erpType, apiName, extParams, Util.requestParamerterToMap(request.getRequest()));
	}
}
