package com.xl.xlpay.nodesrv.srv.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xl.xlpay.nodesrv.util.Util;

public class NodeSrvAPI {
	
	private static Logger logger = LoggerFactory.getLogger(NodeSrvAPI.class);
	
	private String name;
	private String uri;
	private String server;
	private String url;
	private Integer version;
	private String comment;
	private List<NodeSrvAPIParam> params;
	
	public void addParam(String paramName,String paramAlias, String paramType, String paramComment, boolean isRequired) {
		if (this.params == null) {
			this.params = new ArrayList<NodeSrvAPIParam>();
		}
		this.params.add(new NodeSrvAPIParam(paramName, paramAlias, paramType, paramComment, isRequired));
	}

	public NodeSrvAPI() {
		super();
	}
	public NodeSrvAPI(String name, String uri, Integer version, String comment) {
		super();
		this.name = name;
		this.uri = uri;
		this.version = version;
		this.comment = comment;
		this.params = new ArrayList<NodeSrvAPIParam>();
	}
	public NodeSrvAPI(String name, String uri, Integer version, String comment, List<NodeSrvAPIParam> params) {
		super();
		this.name = name;
		this.uri = uri;
		this.version = version;
		this.comment = comment;
		this.params = params;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(url);
		sb.append("?");
		for (int i=0; i<params.size()-1; i++) {
			if (i>0) sb.append("&");
			sb.append(params.toString());
		}
		return sb.toString();
	}
	@SuppressWarnings("rawtypes")
	public String getParamsValue(String name, Map... valueMaps) {
		String result = null;
		for (Map valueMap: valueMaps){
			if (valueMap == null || valueMap.size() == 0) continue;
			result = getParamsValue(name, valueMap);
			if (result != null) break;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	private String getParamsValue(String name, Map values) {
		if (Util.isEmpty(name)) return null;
		String value = null;
		for (NodeSrvAPIParam param: params) {
			if (!name.equals(param.getName())) continue;
			value = getMapValue(values, param.getName());
			if (value != null) {
				param.setValue(value);
				logger.debug("=>get data: {} = {}", param.getName(), value);
			} else if (param.getAlias() != null) {
				value = getMapValue(values, param.getAlias());
				if (value != null) {
					param.setValue(value);
					logger.debug("=>get data: {} = {}", param.getAlias(), value);
				} else if (param.getAliases() != null && param.getAliases().length > 1) {
					for (int i=0; i<param.getAliases().length-1; i++) {
						value = getMapValue(values, param.getAliases()[i]);
						if (value != null) {
							param.setValue(value);
							logger.debug("=>get data: {} = {}", param.getAliases()[i], value);
						}
					}
				}
			}
			value = null;
		}
		return value;
	}
	
	@SuppressWarnings("rawtypes")
	private String getMapValue(Map m, String key) {
		String result = null;
		if (!Util.isEmpty(key) && m.get(key) != null) {
			if(m.get(key) instanceof String[]){
	            String[] values = (String[])m.get(key);
	            key = "";
	            for(int i=0;i<values.length;i++){
						key += values[i] + ",";
	            }
	            result = key.substring(0, key.length()-1);
	        }else{
	        	result = m.get(key).toString();
	        }
		}
		return result;
	}
	public Map<String, Object> paramsMapValue(@SuppressWarnings("rawtypes") Map... paramsMapValues) {
		if (params == null) return null;
		Map<String, Object> result = new HashMap<String, Object>();
		for (NodeSrvAPIParam param: params) {
			String value = getParamsValue(param.getName(), paramsMapValues);
			if (value == null) value = param.getValue();
			result.put(param.getName(), value);
		}
		return result;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public List<NodeSrvAPIParam> getParams() {
		return params;
	}
	public void setParams(List<NodeSrvAPIParam> params) {
		this.params = params;
	}
}
