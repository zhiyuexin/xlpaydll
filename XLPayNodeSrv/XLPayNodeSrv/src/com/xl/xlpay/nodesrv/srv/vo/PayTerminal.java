package com.xl.xlpay.nodesrv.srv.vo;

import java.util.Date;

/**
 * ClassName:PayTerminal <br/>
 * @version  
 * @since    JDK 1.7
 * @see 	 
 */
public class PayTerminal {
	private String id;

    private String comCode;

    private String partnerId;

    private String terminalCode;

    private String terminalName;

    private String srvId;

    private Integer terminalType;

    private String ipAddr;

    private String port;
    
    private String terminalId;

    private String terminalSn;

    private Date regTime;

    private Integer scanType;

    private Integer isNeedPrint;

    private Integer printType;

    private String printPort;

    private String payRemark;

    private String payUserData;

    private String config;

    private String remark;

    private String createEmpl;
    
    private String createEmplName;

    private Date createTime;

    private String alterEmpl;
    
    private String alterEmplName;

    private Date alterTime;

    private Integer validState;
    
    private Boolean isRegisted;
    
    private Boolean isValidSn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	public String getSrvId() {
		return srvId;
	}

	public void setSrvId(String srvId) {
		this.srvId = srvId;
	}

	public Integer getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(Integer terminalType) {
		this.terminalType = terminalType;
	}

	public String getIpAddr() {
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTerminalSn() {
		return terminalSn;
	}

	public void setTerminalSn(String terminalSn) {
		this.terminalSn = terminalSn;
	}

	public Date getRegTime() {
		return regTime;
	}

	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}

	public Integer getScanType() {
		return scanType;
	}

	public void setScanType(Integer scanType) {
		this.scanType = scanType;
	}

	public Integer getIsNeedPrint() {
		return isNeedPrint;
	}

	public void setIsNeedPrint(Integer isNeedPrint) {
		this.isNeedPrint = isNeedPrint;
	}

	public Integer getPrintType() {
		return printType;
	}

	public void setPrintType(Integer printType) {
		this.printType = printType;
	}

	public String getPrintPort() {
		return printPort;
	}

	public void setPrintPort(String printPort) {
		this.printPort = printPort;
	}

	public String getPayRemark() {
		return payRemark;
	}

	public void setPayRemark(String payRemark) {
		this.payRemark = payRemark;
	}

	public String getPayUserData() {
		return payUserData;
	}

	public void setPayUserData(String payUserData) {
		this.payUserData = payUserData;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateEmpl() {
		return createEmpl;
	}

	public void setCreateEmpl(String createEmpl) {
		this.createEmpl = createEmpl;
	}

	public String getCreateEmplName() {
		return createEmplName;
	}

	public void setCreateEmplName(String createEmplName) {
		this.createEmplName = createEmplName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAlterEmpl() {
		return alterEmpl;
	}

	public void setAlterEmpl(String alterEmpl) {
		this.alterEmpl = alterEmpl;
	}

	public String getAlterEmplName() {
		return alterEmplName;
	}

	public void setAlterEmplName(String alterEmplName) {
		this.alterEmplName = alterEmplName;
	}

	public Date getAlterTime() {
		return alterTime;
	}

	public void setAlterTime(Date alterTime) {
		this.alterTime = alterTime;
	}

	public Integer getValidState() {
		return validState;
	}

	public void setValidState(Integer validState) {
		this.validState = validState;
	}

	public Boolean getIsRegisted() {
		return isRegisted;
	}

	public void setIsRegisted(Boolean isRegisted) {
		this.isRegisted = isRegisted;
	}

	public Boolean getIsValidSn() {
		return isValidSn;
	}

	public void setIsValidSn(Boolean isValidSn) {
		this.isValidSn = isValidSn;
	}
}