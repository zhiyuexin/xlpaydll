package com.xl.xlpay.nodesrv.srv;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import com.xl.xlpay.nodesrv.srv.vo.SystemRuntimeParam;
import com.xl.xlpay.nodesrv.util.Util;
import com.zh.annotation.rest.Consumes;
import com.zh.annotation.rest.ContextResource;
import com.zh.annotation.rest.MediaType;
import com.zh.annotation.rest.NeedToken;
import com.zh.annotation.rest.POST;
import com.zh.annotation.rest.Path;
import com.zh.annotation.rest.PathParam;
import com.zh.annotation.rest.Produces;
import com.zh.annotation.rest.WebServlet;
import com.zh.rest.resource.ServletRequest;
/*
 * 系统
 */
@WebServlet
@Path("/sys")
public class Sys {
	public static boolean initializing = true;
	
	public static SystemRuntimeParam systemRuntimeParam = null;
	
	public static String AdministratorName = "xlpns";
	
	public static String AdministratorPassword = "xlpns";

	public static String SystemConfigFile = "";
	
	public static String SystemRealPath = "";
	
	public static String SystemContextPath = "";
	
	@ContextResource
	ServletRequest request;

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/uploadFile")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean uploadFile() {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List items = upload.parseRequest(request.getRequest());
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				FileItem item = (FileItem) itr.next();
				if (!item.isFormField()) {
					if (item.getName() != null && !item.getName().equals("")) {
						File tempFile = new File(item.getName());
						FileUtils.forceMkdir(new File(SystemRealPath + "uploadfiles\\"));
						File f = new File(SystemRealPath + "uploadfiles\\", tempFile.getName());
						item.write(f);
					}
				}
			}
			return true;
		} catch (FileUploadException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@POST
	@Path("/getxlpaycfg")
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean getxlpaycfg() {
		HttpServletRequest rq = request.getRequest();
		String username = Util.parseString(rq.getParameter("username"));
		String password = Util.parseString(rq.getParameter("password"));
		Boolean Result = (username.equals(AdministratorName) && password.equals(AdministratorPassword));
		if (Result) {
			HttpSession session = rq.getSession();
			session.setAttribute("AdminLogin", true);
		}
		return Result;
	}
	
	@POST
	@Path("/adminLogin")
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean adminLogin() {
		HttpServletRequest rq = request.getRequest();
		String username = Util.parseString(rq.getParameter("username"));
		String password = Util.parseString(rq.getParameter("password"));
		Boolean Result = (username.equals(AdministratorName) && password.equals(AdministratorPassword));
		if (Result) {
			HttpSession session = rq.getSession();
			session.setAttribute("AdminLogin", true);
		}
		return Result;
	}
	
	@POST
	@Path("/adminLogout")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean adminLogout() {
		HttpSession session = request.getRequest().getSession();
		session.setAttribute("AdminLogin", false);
		return true;
	}
	
	@POST
	@Path("/reg")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String reg() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return doAPI("regPayTerminal", extParams, Util.requestParamerterToMap(request.getRequest()));
	}

	@POST
	@Path("/logon")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String logon() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return doAPI("logOnPayTerminal", extParams, Util.requestParamerterToMap(request.getRequest()));
	}

	@POST
	@Path("/logoff")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String logoff() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return doAPI("logOffPayTerminal", extParams, Util.requestParamerterToMap(request.getRequest()));
	}

	
	@POST
	@Path("/getmaxbillid")
	@Produces(MediaType.TEXT_PLAIN)
	@NeedToken
	public String getMaxBillId() {
		String extParams = "srv_id=" + Util.srvId + ",ip_addr=" + request.getRequest().getRemoteHost() + ",port=" + request.getRequest().getRemotePort();
		return doAPI("getPayTerminalMaxBillId", extParams, Util.requestParamerterToMap(request.getRequest()));
	}

	@POST
	@Path("/getterminalsn/{terminalId}/{userCode}/{userPwd}")
	@Consumes({MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN,MediaType.TEXT_PLAIN})
	@Produces(MediaType.TEXT_PLAIN)
	public String getPayTerminalSn(@PathParam("terminalId") String terminalId,@PathParam("userCode") String userCode,@PathParam("userPwd") String userPwd) {
		String extParams = "terminal_id=" + terminalId + ",user_code=" + userCode + ",user_pwd=" + userPwd;
		return doAPI("getPayTerminalTradeRec", extParams, Util.requestParamerterToMap(request.getRequest()));
	}
	
	@POST
	@Path("/getdate")
	@Produces(MediaType.TEXT_PLAIN)
	public String getdate() {
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
		return sFormat.format(new Date());
	}

	@POST
	@Path("/getsysinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public SystemRuntimeParam getSysInfo() {
		return systemRuntimeParam;
	}
	/**
	 * 加载系统配置
	 */
	@Path("/reloadconfig")
	@Produces(MediaType.APPLICATION_JSON)
	public SystemRuntimeParam loadSystemConfig(){
		systemRuntimeParam = new SystemRuntimeParam();
		return systemRuntimeParam;
	}

	@Path("/getSystemConfigFileContent")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSystemConfigFileContent() {
		return systemRuntimeParam.getSystemConfigFileContent();
	}

	@Path("/setSystemConfigFileContent")
	@Produces(MediaType.TEXT_PLAIN)
	public String setSystemConfigFileContent() {
	    String s = request.getRequest().getParameter("sysconf");
	    s = s.replaceAll("[\r|\n|\r\n|\n\r]", "\r\n");
	    s = s.replaceAll("<br( )*(/){0,1}>", "\r\n");
	    systemRuntimeParam.setSystemConfigFileContent(s);
		return s;
	}
	
	@SuppressWarnings("rawtypes")
	public static String doAPI(String payCode, String erpType, String apiName, String extParams, Map mapParams) {
		return Sys.systemRuntimeParam==null?null:Sys.systemRuntimeParam.getServerResponseStringByParam(payCode, erpType, apiName, extParams, mapParams);
	}
	
	@SuppressWarnings("rawtypes")
	public static String doAPI(String apiName, String extParams, Map mapParams) {
		return Sys.systemRuntimeParam==null?null:Sys.systemRuntimeParam.getServerResponseStringByParam(apiName, null, extParams, mapParams);
	}
}