package com.xl.xlpay.nodesrv.srv.vo;

import com.alibaba.fastjson.JSON;

public class ResultInfo {
	private Boolean srvError;
    private Boolean success;
    private String errCode;
    private String errMessage;
    private Object result;

	public ResultInfo(Object result) {
		super();
		this.result = result;
		this.success = true;
		this.errCode = "0";
		this.srvError = false;
	}
	public ResultInfo(String errCode, String errMessage) {
		super();
		this.errCode = errCode;
		this.errMessage = errMessage;
		this.success = errCode==null?false:(errCode.equals("0"));
	}
	public ResultInfo(String errCode, String errMessage,Boolean srvError) {
		super();
		this.srvError = srvError;
		this.errCode = errCode;
		this.errMessage = errMessage;
	}
	public Boolean getSrvError() {
		return srvError;
	}
	public void setSrvError(Boolean srvError) {
		this.srvError = srvError;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrMessage() {
		return errMessage;
	}
	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
