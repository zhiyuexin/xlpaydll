package com.xl.xlpay.nodesrv.srv.vo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xl.xlpay.nodesrv.util.ListProperties;
import com.xl.xlpay.nodesrv.util.Util;

public class SystemRuntimeParam {
	
	private static Logger logger = LoggerFactory.getLogger(SystemRuntimeParam.class);

	private static String SystemConfigFilePath = "";

	private boolean isRegistered = false;
	
	private String srvId = "";

	private List<ParnterPayCode> payCodeList = null;
	
	private Date payCodeListLastGetTime = null;
	
	private List<NodeSrvAPI> srvAPI = null;
	
	private Date srvAPILastGetTime = null;

	private ListProperties properties = null;
	
	public String getSystemConfigFilePath() {
		return SystemConfigFilePath;
	}
	public void setSystemConfigFilePath(String systemConfigFilePath) {
		Util.SystemConfigFilePath = systemConfigFilePath;
		SystemConfigFilePath = systemConfigFilePath;
	}
	public boolean isRegistered() {
		return isRegistered;
	}
	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}
	public String getSrvId() {
		return srvId;
	}
	public void setSrvId(String srvId) {
		this.srvId = srvId;
	}
	public List<ParnterPayCode> getPayCodeList() {
		return payCodeList;
	}
	public void setPayCodeList(List<ParnterPayCode> payCodeList) {
		this.payCodeList = payCodeList;
	}
	public List<NodeSrvAPI> getSrvAPI() {
		return srvAPI;
	}
	public void setSrvAPI(List<NodeSrvAPI> srvAPI) {
		this.srvAPI = srvAPI;
	}
	public ListProperties getProperties() {
		return properties;
	}
	public void setProperties(ListProperties properties) {
		this.properties = properties;
	}
	public Date getSrvAPILastGetTime() {
		return srvAPILastGetTime;
	}
	public void setSrvAPILastGetTime(Date srvAPILastGetTime) {
		this.srvAPILastGetTime = srvAPILastGetTime;
	}
	public Date getPayCodeListLastGetTime() {
		return payCodeListLastGetTime;
	}
	public void setPayCodeListLastGetTime(Date payCodeListLastGetTime) {
		this.payCodeListLastGetTime = payCodeListLastGetTime;
	}
	// 
	public SystemRuntimeParam() {
		super();
		if (!Util.isEmpty(SystemConfigFilePath)) {
			loadSystemPropertiesFromConfigFile(SystemConfigFilePath);
			loadPayCodeList(true);
			regNodeServer();
		}
	}
	// 
	public SystemRuntimeParam(String systemConfigFilePath) {
		super();
		loadSystemPropertiesFromConfigFile(systemConfigFilePath);
		loadPayCodeList(true);
		regNodeServer();
	}
	// 
	public void loadSystemPropertiesFromConfigFile(String systemConfigFilePath) {
		if (Util.isEmpty(systemConfigFilePath)) {
			logger.error("请在web.xml中配置SystemConfigFilePath配置文件路径");
			return;
		}
		setSystemConfigFilePath(systemConfigFilePath);
		if (properties == null) {
			properties = new ListProperties();
		}
		FileInputStream fs =null;
		try {
			logger.info("loading system config file \"{}\"", SystemConfigFilePath);
			fs = new FileInputStream(new File(SystemConfigFilePath));
			properties.load(fs);
		} catch (Exception e) {
			logger.error("加载服务配置信息出错:{}", e.getMessage());
		} finally {
			if (fs != null){
				try {
					fs.close();
				} catch (IOException e) {
					e.printStackTrace();
					logger.error("加载服务配置信息出错:{}", e.getMessage());
				}
			}
		}
	}
	// 
	public String getProperty(String name) {
		if (Util.isEmpty(name)) return null;
		if (properties == null) return null;
		try {
			return properties.getProperty(name);
		} catch (Exception e) {
			logger.error("获取属性信息出错:{}", e.getMessage());
			return null;
		}
	}
	// 
	public void setProperty(String name, String value) {
		try {
			if (properties == null) {
				properties = new ListProperties();
			}
			properties.setProperty(name, value);
		} catch (Exception e) {
			logger.error("设置属性信息出错:{}", e.getMessage());
		}
	}
	// 
	public void regNodeServer() {
		isRegistered = false;
		InetAddress ia = null;
		String terminal_id = null;
		Map<String, Object> params = null;
		try {
			terminal_id = Util.getLocalMac(ia);
			srvId = terminal_id;
			String reg_url = getApiUrl("regPayNodeServer");	
			if (reg_url == null) {
				logger.error("注册失败，未获取到注册API信息");
				return;
			}
			logger.info("注册:{}", reg_url);
			// 生成注册信息
			ia = InetAddress.getLocalHost();
			params = new HashMap<String, Object>();
			params.put("terminal_id", terminal_id);
			params.put("terminal_sn", properties.getProperty("terminal.sn"));
			params.put("terminal_code", properties.getProperty("terminal.code"));
			params.put("terminal_name", properties.getProperty("terminal.name"));
			params.put("terminal_type", properties.getProperty("terminal.type"));
			params.put("com_code", properties.getProperty("com_code"));
			params.put("ip_addr", ia==null?"":ia.getHostAddress());
			params.put("port", "");
			// 提交信息注册
			String str = Util.doPost(reg_url, params);
			logger.info("返回信息:{}", str);
			ResInfo reg_info = JSON.parseObject(str, ResInfo.class);
			if (!reg_info.getSuccess()) {
				logger.error("注册失败:{}", str);
			}
			JSONObject reg_result = reg_info.getResult();
			if (reg_result != null) {
				isRegistered = Util.parseBoolean(reg_result.get("isRegisted"));
			}
			properties.setProperty("terminal.isRegisted", isRegistered?"true":"false");
			if (!isRegistered) {
				logger.error("该服务器未注册，请与管理员联系，本机注册ID为{}，请正确设置注册序列号（terminal.sn）信息后再启动服务。", terminal_id);		
			}else{
				srvId = Util.parseStr(reg_result.get("id"));
				properties.setProperty("srv_id", srvId);
				logger.info("注册成功！SrvId:" + srvId);
			}
		} catch (UnknownHostException e) {
			isRegistered = false;
			e.printStackTrace();
			logger.error("注册失败:{}", e.getMessage());
			return;
		} catch (SocketException e) {
			isRegistered = false;
			e.printStackTrace();
			logger.error("注册失败:{}", e.getMessage());
			return;
		}catch(Exception e){
			isRegistered = false;
			e.printStackTrace();
			logger.error("注册失败:{}", e.getMessage());
		}
	}

	public void getAPI() {
		if (srvAPILastGetTime != null && srvAPI != null && srvAPI.size() > 0) {
			if ((new Date().getTime() - srvAPILastGetTime.getTime()) < 1200000 ) {
				return;
			}
		}
		srvAPILastGetTime = null;
		String server = getPayServer();
		if (Util.isEmpty(server)) return;
		String url_getApiList = getProperty("url.get_api_list");
		if (Util.isEmpty(url_getApiList)) {
			url_getApiList = "xlpns/get_api_list.json";
			logger.info("未获取到系统url.get_api_list配置，请在配置文件中配置，当前采用默认配置:{}", url_getApiList);
			setProperty("url.get_api_list", url_getApiList);
		}
		String s = server + url_getApiList;
		s = Util.doPost(s);
		if (Util.isEmpty(s)) return;
		Pattern pattern = Pattern.compile(".*\"success\":false.*");
		Matcher matcher = pattern.matcher(s);
		if (matcher.matches()) {
			logger.error("get API list error: {}", s);
			return;
		}
		logger.info("system APIs:" + s);
		try {
			srvAPI = new ArrayList<NodeSrvAPI>();
			JSONArray apiList = JSON.parseArray(s);
			for (Object o:apiList) {
				NodeSrvAPI api = JSON.toJavaObject((JSONObject)o, NodeSrvAPI.class);
				api.setServer(server);
				api.setUrl(server + api.getUri());
				srvAPI.add(api);
			}
			srvAPILastGetTime = new Date();
		} catch(Exception e) {
			e.printStackTrace();
			try {
				JSONObject ro = JSON.parseObject(s);
				if (ro.get("success") != null) {
					if (!ro.getBooleanValue("success")){
						logger.error("获取系统API列表失败！{}", s);
						return;
					}
				}
			} catch(Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public String getApiUrl(String apiName) {
		if (Util.isEmpty(apiName)) return null;
		String server = getPayServer();
		if (Util.isEmpty(server)) return null;
		if (srvAPI == null || srvAPI.size() == 0) {
			getAPI();
		}
		if (srvAPI == null) return null;
		
		String result = null;
		for (NodeSrvAPI api:srvAPI) {
			if (apiName.equals(api.getName())) {
				return server + api.getUri();
			}
		}
		
		result = getProperty(apiName);
		if (result == null && server != null){
			result = apiName;
		}
		if (server !=null && result != null && result.indexOf(server) == -1){
			result = server + result;
		}
		return result;
    }

	public NodeSrvAPI getApi(String apiName) {
		if (Util.isEmpty(apiName)) return null;
		String server = getPayServer();
		if (Util.isEmpty(server)) return null;
		if (srvAPI == null || srvAPI.size() == 0) {
			getAPI();
		}
		if (srvAPI == null) return null;
		
		for (NodeSrvAPI api:srvAPI) {
			if (apiName.equals(api.getName())) {
				return api;
			}
		}
		return null;
    }
	
	public String getPayServer() {
		String server = getProperty("server");
		if (Util.isEmpty(server)) {
			loadSystemPropertiesFromConfigFile(SystemConfigFilePath);
			logger.error("未获取到系统server配置，请在配置文件中配置");
			return null;
		}
		if (!server.substring(server.length()-1).equals("/")) {
			server = server + "/";
			setProperty("server", server);
		}
		return server;
	}
	
	public void loadPayCodeList(boolean forceRefresh) {
		if (payCodeListLastGetTime != null && payCodeList != null && payCodeList.size() > 0) {
			if ((new Date().getTime() - payCodeListLastGetTime.getTime()) < 1200000 ) {
				return;
			}
		}
		payCodeListLastGetTime = null;
		String url = getApiUrl("getPaycodeList");
		if (Util.isEmpty(url)) return;
		String s;
		if (forceRefresh){
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("forceRefresh", true);
			s = Util.doPost(url, params);
		}else{
			s = Util.doPost(url);
		}
		
		if (Util.isEmpty(s)) return;
		logger.info("system PayCodeList:" + s);
		Pattern pattern = Pattern.compile(".*\"success\":false.*");
		Matcher matcher = pattern.matcher(s);
		if (matcher.matches()) {
			logger.error("load system PayCodeList error: {}", s);
		}else {
			payCodeList = new ArrayList<ParnterPayCode>();
			JSONArray codeList = JSON.parseArray(s);
			for (Object o:codeList) {
				ParnterPayCode code = JSON.toJavaObject((JSONObject)o, ParnterPayCode.class);
				payCodeList.add(code);
			}
			payCodeListLastGetTime = new Date();
		}
	}

	public String getSystemConfigFileContent() {
		FileInputStream fis = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		StringBuilder sb;
		try {
			fis = new FileInputStream(SystemConfigFilePath);   
			isr = new InputStreamReader(fis, "UTF-8");   
			br = new BufferedReader(isr);   
			String line = null;   
			sb = new StringBuilder();
			while ((line = br.readLine()) != null) { 
			    sb.append(line).append("\r\n");   
			}   
			return sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (br != null) br.close();
				if (isr != null) isr.close();
				if (fis != null) fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void setSystemConfigFileContent(String content) {
		OutputStreamWriter outs = null;
		try {
	        outs = new OutputStreamWriter(new FileOutputStream(SystemConfigFilePath),"UTF-8");
	        outs.write(content);
	        outs.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if (outs != null) outs.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	public ParnterPayCode getPayCode(String partnerId, String payCode, String comCode) {
		if (Util.isEmpty(partnerId) || Util.isEmpty(payCode) || Util.isEmpty(comCode)) return null;
		if (payCodeList == null) {
			loadPayCodeList(false);
		}
		if (payCodeList == null) return null;
		for (ParnterPayCode p: payCodeList) {
			if (partnerId.equals(p.getPartnerId())
					 && payCode.equals(p.getPayCode())
					 && comCode.equals(p.getComCode())){
				return p;
			}
		}
		return null;
	}

	// 将传入的扩展参数字符串转换为Map（解决传入中文问题）
	//	function TfrmPay.GetExtParams: String;
	//	begin
	//	  Result := 'com_code=' + ComCode +
	//	    ',operator_id=' + OperatorId +
	//	    ',operator_name=' + OperatorName;
	//	end;
	public Map<String, String> stringParamToMap(String extParams) {
		Map<String, String> result = new HashMap<String, String>();
		if (extParams != null && !extParams.isEmpty()){
			String[] params = extParams.split(",");
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null && !params[i].isEmpty()){
					String[] values = params[i].split("=");
					if (values != null && values.length>1) {
						result.put(values[0], values[1]);
					}
				}
			}
		}
		return result;
	}
    @SuppressWarnings("rawtypes")
    public String getServerResponseStringByParam(String apiName, ParnterPayCode paycode, String extParams, Map extMapParams) {
    	ResultInfo result;
    	NodeSrvAPI api = getApi(apiName);
    	if (api == null) {
			result = new ResultInfo("E402", "invalid api request");
			return result.toString();
    	}
		return Util.doPost(	api,
							extMapParams,
			    			extParams==null?null:stringParamToMap(extParams),
			    			paycode==null?null:paycode.toMap());//参数优先级依次降低
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public String getServerResponseStringByParam(String payCode, String partnerId, String apiName, String extParams, Map extMapParams) {
    	ResultInfo result;    	
		// 判断payType和erpType的合法性
		if (Util.isEmpty(partnerId)){
			result = new ResultInfo("E502", "invalid param partner_id");
			return result.toString();
		}
		if (Util.isEmpty(payCode)){
			result = new ResultInfo("E503", "invalid param pay_code");
			return result.toString();
		}
		String comCode = null;
		try {
			comCode = Util.parseStr(extMapParams.get("com_code"));
			if (Util.isEmpty(comCode)) comCode = Util.getQueryStringByName(extParams, "com_code");
			if (Util.isEmpty(comCode)) comCode = getProperty("com_code");
		} catch(Exception e) {
		}
		if (comCode == null) {
			result = new ResultInfo("E501", "invalid param com_code"); 
			return result.toString();
		}
		String deptCode = null;
		try {
			deptCode = Util.parseStr(extMapParams.get("dept_code"));
			if (Util.isEmpty(deptCode)) deptCode = Util.getQueryStringByName(extParams, "dept_code");
			if (Util.isEmpty(deptCode)) { //客户端未传入，取节点服务器配置
				deptCode = getProperty("dept_code");
				if (Util.isEmpty(deptCode)) deptCode = "999999"; //节点服务器不设置，则取“999999”
				extMapParams.put("dept_code", deptCode);
			}
		} catch(Exception e) {
		}
    	ParnterPayCode paycode = getPayCode(partnerId, payCode, comCode);
		if (paycode == null){
			result = new ResultInfo("E504", "failed to find configuration of pay_code"); 
			return result.toString();
		}
		return getServerResponseStringByParam(apiName, paycode, extParams, extMapParams);
    }
}
