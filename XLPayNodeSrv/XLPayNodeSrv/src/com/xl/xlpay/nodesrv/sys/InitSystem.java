package com.xl.xlpay.nodesrv.sys;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xl.xlpay.nodesrv.srv.Sys;
import com.xl.xlpay.nodesrv.srv.vo.SystemRuntimeParam;
import com.xl.xlpay.nodesrv.util.IpUtil;
import com.xl.xlpay.nodesrv.util.Util;


/**
 * Servlet implementation class InitSystem
 */
public class InitSystem extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(InitSystem.class);
	
	public InitSystem() {
        super();
        logger.info("XLPay Node Server");
        logger.info("ver 2.5 (20160413)");
	}
	
	@Override
	public void init() throws ServletException {
        super.init();
        logger.info("service starting ...");
        SysContextListener.timerInterval = 10;
        // 初始化管理用户名及密码
        Sys.AdministratorName = Util.parseStr(getInitParameter("AdministratorName"),"xlpns");
        Sys.AdministratorPassword = Util.parseStr(getInitParameter("AdministratorPassword"),"xlpns");
        
        Sys.initializing = true;
		try {
	        ServletContext context = this.getServletContext();
	        String path = context.getRealPath("/");
	        int pos=path.lastIndexOf("\\", path.length()-2);
	        pos=path.lastIndexOf("\\", pos-1);
	        Sys.SystemRealPath = path.substring(0, pos+1);
	        logger.info("service path: {}", Sys.SystemRealPath);
	        logger.info("server ip: {}", IpUtil.getRealIp());
	        logger.info("service run at server: {}", context.getServerInfo());
	        Sys.SystemContextPath = context.getContextPath();
	        logger.info("service context path: {}", Sys.SystemContextPath);
	        logger.info("system configuration page: {}", context.getContextPath() + "/SystemConfig.jsp");
	        // 获取配置文件信息
	        path = getInitParameter("SystemConfigFilePath");
	        // 自动创建配置文件
			String forlder = path.substring(0, path.lastIndexOf(File.separatorChar));
			File f = new File(forlder);
			if (!f.exists()) {
				FileUtils.forceMkdir(f); // f.mkdir();
		        logger.error("system config file is empty! please login SystemConfig.jsp to configurate.");
			}else{
				f = new File(path);
				if (!f.exists()) {
					f.createNewFile();
			        logger.error("system config file is empty! please login SystemConfig.jsp to configurate.");
				}
			}
			Sys.systemRuntimeParam = new SystemRuntimeParam(path);
	        logger.info("service started.");
		} catch (Exception e) {
	        logger.error("starting service occured an error: {}", e.getMessage());
			e.printStackTrace();
		} finally {
			Sys.initializing = false;
		}
        
        

//      CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build(false);
//      cacheManager.init();
//      Cache<String, String> cache = cacheManager.createCache("cache",
//      		CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class).build());
//
//      if (SystemConfigFile !=null) cache.put("SystemConfigFile", SystemConfigFile);
//      if (db_url !=null) cache.put("db.url", db_url);
//      if (db_driver !=null) cache.put("db.driver", db_driver);
//      if (db_user !=null) cache.put("db.user", db_user);
//      if (db_password !=null) cache.put("db.password", db_password);
//      cacheManager.close();
    }
    
}
