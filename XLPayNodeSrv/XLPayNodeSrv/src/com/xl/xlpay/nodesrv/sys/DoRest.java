package com.xl.xlpay.nodesrv.sys;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.xl.xlpay.nodesrv.srv.Sys;
import com.zh.rest.route.RestFilter;

/**
 * 只需要继承RestFilter并调用super方法即可。<br/>
 * <br/>
 * 你需要在web.xml中指定过滤器的初始化参数Applications来注册应用，多个应用可以用逗号分隔。
 */
public class DoRest extends RestFilter implements Filter {
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		//LOGGER.debug("执行过滤");
		if (Sys.systemRuntimeParam !=null && Sys.systemRuntimeParam.isRegistered()){
			super.doFilter(request, response, chain);
		}else{
			HttpServletRequest req = (HttpServletRequest) request;
			String s = req.getRequestURI();
			Pattern pattern = Pattern.compile("[/\\w*]*/sys[/\\w*]+|[/].*(\\.(?i)(jpg|png|gif|bmp|js|css|jsp|html))$");
			Matcher matcher = pattern.matcher(s);
			if (matcher.matches()) {
				super.doFilter(request, response, chain);
			}else{		  
				System.out.println("系统未注册，访问被拒绝： " + s);
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		super.init(fConfig);
	}

}
