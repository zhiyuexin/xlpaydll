package com.xl.xlpay.nodesrv.sys;

import java.util.TimerTask;

import javax.servlet.ServletContext;

import com.xl.xlpay.nodesrv.srv.Sys;
import com.xl.xlpay.nodesrv.srv.vo.SystemRuntimeParam;

public class SysTask extends TimerTask {
//	private static final int C_SCHEDULE_HOUR = 0;
	private static boolean isRunning = false;
	private ServletContext context = null;

	public SysTask(ServletContext context) {
		this.context = context;
	}

	@Override
	public void run() {
		if (Sys.initializing) return;
		if (isRunning) {
			context.log("上一个任务正在执行中");
			return;
		}
		isRunning = true;
		try{
			if (Sys.systemRuntimeParam == null) {
				if (!Sys.initializing) Sys.systemRuntimeParam = new SystemRuntimeParam();
				if (Sys.systemRuntimeParam == null){
					context.log("系统没有初始化！！！请在web.xml中配置SystemConfigFilePath信息后重启服务器");
					return;
				}
			}
			Sys.systemRuntimeParam.getAPI();
			if (!Sys.systemRuntimeParam.isRegistered()) {
				context.log("注册节点服务器...");
				Sys.systemRuntimeParam.regNodeServer();
			}
			if (Sys.systemRuntimeParam.getPayCodeList() == null) {
				Sys.systemRuntimeParam.loadPayCodeList(true);
			}else{
				Sys.systemRuntimeParam.loadPayCodeList(false);
			}
		} finally {
			isRunning = false;
		}
//		Calendar c = Calendar.getInstance();
//		if (C_SCHEDULE_HOUR == c.get(Calendar.HOUR_OF_DAY)) {
//			isRunning = true;
//			context.log("开始执行指定任务");
//			// -------------------开始保存当日历史记录
//
//			// 在这里编写自己的功能，例：
//			// File file = new File("temp");
//			// file.mkdir();
//			// 启动tomcat，可以发现在tomcat根目录下，会自动创建temp文件夹
//
//			// -------------------结束
//			isRunning = false;
//			context.log("指定任务执行结束");
//		}
	}

}
