package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class SystemConfig_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"zh-CN\">\r\n");
      out.write("  <head>\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("    <title>Node Server Runtime Configuration</title>\r\n");
      out.write("\r\n");
      out.write("    <!-- Bootstrap -->\r\n");
      out.write("    <link href=\"bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->\r\n");
      out.write("    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->\r\n");
      out.write("    <!--[if lt IE 9]>\r\n");
      out.write("      <script src=\"resources/js/html5shiv.min.js\"></script>\r\n");
      out.write("      <script src=\"resources/js/respond.min.js\"></script>\r\n");
      out.write("    <![endif]-->\r\n");
      out.write("\r\n");
      out.write("    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\r\n");
      out.write("    <script src=\"resources/js/jquery.min.js\"></script>\r\n");
      out.write("    <!-- Include all compiled plugins (below), or include individual files as needed -->\r\n");
      out.write("    <script src=\"bootstrap/js/bootstrap.min.js\"></script>\r\n");
      out.write("    <script src=\"resources/js/jsonFormater.js\" type=\"text/javascript\"></script>\r\n");
      out.write("    <link href=\"resources/css/jsonFormater.css\" type=\"text/css\" rel=\"stylesheet\"/>\r\n");
      out.write("  </head>\r\n");

Boolean AdminLogin = false;
if (session.getAttribute("AdminLogin") != null) {
	AdminLogin = (Boolean)session.getAttribute("AdminLogin");
}
if (!AdminLogin) {
	response.sendRedirect("AdminLogin.jsp");
}

      out.write("\r\n");
      out.write("  <body>\r\n");
      out.write("  \t<ul class=\"nav nav-tabs\">\r\n");
      out.write("\t  <li id=\"liRuntimeConfiguration\" role=\"presentation\" class=\"active\"><a href=\"javascript:ShowRuntimeConfig();\">Runtime Configuration</a></li>\r\n");
      out.write("\t  <li id=\"liSystemConfiguration\" role=\"presentation\" class=\"\"><a href=\"javascript:ShowSystemConfig();\">System Configuration</a></li>\r\n");
      out.write("\t  <li id=\"liSystemConfiguration\" role=\"presentation\" class=\"\"><a href=\"javascript:LogOff();\">Log Off</a></li>\r\n");
      out.write("\t  <!-- <li id=\"liRegisterKeyFile\" role=\"presentation\" class=\"\"><a href=\"javascript:ShowSystemRegisterKeyFile();\">System RegisterKeyFile</a></li> -->\r\n");
      out.write("\t</ul>\r\n");
      out.write("\t\r\n");
      out.write("\t<div id=\"pnlRuntimeConfiguration\" class=\"panel panel-default\">\r\n");
      out.write("\t  <div class=\"panel-body\">\r\n");
      out.write("\t    <h2 id=\"config_title1\"></h2>\r\n");
      out.write("\t    <h3 id=\"config_title2\"></h3>\r\n");
      out.write("\t    <div id=\"runtime_config_content\" class=\"Canvas\"></div>\r\n");
      out.write("\t    <br />\r\n");
      out.write("\t    <a class=\"btn btn-lg btn-success\" href=\"javascript:reloadRuntimeConfig();\" role=\"button\">reload runtime configuration</a>\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t  </div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<div id=\"pnlSystemConfiguration\" style=\"display: none\" class=\"panel panel-default\">\r\n");
      out.write("\t  <div class=\"panel-body\">\r\n");
      out.write("\t    <textarea id=system_config_content class=\"form-control\" placeholder=\"\" rows=\"20\"></textarea>\r\n");
      out.write("\t    <br />\r\n");
      out.write("\t    <a class=\"btn btn-lg btn-success\" href=\"javascript:setSystemConfig();\" role=\"button\">set system configuration</a>\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t  </div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<div id=\"pnlRegisterKeyFile\" style=\"display: none\" class=\"panel panel-default\">\r\n");
      out.write("\t\t<form id=\"file_upload_id\" name=\"file_upload_name\" action=\"sys/uploadFile\" method=\"post\" enctype=\"multipart/form-data\">  \r\n");
      out.write("\t        <div><input type=\"file\" name=\"file_upload\"/></div>  \r\n");
      out.write("\t        <div><input type=\"submit\" value=\"submit\"/></div>  \r\n");
      out.write("\t    </form>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<div class=\"container\">\r\n");
      out.write("\t    <div class=\"modal\" id=\"msgWin\" aria-hidden=\"true\">\r\n");
      out.write("\t        <div class=\"modal-dialog\">\r\n");
      out.write("\t            <div class=\"modal-content\">\r\n");
      out.write("\t                <div class=\"modal-head\">\r\n");
      out.write("\t                    <a href=\"#\" class=\"close\" data-dismiss=\"modal\">&times; &nbsp;&nbsp;</a>\r\n");
      out.write("\t                    <h4 id=\"msgWin_title\">&nbsp;&nbsp;warn</h4>\r\n");
      out.write("\t                </div>\r\n");
      out.write("\t                <div class=\"modal-body\" id=\"msgWin_content\">\r\n");
      out.write("\t                </div>\r\n");
      out.write("\t                <div class=\"modal-footer\">\r\n");
      out.write("\t                    <button type=\"button\" onclick=\"javascript:closeMsgDialog();\" class=\"btn btn-primary\">OK</button>\r\n");
      out.write("\t                </div>\r\n");
      out.write("\t            </div>\r\n");
      out.write("\t        </div>\r\n");
      out.write("\t    </div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("    <script>    \r\n");
      out.write("    var format = function (json) {\r\n");
      out.write("        var options = {\r\n");
      out.write("            dom: '#runtime_config_content',\r\n");
      out.write("            isCollapsible: true,\r\n");
      out.write("            quoteKeys: true,\r\n");
      out.write("            tabSize: 2\r\n");
      out.write("        };\r\n");
      out.write("        window.jf = new JsonFormater(options);\r\n");
      out.write("        jf.doFormat(json);\r\n");
      out.write("    };\r\n");
      out.write("\r\n");
      out.write("    var reloadRuntimeConfig = function() {\r\n");
      out.write("    \tloadRuntimeConfig(true);\r\n");
      out.write("    };\r\n");
      out.write("\r\n");
      out.write("    var loadRuntimeConfig = function(reload) {\r\n");
      out.write("        var title = \"XLPay Node Server Runtime Configuration\";\r\n");
      out.write("    \t$('#config_title1').html(title);\r\n");
      out.write("    \t$('#config_title2').html(\"loading ...\");\r\n");
      out.write("    \t$('#runtime_config_content').html(\"\");\r\n");
      out.write("        $.ajax({\r\n");
      out.write("            type: \"POST\",\r\n");
      out.write("            url: reload?\"sys/reloadconfig\":\"sys/getsysinfo\",\r\n");
      out.write("            data: null ,\r\n");
      out.write("            dataType: \"json\",\r\n");
      out.write("            success: function success(data, textStatus, jqXHR) {\r\n");
      out.write("    \t\t\tconsole.log(\"result\", data.result);\r\n");
      out.write("            \tif (data.status == 0) {\r\n");
      out.write("            \t\tvar code = (data.result['properties']==null?'':(data.result['properties']['terminal.code']==null?'':data.result['properties']['terminal.code'])) + '&nbsp;&nbsp;';\r\n");
      out.write("            \t\tvar name = data.result['properties']==null?'':(data.result['properties']['terminal.name']==null?'':data.result['properties']['terminal.name']);\r\n");
      out.write("            \t\t$('#config_title1').html(code + title + (reload?\" (reloaded)\":\"\"));\r\n");
      out.write("            \t\t$('#config_title2').html(name + (data.result['registered']?\"\":\" <span style='color:red'>未注册</span>\"));\r\n");
      out.write("                \tformat(data.result);\r\n");
      out.write("            \t}else{\r\n");
      out.write("            \t\t$('#config_title2').html((reload?\"re\":\"\") + \"load config failed\");\r\n");
      out.write("            \t}\r\n");
      out.write("            }});\r\n");
      out.write("    };\r\n");
      out.write("    \r\n");
      out.write("    var loadSystemConfig = function() {\r\n");
      out.write("        $.ajax({\r\n");
      out.write("            type: \"POST\",\r\n");
      out.write("            url: \"sys/getSystemConfigFileContent\",\r\n");
      out.write("            data: null ,\r\n");
      out.write("            dataType: \"json\",\r\n");
      out.write("            success: function success(data, textStatus, jqXHR) {\r\n");
      out.write("    \t\t\tconsole.log(\"result\", data);\r\n");
      out.write("            \tif (data.status == 0) {\r\n");
      out.write("            \t\t$('#system_config_content').val(data.result);\r\n");
      out.write("            \t}else{\r\n");
      out.write("            \t\t$('#system_config_content').val(\"error\");\r\n");
      out.write("            \t}\r\n");
      out.write("            }});\r\n");
      out.write("    };\r\n");
      out.write("    \r\n");
      out.write("    var setSystemConfig = function() {\r\n");
      out.write("    \tvar sysconf = $('#system_config_content').val();\r\n");
      out.write("        $.ajax({\r\n");
      out.write("            type: \"POST\",\r\n");
      out.write("            url: \"sys/setSystemConfigFileContent\",\r\n");
      out.write("            data: {\r\n");
      out.write("            \tsysconf: sysconf\r\n");
      out.write("            },\r\n");
      out.write("            dataType: \"json\",\r\n");
      out.write("            success: function success(data, textStatus, jqXHR) {\r\n");
      out.write("    \t\t\tconsole.log(\"result\", data);\r\n");
      out.write("            \tif (data.status == 0) {\r\n");
      out.write("            \t\t$('#system_config_content').val(data.result);\r\n");
      out.write("            \t\tshowMsg('reset system configuration successful !<br/>please reload runtime configuration manually!');\r\n");
      out.write("            \t}else{\r\n");
      out.write("            \t\t$('#system_config_content').val(\"error\");\r\n");
      out.write("            \t}\r\n");
      out.write("            }});\r\n");
      out.write("    };\r\n");
      out.write("\r\n");
      out.write("    var ShowRuntimeConfig = function(forceload) {\r\n");
      out.write("    \tif ($('#runtime_config_content').html() == '' || forceload) {\r\n");
      out.write("    \t\tloadRuntimeConfig();\r\n");
      out.write("    \t}\r\n");
      out.write("    \t$('#pnlRuntimeConfiguration').css('display','block');\r\n");
      out.write("    \t$('#liRuntimeConfiguration').attr(\"class\", \"active\");\r\n");
      out.write("    \t$('#pnlSystemConfiguration').css('display','none');\r\n");
      out.write("    \t$('#liSystemConfiguration').attr(\"class\", \"\");\r\n");
      out.write("    };\r\n");
      out.write("    var ShowSystemConfig = function(forceload) {\r\n");
      out.write("    \tif ($('#system_config_content').val() == '' || forceload) {\r\n");
      out.write("    \t\tloadSystemConfig();\r\n");
      out.write("    \t}\r\n");
      out.write("    \t$('#pnlRuntimeConfiguration').css('display','none');\r\n");
      out.write("    \t$('#liRuntimeConfiguration').attr(\"class\", \"\");\r\n");
      out.write("    \t$('#pnlSystemConfiguration').css('display','block');\r\n");
      out.write("    \t$('#liSystemConfiguration').attr(\"class\", \"active\");\r\n");
      out.write("    };\r\n");
      out.write("    var ShowSystemRegisterKeyFile = function() {\r\n");
      out.write("\r\n");
      out.write("    \t$('#pnlRegisterKeyFile').css('display','block');\r\n");
      out.write("    \t$('#liRegisterKeyFile').attr(\"class\", \"active\");\r\n");
      out.write("    \t\r\n");
      out.write("    \t$('#pnlRuntimeConfiguration').css('display','none');\r\n");
      out.write("    \t$('#liRuntimeConfiguration').attr(\"class\", \"\");\r\n");
      out.write("    \t$('#pnlSystemConfiguration').css('display','none');\r\n");
      out.write("    \t$('#liSystemConfiguration').attr(\"class\", \"\");\r\n");
      out.write("    }\r\n");
      out.write("    loadRuntimeConfig(false);\r\n");
      out.write("    \r\n");
      out.write("    var showMsg = function(msg) {\r\n");
      out.write("\t\t$('#msgWin_content').html(msg);\r\n");
      out.write("        $(\"#msgWin\").modal(\"show\");\r\n");
      out.write("        //setTimeout(function(){$(\"#msgWin\").modal(\"hide\")},20000);\r\n");
      out.write("    }\r\n");
      out.write("    var closeMsgDialog = function() {\r\n");
      out.write("    \t$(\"#msgWin\").modal(\"hide\");\r\n");
      out.write("    }\r\n");
      out.write("    var LogOff = function() {\r\n");
      out.write("\t    $.ajax({\r\n");
      out.write("\t        type: \"POST\",\r\n");
      out.write("\t        url: \"sys/adminLogout\",\r\n");
      out.write("\t        data: null,\r\n");
      out.write("\t        dataType: \"json\",\r\n");
      out.write("\t        success: function success(data, textStatus, jqXHR) {\r\n");
      out.write("        \t\twindow.location.href = \"AdminLogin.jsp\";\r\n");
      out.write("\t        }});\r\n");
      out.write("    }\r\n");
      out.write("    </script>\r\n");
      out.write("  </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
