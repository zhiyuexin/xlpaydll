package com.zh.exception;

/**
 * 系统不可用
 * 
 * @author 张海
 *
 */
public class SystemUnavailableException extends ApplicationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 755307008669132991L;
	
	// 默认
	protected ExceptionMessage errorMess = ExceptionMessage.SYSTEM_UNAVAILABLE;

	public SystemUnavailableException() {
		super("");
	}

	public SystemUnavailableException(Throwable cause) {
		super(cause);
	}

	public SystemUnavailableException(String errMessage) {
		super(errMessage);
	}

	public SystemUnavailableException(ExceptionMessage mess) {
		super(mess);
	}

	public SystemUnavailableException(String errMessage, Throwable cause) {
		super(errMessage, cause);
	}

	public SystemUnavailableException(ExceptionMessage error, Throwable cause) {
		super(error, cause);
	}

	public SystemUnavailableException(ExceptionMessage error, String errMessage) {
		super(error, errMessage);
	}

	public SystemUnavailableException(ExceptionMessage error, String errMessage,
			Throwable cause) {
		super(error, errMessage, cause);
	}
}
