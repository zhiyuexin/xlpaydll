package com.zh.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

//import javax.ejb.EJBException;

/**
 * unchecked exception - root is RuntimeException
 * 用于包装checked异常，将其变为unchecked异常并抛给客户端成为RemoteException
 * 派生自EJBException，具有唯一标识ID，并记录是否已输出到日志中
 * 
 * @author 张海
 * 
 */
public class LoggableEJBException extends Exception /*extends EJBException*/ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4671860136797994044L;

	/** A wrapped Throwable */
	protected Throwable cause = null;

	protected boolean isLogged;
	protected String uniqueID;

	// 默认
	protected ExceptionMessage errorMess = ExceptionMessage.LOGGABLE_EJB_EXCEPTION;

	{
		isLogged = false;
		uniqueID = ExceptionIDGenerator.getExceptionID();
	}

	public ExceptionMessage getError() {
		return errorMess;
	}

	public LoggableEJBException() {
		super("发生EJB应用异常");
	}

	public LoggableEJBException(String message) {
		super(message);
	}

	public LoggableEJBException(Exception cause) {
		super();
		this.cause = cause;
	}

	public LoggableEJBException(ExceptionMessage mess) {
		super();
		this.errorMess = mess;
	}

	public LoggableEJBException(String message, Exception cause) {
		super(message);
		this.cause = cause;
	}

	public LoggableEJBException(ExceptionMessage error, String message) {
		super(message);
		this.errorMess = error;
	}

	public LoggableEJBException(ExceptionMessage error, Throwable cause) {
		super();
		this.cause = cause;
		this.errorMess = error;
	}

	public LoggableEJBException(ExceptionMessage error, String message,
			Exception cause) {
		super(message);
		this.cause = cause;
		this.errorMess = error;
	}

	// Created to match the JDK 1.4 Throwable method.
	public synchronized Throwable initCause(Throwable cause) {
		if (this.cause == null)
			if (this != cause)
				this.cause = cause;
		return cause;
	}

	public void setLogged(boolean logged) {
		isLogged = logged;
	}

	public boolean getLogged() {
		return isLogged;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	/**
	 * 获得异常信息，包含封装的嵌套异常
	 */
	public String getMessage() {
		// Get this exception's message.
		String msg = this.errorMess.getExceptionMessage()
				+ ("".equals(super.getMessage()) || super.getMessage() == null ? ""
						: " - " + super.getMessage());
		Throwable parent = this;
		Throwable child;
		// Look for nested exceptions.
		while ((child = getNestedException(parent)) != null) {
			// Get the child's message.
			String msg2 = child.getMessage();
			// If we found a message for the child exception,
			// we append it.
			if (msg2 != null) {
				if (msg != null) {
					msg += "; %嵌套EJB异常%: " + msg2;
				} else {
					msg = msg2;
				}
			}
			// Any nested ApplicationException will append its own
			// children, so we need to break out of here.
			if (child instanceof LoggableEJBException) {
				break;
			}
			parent = child;
		}
		// Return the completed message.
		return msg;
	}

	/**
	 * 输出跟踪信息到字符序列
	 * 
	 * @return
	 */
	public String getTrace() {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		printStackTrace(pw);
		return sw.toString();
	}

	public Throwable getNestedException(Throwable e) {
		if (e instanceof LoggableEJBException)
			if (e != null) {
				return e.getCause();
			}
		return null;
	}

	public Throwable getCause() {
		return cause;
	}

	public static void main(String args[]) {
		try {
			try {
				try {
					System.out.println(100 / 0);
				} catch (Exception e) {
					throw new SystemUnavailableException(
							ExceptionMessage.APPLICATION_EXCEPTION, "最底层除0错误！",
							e);
				}
			} catch (Exception t) {
				throw new SystemUnavailableException("第二层的错误！", t);
			}
		} catch (Exception t) {
			LoggableEJBException le = new LoggableEJBException("提交订单失败！", t);
			le.setLogged(true);
			// ApplicationException ae = new
			// SystemUnavailableException("系统不可用：请联系管理员", t);
			System.out.println(le.getMessage());
			System.out.println("====================================");
			System.out.println(le.getTrace());
		}
	}
}
