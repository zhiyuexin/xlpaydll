package com.zh.exception;

/**
 * 异常信息
 * 
 * @author ZhangHai
 *
 */
public enum ExceptionMessage {

	/**
	 * APE-0001: 发生应用程序异常
	 */
	APPLICATION_EXCEPTION("APE-0001"),

	/**
	 * APE-1001: 系统不可用
	 */
	SYSTEM_UNAVAILABLE("APE-1001"),

	/**
	 * LE-0001: 业务逻辑应用异常
	 */
	LOGGABLE_EJB_EXCEPTION("LE-0001"),

	/**
	 * S-0000: 未知异常
	 */
	UNKOWN_EXCEPTION("S-0000"),

	/**
	 * S-1000: 内容未找到
	 */
	NOT_FOUND("S-1000"),

	/**
	 * S-1011: 参数不匹配
	 */
	ARGUMENTS_MISMATCHING("S-1011"),

	/**
	 * S-1012: 非法的参数
	 */
	ILLEGAL_ARGUMENT_EXCEPTION("S-1012"),

	/**
	 * S-1013: 数值格式错误
	 */
	NUMBER_FORMAT_EXCEPTION("S-1013"),

	/**
	 * S-1021: Map参数未指定
	 */
	KEY_MISMATCHING("S-1021"),

	/**
	 * S-1900: JAVA反射异常
	 */
	RELECT_EXCEPTION("S-1900"),

	/**
	 * S-1901: 方法未定义
	 */
	METHOD_NOT_DEFINED("S-1901"),

	/**
	 * S-1902: 属性未定义
	 */
	FIELD_NOT_DEFINED("S-1902"),

	/**
	 * E-2000: EJB系统异常
	 */
	EJB_EXCEPTION("E-2000"),

	/**
	 * E-2010: 远程业务异常
	 */
	REMOTE_EXCEPTION("E-2010"),

	/**
	 * E-2100: 查询结果集错误
	 */
	QUERY_RESULT_ERROR("E-2100"),

	/**
	 * E-2105: 渴望带标量的结果
	 */
	RESULT_MAPPING_MISSING("E-2105"),

	/**
	 * H-3001: 账号过期
	 */
	DUE_ACCOUNT("H-3001"),

	/**
	 * H-3002: 人员状态无效
	 */
	EMPLOYEE_INVALID("H-3002"),

	/**
	 * H-3100: 拒绝访问
	 */
	ACCESS_DENIED("H-3100"),

	/**
	 * D-4100: 动态委派异常
	 */
	DELEGATE_EXCEPTION("D-4100"),

	/**
	 * D-4101: 动态业务委派工厂加载失败
	 */
	DELEGATE_FACTORY_CLASS_NOT_FOUND("D-4101"),

	/**
	 * D-4109: 动态委派Naming异常
	 */
	DELEGATE_NAMING_EXCEPTION("D-4109"),

	/**
	 * D-4200: 会话异常
	 */
	SESSION_EXCEPTION("D-4200"),

	/**
	 * REST-1001: RESTful异常
	 */
	RESTFUL_EXCEPTION("REST-1001"),

	/**
	 * J-6000: 数据库异常
	 */
	JDBC_EXCEPTION("J-6000");

	private String errcode;

	private ExceptionMessage(String errcode) {
		this.errcode = errcode;
	}

	public String getExceptionCode() {
		return errcode;
	}

	public String getExceptionMessage() {
		switch (this) {
		case NOT_FOUND:
			return "内容未找到";
		case ARGUMENTS_MISMATCHING:
			return "参数不匹配";
		case ILLEGAL_ARGUMENT_EXCEPTION:
			return "非法的参数";
		case NUMBER_FORMAT_EXCEPTION:
			return "数值格式错误";
		case KEY_MISMATCHING:
			return "Map参数未指定";
		case RELECT_EXCEPTION:
			return "JAVA反射异常";
		case METHOD_NOT_DEFINED:
			return "方法未定义";
		case FIELD_NOT_DEFINED:
			return "属性未定义";
		case EJB_EXCEPTION:
			return "EJB系统异常";
		case REMOTE_EXCEPTION:
			return "远程业务异常";
		case QUERY_RESULT_ERROR:
			return "查询结果集错误";
		case RESULT_MAPPING_MISSING:
			return "渴望带标量的结果";
		case DUE_ACCOUNT:
			return "账号过期";
		case EMPLOYEE_INVALID:
			return "人员状态无效";
		case DELEGATE_EXCEPTION:
			return "动态委派异常";
		case DELEGATE_FACTORY_CLASS_NOT_FOUND:
			return "动态业务委派工厂加载失败";
		case DELEGATE_NAMING_EXCEPTION:
			return "动态委派Naming异常";
		case SESSION_EXCEPTION:
			return "会话异常";
		case JDBC_EXCEPTION:
			return "数据库异常";
		case APPLICATION_EXCEPTION:
			return "发生应用程序异常";
		case SYSTEM_UNAVAILABLE:
			return "系统不可用";
		case LOGGABLE_EJB_EXCEPTION:
			return "业务逻辑应用异常";
		case RESTFUL_EXCEPTION:
			return "RESTful异常";
		case ACCESS_DENIED:
			return "拒绝访问";
		default: // UNKOWN_EXCEPTION
			return "未知错误";
		}
	}
}
