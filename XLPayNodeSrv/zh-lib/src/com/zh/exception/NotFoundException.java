package com.zh.exception;

/**
 * 未找到查找的内容
 * 
 * @author 张海
 *
 */
public class NotFoundException extends ApplicationException {
	
	private static final long serialVersionUID = 1L;
	
	// 默认
	protected ExceptionMessage errorMess = ExceptionMessage.NOT_FOUND;

	public NotFoundException() {
		super("");
	}

	public NotFoundException(Throwable cause) {
		super(cause);
	}

	public NotFoundException(String errMessage) {
		super(errMessage);
	}

	public NotFoundException(ExceptionMessage mess) {
		super(mess);
	}

	public NotFoundException(String errMessage, Throwable cause) {
		super(errMessage, cause);
	}

	public NotFoundException(ExceptionMessage error, Throwable cause) {
		super(error, cause);
	}

	public NotFoundException(ExceptionMessage error, String errMessage) {
		super(error, errMessage);
	}

	public NotFoundException(ExceptionMessage error, String errMessage,
			Throwable cause) {
		super(error, errMessage, cause);
	}
}
