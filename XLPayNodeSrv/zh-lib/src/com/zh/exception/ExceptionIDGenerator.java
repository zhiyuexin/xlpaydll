package com.zh.exception;

import java.util.UUID;

/**
 * 为定制异常类生成UUID，从而判断异常是否已经写日志，避免重复。
 * 
 * @author 张海
 * 
 */
public class ExceptionIDGenerator {

	public static String getExceptionID() {
		String s = UUID.randomUUID().toString();
		return s.replaceAll("-", "");
	}
}
