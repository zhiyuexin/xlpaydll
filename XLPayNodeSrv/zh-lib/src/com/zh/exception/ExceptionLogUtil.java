package com.zh.exception;

import java.rmi.RemoteException;

/**
 * 应用程序异常(checked exception)即Exception的子类 如果无法处理则包装给(unchecked exception)
 * 即RuntimeException的子类，如EJBException。 非受查异常最终都会被EJB容器包装成RemoteException抛给客户端。
 * 
 * 而在 RemoteException 中，实际的异常被存储到一个称为 detail （它是 Throwable 类型的）的公共属性中。
 * 在大多数情况下，这个公共属性保存有一个异常。
 * 
 * 处理复杂RemoteException包装到LoggableEJBException的例子：
 * 
 * try { ... } catch (RemoteException re) { throw
 * ExceptionLogUtil.createLoggableEJBException(re); }
 * 
 * @author 张海
 * 
 */
public class ExceptionLogUtil {

	public static LoggableEJBException createLoggableEJBException(
			RemoteException re) {

		Throwable t = re.detail;
		if (t != null && t instanceof Exception) {
			return new LoggableEJBException((Exception) re.detail);
		} else {
			return new LoggableEJBException(re);
		}
	}

	public static LoggableEJBException createLoggableEJBException(String code,
			RemoteException re) {

		Throwable t = re.detail;
		if (t != null && t instanceof Exception) {
			return new LoggableEJBException(code, (Exception) re.detail);
		} else {
			return new LoggableEJBException(code, re);
		}
	}

	public static LoggableEJBException createLoggableEJBException(
			ExceptionMessage code, RemoteException re) {

		Throwable t = re.detail;
		if (t != null && t instanceof Exception) {
			return new LoggableEJBException(code, (Exception) re.detail);
		} else {
			return new LoggableEJBException(code, re);
		}
	}

	public static LoggableEJBException createLoggableEJBException(
			ExceptionMessage code, String mess, RemoteException re) {

		Throwable t = re.detail;
		if (t != null && t instanceof Exception) {
			return new LoggableEJBException(code, mess, (Exception) re.detail);
		} else {
			return new LoggableEJBException(code, mess, re);
		}
	}
}
