package com.zh.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * checked exception - root is Exception 用于包装业务checked异常，如非指定不会引起EJB容器回滚
 * 可以被LoggableEJBException包装，变成unchecked异常 具有唯一标识ID，并记录是否已输出到日志中
 * 
 * @author 张海
 * 
 */
public class ApplicationException extends java.lang.Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6895726358648839676L;

	/** A wrapped Throwable */
	protected Throwable cause = null;

	protected boolean isLogged;
	protected String uniqueID;

	// 默认
	protected ExceptionMessage errorMess = ExceptionMessage.APPLICATION_EXCEPTION;

	{
		isLogged = false;
		uniqueID = ExceptionIDGenerator.getExceptionID();
	}

	public ExceptionMessage getError() {
		return errorMess;
	}

	public ApplicationException() {
		super("发生应用程序异常");
	}

	public ApplicationException(String message) {
		super(message);
	}

	public ApplicationException(Throwable cause) {
		super();
		this.cause = cause;
	}

	public ApplicationException(ExceptionMessage mess) {
		super();
		this.errorMess = mess;
	}

	public ApplicationException(String message, Throwable cause) {
		super(message);
		this.cause = cause;
	}

	public ApplicationException(ExceptionMessage error, String message) {
		super(message);
		this.errorMess = error;
	}

	public ApplicationException(ExceptionMessage error, Throwable cause) {
		super();
		this.errorMess = error;
		this.cause = cause;
	}

	public ApplicationException(ExceptionMessage error, String message,
			Throwable cause) {
		super(message);
		this.errorMess = error;
		this.cause = cause;
	}

	// Created to match the JDK 1.4 Throwable method.
	public synchronized Throwable initCause(Throwable cause) {
		if (this.cause == null)
			if (this != cause)
				this.cause = cause;
		return cause;
	}

	public void setLogged(boolean logged) {
		isLogged = logged;
	}

	public boolean getLogged() {
		return isLogged;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	/**
	 * 获得异常信息，包含封装的嵌套异常
	 */
	public String getMessage() {
		// Get this exception's message.
		String msg = this.errorMess.getExceptionMessage()
				+ ("".equals(super.getMessage()) || super.getMessage() == null ? ""
						: " - " + super.getMessage());
		Throwable parent = this;
		Throwable child;
		// Look for nested exceptions.
		while ((child = getNestedException(parent)) != null) {
			// Get the child's message.
			String msg2 = child.getMessage();
			// If we found a message for the child exception,
			// we append it.
			if (msg2 != null) {
				if (msg != null) {
					msg += "; $嵌套应用程序异常$: " + msg2;
				} else {
					msg = msg2;
				}
			}
			// Any nested ApplicationException will append its own
			// children, so we need to break out of here.
			if (child instanceof ApplicationException) {
				break;
			}
			parent = child;
		}
		// Return the completed message.
		return msg;
	}

	/**
	 * 输出跟踪信息到字符序列
	 * 
	 * @return
	 */
	public String getTrace() {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		printStackTrace(pw);
		return sw.toString();
	}

	/**
	 * 获取被嵌入的异常
	 * 
	 * @param e
	 *            包裹异常
	 * @return 嵌入异常
	 */
	public Throwable getNestedException(Throwable e) {
		if (e instanceof ApplicationException)
			if (e != null) {
				return e.getCause();
			}
		return null;
	}

	public Throwable getCause() {
		return cause;
	}
}
