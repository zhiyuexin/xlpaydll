package com.zh.exception;

/**
 * Java反射异常
 * 
 * @author 张海
 *
 */
public class RelectionException extends ApplicationException {
	
	private static final long serialVersionUID = 1L;
	
	// 默认
	protected ExceptionMessage errorMess = ExceptionMessage.RELECT_EXCEPTION;

	public RelectionException() {
		super("");
	}

	public RelectionException(Throwable cause) {
		super(cause);
	}

	public RelectionException(String errMessage) {
		super(errMessage);
	}

	public RelectionException(ExceptionMessage mess) {
		super(mess);
	}

	public RelectionException(String errMessage, Throwable cause) {
		super(errMessage, cause);
	}

	public RelectionException(ExceptionMessage error, Throwable cause) {
		super(error, cause);
	}

	public RelectionException(ExceptionMessage error, String errMessage) {
		super(error, errMessage);
	}

	public RelectionException(ExceptionMessage error, String errMessage,
			Throwable cause) {
		super(error, errMessage, cause);
	}
}
