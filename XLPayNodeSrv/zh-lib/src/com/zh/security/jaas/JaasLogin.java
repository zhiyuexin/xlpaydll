package com.zh.security.jaas;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpSession;

public interface JaasLogin {

	public static final String SESSION_SUBJECT = "Subject";
	public static final String SESSION_LOGINCONTEXT = "LoginContext";
	public static final String SESSION_PRINCIPALS = "Principals";
	/**
	 * 调用者实体
	 */
	public static final String SESSION_CALLER = "Caller";
	/**
	 * 调用者拥有的角色
	 */
	public static final String SESSION_CALLER_ROLES = "Roles";

	public String doJaasLogin(String username, String password)
			throws LoginException;

	public String doJaasLogin(String username, String password,
			HttpSession session) throws LoginException;

	public String doJaasLogout() throws LoginException;

	public String doJaasLogout(HttpSession session) throws LoginException;

}
