package com.zh.security.jaas;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpSession;

public class DefaultJaasLogin implements JaasLogin {

	@Override
	public String doJaasLogin(String username, String password)
			throws LoginException {
		throw new LoginException("没有实现JaasLogin");
	}

	@Override
	public String doJaasLogin(String username, String password,
			HttpSession session) throws LoginException {
		throw new LoginException("没有实现JaasLogin");
	}

	@Override
	public String doJaasLogout() throws LoginException {
		return null;
	}

	@Override
	public String doJaasLogout(HttpSession session) throws LoginException {
		return null;
	}

}
