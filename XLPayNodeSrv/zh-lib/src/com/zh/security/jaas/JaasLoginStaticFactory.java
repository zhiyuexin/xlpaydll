package com.zh.security.jaas;

public class JaasLoginStaticFactory {

	private static final String STATIC_FACTORY_BINDING = "com.zh.security.jaas.StaticJaasLoginBinding";

	private static JaasLogin instance;

	public static JaasLogin getInstance() {
		try {
			Thread.currentThread().getContextClassLoader()
					.loadClass(STATIC_FACTORY_BINDING);
			Class.forName(STATIC_FACTORY_BINDING);
		} catch (ClassNotFoundException e) {
			// 加载默认的动态委派工厂
			System.out.println("没有找到特定的工厂邦定类 - {}" + STATIC_FACTORY_BINDING);
			bindDefault();
		}
		return instance;
	}

	private static void bindDefault() {
		instance = new DefaultJaasLogin();
	}

	public static void bind(JaasLogin login) {
		instance = login;
	}

}
