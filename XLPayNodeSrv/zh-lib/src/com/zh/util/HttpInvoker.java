package com.zh.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HttpInvoker {

	public static void readContentFromGet() throws IOException {

		// // 拼凑get请求的URL字串，使用URLEncoder.encode对特殊和不可见字符进行编码
		// String getURL = GET_URL + " ?username= "
		// + URLEncoder.encode("fat man", " utf-8 ");
		//
		// URL getUrl = new URL(getURL);
		//
		// // 根据拼凑的URL，打开连接，URL.openConnection()函数会根据
		// //
		// URL的类型，返回不同的URLConnection子类的对象，在这里我们的URL是一个http，因此它实际上返回的是HttpURLConnection
		// HttpURLConnection connection = (HttpURLConnection) getUrl
		// .openConnection();
		//
		// // 建立与服务器的连接，并未发送数据
		// connection.connect();
		//
		// // 发送数据到服务器并使用Reader读取返回的数据
		// BufferedReader reader = new BufferedReader(new InputStreamReader(
		// connection.getInputStream()));
		//
		// System.out.println(" ============================= ");
		// System.out.println(" Contents of get request ");
		// System.out.println(" ============================= ");
		//
		// String lines;
		// while ((lines = reader.readLine()) != null) {
		// System.out.println(lines);
		// }
		// reader.close();
		//
		// // 断开连接
		// connection.disconnect();
		// System.out.println(" ============================= ");
		// System.out.println(" Contents of get request ends ");
		// System.out.println(" ============================= ");
	}

	@SuppressWarnings("rawtypes")
	public static String doPost(String uri, Map args) throws Exception {
		HttpURLConnection conn = null;
		PrintWriter printWriter = null;
		BufferedReader bufferedReader = null;
		StringBuffer responseResult = null;

		try {
			StringBuffer params = new StringBuffer();

			if (args != null) {
				Iterator it = args.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry element = (Map.Entry) it.next();
					params.append(element.getKey());
					params.append("=");
					params.append(element.getValue());
					params.append("&");
				}
				if (params.length() > 0) {
					params.deleteCharAt(params.length() - 1);
				}
			}

			URL postUrl = new URL(uri);
			// 打开URL连接
			conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);

			conn.setRequestMethod("POST");
			// Post 请求不能使用缓存
			conn.setUseCaches(false);
			conn.setInstanceFollowRedirects(true);

			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-Length",
					String.valueOf(params.length()));
			// www-form-urlencoded的意思是正文是urlencoded编码过的form参数
			// 下面我们可以看到我们对正文内容使用URLEncoder.encode进行编码
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			// 获取URLConnection对象对应的输出流
			printWriter = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			printWriter.write(params.toString());
			// flush 输出流的缓冲
			printWriter.flush();

			// 根据ResponseCode判断连接是否成功
			int responseCode = conn.getResponseCode();
			if (responseCode != 200) {
				System.out.println("Post error: " + responseCode);
				throw new Exception("Post error: " + responseCode);
			}

			// 定义BufferedReader输入流来读取URL的ResponseData

			bufferedReader = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "UTF-8"));

			String line;
			responseResult = new StringBuffer();
			while ((line = bufferedReader.readLine()) != null) {
				responseResult.append(line);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (conn != null)
				conn.disconnect();
			try {
				if (printWriter != null)
					printWriter.close();
				if (bufferedReader != null)
					bufferedReader.close();
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
		}
		return responseResult.toString();
	}

	public static String doPost(String uri) throws Exception {
		return doPost(uri, null);
	}

	public static void main(String[] args) {
		Map<String, Object> requestParamsMap = new HashMap<String, Object>();
		requestParamsMap.put("ak", "i8fi0Erfv2311I0P");
		Long ts = Calendar.getInstance().getTimeInMillis();
		requestParamsMap.put("ts", ts.toString());
		requestParamsMap.put(
				"sn",
				EncryptUtil.MD5Encode("10003" + "i8fi0Erfv2311I0P" + ts
						+ "942ddv7Z9W21apcC"));

		System.out
				.println("发送IQ Webservice请求: http://localhost:8080/t/test/hello/ssss");

		// POST
		String s = null;
		try {
			s = HttpInvoker.doPost("http://localhost:8080/t/test/hello/ssss",
					requestParamsMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("接收响应串: " + s);
	}

}
