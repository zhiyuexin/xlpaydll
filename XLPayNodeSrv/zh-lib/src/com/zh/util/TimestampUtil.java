package com.zh.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimestampUtil {

	public final static String DATE_MINUS = "yyyy-MM-dd";
	public final static String DATE_SLANT = "yyyy/MM/dd";
	public final static String DATE_DOT = "yyyy.MM.dd";

	/**
	 * 将Timestamp转换成字符串
	 * 
	 * @param t
	 * @return 返回的格式：yyyy/MM/dd hh:mm:ss:SSS
	 */
	public static String parse(Timestamp t) {
		return parse(t, DATE_SLANT);
	}

	/**
	 * 将Timestamp转换成字符串
	 * 
	 * @param t
	 * @param format
	 *            要求日期的格式<br/>
	 *            可选：yyyy-MM-dd, yyyy/MM/dd, yyyy.MM.dd
	 * @return 返回的格式：<b>format</b> + hh:mm:ss:SSS
	 */
	public static String parse(Timestamp t, String format) {
		if (t == null)
			return (String) null;

		if (!DATE_MINUS.equals(format) && !DATE_SLANT.equals(format)
				&& !DATE_DOT.equals(format))
			return (String) null;

		SimpleDateFormat sf = new SimpleDateFormat(format + " hh:mm:ss:SSS");
		return sf.format(t);
	}

	/**
	 * 将Timestamp转换成字符串, 仅日期部分.
	 * 
	 * @param t
	 * @return 返回的格式：yyyy/MM/dd
	 */
	public static String parseDate(Timestamp t) {
		return parseDate(t, DATE_SLANT);
	}

	/**
	 * 将Timestamp转换成字符串, 仅日期部分.
	 * 
	 * @param t
	 * @param format
	 *            要求日期的格式<br/>
	 *            可选：yyyy-MM-dd, yyyy/MM/dd, yyyy.MM.dd
	 * @return 返回的格式：<b>format</b>
	 */
	public static String parseDate(Timestamp t, String format) {
		if (t == null)
			return (String) null;

		if (!DATE_MINUS.equals(format) && !DATE_SLANT.equals(format)
				&& !DATE_DOT.equals(format))
			return (String) null;

		SimpleDateFormat sf = new SimpleDateFormat(format);
		return sf.format(t);
	}

	/**
	 * 将字符串转换为Timestamp
	 * 
	 * @param s
	 *            格式：<br>
	 *            <code>yyyy/MM/dd hh:mm:ss.SSS</code><br>
	 *            <code>yyyy-MM-dd hh:mm:ss.SSS</code><br>
	 *            <code>yyyy.MM.dd hh:mm:ss.SSS</code><br>
	 * <br>
	 *            毫秒的分隔符也可为冒号，例如<code>hh:mm:ss:SSS</code>
	 * @return
	 */
	public static Timestamp parse(String s) throws IllegalArgumentException,
			NumberFormatException {
		if (s == null)
			return (Timestamp) null;

		int space = s.indexOf(" ");

		if (s.lastIndexOf("/") > 0)
			s = s.replaceAll("/", "-");
		if (StringUtil.findStrCount(s, ".") > 1) {
			// 取得日期部分
			String date = s.substring(0, space > 0 ? space : s.length());
			if (date.indexOf(".") > 0) {
				// .转为-
				if (space > 0)
					s = date.replace(".", "-") + s.substring(space, s.length());
				else
					s = date.replace(".", "-");
			}
		}
		if (StringUtil.findStrCount(s, ":") > 2)
			s = StringUtil.replace(s, s.lastIndexOf(":"), 1, ".");

		// 如果没有时间则补上
		if (space < 0)
			s = s + " 00:00:00";

		return Timestamp.valueOf(s);
	}

	public static void main(String args[]) {
		long n = 1423152000000L;
		Timestamp t = new Timestamp(n);
		System.out.println(t);
		System.out.println(TimestampUtil.parseDate(t));

		String s = "2013-01-01 00:00:01:001";
		t = parse(s);
		System.out.println(t);
		System.out.println(parseDate(t, DATE_DOT));
		System.out.println(parse(t, DATE_DOT));
	}
}
