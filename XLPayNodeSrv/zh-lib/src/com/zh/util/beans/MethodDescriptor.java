package com.zh.util.beans;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zh.util.StringUtil;

public class MethodDescriptor {

	private Method method;
	private Class<?> returnType;
	private Class<?>[] parameterTypes;

	private Logger logger = LoggerFactory.getLogger(MethodDescriptor.class);

	public String getMapKey() {
		StringBuffer sb = new StringBuffer();
		sb.append(method.getName());
		sb.append("(");
		
		for (Class<?> paramtype : parameterTypes) {
			sb.append(paramtype.getName());
			sb.append(",");
		}
		
		return ",".equals(sb.substring(sb.length() - 1, sb.length())) ? sb
				.substring(0, sb.length() - 1) + ")" : sb.toString() + ")";
	}

	/**
	 * 根据方法和参数计算主键
	 * 
	 * @param method
	 * @param args
	 * @return
	 */
	public static String getMapKey(String method, Object... args) {
		Logger logger = LoggerFactory.getLogger(MethodDescriptor.class);
		logger.debug("Method is \"{}\" and args is \"{}\"", method, args);
		
		StringBuffer sb = new StringBuffer();
		sb.append(method);
		sb.append("(");
		
		if (args == null || args.length == 0) {
			// 没有参数
			sb.append(",");
		} else {
			for (Object arg : args) {
				// 参数如果为空就没法判断类型
				if (arg == null)
					sb.append("null");
				else
					sb.append(arg.getClass().getName());
				
				sb.append(",");
			}
		}
		
		logger.debug("getMapKey return {}.", sb.toString());
		return sb.substring(0, sb.length() - 1) + ")";
	}

	/**
	 * 判断此方法描述是否与指定的方法匹配
	 * 
	 * @param method
	 * @param args
	 *            参数列表，null将被忽略
	 * @return
	 */
	public boolean equals(String method, Object... args) {
		if (!this.method.getName().equals(method))
			return false;
		
		if (args == null && parameterTypes.length > 0)
			return false;
		
		if ((args == null || args.length == 0 || args[0] == null)
				&& parameterTypes.length == 0)
			return true;
		
		if (parameterTypes.length != args.length)
			return false;
		
		for (int i = 0; i < parameterTypes.length; i++) {
			if (args[i] != null) {
				String argType = args[i].getClass().getName();
				
				if (argType.lastIndexOf("Map") >= 0) {
					argType = "java.util.Map";
				}
				
				logger.debug("查找方法描述: 方法名{}, 参数类型为{}, 缓存方法的参数为{}.", method,
						argType, parameterTypes[i].getName());
				
				if (!argType.equals(parameterTypes[i].getName()))
					return false;
			}
		}
		
		return true;
	}

	public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}

	public Class<?> getReturnType() {
		return returnType;
	}

	public Method getMethod() {
		return method;
	}

	public String getName() {
		return method.getName();
	}

	public MethodDescriptor(Method method) {
		if (method == null)
			throw new RuntimeException("方法不能为空");
		
		this.method = method;
		parameterTypes = method.getParameterTypes();
		returnType = method.getReturnType();
		StringBuffer log = new StringBuffer();
		
		log.append("获取方法描述：").append(returnType.getName()).append(" ")
				.append(method.getName()).append("(");
		
		for (Class<?> clazz : parameterTypes) {
			log.append(clazz.getName()).append(", ");
		}
		
		if (", ".equals(log.substring(log.length() - 2, log.length())))
			logger.debug(log.substring(0, log.length() - 2) + ")");
		else
			logger.debug(log.toString() + ")");
	}

	@Override
	public String toString() {
		String result = "方法描述："
				+ returnType.getName()
				+ " "
				+ method.getName()
				+ "("
				+ ((parameterTypes == null || parameterTypes.length == 0) ? ""
						: parameterTypes[0].getName()) + ")";
		return result;
	}

	public MethodDescriptor(Object obj, String name, Object... args) {
		if (obj == null)
			throw new RuntimeException("所属对象不能为空");
		
		if (name == null || "".equals(name))
			throw new RuntimeException("方法名不能为空");
		
		try {
			Class<?> clazz = obj.getClass();
			
			if (args == null || args.length == 0) {
				parameterTypes = new Class<?>[0];
				method = clazz.getMethod(name, (Class<?>[]) null);
			} else {
				List<Class<?>> l = new ArrayList<Class<?>>();
				
				for (Object arg : args) {
					l.add(arg.getClass());
				}
				
				parameterTypes = new Class<?>[l.size()];
				Iterator<Class<?>> it = l.iterator();
				int i = 0;
				
				while (it.hasNext())
					parameterTypes[i++] = it.next();
				
				method = clazz.getMethod(name, parameterTypes);
			}
			returnType = method.getReturnType();
			
		} catch (SecurityException e) {
			method = null;
			returnType = null;
			parameterTypes = new Class<?>[0];
		} catch (NoSuchMethodException e) {
			method = null;
			returnType = null;
			parameterTypes = new Class<?>[0];
		}
	}

	public static String getArgsClasses(Object... args) {
		if (args == null || args.length == 0)
			return "";
		
		StringBuffer sb = new StringBuffer();
		
		for (Object o : args) {
			if (o == null)
				sb.append("null, ");
			else if (o instanceof ArrayList) {
				sb.append("\nArrayList:\n\t"
						+ StringUtil.listToString((ArrayList<?>) o, "\t\n")
						+ ", ");
			} else
				sb.append(o.getClass().getName() + ", ");
		}
		
		return ", ".equals(sb.substring(sb.length() - 2, sb.length())) ? sb
				.substring(0, sb.length() - 2) : sb.toString();
	}

}
