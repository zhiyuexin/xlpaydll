package com.zh.util.beans;

import java.lang.reflect.Field;

//@SuppressWarnings({ "rawtypes", "unchecked" })
public class BeanUtils {
	//
	// // private static Logger logger =
	// LoggerFactory.getLogger(BeanUtils.class);
	// private Map<Object, Map> converted = new HashMap<Object, Map>();

	// /**
	// * 自动获取Bean的ID
	// *
	// * @param bean
	// * @return
	// * @throws IllegalAccessException
	// * @throws IllegalArgumentException
	// * @throws SecurityException
	// * @throws NoSuchMethodException
	// * @throws InvocationTargetException
	// */
	// private Object getId(Object bean) throws IllegalArgumentException,
	// IllegalAccessException, NoSuchMethodException, SecurityException,
	// InvocationTargetException {
	// if (bean == null)
	// return null;
	//
	// /**
	// * 如果是缓存过的实体则直接获取ID值
	// */
	// EntityBean entity = EntityCache.get(bean.getClass().getSimpleName());
	// if (entity != null) {
	// String keyName = entity.getPrimaryKey().getFname();
	// if (keyName != null) {
	// Method getter = bean.getClass().getMethod(
	// "get" + keyName.substring(0, 1).toUpperCase()
	// + keyName.substring(1), (Class<?>) null);
	// return getter.invoke(bean, (Object[]) null);
	// }
	// }
	//
	// /**
	// * 非缓存的实体直接判断ID值
	// */
	// Field[] fields = (java.lang.reflect.Field[]) bean.getClass()
	// .getDeclaredFields();
	//
	// for (Field field : fields) {
	// if (field.isAnnotationPresent(Id.class)) {
	// field.setAccessible(true);
	// return field.get(bean);
	// }
	// }
	//
	// /**
	// * 可能是fid或者id
	// */
	// Field field = null;
	// try {
	// field = bean.getClass().getDeclaredField("fid");
	// } catch (NoSuchFieldException e) {
	// try {
	// field = bean.getClass().getDeclaredField("id");
	// } catch (NoSuchFieldException e1) {
	// field = null;
	// }
	// }
	//
	// if (field != null) {
	// field.setAccessible(true);
	// return field.get(bean);
	// }
	//
	// return null;
	// }

	// /**
	// * 将<b>Bean</b>转换为Map<br>
	// * 内嵌的<b>List</b>也会转为Map<br>
	// * 内嵌的注释了<b>CompositeObject</b>的复合对象也会转为Map<br>
	// * 实体成员会忽略其名字的第一个字符<br/>
	// * <br/>
	// * 因为实体Bean嵌套关联，所以第二次出现均以null替代
	// *
	// * @param bean
	// * @return
	// * @throws Exception
	// */
	// public Map beanToMap(Object bean) throws Exception {
	// if (bean == null)
	// return null;
	//
	// // if (converted.containsKey(bean))
	// // return null;
	// // return converted.get(bean);
	//
	// Map m = new HashMap();
	// converted.put(bean, m);
	//
	// /**
	// * 获取bean的所有成员
	// */
	// Field[] fields = (java.lang.reflect.Field[]) bean.getClass()
	// .getDeclaredFields();
	//
	// for (Field field : fields) {
	// /**
	// * 忽略序列化ID
	// */
	// if (field.getName().equals("serialVersionUID"))
	// continue;
	// // logger.debug("field.getName()={}", field.getName());
	//
	// try {
	// /**
	// * 获取成员对应的getter，如果没有getter则忽略这个成员
	// */
	// Method getter = bean.getClass().getMethod(
	// "get" + field.getName().substring(0, 1).toUpperCase()
	// + field.getName().substring(1),
	// (Class<?>[]) null);
	// // logger.debug("getter={}", getter);
	//
	// Object o = getter.invoke(bean, (Object[]) null);
	//
	// m.put(field.getName().substring(1), convert(o));
	//
	// } catch (SecurityException e) {
	// // 非公有方法忽略
	// } catch (NoSuchMethodException e) {
	// // 没有getter方法忽略
	// } catch (Exception e) {
	// throw e;
	// }
	// }
	//
	// return m;
	// }
	//
	// /**
	// * 将标准对象转成通用对象，如BigDecimal转成double、Timestamp转成字符串格式。<br/>
	// * 集合、数组、复合对象都转成Map。
	// *
	// * @param o
	// * @return
	// * @throws Exception
	// */
	// private Object convert(Object o) throws Exception {
	// if (o == null || o instanceof Map)
	// return o;
	//
	// if (o instanceof String || o instanceof Boolean) {
	// return o.toString();
	// }
	//
	// if (o instanceof Number) {
	// return ((Number) o).doubleValue();
	// }
	//
	// if (o instanceof Timestamp) {
	// return TimestampUtil.parse((Timestamp) o);
	// }
	//
	// if (o instanceof Collection) {
	// return collectionToMap((Collection) o);
	// }
	//
	// if (o instanceof Object[]) {
	// List list = new ArrayList();
	// for (Object i : (Object[]) o) {
	// list.add(i);
	// }
	// return collectionToMap(list);
	// }
	//
	// /**
	// * 转化被注释的复合对象<br/>
	// * 如果复合对象已经被转化过说明是实体关联及反向关联，所以为了避免死循环转化这里的反向关联的实体用其id来表示。
	// */
	// if (converted.containsKey(o))
	// return getId(o);
	// else
	// return beanToMap(o);
	// }
	//
	// /**
	// * 将集合内的对象转换为Map<br/>
	// * <br/>
	// * 内嵌的集合与复合对象也会转为Map<br/>
	// * 实体成员会忽略其名字的第一个字符
	// *
	// * @param list
	// * @return
	// * @throws Exception
	// */
	// public List collectionToMap(Collection list) throws Exception {
	// if (list == null)
	// return null;
	//
	// List result = new ArrayList();
	//
	// Iterator it = list.iterator();
	// while (it.hasNext()) {
	// Object o = it.next();
	// if (o instanceof Collection) {
	// result.add(collectionToMap((Collection) o));
	// } else if (o instanceof Map) {
	// result.add(o);
	// } else {
	// result.add(beanToMap(o));
	// }
	// }
	//
	// return result;
	// }

	// public static Map jsonToMap(JSONObject json) throws Exception {
	// if (json == null)
	// return null;
	// Field f = json.getClass().getDeclaredField("map");
	// f.setAccessible(true);
	// Map m = (Map) f.get(json);
	// Set entries = m.entrySet();
	// for (Object o : entries) {
	// Map.Entry me = (Entry) o;
	// if (me.getValue() instanceof JSONObject)
	// m.put(me.getKey(), jsonToMap((JSONObject) me.getValue()));
	// else if (me.getValue() instanceof JSONArray)
	// m.put(me.getKey(), jsonArrayToList((JSONArray) me.getValue()));
	// else
	// m.put(me.getKey(), me.getValue());
	// }
	// return m;
	// }
	//
	// public static List jsonArrayToList(JSONArray jarr) throws Exception {
	// if (jarr == null)
	// return null;
	// List result = new ArrayList();
	// for (int i = 0; i < jarr.length(); i++) {
	// Object o = jarr.get(i);
	// if (o instanceof JSONObject)
	// result.add(jsonToMap((JSONObject) o));
	// else if (o instanceof JSONArray)
	// result.add(jsonArrayToList((JSONArray) o));
	// else
	// result.add(o);
	// }
	// return result;
	// }

	public static Object clone(Object bean) throws IllegalAccessException,
			InstantiationException {
		if (bean == null)
			return null;

		Class<?> clazz = bean.getClass();
		Object newbean = clazz.newInstance();

		Field[] fields = clazz.getDeclaredFields();

		for (Field field : fields) {
			if (field.getName().equals("serialVersionUID"))
				continue;
			field.setAccessible(true);
			field.set(newbean, field.get(bean));
		}

		return newbean;
	}

	// public static void main(String args[]) {
	// List l = new ArrayList();
	// TScmFywldwm w = new TScmFywldwm();
	// w.setFamt(BigDecimal.valueOf(11));
	// w.setFfywldwmid(1);
	// l.add(w);
	// try {
	// List o = BeanUtils.listToMap(l);
	// System.out.println(o);
	// String json = JSONObject.valueToString(o);
	// System.out.println(json);
	// json = MyJsonObject.valueToString(o);
	// System.out.println(json);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// TSysMenu p = new TSysMenu();
	// p.setFid(1);
	// p.setFname("fsdfdsf");
	// p.setFistip(BigDecimal.valueOf(1));
	// p.setFlevel(BigDecimal.valueOf(1));
	//
	// List children = new ArrayList();
	// TSysMenu c = new TSysMenu();
	// c.setFid(2);
	// c.setFname("C1");
	// c.setFisleaf(BigDecimal.valueOf(1));
	// c.setFlevel(BigDecimal.valueOf(2));
	// c.setFparentid(BigDecimal.valueOf(1));
	// children.add(c);
	//
	// c = new TSysMenu();
	// c.setFid(3);
	// c.setFname("C2");
	// c.setFisleaf(BigDecimal.valueOf(1));
	// c.setFlevel(BigDecimal.valueOf(2));
	// c.setFparentid(BigDecimal.valueOf(1));
	// children.add(c);
	//
	// p.setFchildren(children);
	//
	// Map m = null;
	// try {
	// m = new BeanUtils().beanToMap(p);
	// } catch (Throwable e) {
	// e.printStackTrace();
	// }
	// System.out.println(m);
	// }

}
