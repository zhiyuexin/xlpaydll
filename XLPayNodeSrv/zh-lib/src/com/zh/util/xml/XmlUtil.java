package com.zh.util.xml;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XmlUtil {

	public static void main(String[] args) {
		// String s =
		// "/C:/IBM/WebSphere/AppServer/profiles/AppSrv01/installedApps/ZhangHai-THINKNode01Cell/zss7.ear/Zss7CoreEjb.jar!/com/zh/frame/sys/test.xml";

		System.out.println(XmlUtil.class.getResource("/").getPath());
		System.out.println(XmlUtil.class.getResource("").getPath());
		System.out.println(System.getProperty("user.dir"));

		// XmlUtil xml = new XmlUtil(XmlUtil.class.getResource("").getPath()
		// + "test.xml");
		try {
			XmlUtil xml = new XmlUtil(
					XmlUtil.class.getResourceAsStream("test.xml"));
			List<Element> l = xml.getElements("");

			// for (Element item : l) {
			// System.out.println("1: " + item);
			// System.out.println("2: " + item.getName()); // item
			// System.out.println("3: " + StringUtil.trim(item.getText())); //
			// System.out.println("4: " + StringUtil.trim(item.getData())); //
			// item
			// System.out.println("5: " + item.attributeValue("id")); // 1
			// System.out.println("5: " + item.attributeValue("name")); // null
			// System.out.println("6: " + item.getPath()); // /root/item
			// }

			l = xml.getElements("methods", "childs");

			for (Element item : l) {
				System.out.println("1: " + item);
				System.out.println("2: " + item.getName()); // item
				// System.out.println("3: " + StringUtil.trim(item.getText()));
				// //
				// System.out.println("4: " + StringUtil.trim(item.getData()));
				// // item
				System.out.println("5: " + item.attributeValue("id")); // 1
				System.out.println("5: " + item.attributeValue("name")); // null
				System.out.println("6: " + item.getPath()); // /root/item
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Document document;

	public XmlUtil(String path) throws DocumentException {
		File xmlFile = new File(path);
		if (xmlFile.exists()) {
			document = new SAXReader().read(xmlFile);
		}
	}

	public XmlUtil(File file) throws DocumentException {
		if (file.exists()) {
			document = new SAXReader().read(file);
		}
	}

	public XmlUtil(InputStream in) throws DocumentException {
		document = new SAXReader().read(in);
	}

	/**
	 * 取得指定节点下的所有子节点
	 * 
	 * @param parent
	 *            空串为根节点，节点间以.分割
	 * @return
	 * @throws DocumentException
	 */
	@SuppressWarnings("unchecked")
	public List<Element> getElements(String parent) throws DocumentException {
		if (document == null)
			return null;

		Element root = document.getRootElement();

		if (parent == null || "".equals(parent))
			return root.elements();

		String nodes[] = parent.split("[.]");
		return getElements(nodes, 0, root);
	}

	@SuppressWarnings("unchecked")
	private List<Element> getElements(String[] nodes, int index, Element parent) {
		if (nodes == null || parent == null || index < 0
				|| nodes.length <= index)
			return null;

		Element node = parent.element(nodes[index]);
		if (node == null)
			return null;

		if (node.getName().equals(nodes[nodes.length - 1])) {
			return node.elements();
		} else {
			return getElements(nodes, index + 1, node);
		}
	}

	/**
	 * 取得指定节点下的所有具名节点
	 * 
	 * @param parent
	 *            空串为根节点，节点间以.分割
	 * @param name
	 *            查找的子节点
	 * @return
	 * @throws DocumentException
	 */
	@SuppressWarnings("unchecked")
	public List<Element> getElements(String parent, String name)
			throws DocumentException {

		if (document == null)
			return null;

		Element root = document.getRootElement();

		if (parent == null || "".equals(parent))
			return root.elements(name);

		String nodes[] = parent.split("[.]");
		return getElements(nodes, 0, root, name);
	}

	@SuppressWarnings("unchecked")
	private List<Element> getElements(String[] nodes, int index,
			Element parent, String name) {

		if (nodes == null || parent == null || index < 0
				|| nodes.length <= index)
			return null;

		Element node = parent.element(nodes[index]);
		if (node == null)
			return null;

		if (node.getName().equals(nodes[nodes.length - 1])) {
			return node.elements(name);
		} else {
			return getElements(nodes, index + 1, node, name);
		}
	}
}
