package com.zh.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	public static void main(String[] args) {
	}

	public static String decodeUnicode(String theString) {
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		for (int x = 0; x < len;) {
			aChar = theString.charAt(x++);
			if (aChar == '\\') {
				aChar = theString.charAt(x++);
				if (aChar == 'u') {
					// Read the xxxx
					int value = 0;
					for (int i = 0; i < 4; i++) {
						aChar = theString.charAt(x++);
						switch (aChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							value = (value << 4) + aChar - '0';
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							value = (value << 4) + 10 + aChar - 'a';
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							value = (value << 4) + 10 + aChar - 'A';
							break;
						default:
							throw new IllegalArgumentException(
									"Malformed   \\uxxxx   encoding.");
						}

					}
					outBuffer.append((char) value);
				} else {
					if (aChar == 't')
						aChar = '\t';
					else if (aChar == 'r')
						aChar = '\r';
					else if (aChar == 'n')
						aChar = '\n';
					else if (aChar == 'f')
						aChar = '\f';
					outBuffer.append(aChar);
				}
			} else
				outBuffer.append(aChar);
		}
		return outBuffer.toString();
	}

	/**
	 * 返回串中出现的符合正则表达式的字符串
	 * 
	 * @param source
	 *            原始字符串
	 * @param regx
	 *            正则表达式
	 * @return 符合表达式的所有子串
	 */
	public static List<String> getMatched(String source, String regx) {
		Pattern p = Pattern.compile(regx);
		Matcher m = p.matcher(source);
		List<String> result = new ArrayList<String>();
		while (m.find()) {
			result.add(m.group());
		}
		return result;
	}

	/**
	 * 返回指定串中已匹配的值队列间的其他串<br/>
	 * 例如：<code>name={name},value={value}</code> 的串，传入的参数队列是<br/>
	 * <code>"{name}"</code><br/>
	 * <code>"{value}"</code><br/>
	 * 那么返回的是<br/>
	 * <code>"name="</code><br/>
	 * <code>",value="</code><br/>
	 * 
	 * @param matched
	 *            传入已匹配的串队列
	 * @param source
	 *            原始字符串
	 * @return 返回中间间隔的其他串
	 */
	public static List<String> separator(List<String> matched, String source) {
		if (matched == null)
			return null;

		List<String> result = new ArrayList<String>();

		int pos = 0;
		int index;
		for (int i = 0; i < matched.size(); i++) {
			if (matched.get(i) == null || matched.get(i).equals(""))
				continue;

			index = source.indexOf(matched.get(i), pos);

			// 忽略第一个匹配即首位的前间隔判断
			if (pos == 0 && index == 0) {
				pos = matched.get(i).length();
				continue;
			}

			result.add(source.substring(pos, index));
			pos = index + matched.get(i).length();
		}
		// 忽略最后一个匹配即末尾的后间隔判断
		if (matched.size() > 0 && pos < source.length())
			result.add(source.substring(pos, source.length()));

		return result;
	}

	/**
	 * 统计指定内容在字符串中出现的次数
	 * 
	 * @param src
	 * @param keyword
	 * @return
	 */
	public static int findStrCount(String src, String keyword) {
		int count = 0;
		Pattern p = Pattern.compile(keyword);
		Matcher m = p.matcher(src);
		while (m.find()) {
			count++;
		}
		return count;
	}

	/**
	 * 在字符串指定位置插入字符
	 * 
	 * @param src
	 *            源字符串
	 * @param start
	 *            插入的位置
	 * @param len
	 *            截取长度
	 * @param newstr
	 *            插入的串
	 * @return
	 */
	public static String replace(String src, int start, int len, String newstr) {
		return src.substring(0, start) + newstr + src.substring(start + len);
	}

	/**
	 * 去掉空字符，忽略null
	 * 
	 * @param src
	 * @return
	 */
	public static String trim(Object src) {
		return src == null ? null : src.toString().trim();
	}

	/**
	 * 将列表内的对象转成串
	 * 
	 * @param list
	 *            列表
	 * @param separator
	 *            分割符
	 * @return 返回以分割符表示的串
	 */
	public static String listToString(List<?> list, String separator) {
		if (list == null)
			return "null";
		StringBuffer sb = new StringBuffer();
		for (Object o : list) {
			if (o == null)
				sb.append("null");
			else
				sb.append(o.toString());
			sb.append(separator);
		}
		if (list.size() > 0)
			return sb.substring(0, sb.length() - separator.length());
		return "";
	}

}
