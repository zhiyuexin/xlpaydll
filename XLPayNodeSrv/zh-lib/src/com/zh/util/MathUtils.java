package com.zh.util;

import java.math.BigDecimal;

public class MathUtils {
	public static void main(String agrs[]) {
		// ����
		System.out.println(digitUppercase(0)); // ��Ԫ��
		System.out.println(digitUppercase(123)); // Ҽ�۷�ʰ��Ԫ��
		System.out.println(digitUppercase(1000000)); // Ҽ����Ԫ��
		System.out.println(digitUppercase(100000001)); // Ҽ����ҼԪ��
		System.out.println(digitUppercase(1000000000)); // Ҽʰ��Ԫ��
		System.out.println(digitUppercase(1234567890)); // Ҽʰ������Ǫ������ʰ½����Ǫ�ư۾�ʰԪ��
		System.out.println(digitUppercase(1001100101)); // Ҽʰ����Ҽ��Ҽʰ����Ҽ����ҼԪ��
		System.out.println(digitUppercase(110101010)); // Ҽ��ҼǪ��Ҽʰ��ҼǪ��ҼʰԪ��

		// С��
		System.out.println(digitUppercase(0.12)); // Ҽ�Ƿ���
		System.out.println(digitUppercase(123.34)); // Ҽ�۷�ʰ��Ԫ��������
		System.out.println(digitUppercase(1000000.56)); // Ҽ����Ԫ���½��
		System.out.println(digitUppercase(100000001.78)); // Ҽ����ҼԪ��ǰƷ�
		System.out.println(digitUppercase(1000000000.90)); // Ҽʰ��Ԫ����
		System.out.println(digitUppercase(1234567890.03)); // Ҽʰ������Ǫ������ʰ½����Ǫ�ư۾�ʰԪ����
		System.out.println(digitUppercase(1001100101.00)); // Ҽʰ����Ҽ��Ҽʰ����Ҽ����ҼԪ��
		System.out.println(digitUppercase(110101010.10)); // Ҽ��ҼǪ��Ҽʰ��ҼǪ��ҼʰԪҼ��

		// ����
		System.out.println(digitUppercase(-0.12)); // ��Ҽ�Ƿ���
		System.out.println(digitUppercase(-123.34)); // ��Ҽ�۷�ʰ��Ԫ��������
		System.out.println(digitUppercase(-1000000.56)); // ��Ҽ����Ԫ���½��
		System.out.println(digitUppercase(-100000001.78)); // ��Ҽ����ҼԪ��ǰƷ�
		System.out.println(digitUppercase(-1000000000.90)); // ��Ҽʰ��Ԫ����
		System.out.println(digitUppercase(-1234567890.03)); // ��Ҽʰ������Ǫ������ʰ½����Ǫ�ư۾�ʰԪ����
		System.out.println(digitUppercase(-1001100101.00)); // ��Ҽʰ����Ҽ��Ҽʰ����Ҽ����ҼԪ��
		System.out.println(digitUppercase(-110101010.10)); // ��Ҽ��ҼǪ��Ҽʰ��ҼǪ��ҼʰԪҼ��
	}

	/**
	 * ���ֽ���дת����˼����д��������Ȼ������ʰ�滻���� Ҫ�õ��������ʽ
	 */
	public static String digitUppercase(double n) {
		String fraction[] = { "��", "��" };
		String digit[] = { "��", "Ҽ", "��", "��", "��", "��", "½", "��", "��", "��" };
		String unit[][] = { { "Ԫ", "��", "��" }, { "", "ʰ", "��", "Ǫ" } };

		String head = n < 0 ? "��" : "";
		n = Math.abs(n);

		String s = "";
		for (int i = 0; i < fraction.length; i++) {
			s += (digit[(int) (Math.floor(n * 10 * Math.pow(10, i)) % 10)] + fraction[i])
					.replaceAll("(��.)+", "");
		}
		if (s.length() < 1) {
			s = "��";
		}
		int integerPart = (int) Math.floor(n);

		for (int i = 0; i < unit[0].length && integerPart > 0; i++) {
			String p = "";
			for (int j = 0; j < unit[1].length && n > 0; j++) {
				p = digit[integerPart % 10] + unit[1][j] + p;
				integerPart = integerPart / 10;
			}
			s = p.replaceAll("(��.)*��$", "").replaceAll("^$", "��") + unit[0][i]
					+ s;
		}
		return head
				+ s.replaceAll("(��.)*��Ԫ", "Ԫ").replaceFirst("(��.)+", "")
						.replaceAll("(��.)+", "��").replaceAll("^��$", "��Ԫ��");
	}

	/**
	 * �ӹ�ǧ�ַ�
	 * 
	 * @param num
	 * @return
	 */
	public static String formatCommas(BigDecimal num) {
		StringBuffer result = new StringBuffer();
		String str = num.toString();
		int pos = str.indexOf(".");
		int len = str.length() - 3;
		int thou = len / 3;
		if (len % 3 == 0)
			thou--;
		int di = len - thou * 3;
		result.append(str.substring(0, di));
		for (int i = 0; i < thou; i++) {
			if (result.length() > 0)
				result.append(",");
			result.append(str.substring(di + i * 3, di + (i + 1) * 3));
		}
		result.append(str.substring(pos));
		return result.toString();
	}

	public static Double convertDouble(BigDecimal num) {
		return num == null ? null : num.doubleValue();
	}

	public static Long convertLong(BigDecimal num) {
		return num == null ? null : num.longValue();
	}
}
