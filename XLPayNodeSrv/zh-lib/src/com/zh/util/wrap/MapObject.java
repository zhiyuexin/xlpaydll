package com.zh.util.wrap;

import java.util.Map;

import com.zh.exception.ApplicationException;
import com.zh.exception.ExceptionMessage;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class MapObject {

	protected Map map;

	/**
	 * 包装一个Map对象
	 * 
	 * @param map
	 * @throws ApplicationException
	 *             Map对象为null抛出此异常
	 */
	public MapObject(Map map) throws ApplicationException {
		if (map == null)
			throw new ApplicationException(
					ExceptionMessage.ARGUMENTS_MISMATCHING, "参数不能为空");
		this.map = map;
	}

	/**
	 * 带判断的get方法
	 * 
	 * @param key
	 * @return
	 * @throws ApplicationException
	 */
	public Object obtain(Object key) throws ApplicationException {
		if (!map.containsKey(key))
			throw new ApplicationException(ExceptionMessage.KEY_MISMATCHING,
					"Map参数未指定key=\"" + key + "\"");

		Object value = map.get(key);
		if (value == null)
			throw new ApplicationException(ExceptionMessage.KEY_MISMATCHING,
					"Map参数不能为null: {key=\"" + key + "\"}");

		return value;
	}

	/**
	 * 不带判断
	 * 
	 * @param key
	 * @return
	 * @throws ApplicationException
	 */
	public Object get(Object key) {
		return map.get(key);
	}

	public void put(Object key, Object value) {
		// if (!map.containsKey(key))
		// throw new ApplicationException(ExceptionCode.KEY_MISMATCHING,
		// "Map参数未指定key=\"" + key + "\"");
		map.put(key, value);
	}

	/**
	 * Map无顺序，所以仅适用于LinkedHashMap
	 * 
	 * @param index
	 * @return
	 * @throws ApplicationException
	 */
	public Object get(int index) throws ApplicationException {
		Object[] arr = map.values().toArray();
		if (index < 1 || index > arr.length)
			throw new ApplicationException(
					ExceptionMessage.ARGUMENTS_MISMATCHING, index + "超越集合边界("
							+ arr.length + ")");
		return arr[index - 1];
	}

	public Map getMap() {
		return map;
	}
}
