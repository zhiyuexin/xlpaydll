unit uDM;

interface

uses
 {vcl:} SysUtils, Classes, DB, DBClient, MidasLib,
 {RemObjects:} uRODataSnapModule, uROServer,
  Provider, MemDS, DBAccess, Uni,
  UniProvider, AccessUniProvider, SQLiteUniProvider,
  SQLServerUniProvider, OracleUniProvider, MySQLUniProvider,
  InterBaseUniProvider, DB2UniProvider, ASEUniProvider,
  AdvantageUniProvider, ODBCUniProvider, PostgreSQLUniProvider,
  DAScript, UniScript, DASQLMonitor, UniSQLMonitor,
  DateUtils, IdBaseComponent, IdComponent, IdIPWatch;

type

  TMaxBillId = Class(TComponent)
  private
    FMaxBillId: Integer;
    FPosNo: String;
  public
    property PosNo: String read FPosNo write FPosNo;
    property MaxBillId: Integer read FMaxBillId write FMaxBillId;
  end;

  TConnPool = Class(TComponent)
  private
    FConn: TUniConnection;
    FName: String;
    FComCode: String;
    FPort: Integer;
    FServer: String;
    FProvider: String;
    FUsername: String;
    FDatabase: String;
    FPassword: String;
    FMaxBillId: Array[0..255] of TMaxBillId;
  public
    Next: TConnPool;
    property Conn: TUniConnection read FConn write FConn;
    property Name: String read FName write FName;
    property ComCode: String read FComCode write FComCode;
    property Provider: String read FProvider write FProvider;
    property Server: String read FServer write FServer;
    property Port: Integer read FPort write FPort;
    property Database: String read FDatabase write FDatabase;
    property Username: String read FUsername write FUsername;
    property Password: String read FPassword write FPassword;

    constructor Create(AOwner: TComponent); override;
    function Connect(ComCode, Name, Provider, Server: String; Port: Integer; Database, Username, Password: String): Boolean;

    function GetMaxBillId(PosNo: String; ForceGetFromDB: Boolean = False): Integer;
  end;

  TLsConnPool = Class(TComponent)
  private
    FFirst: TConnPool;
    FLast: TConnPool;
  public
    property First: TConnPool read FFirst write FFirst;
    property Last: TConnPool read FLast write FLast;
    constructor Create(AOwner: TComponent); override;
    function Add(ComCode, Name, Provider, Server: String; Port: Integer; Database, Username, Password: String): TConnPool;
    function GetConnByComCode(ComCode: String): TConnPool;
    function Patch(LsPatch, BackPatch: TStringList): Boolean;
  end;


  TDM = class(TRODataSnapModule)
    Conn: TUniConnection;
    qry: TUniQuery;
    cmd: TUniScript;
    dpqry: TDataSetProvider;
    SQL: TUniSQL;
    qry1: TUniQuery;
    dpqry1: TDataSetProvider;
    qry2: TUniQuery;
    dpqry2: TDataSetProvider;
    SQLMonitor: TUniSQLMonitor;
    qryTradePatchList: TUniQuery;
    qryTradePatchListF_TRADE_NO: TStringField;
    qryTradePatchListF_COM_CODE: TStringField;
    qryTradePatchListF_COM_NAME: TStringField;
    qryTradePatchListF_VIP_CODE: TStringField;
    qryTradePatchListF_VIP_ID: TFloatField;
    qryTradePatchListF_TRADE_AMT: TFloatField;
    qryTradePatchListF_TRADE_TIME: TDateTimeField;
    qryTradePatchListF_CREATE_TIME: TSQLTimeStampField;
    qryLsDbms: TUniQuery;
    qryLsDbmsF_COM_CODE: TStringField;
    qryLsDbmsF_DBMS_TYPE: TStringField;
    qryLsDbmsF_SRV_NAME: TStringField;
    qryLsDbmsF_SRV_PORT: TFloatField;
    qryLsDbmsF_SRV_DATABASE: TStringField;
    qryLsDbmsF_SRV_USER: TStringField;
    qryLsDbmsF_SRV_PWD: TStringField;
    qryLsDbmsF_SRV_PARAM: TStringField;
    sqlLog: TUniSQL;
    IPWatch: TIdIPWatch;
    procedure ConnAfterConnect(Sender: TObject);
    procedure RODataSnapModuleCreate(Sender: TObject);
    procedure ConnAfterDisconnect(Sender: TObject);
    procedure ConnBeforeConnect(Sender: TObject);
    procedure ConnBeforeDisconnect(Sender: TObject);
    procedure ConnError(Sender: TObject; E: EDAError; var Fail: Boolean);
    procedure SQLMonitorSQL(Sender: TObject; Text: string; Flag: TDATraceFlag);
  private
    FDataVersion: String;
    { Private declarations }
  public
    GettingDataVersion: Boolean;
    LsConnPool: TLsConnPool;
    ServerMachineIP, ServerMachineName: String;
    function Query(sql: string): TDataSet;
    procedure LogToDB(sCode, sName, sLog: String);
  end;
var
  DM: TDM;


implementation

uses {RemObjects:}  uRODataSnap_Invk,
  ActiveX, uCommon;

{$R *.DFM}

procedure Create_DataSnapModule(out oInstance: IUnknown);
begin
  oInstance := TDM.Create(nil);
end;

procedure TDM.ConnAfterConnect(Sender: TObject);
begin
  //Log('数据库连接已建立。', True);
end;

procedure TDM.RODataSnapModuleCreate(Sender: TObject);
begin
  try
    IPWatch.Active := True;
    ServerMachineIP := IPWatch.LocalIP;
    ServerMachineName := IPWatch.LocalName;
    IPWatch.Active := False;

    Conn.Disconnect;
    uCommon.GetConnectionConfig(Conn);
    Log('<初始化>连接数据库...');
    Log('<初始化>DBTYPE:' + Conn.ProviderName);
    Log('<初始化>SERVER:' + Conn.Server);
    Log('<初始化>USER:' + Conn.Username);
    Conn.Connect;
    if (Conn.Connected) then
    begin
      Log('<初始化>连接数据库成功!');
    end;
    LsConnPool := TLsConnPool.Create(Self);
  except
    on e: Exception do
    begin
      Log('<初始化>连接数据库时发生出错(' + E.ClassName + '):' + E.Message);
    end;
  end;
end;

procedure TDM.ConnAfterDisconnect(Sender: TObject);
begin
  if (Conn <> nil) and (Conn.ProviderName = 'SQL Server') then
    CoUninitialize;
  //Log('数据库连接已断开。', True);
end;

procedure TDM.ConnBeforeConnect(Sender: TObject);
begin
  //Log('连接数据库[' + Conn.ProviderName + ',' + Conn.Server + ',' + Conn.Database + ']...', True);
  if (Conn <> nil) and (Conn.ProviderName = 'SQL Server') then
    CoInitialize(nil);
end;

procedure TDM.ConnBeforeDisconnect(Sender: TObject);
begin
  //Log('断开数据库连接...', True);
end;

procedure TDM.ConnError(Sender: TObject; E: EDAError; var Fail: Boolean);
begin
  Log('Error:' + E.Message);
end;

procedure TDM.SQLMonitorSQL(Sender: TObject; Text: string; Flag: TDATraceFlag);
begin
//  if (copy(Text, 1, 8) <> 'Connect:') and (copy(Text, 1, 11) <> 'Disconnect:') then
//    Log('[SQL]' + Text);
end;


function TDM.Query(sql: string): TDataSet;
begin
  qry2.Active := False;
  qry2.SQL.Text := sql;
  qry2.Active := True;
  Result := TDataSet(qry2);
end;



{ TConnPool }

function TConnPool.Connect(ComCode, Name, Provider, Server: String;
  Port: Integer; Database, Username, Password: String): Boolean;
begin
  FComCode := ComCode;
  FName := Name;
  FProvider := Provider;
  FServer := Server;
  FPort := Port;
  FDatabase := Database;
  FUsername := Username;
  FPassword := Password;

  if FConn = nil then
    FConn := TUniConnection.Create(Self);
  if FConn.Connected then FConn.Disconnect;
  FConn.LoginPrompt := False;
  FConn.ProviderName := Provider;
  FConn.Server := Server;
  FConn.Port := Port;
  FConn.Database := Database;
  FConn.Username := Username;
  FConn.Password := Password;

//  FConn.Pooling := True;
//  FConn.PoolingOptions.MaxPoolSize := 25;     
  Log('连接龙商数据库:ComCode:' + ComCode + ',ProviderName:' + Provider + ',Server:' + Server + ',Port:' + IntToStr(Port) + ',Database:' + Database + ',Username:' + Username);
  try
    FConn.Connect;
  except
    on E: Exception do
      Log('连接龙商数据库发生错误(' + E.ClassName + '):' + E.Message);
  end;

  Result := FConn.Connected;
end;

constructor TConnPool.Create(AOwner: TComponent);
var
  i: Integer;
begin
  inherited Create(AOwner);
  FConn := TUniConnection.Create(Self);
  Next := nil;
  for i := low(FMaxBillId) to high(FMaxBillId) do
  begin
    FMaxBillId[i] := nil;
  end;
end;

function TConnPool.GetMaxBillId(PosNo: String; ForceGetFromDB: Boolean = False): Integer;
var
  i: Integer;
  qry: TUniQuery;
begin
  Result := -1;
  try
    if PosNo = '' then Exit;
    //内存查找
    for i := low(FMaxBillId) to high(FMaxBillId) do
    begin
      if FMaxBillId[i] <> nil then
      begin
        if FMaxBillId[i].PosNo = PosNo then
        begin
          if not ForceGetFromDB then
          begin
            FMaxBillId[i].MaxBillId := FMaxBillId[i].MaxBillId + 1;
            Result := FMaxBillId[i].MaxBillId;
            Exit;
          end
          else   //强制从数据库中获取
          begin
            break;
          end;
        end;
      end
      else
      begin
        Break;
      end;
    end;
    if ((FMaxBillId[i] <> nil) and (not ForceGetFromDB)) then
    begin
      Log('<SERVICE.ERROR>获取最大流水号时出错:未找到款机号''' + POSNO + '''对应的最大流水号');
      Exit;
    end;
    if FMaxBillId[i] = nil then
    begin
      FMaxBillId[i] := TMaxBillId.Create(Self);
      FMaxBillId[i].PosNo := PosNo;
      FMaxBillId[i].MaxBillId := -1;
    end;
    //数据库查找
    if not FConn.Connected then
      FConn.Connect;
    if FConn.Connected then
    begin
      qry := TUniQuery.Create(nil);
      try
        qry.Connection := FConn;
        qry.SQL.Text := 'SELECT IsNull(MAX(BILL_ID) + 1,-1) BILL_ID FROM Pos_Sale WHERE POS_NO = ''' + PosNo + '''';
        qry.Open;
        if qry.Eof then
        begin
          Log('<SERVICE.ERROR>获取最大流水号时出错:获取款机号''' + POSNO + '''对应的最大流水号失败:'#13#10 + qry.SQL.Text);
          Exit;
        end;
        if qry.FieldByName('BILL_ID').AsInteger <> -1 then
        begin
          FMaxBillId[i].MaxBillId := qry.FieldByName('BILL_ID').AsInteger;
        end
        else   //从历史表里找
        begin
          qry.Close;
          qry.SQL.Text := 'SELECT IsNull(MAX(BILL_ID) + 1,-1) BILL_ID FROM Hpos_Sale WHERE POS_NO = ''' + PosNo + '''';
          qry.Open;
          if qry.Eof then
          begin
            Log('<SERVICE.ERROR>获取最大流水号时出错:获取款机号''' + POSNO + '''对应的最大流水号失败:'#13#10 + qry.SQL.Text);
            Exit;
          end;
          if qry.FieldByName('BILL_ID').AsInteger <> -1 then
          begin
            FMaxBillId[i].MaxBillId := qry.FieldByName('BILL_ID').AsInteger;
          end
          else
          begin
            FMaxBillId[i].MaxBillId := 1;
          end;
        end;
        qry.Close;
      finally
        qry.Free;
      end;
    end;
    result := FMaxBillId[i].MaxBillId;
    Log('<SERVICE>获取最大流水号[' + POSNO + ']:' + IntToStr(result));
  except
    on E: Exception do
    begin
      Log('<SERVICE.ERROR>获取最大流水号时出错:未找到款机号''' + POSNO + '''对应的最大流水号');
      Log('<SERVICE.ERROR>获取最大流水号时出错(' + E.ClassName + '):' + E.Message);
    end;
  end;
end;

{ TLsConnPool }
function TLsConnPool.Add(ComCode, Name, Provider, Server: String;
  Port: Integer; Database, Username, Password: String): TConnPool;
begin
  Result := nil;
  Provider := UpperCase(Provider);
  if ((Provider <> 'ASE') and (Provider <> 'SYBASE')) then Exit;
  Result := GetConnByComCode(ComCode);
  if Result = nil then
  begin
    if FFirst = nil then
    begin
      FFirst := TConnPool.Create(Self);
      FFirst.Connect(ComCode, Name, 'ASE', Server, Port, Database, Username, Password);
      FLast := FFirst;
    end
    else
    begin
      FLast.Next := TConnPool.Create(Self);
      FLast.Next.Connect(ComCode, Name, 'ASE', Server, Port, Database, Username, Password);
      FLast := FLast.Next;
    end;
    Result := FLast;
  end
  else
  begin
    // Log('<LSDB>Connect to LSDB: ComCode:' + ComCode + ',Name:' + Name + ',Server:' + Server + ',Port:' + IntToStr(Port) + ',Database:' + Database);
    Result.Connect(ComCode, Name, 'ASE', Server, Port, Database, Username, Password);
  end;
end;

constructor TLsConnPool.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFirst := nil;
  FLast := nil;
end;

function TLsConnPool.GetConnByComCode(ComCode: String): TConnPool;
var
  p: TConnPool;
begin
  Result := nil;
  p := FFirst;
  while p <> nil do
  begin
    if p.ComCode = ComCode then
    begin
      Result := p;
      Break;
    end;
    p := p.Next;
  end;
end;

function TLsConnPool.Patch(LsPatch, BackPatch: TStringList): Boolean;
var
  i: Integer;
  ComCode, sql: String;
begin
  try
    LOG('<SERVICE.PATCH.211>补写流水...');
    DM.LogToDB('211', '同步流水', '开始补写流水，验证后回写');
    for i := 0 to LsPatch.Count - 1 do
    begin
      ComCode := LsPatch.Names[i];
      sql := 'BEGIN TRAN '#13#10 + LsPatch.ValueFromIndex[i] + #13#10 + 'COMMIT TRAN'#13#10;
      //sql := LsPatch.ValueFromIndex[i];
      LOG('<SERVICE.PATCH.212>ComCode：' + ComCode + #13#10 + sql);
      LOG('<SERVICE.PATCH.212>补写流水(ComCode：' + ComCode + ')...');
      //DM.LogToDB('212', '同步流水', '补写流水(ComCode：' + ComCode + ')'#13#10 + sql);
      try
        GetConnByComCode(ComCode).Conn.ExecSQL(sql, []);
      except
        on E: Exception do
        begin
          Log('<SERVICE.PATCH.ERROR.296>补写流水时出错(' + E.ClassName + '):' + E.Message);
          DM.LogToDB('296', '[错误]同步流水', '补写流水时出错(' + E.ClassName + '):' + E.Message);
        end;
      end;
    end;
    LOG('<SERVICE.PATCH.213>回写流水信息...');
    DM.LogToDB('213', '同步流水', '开始回写流水信息');
    for i := 0 to BackPatch.Count - 1 do
    begin
      with TUniQuery.Create(nil) do
      begin
        try
          ComCode := BackPatch.Names[i];
          Connection := GetConnByComCode(ComCode).Conn;
          Connection.Disconnect;
          if not Connection.Connected then
          begin
            Connection.Connect;
          end;
          SQL.Text := BackPatch.ValueFromIndex[i];
          LOG('<SERVICE.PATCH.214>查询校验(ComCode：' + ComCode + ')信息...');
          DM.LogToDB('214', '同步流水', '查询(ComCode：' + ComCode + '):'#13#10 + SQL.Text);
          try
            Open;
          except
            on E: Exception do
            begin
              Log('<SERVICE.PATCH.ERROR.297>查询时出错(' + E.ClassName + '):' + E.Message);
              DM.LogToDB('297', '[错误]同步流水', '查询时出错(' + E.ClassName + '):' + E.Message);
              Connection.Disconnect;
              if not Connection.Connected then
              begin
                Connection.Connect;
                if Connection.Connected then
                begin
                  Open;
                end;
              end;
            end;
          end;
          while not Eof do
          begin
            LOG('<SERVICE.PATCH.214>回写流水信息...');
            DM.LogToDB('214', '同步流水',
              '<SERVICE.PATCH>回写:'#13#10 + 'UPDATE T_BAS_TRADE SET F_POS_DEVICE_NO = ''' + FieldByName('POS_NO').AsString
              + ''', F_POS_SALE_NO=''' + FieldByName('BILL_ID').AsString
              + ''' WHERE F_TRADE_NO = ''' + FieldByName('AUTH_TRADE_CODE').AsString + ''''#13#10
              + 'UPDATE T_BAS_ORDER SET F_SALE_NO = ''' + FieldByName('POS_NO').AsString
              + 'N' + FieldByName('BILL_ID').AsString
              + ''' WHERE F_TRADE_NO = ''' + FieldByName('AUTH_TRADE_CODE').AsString + '''');
            try
              DM.Conn.ExecSQL('UPDATE T_BAS_TRADE SET F_POS_DEVICE_NO = ''' + FieldByName('POS_NO').AsString
                + ''', F_POS_SALE_NO=''' + FieldByName('BILL_ID').AsString
                + ''' WHERE F_TRADE_NO = ''' + FieldByName('AUTH_TRADE_CODE').AsString + '''', []);
                //UPDATE T_BAS_ORDER SET F_SALE_NO = 'N' WHERE
              DM.Conn.ExecSQL('UPDATE T_BAS_ORDER SET F_SALE_NO = ''' + FieldByName('POS_NO').AsString
                + 'N' + FieldByName('BILL_ID').AsString
                + ''' WHERE F_TRADE_NO = ''' + FieldByName('AUTH_TRADE_CODE').AsString + '''', []);
            except
              on E: Exception do
              begin
                Log('<SERVICE.PATCH.ERROR.298>回写流水时出错(' + E.ClassName + '):' + E.Message);
                DM.LogToDB('298', '[错误]同步流水', '回写流水时出错(' + E.ClassName + '):' + E.Message);
              end;
            end;
            Next;
          end;
        finally
          Free;
        end;
      end;
    end;
    LOG('<SERVICE.PATCH.219>回写流水完成');
    DM.LogToDB('219', '同步流水', '回写流水完成');
  finally
    LsPatch.Free;
    BackPatch.Free;
  end;
end;

procedure TDM.LogToDB(sCode, sName, sLog: String);
begin
  try
    if not DM.Conn.Connected then
      DM.Conn.Connect;
    if not DM.Conn.Connected then
    begin
      Log('<LogToDB.ERROR>写数据库日志失败:数据库连接失败(' + DM.Conn.Server + ')!');
      Exit;
    end;
  Except
    on E: Exception do
    begin
      Log('<LogToDB.ERROR>写数据库日志时出错(' + E.ClassName + '):' + E.Message);
      Exit;
    end;
  end;
  try
    sqlLog.ParamByName('F_CODE').AsString := sCode;
    sqlLog.ParamByName('F_NAME').AsString := sName;
    sqlLog.ParamByName('F_LOG').AsString := sLog;
    sqlLog.Execute;
  except
    on E: Exception do
    begin
      Log('<LogToDB.ERROR>写数据库日志时出错(' + E.ClassName + '):' + E.Message + #13#10 + '日志内容:CODE:' + sCode + ',NAME:' + sName + ',LOG:' + sLog);
    end;
  end;
end;

initialization
  { To adjust scalability of the DataSnapModule and avoid recreation of
    instances for each roundtrip, you might want to choose a  different
    ClassFactory type.

    Make sure to check the following url for more information:
    http://www.remobjects.com?ro16 }

  //TROPooledClassFactory.Create('IAppServer', Create_DataSnapModule, TAppServer_Invoker, 25);
  TROClassFactory.Create('SrvMDS', Create_DataSnapModule, TAppServer_Invoker);
end.
