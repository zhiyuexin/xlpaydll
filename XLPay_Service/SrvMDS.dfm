object MDS: TMDS
  OldCreateOrder = False
  OnCreate = ServiceCreate
  DisplayName = 'PosMutiDisplayService'
  AfterInstall = ServiceAfterInstall
  OnContinue = ServiceContinue
  OnPause = ServicePause
  OnStart = ServiceStart
  OnStop = ServiceStop
  Left = 513
  Top = 380
  Height = 294
  Width = 290
  object MSG_BIN_HTTP: TROBinMessage
    Envelopes = <>
    Left = 104
    Top = 16
  end
  object MSG_SOAP_HTTP: TROSOAPMessage
    Envelopes = <>
    SerializationOptions = [xsoSendUntyped, xsoStrictStructureFieldOrder, xsoDocument, xsoSplitServiceWsdls]
    Left = 176
    Top = 16
  end
  object ROHTTPServer: TROBPDXHTTPServer
    Dispatchers = <
      item
        Name = 'MSG_BIN_HTTP'
        Message = MSG_BIN_HTTP
        Enabled = True
        PathInfo = 'BIN'
      end
      item
        Name = 'MSG_SOAP_HTTP'
        Message = MSG_SOAP_HTTP
        Enabled = True
        PathInfo = 'SOAP'
      end>
    BPDXServer.ReleaseDate = '2002-09-01'
    BPDXServer.ListenerThreadPriority = tpIdle
    BPDXServer.SpawnedThreadPriority = tpIdle
    BPDXServer.Suspend = False
    BPDXServer.UseSSL = False
    BPDXServer.UseThreadPool = False
    BPDXServer.ServerPort = 8090
    BPDXServer.ProtocolToBind = wpTCPOnly
    BPDXServer.SocketOutputBufferSize = bsfNormal
    BPDXServer.ServerType = stThreadBlocking
    BPDXServer.ThreadCacheSize = 10
    BPDXServer.Timeout = 50000
    BPDXServer.SupportKeepAlive = False
    Port = 8090
    SupportKeepAlive = False
    Left = 32
    Top = 16
  end
  object ROTCPServer: TROBPDXTCPServer
    Dispatchers = <
      item
        Name = 'MSG_BIN_TCP'
        Message = MSG_BIN_TCP
        Enabled = True
      end>
    BPDXServer.ReleaseDate = '2002-09-01'
    BPDXServer.ListenerThreadPriority = tpIdle
    BPDXServer.SpawnedThreadPriority = tpIdle
    BPDXServer.Suspend = False
    BPDXServer.UseSSL = False
    BPDXServer.UseThreadPool = False
    BPDXServer.ServerPort = 8090
    BPDXServer.ProtocolToBind = wpTCPOnly
    BPDXServer.SocketOutputBufferSize = bsfNormal
    BPDXServer.ServerType = stThreadBlocking
    BPDXServer.ThreadCacheSize = 10
    Port = 8090
    Left = 32
    Top = 72
  end
  object MSG_BIN_TCP: TROBinMessage
    Envelopes = <>
    Left = 104
    Top = 72
  end
  object cdsPlaySourceDetail: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64896
    Top = 65184
    object cdsPlaySourceDetailID: TIntegerField
      FieldName = 'ID'
    end
    object cdsPlaySourceDetailPSID: TIntegerField
      FieldName = 'PSID'
    end
    object cdsPlaySourceDetailPropertyID: TIntegerField
      FieldName = 'PropertyID'
    end
    object cdsPlaySourceDetailPropertyName: TStringField
      FieldName = 'PropertyName'
    end
    object cdsPlaySourceDetailPropertyValue: TStringField
      FieldName = 'PropertyValue'
      Size = 1024
    end
    object cdsPlaySourceDetailSortID: TIntegerField
      FieldName = 'SortID'
    end
    object cdsPlaySourceDetailRMK: TStringField
      FieldName = 'RMK'
    end
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64896
    Top = 65184
    object ClientDataSet1ID: TIntegerField
      FieldName = 'ID'
    end
    object ClientDataSet1PSID: TIntegerField
      FieldName = 'PSID'
    end
    object ClientDataSet1PropertyID: TIntegerField
      FieldName = 'PropertyID'
    end
    object ClientDataSet1PropertyName: TStringField
      FieldName = 'PropertyName'
    end
    object ClientDataSet1PropertyValue: TStringField
      FieldName = 'PropertyValue'
      Size = 1024
    end
    object ClientDataSet1SortID: TIntegerField
      FieldName = 'SortID'
    end
    object ClientDataSet1RMK: TStringField
      FieldName = 'RMK'
    end
  end
  object ClientDataSet2: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64896
    Top = 65184
    object ClientDataSet2ID: TIntegerField
      FieldName = 'ID'
    end
    object ClientDataSet2PSID: TIntegerField
      FieldName = 'PSID'
    end
    object ClientDataSet2PropertyID: TIntegerField
      FieldName = 'PropertyID'
    end
    object ClientDataSet2PropertyName: TStringField
      FieldName = 'PropertyName'
    end
    object ClientDataSet2PropertyValue: TStringField
      FieldName = 'PropertyValue'
      Size = 1024
    end
    object ClientDataSet2SortID: TIntegerField
      FieldName = 'SortID'
    end
    object ClientDataSet2RMK: TStringField
      FieldName = 'RMK'
    end
  end
  object ServerHTTPS: TRtcHttpServer
    MultiThreaded = True
    Timeout.AfterConnecting = 300
    ServerPort = '443'
    RestartOn.ListenLost = True
    FixupRequest.RemovePrefix = True
    MaxRequestSize = 128000
    MaxHeaderSize = 16000
    Left = 104
    Top = 184
  end
  object ServerHTTP: TRtcHttpServer
    MultiThreaded = True
    Timeout.AfterConnecting = 300
    ServerPort = '80'
    RestartOn.ListenLost = True
    FixupRequest.RemovePrefix = True
    MaxRequestSize = 128000
    MaxHeaderSize = 16000
    Left = 33
    Top = 184
  end
  object TcpServer: TRtcTcpServer
    MultiThreaded = True
    OnDisconnecting = TcpServerDisconnecting
    OnConnect = TcpServerConnect
    OnException = TcpServerException
    OnDataReceived = TcpServerDataReceived
    Left = 32
    Top = 128
  end
  object Client: TRtcTcpClient
    MultiThreaded = True
    Timeout.AfterConnecting = 1800
    Left = 104
    Top = 128
  end
  object Timer: TTimer
    Interval = 3000
    OnTimer = TimerTimer
    Left = 176
    Top = 128
  end
end
