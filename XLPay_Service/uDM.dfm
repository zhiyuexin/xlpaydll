object DM: TDM
  OldCreateOrder = True
  OnCreate = RODataSnapModuleCreate
  Providers = <>
  Left = 400
  Top = 282
  Height = 289
  Width = 483
  object Conn: TUniConnection
    ProviderName = 'Oracle'
    Port = 5000
    SpecificOptions.Strings = (
      'Oracle.Direct=True')
    PoolingOptions.MaxPoolSize = 500
    PoolingOptions.MinPoolSize = 3
    Pooling = True
    Username = 'dm'
    Password = 'dm'
    Server = '10.2.26.21:1521:dm'
    LoginPrompt = False
    AfterConnect = ConnAfterConnect
    BeforeConnect = ConnBeforeConnect
    AfterDisconnect = ConnAfterDisconnect
    BeforeDisconnect = ConnBeforeDisconnect
    OnError = ConnError
    Left = 32
    Top = 18
  end
  object qry: TUniQuery
    Connection = Conn
    Left = 87
    Top = 18
  end
  object cmd: TUniScript
    Connection = Conn
    Left = 190
    Top = 18
  end
  object dpqry: TDataSetProvider
    DataSet = qry
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 142
    Top = 18
  end
  object SQL: TUniSQL
    Connection = Conn
    Left = 238
    Top = 18
  end
  object qry1: TUniQuery
    Connection = Conn
    Left = 86
    Top = 66
  end
  object dpqry1: TDataSetProvider
    DataSet = qry1
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 142
    Top = 66
  end
  object qry2: TUniQuery
    Connection = Conn
    Left = 190
    Top = 66
  end
  object dpqry2: TDataSetProvider
    DataSet = qry2
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 238
    Top = 66
  end
  object SQLMonitor: TUniSQLMonitor
    OnSQL = SQLMonitorSQL
    Left = 32
    Top = 73
  end
  object qryTradePatchList: TUniQuery
    Connection = Conn
    SQL.Strings = (
      'SELECT * FROM V_BAS_PATCH_TRADE')
    Left = 52
    Top = 150
    object qryTradePatchListF_TRADE_NO: TStringField
      FieldName = 'F_TRADE_NO'
      Required = True
      Size = 40
    end
    object qryTradePatchListF_COM_CODE: TStringField
      FieldName = 'F_COM_CODE'
    end
    object qryTradePatchListF_COM_NAME: TStringField
      FieldName = 'F_COM_NAME'
      ReadOnly = True
      Size = 80
    end
    object qryTradePatchListF_VIP_CODE: TStringField
      FieldName = 'F_VIP_CODE'
      Size = 40
    end
    object qryTradePatchListF_VIP_ID: TFloatField
      FieldName = 'F_VIP_ID'
    end
    object qryTradePatchListF_TRADE_AMT: TFloatField
      FieldName = 'F_TRADE_AMT'
    end
    object qryTradePatchListF_TRADE_TIME: TDateTimeField
      FieldName = 'F_TRADE_TIME'
    end
    object qryTradePatchListF_CREATE_TIME: TSQLTimeStampField
      FieldName = 'F_CREATE_TIME'
    end
  end
  object qryLsDbms: TUniQuery
    Connection = Conn
    SQL.Strings = (
      
        'SELECT F_COM_CODE, F_DBMS_TYPE, F_SRV_NAME, F_SRV_PORT, F_SRV_DA' +
        'TABASE, F_SRV_USER, F_SRV_PWD, F_SRV_PARAM FROM T_SYS_DBMS WHERE' +
        ' F_SYSTEM_TYPE = 1 AND F_VALID_STATE = 0')
    Left = 144
    Top = 145
    object qryLsDbmsF_COM_CODE: TStringField
      FieldName = 'F_COM_CODE'
    end
    object qryLsDbmsF_DBMS_TYPE: TStringField
      FieldName = 'F_DBMS_TYPE'
    end
    object qryLsDbmsF_SRV_NAME: TStringField
      FieldName = 'F_SRV_NAME'
      Size = 40
    end
    object qryLsDbmsF_SRV_PORT: TFloatField
      FieldName = 'F_SRV_PORT'
    end
    object qryLsDbmsF_SRV_DATABASE: TStringField
      FieldName = 'F_SRV_DATABASE'
      Size = 80
    end
    object qryLsDbmsF_SRV_USER: TStringField
      FieldName = 'F_SRV_USER'
      Size = 40
    end
    object qryLsDbmsF_SRV_PWD: TStringField
      FieldName = 'F_SRV_PWD'
      Size = 40
    end
    object qryLsDbmsF_SRV_PARAM: TStringField
      FieldName = 'F_SRV_PARAM'
      Size = 256
    end
  end
  object sqlLog: TUniSQL
    Connection = Conn
    SQL.Strings = (
      'INSERT INTO T_LOG_TRADE_SERVICE(F_CODE, F_NAME, F_LOG)'
      'VALUES(:F_CODE, :F_NAME, :F_LOG)')
    Left = 219
    Top = 144
    ParamData = <
      item
        DataType = ftString
        Name = 'F_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'F_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'F_LOG'
        ParamType = ptInput
      end>
  end
  object IPWatch: TIdIPWatch
    Active = False
    HistoryFilename = 'iphist.dat'
    Left = 313
    Top = 124
  end
end
