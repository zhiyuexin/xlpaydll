unit uDMFileProvider;

interface

uses
  SysUtils, Classes, rtcDataSrv, rtcInfo, rtcConn, rtcTcpSrv, dcInternal, MidasLib,
  dcFileInfo, uDM, rtcFunction, rtcScript, VCLUnZip, VCLZip;
var
  MAX_SEND_BLOCK_SIZE: int64 = 1460 * 44; // larger files will be sent in smaller blocks
  MAX_ACCEPT_BODY_SIZE: int64 = 128000;
type
  TStringObject = class
  public
    value: String;
  end;

  TFile_Provider = class(TDataModule)
    FileProvider: TRtcDataProvider;
    TimeProvider: TRtcDataProvider;
    FileInfo: TdcFileInfo;
    ServerLink: TRtcDataServerLink;
    PageProvider: TRtcDataProvider;
    RtcScriptEngine1: TRtcScriptEngine;
    RtcFunctionGroup: TRtcFunctionGroup;
    RtcFncTime: TRtcFunction;
    RtcFncFileInfo: TRtcFunction;
    RtcFncQuery: TRtcFunction;
    RtcFncNow: TRtcFunction;
    RtcFncDate: TRtcFunction;
    RtcFncFormatDateTime: TRtcFunction;
    RtcFncReplace: TRtcFunction;
    RtcFunction1: TRtcFunction;
    RtcFncCopy: TRtcFunction;
    UploadProvider: TRtcDataProvider;
    VCLZip1: TVCLZip;
    RtcFncExecSQL: TRtcFunction;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure FileProviderCheckRequest(Sender: TRtcConnection);
    procedure FileProviderDataReceived(Sender: TRtcConnection);
    procedure FileProviderDataSent(Sender: TRtcConnection);
    procedure FileProviderDisconnect(Sender: TRtcConnection);
    procedure PageProviderCheckRequest(Sender: TRtcConnection);
    procedure PageProviderDataReceived(Sender: TRtcConnection);
    procedure RtcFncCopyExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure RtcFncDateExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure RtcFncExecSQLExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure RtcFncFileInfoExecute(Sender: TRtcConnection; Param:
      TRtcFunctionInfo; Result: TRtcValue);
    procedure RtcFncFormatDateTimeExecute(Sender: TRtcConnection; Param:
      TRtcFunctionInfo; Result: TRtcValue);
    procedure RtcFncNowExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure RtcFncQueryExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure RtcFncReplaceExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure RtcFncTimeExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo;
      Result: TRtcValue);
    procedure TimeProviderCheckRequest(Sender: TRtcConnection);
    procedure TimeProviderDataReceived(Sender: TRtcConnection);
    procedure UploadProviderCheckRequest(Sender: TRtcConnection);
    procedure UploadProviderDataReceived(Sender: TRtcConnection);
  private
    HostList: TStringList;
    PageList: TStringList;
    ExtList: TStringList;
    CTypesList: TList;

  protected
    function GetDocRoot(Host: RtcString): String;
    function GetContentType(FName: String): RtcString;
    function RepairWebFileName(FileName, DocRoot: String): String;

  public
    procedure ClearHosts;
    procedure AddHost(a: String);
    procedure ClearIndexPages;
    procedure AddIndexPage(a: String);
    procedure ClearContentTypes;
    procedure AddContentType(a: String);
  end;


function GetDocRoot(Host: RtcString): String;
function RepairWebFileName(FileName, DocRoot: String): String;
function GetFileProvider: TFile_Provider;

var
  File_Provider: TFile_Provider;

implementation

uses uCommon, windows, DB;
{$R *.dfm}


procedure TFile_Provider.FileProviderCheckRequest(Sender: TRtcConnection);
  procedure CheckDiskFile(Sender: TRtcDataServer);
  var
    fsize: int64;
    Content_Type: RtcString;
    rang_type, rang_from, rang_to: RtcString;
    rang_start, rang_end: int64;

    MyFileName,
    DocRoot, DocFile: String;
    RequestStr: string;
    LayoutID, ClientIP, ClientNo, ClientType, ComputerName: string;
    i, ClientPort: Integer;
    FileList: TStringList;

    function RepairFileName(FileName: String): String;
    var
      a: integer;
      FullName: String;
    begin
      FileName := StringReplace(FileName, '/', '\', [rfreplaceall]);
      for a := 0 to HostList.Count - 1 do
      begin
        if DocRoot = TStringObject(HostList.Objects[a]).value then
        begin
          FileName := StringReplace(UpperCase(FileName), HostList.Strings[a] + '\', '', [rfreplaceall]);
          Break;
        end;
      end;

      FullName := ExpandFileName(DocRoot + FileName);
      if (Pos('\..', FullName) > 0) or (UpperCase(Copy(FullName, 1, length(DocRoot))) <> UpperCase(DocRoot)) then
      begin
        Sender.Accept;
        //Log('DENY ' + Sender.PeerAddr + ' > ' + Sender.Request.Host + ' "' + Sender.Request.Method + ' ' + Sender.Request.URI + '"' + ' 0' + ' REF "' + Sender.Request.Referer + '"' + ' AGENT "' + Sender.Request.Agent + '" > Invalid FileName: "' + RtcString(FullName) + '".');
        Sender.Response.Status(403, 'Forbidden');
        Sender.Write('Status 403: Forbidden');
        Result := '';
        fsize := -1;
        Exit;
      end
      else
      begin
        FileName := FullName;
        Delete(FileName, 1, length(DocRoot));
      end;

      if Copy(FileName, length(FileName), 1) <> '\' then
        if DirectoryExists(DocRoot + FileName) then
        begin
          Sender.Accept;
          Sender.Response.Status(301, 'Moved Permanently');
          Sender.Response['LOCATION'] := Sender.Request.FileName + '/';
          Sender.Write('Status 301: Moved Permanently');
          Result := '';
          Exit;
        end;
      if Copy(FileName, length(FileName), 1) = '\' then
      begin
        for a := 0 to PageList.Count - 1 do
        begin
          fsize := File_Size(FullName + PageList.Strings[a]);
          if fsize >= 0 then
          begin
            FileName := FileName + PageList.Strings[a];
            Break;
          end;
        end;
      end
      else
        fsize := File_Size(FullName);
      Result := StringReplace(FileName, '\', '/', [rfreplaceall]);
    end;

    procedure ParseRange(s: RtcString; var r_type, r_from, r_to: RtcString);
    var
      a: integer;
    begin
      r_type := ''; r_from := ''; r_to := '';
      a := pos(RtcChar('='), s);
      if a > 0 then
      begin
        r_type := UpperCase(Trim(Copy(s, 1, a - 1)));
        Delete(s, 1, a);
        a := Pos(RtcChar('-'), s);
        if a > 0 then
        begin
          r_from := Trim(Copy(s, 1, a - 1));
          Delete(s, 1, a);
          a := Pos(RtcChar(','), s);
          if a > 0 then
            r_to := Trim(Copy(s, 1, a - 1))
          else
            r_to := Trim(s);
        end
        else
        begin
          a := Pos(RtcChar(','), s);
          if a > 0 then
            r_from := Trim(Copy(s, 1, a - 1))
          else
            r_from := Trim(s);
        end;
      end;
    end;

    function RenderFile(filename: string; DocRoot: string = ''): Boolean;
    var
      MyFileName: string;
    begin
      Result := False;
      if DocRoot = '' then
      begin
        MyFileName := ExtractFileName(filename);
        DocRoot := ExtractFilePath(filename);
        fsize := File_Size(filename);
      end
      else
        MyFileName := RepairFileName(filename);
      if MyFileName <> '' then
      begin
        if fsize > 0 then
        begin
          Sender.Accept; // found the file, we will be responding to this request.
          // Check if we have some info about the content type for this file ...
          Content_Type := GetContentType(MyFileName);
          Sender.Request.Info.asText['fname'] := StringReplace(MyFileName, '/', '\', [rfReplaceAll]);
          Sender.Request.Info.asText['root'] := DocRoot;
          Sender.Request.Info.asLargeInt['$time'] := GetTickCount;
          Sender.Request.Info.asLargeInt['from'] := 0;
          // Response['Cache-Control']:='max-age=600'; // instruct the browser to load the file from cache, if file in cache is younger than 600 seconds (10 minutes)
          Sender.Response.ContentType := Content_Type;
          Sender.Response.ContentLength := fsize;
          if Sender.Request.Method = 'HEAD' then
            Sender.Response.SendContent := False
          else if Sender.Request.Method = 'GET' then
          begin
            if Sender.Request['RANGE'] <> '' then
            begin
              ParseRange(Trim(Sender.Request['RANGE']), rang_type, rang_from, rang_to);
              if rang_type = 'BYTES' then // we will support a single bytes range
              begin
                if rang_from <> '' then
                begin
                  rang_end := fsize - 1;
                  rang_start := Str2Int64Def(rang_from, 0);
                  if rang_to <> '' then
                    rang_end := Str2Int64Def(rang_to, fsize - 1);
                  if rang_start <= rang_end then
                  begin
                    if rang_end >= fsize then
                      rang_end := fsize - 1;
                    rang_from := Int2Str(rang_start);
                    rang_to := Int2Str(rang_end);
                    Sender.Request.Info.asLargeInt['from'] := rang_start;
                    Sender.Response.ContentLength := rang_end - rang_start + 1;
                    Sender.Response.Status(206, 'Partial Content');
                    Sender.Response['Content-Range'] := 'bytes ' + rang_from + '-' + rang_to + '/' + Int2Str(fsize);
                  end
                  else
                  begin
                    Sender.Response.Status(416, 'Request range not satisfiable');
                    Sender.Request.ContentLength := 0;
                  end;
                end
                else
                begin
                  rang_end := -1;
                  if rang_to <> '' then
                    rang_end := Str2Int64Def(rang_to, -1);
                  if rang_end > 0 then
                  begin
                    if rang_end > fsize then
                      rang_end := fsize;
                    rang_start := fsize - rang_end;
                    rang_end := fsize - 1;
                    if rang_to >= rang_from then
                    begin
                      rang_from := Int2Str(rang_start);
                      rang_to := Int2Str(rang_end);
                      Sender.Request.Info.asLargeInt['from'] := rang_start;
                      Sender.Response.ContentLength := rang_end - rang_start + 1;
                      Sender.Response.Status(206, 'Partial Content');
                      Sender.Response['CONTENT-RANGE'] := 'bytes ' + rang_from + '-' + rang_to + '/' + Int2Str(fsize);
                    end
                    else
                    begin
                      Sender.Response.Status(416, 'Request range not satisfiable');
                      Sender.Response.ContentLength := 0;
                    end;
                  end;
                end;
              end;
            end;
          end;
          //if Sender.Request['RANGE'] <> '' then
          //  Log('PART ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' ' + Int2Str(fsize) + ' RANGE "' + Request['RANGE'] + '"' + ' (' + Response['CONTENT-RANGE'] + ')' + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '"' + ' TYPE "' + Content_Type + '"')
          //else
          //  Log('SEND ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' ' + Int2Str(fsize) + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '"' + ' TYPE "' + Content_Type + '"');
          {$IFDEF RTC_FILESTREAM}
          if Sender.Response.ContentLength > 0 then
          begin
            Request.Info.Obj['file'] := TRtcFileStream.Create;
            with TRtcFileStream(Request.Info.Obj['file']) do
            begin
              Open(DocRoot + Request.Info.asText['fname']);
              Seek(Request.Info.asLargeInt['from']);
            end;
          end;
          {$ENDIF}
          Write;
          Result := True;
        end
        else if fsize = 0 then  // Found the file, but it is empty.
        begin
          Sender.Accept;
          //Log('SEND ' + Sender.PeerAddr + ' > ' + Sender.Request.Host + ' "' + Sender.Request.Method + ' ' + Sender.Request.URI + '"' + ' 0' + ' REF "' + Sender.Request.Referer + '"' + ' AGENT "' + Sender.Request.Agent + '"');
          Write;
          Result := True;
        end;
      end;
    end;
  begin
    with Sender do
    begin
      // Check HOST and find document root
      DocRoot := GetDocRoot(Request.FileName);
      if DocRoot = '' then
      begin
        Sender.Accept;
        //Log('BAD! ' + Sender.PeerAddr + ' > ' + Sender.Request.Host + ' "' + Sender.Request.Method + ' ' + Sender.Request.URI + '"' + ' 0' + ' REF "' + Sender.Request.Referer + '"' + ' AGENT "' + Sender.Request.Agent + '" > Invalid Host: "' + Request.Host + '".');
        Response.Status(400, 'Bad Request');
        Write('Status 400: Bad Request');
        Exit;
      end;

      // Check File Name and send Result Header
      MyFileName := RepairFileName(String(Request.FileName));
      if MyFileName <> '' then
      begin
        if RenderFile(MyFileName, DocRoot) then
        begin
        end
        else
        begin
          // File not found.
          RequestStr := Copy(UpperCase(Request.FileName), 2, Length(Request.FileName) - 1);
          if RequestStr = 'DEFAULT' then  //
          begin
//            ClientIP := Request.Query.Value['ClientIP'];
//            ClientNo := Request.Query.Value['ClientNo'];
            if FileExists(DocRoot + '\index.html') then
            begin
              Accept;
              Response.Status(302, 'Moved');
              Response['Location'] := 'index_' + LayoutID + '.html';
              Write('Status 302: Moved');
            end
            else
            begin
              Log('File not found:index.html');
              Response.Status(404, 'File not found:index.html');
              Write('Status 404: Default page not found');
            end;
          end
          else if RequestStr = 'GETTIME' then
          begin
            Accept;
            Write(FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now));
          end
          else  // File not found.
          begin
            Accept;
            //Log('FAIL ' + Sender.PeerAddr + ' > ' + Sender.Request.Host + ' "' + Sender.Request.Method + ' ' + Sender.Request.URI + '"' + ' 0' + ' REF "' + Sender.Request.Referer + '"' + ' AGENT "' + Sender.Request.Agent + '" > File not found: "' + RtcString(MyFileName) + '".');
            Response.Status(404, 'File not found');
            Write('Status 404: File not found ' + RequestStr);
          end;
        end;
      end;
    end;
  end;
begin
  with TRtcDataServer(Sender).Request do
    if (Method = 'GET') or (Method = 'HEAD') then
      CheckDiskFile(TRtcDataServer(Sender));
end;


function GetFileProvider: TFile_Provider;
begin
  if not assigned(File_Provider) then
    File_Provider := TFile_Provider.Create(nil);
  Result := File_Provider;
end;

function RepairWebFileName(FileName, DocRoot: String): String;
begin
  Result := GetFileProvider.RepairWebFileName(FileName, DocRoot);
end;

function GetDocRoot(Host: RtcString): String;
begin
  Result := GetFileProvider.GetDocRoot(Host);
end;


procedure TFile_Provider.DataModuleDestroy(Sender: TObject);
begin
  File_Provider := nil;
  ClearHosts;
  HostList.Free;
  ClearIndexPages;
  PageList.Free;
  ClearContentTypes;
  ExtList.Free;
  CTypesList.Free;
end;

procedure TFile_Provider.DataModuleCreate(Sender: TObject);
begin
  HostList := TStringList.Create;
  HostList.Sorted := True;
  PageList := TStringList.Create;
  ExtList := TStringList.Create;
  CTypesList := TList.Create;
end;

procedure TFile_Provider.AddContentType(a: String);
var
  elist, ext: String;
  loc: integer;
  htext: String;
  octype: TStringObject;
begin
  loc := Pos('=', a);
  if loc > 0 then
  begin
    elist := Trim(Copy(a, 1, loc - 1));
    htext := Trim(Copy(a, loc + 1, MaxInt));

    octype := TStringObject.Create;
    octype.value := htext;

    CTypesList.Add(octype);

    while elist <> '' do
    begin
      if Pos(',', elist) > 0 then
      begin
        ext := UpperCase(Trim(Copy(elist, 1, Pos(',', elist) - 1)));
        if Copy(ext, 1, 1) <> '.' then ext := '.' + ext;
        Delete(elist, 1, Pos(',', elist));
        elist := Trim(elist);
      end
      else
      begin
        ext := UpperCase(elist);
        if Copy(ext, 1, 1) <> '.' then ext := '.' + ext;
        elist := '';
      end;
      ExtList.AddObject(ext, octype);
    end;
  end;
  ExtList.Sort;
  ExtList.Sorted := True;
end;

procedure TFile_Provider.AddHost(a: String);
var
  loc: integer;
  hname, hdir: String;
  odir: TStringObject;
begin
  loc := Pos('=', a);
  if loc > 0 then
  begin
    hname := UpperCase(Trim(Copy(a, 1, loc - 1)));
    hdir := Trim(Copy(a, loc + 1, MaxInt));
    hdir := StringReplace(hdir, '/', '\', [rfReplaceAll]);

    // Resolve a problem with relative paths
    if (Pos(':', hdir) = 0) and (Copy(hdir, 1, 1) <> '\') then
      hdir := ExtractFilePath(AppFileName) + hdir;

    // Remove trailing \
    if Copy(hdir, length(hdir), 1) = '\' then
      Delete(hdir, length(hdir), 1);

    hdir := ExpandFileName(hdir);

    odir := TStringObject.Create;
    odir.value := hdir;

    HostList.AddObject(hname, odir);
  end;
  HostList.Sort;
  HostList.Sorted := True;
end;

procedure TFile_Provider.AddIndexPage(a: String);
begin
  if Trim(a) <> '' then
    PageList.Add(Trim(a));
end;

procedure TFile_Provider.ClearContentTypes;
var
  a: integer;
begin
  for a := 0 to CTypesList.Count - 1 do
    with TStringObject(CTypesList.Items[a]) do
    begin
      value := '';
      Free;
    end;
  CTypesList.Clear;
  ExtList.Clear;
end;

procedure TFile_Provider.ClearHosts;
var
  a: integer;
begin
  for a := 0 to HostList.Count - 1 do
    with TStringObject(HostList.Objects[a]) do
    begin
      value := '';
      Free;
    end;
  HostList.Clear;
end;

procedure TFile_Provider.ClearIndexPages;
begin
  PageList.Clear;
end;

procedure TFile_Provider.FileProviderDataReceived(Sender: TRtcConnection);
var
  DocRoot: String;
  s: RtcString;
begin
  with TRtcDataServer(Sender) do
  begin
    if Request.Complete then
    begin
      if Response.DataOut < Response.DataSize then // need to send more content
      begin
        {$IFDEF RTC_FILESTREAM}
        if Response.DataSize - Response.DataOut > MAX_SEND_BLOCK_SIZE then
          s := TRtcFileStream(Request.Info.Obj['file']).Read(MAX_SEND_BLOCK_SIZE)
        else
          s := TRtcFileStream(Request.Info.Obj['file']).Read(Response.DataSize - Response.DataOut);
        {$ELSE}
        DocRoot := Request.Info['root'];
        if Response.DataSize - Response.DataOut > MAX_SEND_BLOCK_SIZE then
          s := Read_File(DocRoot + Request.Info.asText['fname'], Request.Info.asLargeInt['from'] + Response.DataOut, MAX_SEND_BLOCK_SIZE)
        else
          s := Read_File(DocRoot + Request.Info.asText['fname'], Request.Info.asLargeInt['from'] + Response.DataOut, Response.DataSize - Response.DataOut);
        {$ENDIF}
        if s = '' then // Error reading file.
        begin
          //Log('CRC! ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' > Error reading File: "' + RtcString(DocRoot + Request.Info.asText['fname']) + '".');
          Disconnect;
        end
        else
          Write(s);
      end;
    end
    else if Request.DataSize > MAX_ACCEPT_BODY_SIZE then // Do not accept requests with body longer than 128K
    begin
      //Log('BAD! ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' 0' + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '" ' + '> Content size exceeds 128K limit (size=' + Int2Str(Request.DataSize) + ' bytes).');
      Response.Status(400, 'Bad Request');
      Write('Status 400: Bad Request');
      Disconnect;
    end;
  end;
end;

procedure TFile_Provider.FileProviderDataSent(Sender: TRtcConnection);
var
  DocRoot: String;
  s: RtcString;
begin
  with TRtcDataServer(Sender) do
  begin
    if Request.Complete then
    begin
      if Response.DataOut < Response.DataSize then // need to send more content
      begin
        {$IFDEF RTC_FILESTREAM}
        if Response.DataSize - Response.DataOut > MAX_SEND_BLOCK_SIZE then
          s := TRtcFileStream(Request.Info.Obj['file']).Read(MAX_SEND_BLOCK_SIZE)
        else
          s := TRtcFileStream(Request.Info.Obj['file']).Read(Response.DataSize - Response.DataOut);
        {$ELSE}
        DocRoot := Request.Info['root'];

        if Response.DataSize - Response.DataOut > MAX_SEND_BLOCK_SIZE then
          s := Read_File(DocRoot + Request.Info.asText['fname'], Request.Info.asLargeInt['from'] + Response.DataOut, MAX_SEND_BLOCK_SIZE)
        else
          s := Read_File(DocRoot + Request.Info.asText['fname'], Request.Info.asLargeInt['from'] + Response.DataOut, Response.DataSize - Response.DataOut);
        {$ENDIF}

        if s = '' then // Error reading file.
        begin
          //Log('CRC! ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' > Error reading File: "' + RtcString(DocRoot + Request.Info.asText['fname']) + '".');
          Disconnect;
        end
        else
          Write(s);
      end;
    end
    else if Request.DataSize > MAX_ACCEPT_BODY_SIZE then // Do not accept requests with body longer than 128K
    begin
      //Log('BAD! ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' 0' + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '" ' + '> Content size exceeds 128K limit (size=' + Int2Str(Request.DataSize) + ' bytes).');
      Response.Status(400, 'Bad Request');
      Write('Status 400: Bad Request');
      Disconnect;
    end;
  end;
end;

procedure TFile_Provider.FileProviderDisconnect(Sender: TRtcConnection);
var
  tim: int64;
begin
  with TRtcDataServer(Sender) do
  begin
    if Request.DataSize > Request.DataIn then
    begin // did not receive a complete request 
      Log('ERR! ' + PeerAddr + ' > ' + Request['HOST'] {.rHost} + ' "' + Request.Method + ' ' + Request.URI + '"' + ' 0' + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '" ' + '> DISCONNECTED while receiving a Request (' + Int2Str(Request.DataIn) + ' of ' + Int2Str(Request.DataSize) + ' bytes received).');
    end
    else if Response.DataSize > Response.DataOut then
    begin
      tim := GetTickCount - Request.Info.asLargeInt['$time'];
      if tim <= 0 then tim := 1;

      if Response['CONTENT-RANGE'] = '' then // no need to show this for partial downloads  // did not send a complete result   
        Log('ERR! ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' -' + Int2Str(Response.DataSize - Response.DataOut) + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '" ' + Int2Str(Response.DataOut) + '/' + Int2Str(Response.DataSize) + ' bytes in ' + Int2Str(GetTickCount - Request.Info.asLargeInt['$time']) + ' ms =' + Int2Str(Response.DataOut div tim) + ' kbits')
      else // did not send a complete result           
        Log('BRK! ' + PeerAddr + ' > ' + Request.Host + ' "' + Request.Method + ' ' + Request.URI + '"' + ' -' + Int2Str(Response.DataSize - Response.DataOut) + ' REF "' + Request.Referer + '"' + ' AGENT "' + Request.Agent + '" ' + Int2Str(Response.DataOut) + ' bytes in ' + Int2Str(GetTickCount - Request.Info.asLargeInt['$time']) + ' ms =' + Int2Str(Response.DataOut div tim) + ' kbits');
    end;
  end;
end;

function TFile_Provider.GetContentType(FName: String): RtcString;
var
  loc: integer;
  ext: String;
begin
  ext := UpperCase(ExtractFileExt(FName));
  loc := ExtList.IndexOf(ext);
  if loc >= 0 then
    Result := RtcString(TStringObject(ExtList.Objects[loc]).value)
  else
  begin
    loc := ExtList.IndexOf('*');
    if loc >= 0 then
      Result := RtcString(TStringObject(ExtList.Objects[loc]).value)
    else
      Result := '';
  end;
end;

function TFile_Provider.GetDocRoot(Host: RtcString): String;
var
  loc: integer;
begin
  Host := UpperCase(Host);
  for loc := 0 to HostList.Count - 1 do
  begin
    if Copy(Host, 2, Length(HostList.Strings[loc])) = HostList.Strings[loc] then
    begin
      if Copy(Host, Length(HostList.Strings[loc]) + 2, 1) = '/' then
      begin
        Result := TStringObject(HostList.Objects[loc]).value;
        Exit;
      end;
    end;
  end;
  loc := HostList.IndexOf(String(Host));
  if loc >= 0 then
    Result := TStringObject(HostList.Objects[loc]).value
  else
  begin
    loc := HostList.IndexOf('*');
    if loc >= 0 then
      Result := TStringObject(HostList.Objects[loc]).value
    else
      Result := '';
  end;
end;

procedure TFile_Provider.PageProviderCheckRequest(Sender: TRtcConnection);
var
  PageType: string;
begin
  PageType := LowerCase(copy(TRtcDataServer(Sender).Request.FileName, length(TRtcDataServer(Sender).Request.FileName) - 3, 4));
  if (PageType = '.psp') or (PageType = '.asp') then TRtcDataServer(Sender).Accept;
end;

procedure TFile_Provider.PageProviderDataReceived(Sender: TRtcConnection);
var
  Srv: TRtcDataServer;
  len: cardinal;
  Body: RtcString;
  x: TRtcValue;
  s: string;
  i: Integer;
begin
  with TRtcDataServer(Sender) do
  begin
    Request.Params.AddText(Read); // <- fill data as it comes in
    if Request.Complete then  // process the request when you get everything
    begin
      s := StringReplace(GetDocRoot(Request.Host) + Request.FileName, '/', '\', [rfReplaceAll]);
      s := Read_File(s);
      try
        try
          for i := 0 to Request.Query.ItemCount - 1 do
            s := StringReplace(s, 'Request("' + Request.Query.ItemName[i] + '")', StringReplace(StringReplace(Request.Query.ItemValue[i], '%27', '''', [rfReplaceAll]), '%22', '"', [rfReplaceAll]), [rfReplaceAll, rfIgnoreCase]);
          x := RtcScriptEngine1.Execute(sender, RtcScriptEngine1.Compile(s));
          s := x.asString;
        finally
          x.free;
        end;
      except
        on e: exception do
        begin  // make a HTML page from the error message:
          s := '<html><body><b>' + e.ClassName + ':</b><br>'
            + StringReplace(e.message, #13#10, '<br>', [rfReplaceAll])
            + s
            + '</body></html>';
        end;
      end;
      //Write(s);
      try
        len := Length(s);
        if len > 0 then Response['Content-Type'] := 'text/html';
        Response['Cache-Control'] := 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0';
        Response['Pragma'] := 'no-cache';
        Write(s);
      finally
        UnLockSession;
      end;
    end;
  end;
end;

function TFile_Provider.RepairWebFileName(FileName, DocRoot: String): String;
var
  a: integer;
  FullName: String;
begin
  FileName := StringReplace(FileName, '/', '\', [rfreplaceall]);

  FullName := ExpandFileName(DocRoot + FileName);
  // Check if our path is inside DocRoot
  if (Pos('\..', FullName) > 0) or
    (UpperCase(Copy(FullName, 1, length(DocRoot))) <> UpperCase(DocRoot)) then
  begin
    Result := '';
    Exit;
  end;
  FileName := FullName;
  Delete(FileName, 1, length(DocRoot));
  // Check if FileName is a folder with missing '\' at the end.
  if Copy(FileName, length(FileName), 1) <> '\' then
    if DirectoryExists(DocRoot + FileName) then
    begin
      Result := '';
      Exit;
    end;
  // Check if FileName is a folder with existing index file
  if Copy(FileName, length(FileName), 1) = '\' then
  begin
    for a := 0 to PageList.Count - 1 do
    begin
      if File_Exists(FullName + PageList.Strings[a]) then
      begin
        FileName := FileName + PageList.Strings[a];
        Break;
      end;
    end;
  end;
  Result := StringReplace(FileName, '\', '/', [rfreplaceall]);
end;

procedure TFile_Provider.RtcFncCopyExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
begin
  Result.asString := Copy(Param.asString['str'], Param.asInteger['index'], Param.asInteger['count']);
end;

procedure TFile_Provider.RtcFncDateExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
begin
  Result.asDateTime := Date;
end;

procedure TFile_Provider.RtcFncFileInfoExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
var
  data: TRtcDataSet;
  i: Integer;
  FileName: string;
begin
  data := TRtcDataSet.Create;
  for i := 0 to Param.Count - 1 do
  begin
    data.Append;
    FileName := Param.asString['file' + IntToStr(i + 1)];
    FileInfo.FILENAME := GetDocRoot(TRtcDataServer(Sender).Request.Host) + '\' + FileName;
    if FileInfo.Execute then
    begin
      data.FieldByName('URL').asString := 'http://' + TRtcDataServer(Sender).Request.Host + '/' + FileName;
      data.FieldByName('FileName').asString := ExtractFileName(StringReplace(FileInfo.FILENAME, '/', '\', [rfReplaceAll]));
      data.FieldByName('Date').asString := FormatDateTime('yyyy-mm-dd', FileInfo.TimeLastModified);
      data.FieldByName('Time').asString := FormatDateTime('hh:nn:ss', FileInfo.TimeLastModified);
      data.FieldByName('FileSize').asOID := FileInfo.FileSize;
      data.FieldByName('FileType').asString := FileInfo.FileType;
      data.FieldByName('TimeCreated').asDateTime := FileInfo.TimeCreated;
      data.FieldByName('TimeLastModified').asDateTime := FileInfo.TimeLastModified;
      data.FieldByName('TimeLastAccessed').asDateTime := FileInfo.TimeLastAccessed;
    end
    else
    begin
      data.FieldByName('URL').asString := '';
      data.FieldByName('FileName').asString := FileName;
      data.FieldByName('Date').asString := '';
      data.FieldByName('Time').asString := '';
      data.FieldByName('FileSize').asOID := 0;
      data.FieldByName('FileType').asString := '';
      data.FieldByName('TimeCreated').asDateTime := 0;
      data.FieldByName('TimeLastModified').asDateTime := 0;
      data.FieldByName('TimeLastAccessed').asDateTime := 0;
    end;
  end;
  Result.asDataSet := data;
end;

procedure TFile_Provider.RtcFncFormatDateTimeExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
begin
  Result.asString := FormatDateTime(Param.asString['format'], Param.asDateTime['datetime']);
end;

procedure TFile_Provider.RtcFncNowExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
begin
  Result.asDateTime := Now;
end;

procedure TFile_Provider.RtcFncQueryExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
var
  sql: string;
  i: Integer;
  data: TRtcDataSet;
begin
  sql := Param.asString['sql'];
  DM.qry2.Active := False;
  DM.qry2.SQL.Text := sql;
  DM.qry2.Active := True;
  data := TRtcDataSet.Create;
  try
    while not DM.qry2.Eof do
    begin
      data.Append;
      for i := 0 to DM.qry2.FieldCount - 1 do
      begin
        data.FieldByName(DM.qry2.Fields[i].FieldName).asValue := DM.qry2.Fields[i].Value;
      end;
      DM.qry2.Next;
    end;
  finally
    DM.qry2.Active := False;
  end;
  Result.asDataSet := data;
end;

procedure TFile_Provider.RtcFncExecSQLExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
var
  sql: string;
  i: Integer;
  data: TRtcDataSet;
begin
  sql := Param.asString['sql'];
  data := TRtcDataSet.Create;
  try
    DM.SQL.SQL.Clear;
    DM.SQL.SQL.Text := sql;
    DM.SQL.Execute;
  finally

  end;
end;

procedure TFile_Provider.RtcFncReplaceExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
begin
  Result.asString := StringReplace(Param.asString['str'], Param.asString['old'], Param.asString['new'], [rfReplaceAll]);
end;

procedure TFile_Provider.RtcFncTimeExecute(Sender: TRtcConnection; Param: TRtcFunctionInfo; Result: TRtcValue);
begin
  Result.asDateTime := Time;
end;

procedure TFile_Provider.TimeProviderCheckRequest(Sender: TRtcConnection);
begin
  with TRtcDataServer(Sender) do
    if UpperCase(Request.FileName) = '/$TIME' then
      Accept;
end;

procedure TFile_Provider.TimeProviderDataReceived(Sender: TRtcConnection);
begin
  with TRtcDataServer(Sender) do
    if Request.Complete then
      Write('<html><body>Your IP: ' + PeerAddr + '<br>' +
        'Server Time: ' + RtcString(TimeToStr(Now)) + '<br>' +
        'Connection count: ' + Int2Str(TotalConnectionCount) + '</body></html>');
end;

procedure TFile_Provider.UploadProviderCheckRequest(Sender: TRtcConnection);
begin
  //Log('<Request.Method>' + TRtcDataServer(Sender).Request.Method);
  //Log('<Request.ObjFile>' + TRtcDataServer(Sender).Request.Query['ObjFile']);
  with TRtcDataServer(Sender) do
    if (Request.Method = 'PUT') and (Request.Query['ObjFile'] <> '') then
    begin
      if UpperCase(string(Request.FileName)) = '/UPLOAD' then
      begin // 按原文件名保存在Layout目录下
        Request.Info['ObjFile'] := ExtractFilepath(ParamStr(0)) + 'Layout\'
          + string(URL_Decode(Request.Query['ObjFile']));
        //Log('<UPLOAD>' + ExtractFilepath(ParamStr(0)) + 'Layout\' + string(URL_Decode(Request.Query['ObjFile'])));
        Accept;
      end
      else if UpperCase(string(Request.FileName)) = '/DOWN' then
      begin
        Request.Info['ObjFile'] := string(URL_Decode(Request.Query['ObjFile']));
        //Log('<DOWN>' + string(URL_Decode(Request.Query['ObjFile'])));
        Accept;
      end;
    end;
end;

procedure TFile_Provider.UploadProviderDataReceived(Sender: TRtcConnection);
var
  s: AnsiString;
begin
  with TRtcDataServer(Sender) do
  begin
    if UpperCase(string(Request.FileName)) = '/UPLOAD' then
    begin
      if Request.Started then
      begin
        if not DirectoryExists(ExtractFilepath(ParamStr(0)) + 'Layout') then
          CreateDir(ExtractFilepath(ParamStr(0)) + 'Layout');
        Delete_File(Request.Info['ObjFile']);
      end;
      s := Read; // 注意，Read取回的是AnsiString的，所以变量也要定义成AnsiString
      Write_File(string(Request.Info.asString['ObjFile']), s, Request.ContentIn - length(s));
      //Log('<DataReceived>' + string(Request.Info.asString['ObjFile']));
      if Request.Complete then
      begin
        if ExtractFileExt(string(Request.Info.asString['ObjFile'])) = '.zip' then
        begin
          with TVCLUnZip.Create(nil) do
          begin
            try
              DestDir := LayoutPath + Copy(ExtractFileName(string(Request.Info.asString['ObjFile'])), 1, Length(ExtractFileName(string(Request.Info.asString['ObjFile']))) - 4) + '\';
              DoAll := True;
              DoProcessMessages := False;
              KeepZipOpen := False;
              OverwriteMode := Always;
              ZipName := string(Request.Info.asString['ObjFile']);
              RecreateDirs := True;
              RootDir := DestDir;//LayoutPath +  ExtractFileName(string(Request.Info.asString['ObjFile'])) + '\';
              //Log('<DataReceived>' + RootDir);
              UnZip;
            finally
              Free;
            end;
          end;
        end;
      end;
    end
    else if UpperCase(string(Request.FileName)) = '/DOWN' then
    begin
      s := Request.Info.asString['ObjFile'];
      if File_Exists(String(s)) then
        Write(Read_File(String(s)));
    end;
    if Request.Complete then
      Write;
  end;
end;

end.
