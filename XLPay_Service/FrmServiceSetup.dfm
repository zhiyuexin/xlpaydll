object fServiceSetup: TfServiceSetup
  Left = 447
  Top = 395
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'XLPay Service'
  ClientHeight = 250
  ClientWidth = 269
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSetting: TPanel
    Left = 0
    Top = 0
    Width = 269
    Height = 250
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object LMDSimpleLabel1: TLMDSimpleLabel
      Left = 76
      Top = 8
      Width = 122
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Caption = 'XLPay Service'
      Options = []
    end
    object btnInstallService: TLMDButton
      Left = 16
      Top = 40
      Width = 110
      Height = 25
      Caption = #23433#35013#26381#21153
      TabOrder = 0
      OnClick = btnInstallServiceClick
    end
    object btnUnInstallService: TLMDButton
      Left = 144
      Top = 40
      Width = 110
      Height = 25
      Caption = #21368#36733#26381#21153
      TabOrder = 1
      OnClick = btnUnInstallServiceClick
    end
    object btnStartService: TLMDButton
      Left = 16
      Top = 80
      Width = 110
      Height = 25
      Caption = #21551#21160#26381#21153
      Default = True
      TabOrder = 2
      OnClick = btnStartServiceClick
    end
    object btnStopService: TLMDButton
      Left = 144
      Top = 80
      Width = 110
      Height = 25
      Caption = #20572#27490#26381#21153
      TabOrder = 3
      OnClick = btnStopServiceClick
    end
    object btnDBConnConfig: TLMDButton
      Left = 144
      Top = 120
      Width = 110
      Height = 25
      Caption = #25968#25454#24211#36830#25509#35774#32622
      TabOrder = 4
      OnClick = btnDBConnConfigClick
    end
    object btnListenPortConfig: TLMDButton
      Left = 16
      Top = 120
      Width = 110
      Height = 25
      Caption = #26381#21153#30417#21548#31471#21475#35774#32622
      TabOrder = 5
      OnClick = btnListenPortConfigClick
    end
    object btnQuit: TLMDButton
      Left = 144
      Top = 160
      Width = 110
      Height = 25
      Cancel = True
      Caption = #36864#20986
      TabOrder = 6
      OnClick = btnQuitClick
    end
    object btnSrvIntervalConfig: TLMDButton
      Left = 18
      Top = 159
      Width = 110
      Height = 25
      Caption = #36718#35810#26102#38388#38388#38548#35774#32622
      TabOrder = 7
      OnClick = btnSrvIntervalConfigClick
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 196
      Width = 269
      Height = 54
      Align = alBottom
      Caption = #26085#24535
      TabOrder = 8
      object btnShowLog: TLMDButton
        Left = 144
        Top = 16
        Width = 110
        Height = 25
        Caption = #26597#30475#26085#24535
        TabOrder = 0
        OnClick = btnShowLogClick
      end
      object edtLogDate: TDateTimePicker
        Left = 18
        Top = 18
        Width = 110
        Height = 21
        Date = 42047.738573379640000000
        Time = 42047.738573379640000000
        TabOrder = 1
      end
    end
  end
  object Conn: TUniConnection
    ProviderName = 'Oracle'
    SpecificOptions.Strings = (
      'Oracle.Direct=True')
    Username = 'dm'
    Password = 'dm'
    Server = '10.2.26.21:1521:dm'
    ConnectDialog = ConnectDialog
    LoginPrompt = False
  end
  object ConnectDialog: TUniConnectDialog
    DatabaseLabel = #25968#25454#24211
    PortLabel = #31471#21475#21495
    ProviderLabel = #25968#25454#24211#31867#22411
    SavePassword = True
    Caption = #25968#25454#24211#36830#25509#35774#32622
    UsernameLabel = #30331#24405#21517#31216
    PasswordLabel = #30331#24405#23494#30721
    ServerLabel = #26381#21153#22120#21517#31216
    ConnectButton = #30830#23450
    CancelButton = #21462#28040
    LabelSet = lsCustom
    Left = 240
  end
end
