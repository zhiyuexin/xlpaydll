object XLPay: TXLPay
  OldCreateOrder = False
  OnCreate = ServiceCreate
  DisplayName = 'XLPayService'
  Interactive = True
  AfterInstall = ServiceAfterInstall
  OnContinue = ServiceContinue
  OnPause = ServicePause
  OnStart = ServiceStart
  OnStop = ServiceStop
  Left = 425
  Top = 210
  Height = 294
  Width = 290
  object cdsPlaySourceDetail: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64896
    Top = 65184
    object cdsPlaySourceDetailID: TIntegerField
      FieldName = 'ID'
    end
    object cdsPlaySourceDetailPSID: TIntegerField
      FieldName = 'PSID'
    end
    object cdsPlaySourceDetailPropertyID: TIntegerField
      FieldName = 'PropertyID'
    end
    object cdsPlaySourceDetailPropertyName: TStringField
      FieldName = 'PropertyName'
    end
    object cdsPlaySourceDetailPropertyValue: TStringField
      FieldName = 'PropertyValue'
      Size = 1024
    end
    object cdsPlaySourceDetailSortID: TIntegerField
      FieldName = 'SortID'
    end
    object cdsPlaySourceDetailRMK: TStringField
      FieldName = 'RMK'
    end
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64896
    Top = 65184
    object ClientDataSet1ID: TIntegerField
      FieldName = 'ID'
    end
    object ClientDataSet1PSID: TIntegerField
      FieldName = 'PSID'
    end
    object ClientDataSet1PropertyID: TIntegerField
      FieldName = 'PropertyID'
    end
    object ClientDataSet1PropertyName: TStringField
      FieldName = 'PropertyName'
    end
    object ClientDataSet1PropertyValue: TStringField
      FieldName = 'PropertyValue'
      Size = 1024
    end
    object ClientDataSet1SortID: TIntegerField
      FieldName = 'SortID'
    end
    object ClientDataSet1RMK: TStringField
      FieldName = 'RMK'
    end
  end
  object ClientDataSet2: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64896
    Top = 65184
    object ClientDataSet2ID: TIntegerField
      FieldName = 'ID'
    end
    object ClientDataSet2PSID: TIntegerField
      FieldName = 'PSID'
    end
    object ClientDataSet2PropertyID: TIntegerField
      FieldName = 'PropertyID'
    end
    object ClientDataSet2PropertyName: TStringField
      FieldName = 'PropertyName'
    end
    object ClientDataSet2PropertyValue: TStringField
      FieldName = 'PropertyValue'
      Size = 1024
    end
    object ClientDataSet2SortID: TIntegerField
      FieldName = 'SortID'
    end
    object ClientDataSet2RMK: TStringField
      FieldName = 'RMK'
    end
  end
  object ServerHTTPS: TRtcHttpServer
    MultiThreaded = True
    Timeout.AfterConnecting = 300
    ServerPort = '443'
    RestartOn.ListenLost = True
    FixupRequest.RemovePrefix = True
    MaxRequestSize = 128000
    MaxHeaderSize = 16000
    Left = 104
    Top = 184
  end
  object ServerHTTP: TRtcHttpServer
    MultiThreaded = True
    Timeout.AfterConnecting = 300
    ServerPort = '80'
    OnException = ServerHTTPException
    RestartOn.ListenLost = True
    OnListenStart = ServerHTTPListenStart
    OnListenStop = ServerHTTPListenStop
    OnListenLost = ServerHTTPListenLost
    OnListenError = ServerHTTPListenError
    FixupRequest.RemovePrefix = True
    OnRequestNotAccepted = ServerHTTPRequestNotAccepted
    MaxRequestSize = 128000
    MaxHeaderSize = 16000
    OnInvalidRequest = ServerHTTPInvalidRequest
    Left = 33
    Top = 182
  end
  object TcpServer: TRtcTcpServer
    MultiThreaded = True
    OnConnect = TcpServerConnect
    OnDataReceived = TcpServerDataReceived
    Left = 31
    Top = 128
  end
  object Client: TRtcTcpClient
    MultiThreaded = True
    Timeout.AfterConnecting = 1800
    Left = 104
    Top = 128
  end
end
