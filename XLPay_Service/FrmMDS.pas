unit FrmMDS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uCommon, uDM, MidasLib,
  {DB}
  DB, Uni, DBAccess, UniDacVcl,
  UniProvider, SQLiteUniProvider, AccessUniProvider,
  SQLServerUniProvider, OracleUniProvider, MySQLUniProvider,
  InterBaseUniProvider, DB2UniProvider, ASEUniProvider,
  {Control}
  LMDCustomButton, LMDButton,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseLabel,
  LMDCustomSimpleLabel, LMDSimpleLabel;

type
  TfMDS = class(TForm)
    Conn: TUniConnection;
    LMDSimpleLabel1: TLMDSimpleLabel;
    ConnectDialog: TUniConnectDialog;
    btnInstallService: TLMDButton;
    btnUnInstallService: TLMDButton;
    btnStartService: TLMDButton;
    btnStopService: TLMDButton;
    btnDBConnConfig: TLMDButton;
    btnListenPortConfig: TLMDButton;
    btnQuit: TLMDButton;
    procedure FormCreate(Sender: TObject);
    procedure btnDBConnConfigClick(Sender: TObject);
    procedure btnInstallServiceClick(Sender: TObject);
    procedure btnListenPortConfigClick(Sender: TObject);
    procedure btnQuitClick(Sender: TObject);
    procedure btnStartServiceClick(Sender: TObject);
    procedure btnStopServiceClick(Sender: TObject);
    procedure btnUnInstallServiceClick(Sender: TObject);
  private
    procedure SetBtnStatus;
  public
    { Public declarations }
  end;

var
  fMDS: TfMDS;

implementation

{$R *.dfm}

procedure TfMDS.FormCreate(Sender: TObject);
begin
  SetBtnStatus;
end;

procedure TfMDS.btnDBConnConfigClick(Sender: TObject);
begin
  try
    Conn.Disconnect;
    uCommon.GetConnectionConfig(Conn);
    if ConnectDialog.Execute then
    begin
      uCommon.SetConnectionConfig(Conn);
    end;
  finally
    Conn.Connected := False;
  end;
end;

procedure TfMDS.btnInstallServiceClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    If uCommon.InstallService then
    begin
      InitServiceRegInfo();
      ShowMessage('安装服务成功！');
    end
    else
      ShowMessage('安装服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfMDS.btnListenPortConfigClick(Sender: TObject);
var
  s: String;
begin
  s := IntToStr(GetDataPort);
  if InputQuery('设置数据服务监听端口', '数据服务监听端口:', s) then
  begin
    try
      SetDataPort(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;

  s := IntToStr(GetCommPort);
  if InputQuery('设置通讯服务监听端口', '通讯服务监听端口:', s) then
  begin
    try
      SetCommPort(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;

  s := IntToStr(GetHttpPort);
  if InputQuery('设置HTTP服务监听端口', 'HTTP服务监听端口:', s) then
  begin
    try
      SetHttpPort(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;

  SetServiceDescription;
end;

procedure TfMDS.btnQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TfMDS.btnStartServiceClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    If uCommon.RunService then
      ShowMessage('启动服务成功！')
    else
      ShowMessage('启动服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfMDS.btnStopServiceClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    If uCommon.StopService then
      ShowMessage('停止服务成功！')
    else
      ShowMessage('停止服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfMDS.btnUnInstallServiceClick(Sender: TObject);
begin
  if not IsServiceStopped then
  begin
    ShowMessage('请先停止服务！');
    exit;
  end;
  try
    Screen.Cursor := crHourGlass;
    If uCommon.UnInstallService then
      ShowMessage('卸载服务成功！')
    else
      ShowMessage('卸载服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfMDS.SetBtnStatus;
var
  Enabled: Boolean;
begin
  Enabled := IsServiceInstalled;
  btnInstallService.Enabled := not Enabled;
  btnUnInstallService.Enabled := Enabled;
  btnDBConnConfig.Enabled := Enabled;
  btnListenPortConfig.Enabled := Enabled;
  //btnCorpSetting.Enabled := Enabled;
  //btnServerConfig.Enabled := Enabled;
  //btnUploadSetting.Enabled := Enabled;
  btnStartService.Enabled := Enabled and not IsServiceRunning;
  btnStopService.Enabled := Enabled and not btnStartService.Enabled;
end;

end.
