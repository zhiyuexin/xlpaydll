unit SerXLPay;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs;

type
  TserviceXLPay = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
  private
    function StartService(): Boolean;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  serviceXLPay: TserviceXLPay;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  serviceXLPay.Controller(CtrlCode);
end;

function TserviceXLPay.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TserviceXLPay.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Started := StartService;
end;

function TserviceXLPay.StartService: Boolean;
begin
  Result := True;
end;

end.
