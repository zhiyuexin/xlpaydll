unit TCPClient;

interface

uses
  SysUtils, Classes, Contnrs, uCommon, uDM,

  rtcSyncObjs,
  rtcInfo, rtcConn,
  rtcThrPool,
  rtcConnProv,

  DBAccess, Uni,

  rtcDataSrv, rtcHttpSrv, rtcTcpSrv, rtcTcpCli;

type
  { TClientData }
  TClientData = class(TObject)
  private
    FIsOnline: Boolean;
    procedure SetIsOnline(const Value: Boolean);
  public
    SessionID: string;
    ClientIP, ClientNo, ComputerName, ClientType, RMK: string;
    ClientPort, LayoutID, WebPort: Integer;
    RegTime: TDateTime;
    Updating: Boolean;
    UpdateTime: TDateTime;
    Modified: Boolean;
    property IsOnline: Boolean read FIsOnline write SetIsOnline;


    constructor Create(); overload;
    constructor Create(pClientNo, pClientIP, pClientType, pComputerName: string; pClientPort, pLayoutID: Integer); overload;
    procedure Reg(pClientIP, pClientNo, pClientType, pComputerName: string; pClientPort, pLayoutID: Integer; pWebPort: Integer = 9080);
    procedure UnReg();
    procedure Save();
  end;

  { TTCPClient }
  TTCPClient = class(TPersistent)
  private
    FClient: TObjectList;
    FClientData: TObjectList;
    FMaxUpdateClientCount: Integer;
    //FSessionID: string;

    function GetClient(Index: Integer): TRtcTcpClient;
    procedure SetClient(Index: Integer; const Value: TRtcTcpClient);
    function GetClientFromIP(IP: string): TRtcTcpClient;
    function GetClientData(Index: Integer): TClientData;
    function GetClientDataFromIP(IP: string): TClientData;
    procedure ClientDisconnect(Sender: TRtcConnection);
    procedure ClientConnect(Sender: TRtcConnection);
    procedure ClientConnectError(Sender: TRtcConnection; E: Exception);
  public
    property Client[Index: Integer]: TRtcTcpClient read GetClient write SetClient;
    property ClientFromIP[IP: string]: TRtcTcpClient read GetClientFromIP;
    //property SessionID: string read FSessionID;
    property ClientData[Index: Integer]: TClientData read GetClientData;
    property ClientDataFromIP[IP: string]: TClientData read GetClientDataFromIP;
    property MaxUpdateClientCount: Integer read FMaxUpdateClientCount write FMaxUpdateClientCount;

    function Add(): Integer; overload;
    function Add(const CastIP, Port: string; AutoConnect: Boolean = True): TRtcTcpClient; overload;
    function Add(const CastIPList: TStringList): Integer; overload;
    function Add(Client: TRtcTcpClient): Integer; overload;
    procedure Add(const ClientIP, ClientNo, ClientType, ComputerName: string; const ClientPort, LayoutID, WebPort: Integer); overload;
    procedure GetClientsFormDB();

    function Count: Integer;
    constructor Create(); overload;
    constructor Create(UpdateCount: Integer); overload;
    destructor Destroy(); overload;
    procedure BroadCast(const Msg: string); overload;
    procedure BroadCast(const Msg: string; const Client: TRtcTcpClient); overload;
    procedure BroadCast(const Msg: string; const index: Integer); overload;
    procedure BroadCast(const Msg, CastIP, Port: string); overload;
    procedure BroadCast(const Msg: string; const CastIPList: TStringList); overload;
    procedure OnLine(const IP: string);
    procedure Save();
    procedure Reg(ClientIP, ClientNo, ClientType, ComputerName: string; ClientPort, LayoutID: Integer; WebPort: Integer = 9080);

    function CanUpdate(IP: string): Boolean;
  end;

implementation

uses DateUtils;
{ TTCPClient }
function TTCPClient.Add: Integer;
begin
  Result := Add(TRtcTcpClient.Create(nil));
end;

function TTCPClient.Add(const CastIPList: TStringList): Integer;
var
  i: Integer;
begin
  Result := 0;
  if CastIPList = nil then Exit;
  for i := 0 to CastIPList.Count - 1 do
  begin
    //log(CastIPList.Names[i] + ':' + CastIPList.ValueFromIndex[i]);
    if (CastIPList.Names[i] <> '') and (CastIPList.ValueFromIndex[i] <> '') then
      Add(CastIPList.Names[i], CastIPList.ValueFromIndex[i]);
    Result := Result + 1;
  end;
end;

function TTCPClient.Add(const CastIP, Port: string; AutoConnect: Boolean = True): TRtcTcpClient;
var
  Client: TRtcTcpClient;
begin
  Client := ClientFromIP[CastIP];
  if Client = nil then
  begin
    Client := TRtcTcpClient.Create(nil);
    Client.ServerAddr := CastIP;
    Client.ServerPort := Port;
    Add(Client);
  end
  else
  begin
    Client.ServerAddr := CastIP;
    Client.ServerPort := Port;
  end;
  Client.OnConnect := ClientConnect;
  Client.OnDisconnect := ClientDisconnect;
  Client.OnConnectFail := ClientDisconnect;
  Client.OnConnectError := ClientConnectError;
  if AutoConnect then Client.Connect();
  Result := Client;
end;

function TTCPClient.Add(Client: TRtcTcpClient): Integer;
var
  c: TClientData;
  g: TGUID;
begin
  Result := FClient.Add(Client);
  c := TClientData.Create;
  c.ClientIP := Client.ServerAddr;
  CreateGUID(g);
  c.SessionID := GUIDToString(g);
  c.Updating := False;
  FClientData.Add(c);
end;

procedure TTCPClient.Add(const ClientIP, ClientNo, ClientType, ComputerName: string; const ClientPort, LayoutID, WebPort: Integer);
var
  c: TRtcTcpClient;
  cd: TClientData;
begin
  cd := ClientDataFromIP[ClientIP];
  if cd = nil then
  begin
    cd := TClientData.Create(ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID);
    FClientData.Add(cd);
  end
  else
  begin
    cd.Reg(ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID, WebPort);
  end;

  c := ClientFromIP[ClientIP];
  if c = nil then
  begin
    c := TRtcTcpClient.Create(nil);
    c.ServerAddr := ClientIP;
    c.ServerPort := IntToStr(ClientPort);
    c.OnConnect := ClientConnect;
    c.OnDisconnect := ClientDisconnect;
    c.OnConnectFail := ClientDisconnect;
    c.OnConnectError := ClientConnectError;
    FClient.Add(c);
  end
  else
  begin
    c.ServerAddr := ClientIP;
    c.ServerPort := IntToStr(ClientPort);
  end;
  if not c.isConnected then
  begin
    //Log('Connect to client :' + ClientIP);
    c.Connect();
  end;
end;

procedure TTCPClient.BroadCast(const Msg, CastIP, Port: string);
var
  Client: TRtcTcpClient;
begin
  Client := Add(CastIP, Port);
  if Client = nil then Exit;
  Client.ServerPort := Port;
  BroadCast(Msg, Client);
end;

procedure TTCPClient.BroadCast(const Msg: string; const Client: TRtcTcpClient);
begin
  if Client = nil then Exit;
  try
    Client.Connect();
    Client.Write(Msg);
  except
  end;
end;

procedure TTCPClient.BroadCast(const Msg: string; const index: Integer);
begin
  try
    if Client[index] = nil then Exit;
    Client[index].Connect();
    Client[index].Write(Msg);
  except
  end;
end;

procedure TTCPClient.BroadCast(const Msg: string);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    BroadCast(Msg, i);
  end;
end;

procedure TTCPClient.BroadCast(const Msg: string; const CastIPList: TStringList);
var
  i: Integer;
begin
  if CastIPList = nil then Exit;
  for i := 0 to CastIPList.Count - 1 do
  begin
    BroadCast(Msg, CastIPList.Names[i], CastIPList.ValueFromIndex[i]);
  end;
end;

constructor TTCPClient.Create;
begin
  inherited;
  FClient := TObjectList.Create;
  FClientData := TObjectList.Create;
  FMaxUpdateClientCount := 10;
end;

constructor TTCPClient.Create(UpdateCount: Integer);
begin
  Create;
  FMaxUpdateClientCount := UpdateCount;
end;

function TTCPClient.Count: Integer;
begin
  Result := FClient.Count;
end;

destructor TTCPClient.Destroy;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    Client[i].Disconnect;
    Client[i].Free;
    TRtcTcpClient(FClient[i]).Free;
  end;
  FClient.Free;
  inherited Destroy;
end;

procedure TTCPClient.ClientDisconnect(Sender: TRtcConnection);
begin
  Log('Client ' + Sender.ServerAddr + ' Disconnect');
  with ClientDataFromIP[Sender.ServerAddr] do
  begin
    IsOnline := False;
    Updating := False;
  end;
end;
procedure TTCPClient.ClientConnect(Sender: TRtcConnection);
begin
  Log('Client ' + Sender.ServerAddr + ' Connect');
  with ClientDataFromIP[Sender.ServerAddr] do
  begin
    IsOnline := True;
    Updating := False;
  end;
end;

procedure TTCPClient.ClientConnectError(Sender: TRtcConnection; E: Exception);
begin
  //Log('Client ' + Sender.ServerAddr + ' ConnectError ' + E.Message);
  with ClientDataFromIP[Sender.ServerAddr] do
  begin
    IsOnline := False;
    Updating := False;
  end;
end;

function TTCPClient.GetClient(Index: Integer): TRtcTcpClient;
begin
  if (Index >= 0) and (Index < FClient.Count) then
    Result := TRtcTcpClient(FClient[Index]);
end;

function TTCPClient.GetClientFromIP(IP: string): TRtcTcpClient;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
  begin
    if Client[i].ServerAddr = IP then
    begin
      Result := Client[i];
      Break;
    end;
  end;
end;

procedure TTCPClient.SetClient(Index: Integer; const Value: TRtcTcpClient);
begin
  FClient[Index] := Value;
end;

function TTCPClient.GetClientData(Index: Integer): TClientData;
begin
  Result := nil;
  if (Index >= 0) and (Index < FClientData.Count) then
    Result := TClientData(FClientData[Index]);
end;

function TTCPClient.GetClientDataFromIP(IP: string): TClientData;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
  begin
    if ClientData[i] = nil then Continue;
    if ClientData[i].ClientIP = IP then
    begin
      Result := ClientData[i];
      Break;
    end;
  end;
end;

function TTCPClient.CanUpdate(IP: string): Boolean;
var
  i, c: Integer;
  d: TClientData;
begin
  d := ClientDataFromIP[IP];
  if not d.IsOnline then OnLine(IP);
  if d.Updating then
  begin
    Result := True;
    Exit;
  end;
  Result := False;
  c := 0;
  for i := 0 to Count - 1 do
  begin
    if ClientData[i].Updating then
    begin
      if MinutesBetween(Now, ClientData[i].UpdateTime) < 20 then
        c := c + 1;
    end;
  end;
  if c < FMaxUpdateClientCount then
  begin
    d.Updating := True;
    d.UpdateTime := Now;
    Result := True;
  end;
end;

procedure TTCPClient.OnLine(const IP: string);
begin
  if ClientFromIP[IP] <> nil then ClientFromIP[IP].Connect();
end;

procedure TTCPClient.Save;
var
  i: Integer;
  OnLine, OffLine: string;
begin
  for i := 0 to Count - 1 do
    ClientData[i].Save;
  Exit;
  OnLine := '(';
  OffLine := '(';
  for i := 0 to count - 1 do
  begin
    if ClientData[i].IsOnline then
      OnLine := OnLine + QuotedStr(ClientData[i].ClientIP) + ','
    else
      OffLine := OffLine + QuotedStr(ClientData[i].ClientIP) + ',';
  end;
  OnLine := OnLine + ''''')';
  OffLine := OffLine + ''''')';
  if OnLine <> '('''')' then
  begin
    DM.SQL.SQL.Text := 'Update tClientList Set [OnLine]=1 Where ClientIP in ' + OnLine;
    DM.SQL.Execute;
  end;
  if OffLine <> '('''')' then
  begin
    DM.SQL.SQL.Text := 'Update tClientList Set [OnLine]=0 Where ClientIP in ' + OffLine;
    DM.SQL.Execute;
  end;
end;

procedure TTCPClient.Reg(ClientIP, ClientNo, ClientType, ComputerName: string; ClientPort, LayoutID: Integer; WebPort: Integer = 9080);
begin
  //DM.RegClient(ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID);
  //Add(ClientIP, IntToStr(ClientPort));
  //Log('REG->' + ClientIP);
  Add(ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID, WebPort);
end;

procedure TTCPClient.GetClientsFormDB;
var
  qry: TUniQuery;
begin
  qry := TUniQuery.Create(nil);
  qry.Connection := DM.Conn;
  with qry do   //DM.qryReg
  begin
    try
      SQL.Text := 'Select distinct ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID, IsNull(WebPort,8090) WebPort From tClientList Where (ClientIP<>'''' and ClientIP is not null)';
      Active := True;
      while not Eof do
      begin
        Add(FieldByName('ClientIP').AsString,
          FieldByName('ClientNo').AsString,
          FieldByName('ClientType').AsString,
          FieldByName('ComputerName').AsString,
          FieldByName('ClientPort').AsInteger,
          FieldByName('LayoutID').AsInteger,
          FieldByName('WebPort').AsInteger);
        Next;
      end;
      Active := False;
    finally
      Free;
    end;
  end;
end;

{ TClientData }

constructor TClientData.Create();
var
  g: TGUID;
begin
  CreateGUID(g);
  SessionID := GUIDToString(g);
  Modified := False;
end;
constructor TClientData.Create(pClientNo, pClientIP, pClientType, pComputerName: string; pClientPort, pLayoutID: Integer);
begin
  Create();
  Reg(pClientNo, pClientIP, pClientType, pComputerName, pClientPort, pLayoutID);
  Modified := False;
end;

procedure TClientData.Reg(pClientIP, pClientNo, pClientType, pComputerName: string; pClientPort, pLayoutID, pWebPort: Integer);
begin
  Modified := (ClientIP <> pClientIP)
    or (ClientNo <> pClientNo)
    or (ClientType <> pClientType)
    or (ComputerName <> pComputerName)
    or (ClientPort <> pClientPort)
    or (LayoutID <> pLayoutID)
    or (WebPort <> pWebPort);
  ClientIP := pClientIP;
  ClientNo := pClientNo;
  ClientType := pClientType;
  ComputerName := pComputerName;
  ClientPort := pClientPort;
  LayoutID := pLayoutID;
  WebPort := pWebPort;
  RegTime := Now();
  Updating := False;
  FIsOnline := True;
  //DM.RegClient(ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID);
end;

procedure TClientData.Save;
begin
  if not Modified then Exit;
  //Log('Save:' + ClientIP);
  if IsOnline then
    DM.RegClient(ClientIP, ClientNo, ClientType, ComputerName, ClientPort, LayoutID, WebPort)
  else
    DM.UnRegClient(ClientIP);
  Modified := False;
end;

procedure TClientData.SetIsOnline(const Value: Boolean);
begin
  FIsOnline := Value;
  //if FIsOnline then  Log('online')
  //else Log('offline');
  Modified := True;
end;

procedure TClientData.UnReg;
begin
  RegTime := Now();
  Updating := False;
  IsOnline := False;
  //DM.UnRegClient(ClientIP);
end;

end.
