unit fDispalyPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, ActnList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, LMDCustomButton, LMDButton, LMDControl, LMDCustomControl,
  LMDCustomPanel, LMDCustomBevelPanel, LMDSimplePanel;

type
  TfrmDispalyPlan = class(TForm)
    pnlTop: TLMDSimplePanel;
    LMDButton3: TLMDButton;
    LMDButton2: TLMDButton;
    LMDButton1: TLMDButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    btnTest: TLMDButton;
    ActionList: TActionList;
    actAdd: TAction;
    actEdit: TAction;
    actDelete: TAction;
    LMDButton4: TLMDButton;
    actActive: TAction;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDispalyPlan: TfrmDispalyPlan;

implementation

{$R *.dfm}

end.
