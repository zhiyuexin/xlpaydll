program XLPay_Service;

uses
  SvcMgr,
  Forms,
  AppService in 'AppService.pas',
  uCommon in 'uCommon.pas',
  SrvXLD in 'SrvXLD.pas' {XLPay: TService},
  FrmServiceSetup in 'FrmServiceSetup.pas' {fServiceSetup},
  SerXLPay in 'SerXLPay.pas' {serviceXLPay: TService},
  uDMFileProvider in 'uDMFileProvider.pas' {File_Provider: TDataModule};

{$R *.RES}

begin
  if not IsDesktopMode(SERVICE_NAME) then
  begin
    SvcMgr.Application.Initialize;
    SvcMgr.Application.Title := 'XLPayService';
    Application.CreateForm(TserviceXLPay, serviceXLPay);
  Application.CreateForm(TFile_Provider, File_Provider);
  SvcMgr.Application.Run;
  end
  else
  begin
    Forms.Application.Initialize;
    Forms.Application.Title := SERVICE_NAME;
    Forms.Application.CreateForm(TfServiceSetup, fServiceSetup);
    Forms.Application.Run;
  end;
(*
  if not IsDesktopMode(SERVICE_NAME) then
  begin
    SvcMgr.Application.Initialize;
    SvcMgr.Application.Title := 'XLDService';
    SvcMgr.Application.CreateForm(TDM, DM);
    SvcMgr.Application.CreateForm(TMDS, MDS);
    SvcMgr.Application.Run;
  end
  else
  begin
    Forms.Application.Initialize;
    Forms.Application.Title := SERVICE_NAME;
    Forms.Application.CreateForm(TfServiceSetup, fServiceSetup);
    Forms.Application.Run;
  end;
*)
end.
