        ��  ��                  �  0   ��
 R O D L F I L E                     <Library Name="LibMDS" UID="{71867887-6814-4AA6-B9CE-84046719CF4A}" Documentation="">
   <Services>
      <Service Name="SerMDS" UID="{B6CF46E5-BF06-445B-A713-19912EFBCF06}">
        <Interfaces>
           <Interface Name="Default" UID="{8DCC57CD-C58C-4F35-AFE6-9BFB60189AB8}" Documentation="Service SerMDS. This service has been automatically generated using the RODL template you can find in the Templates directory.">
      <Operations>
         <Operation Name="Sum" UID="{EBD1F2E6-F88B-41F5-A9B4-4E37E74F04B9}" Documentation="">
      <Parameters>
         <Parameter Name="A" DataType="Integer" Flag="In" />
         <Parameter Name="B" DataType="Integer" Flag="In" />
         <Parameter Name="Result" DataType="Integer" Flag="Result" />
      </Parameters>
         </Operation>
         <Operation Name="GetServerTime" UID="{0C74AFA6-9E68-4118-A1DE-743C91E1CC9B}" Documentation="">
      <Parameters>
         <Parameter Name="Result" DataType="DateTime" Flag="Result" />
      </Parameters>
         </Operation>
      </Operations>

           </Interface> 
        </Interfaces>
      </Service>
   </Services>
</Library>
