unit uTradeDefine;

interface

uses Classes, sysUtils, uCommon;

type
  {TPosSale}
  T_PosSale = Class(TComponent)
  private
    FRecId: Integer;
    FPosNo: String;
    FBookDate: TDateTime;
    FBillId: Integer;
    FBillTime: TDateTime;
    FAuthTradeCode: String;
    function GetSQL: String;
    function GetSQLWhere: String;
  public
    Next: T_PosSale;
    property PosNo: String read FPosNo write FPosNo;
    property BillId: Integer read FBillId write FBillId;
    property BillTime: TDateTime read FBillTime write FBillTime;
    property BookDate: TDateTime read FBookDate write FBookDate;
    property RecId: Integer read FRecId write FRecId;
    property AuthTradeCode: String read FAuthTradeCode write FAuthTradeCode;
    property SQL: String read GetSQL;
    property SQLWhere: String read GetSQLWhere;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
  end;
  TPosSale = Class(TComponent)
  private
    FFirst: T_PosSale;
    FLast: T_PosSale;
    FBillId: Integer;
    FAuthTradeCode: String;
    function GetSQL: String;
    procedure SetBillId(const Value: Integer);
    procedure SetAuthTradeCode(const Value: String);
  public
    property BillId: Integer read FBillId write SetBillId;
    property AuthTradeCode: String read FAuthTradeCode write SetAuthTradeCode;
    property First: T_PosSale read FFirst write FFirst;
    property Last: T_PosSale read FLast write FLast;
    property SQL: String read GetSQL;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
    function Append(): T_PosSale;
  end;
  {TPosSaleM}
  T_PosSaleM = Class(TComponent)
  private
    FOddAmt: String;
    FPosNo: String;
    FGetAmt: String;
    FBillId: Integer;
    FGathId: Integer;
    FAuthTradeCode: String;
    function GetSQL: String;
    function GetSQLWhere: String;
  public
    Next: T_PosSaleM;
    property PosNo: String read FPosNo write FPosNo;
    property BillId: Integer read FBillId write FBillId;
    property GathId: Integer read FGathId write FGathId;
    property GetAmt: String read FGetAmt write FGetAmt;
    property OddAmt: String read FOddAmt write FOddAmt;
    property AuthTradeCode: String read FAuthTradeCode write FAuthTradeCode;
    property SQL: String read GetSQL;
    property SQLWhere: String read GetSQLWhere;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
  end;
  TPosSaleM = Class(TComponent)
  private
    FFirst: T_PosSaleM;
    FLast: T_PosSaleM;
    FBillId: Integer;
    FAuthTradeCode: String;
    function GetSQL: String;
    procedure SetBillId(const Value: Integer);
    procedure SetAuthTradeCode(const Value: String);
  public
    property BillId: Integer read FBillId write SetBillId;
    property AuthTradeCode: String read FAuthTradeCode write SetAuthTradeCode;
    property First: T_PosSaleM read FFirst write FFirst;
    property Last: T_PosSaleM read FLast write FLast;
    property SQL: String read GetSQL;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
    function Append(): T_PosSaleM;
  end;
  {TPosSaleC}
  T_PosSaleC = Class(TComponent)
  private
    FDiscAmt: String;
    FSalesId: String;
    FPosNo: String;
    FSaleAmt: String;
    FSpCode: String;
    FInx: String;
    FBillId: Integer;
    FDiscType: String;
    FSalePrice: String;
    FDeptId: String;
    FSpId: String;
    FSaleQty: String;
    FDiscBill: String;
    FFavAmt: String;
    FAuthTradeCode: String;
    function GetSQL: String;
    function GetSQLWhere: String;
  public
    Next: T_PosSaleC;
    property PosNo: String read FPosNo write FPosNo;
    property BillId: Integer read FBillId write FBillId;
    property Inx: String read FInx write FInx;
    property DeptId: String read FDeptId write FDeptId;
    property SpCode: String read FSpCode write FSpCode;
    property SpId: String read FSpId write FSpId;
    property SalePrice: String read FSalePrice write FSalePrice;
    property SaleQty: String read FSaleQty write FSaleQty;
    property SaleAmt: String read FSaleAmt write FSaleAmt;
    property DiscAmt: String read FDiscAmt write FDiscAmt;
    property DiscType: String read FDiscType write FDiscType;
    property DiscBill: String read FDiscBill write FDiscBill;
    property FavAmt: String read FFavAmt write FFavAmt;
    property SalesId: String read FSalesId write FSalesId;
    property AuthTradeCode: String read FAuthTradeCode write FAuthTradeCode;
    property SQL: String read GetSQL;
    property SQLWhere: String read GetSQLWhere;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
  end;
  TPosSaleC = Class(TComponent)
  private
    FFirst: T_PosSaleC;
    FLast: T_PosSaleC;
    FBillId: Integer;
    FAuthTradeCode: String;
    function GetSQL: String;
    procedure SetBillId(const Value: Integer);
    procedure SetAuthTradeCode(const Value: String);
  public
    property BillId: Integer read FBillId write SetBillId;
    property AuthTradeCode: String read FAuthTradeCode write SetAuthTradeCode;
    property First: T_PosSaleC read FFirst write FFirst;
    property Last: T_PosSaleC read FLast write FLast;
    property SQL: String read GetSQL;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
    function Append(): T_PosSaleC;
  end;
  {TPosSaleAuth}
  T_PosSaleAuth = Class(TComponent)
  private
    FBillDate: TDateTime;
    FAuthTradeCode: String;
    FIsVip: Integer;
    FId: String;
    FBillTime: TDateTime;
    FBillId: Integer;
    FPosNo: String;
    FAuthType: String;
    function GetSQL: String;
    function GetSQLWhere: String;
    function GetSQLValid: String;
  public
    Next: T_PosSaleAuth;
    property Id: String read FId write FId;
    property PosNo: String read FPosNo write FPosNo;
    property BillId: Integer read FBillId write FBillId;
    property IsVip: Integer read FIsVip write FIsVip;
    property AuthType: String read FAuthType write FAuthType;
    property AuthTradeCode: String read FAuthTradeCode write FAuthTradeCode;
    property BillDate: TDateTime read FBillDate write FBillDate;
    property BillTime: TDateTime read FBillTime write FBillTime;
    property SQL: String read GetSQL;
    property SQLWhere: String read GetSQLWhere;
    property SQLValid: String read GetSQLValid;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
  end;
  TPosSaleAuth = Class(TComponent)
  private
    FFirst: T_PosSaleAuth;
    FLast: T_PosSaleAuth;
    FBillId: Integer;
    function GetSQL: String;
    procedure SetBillId(const Value: Integer);
    function GetAuthTradeCode: String;
    function GetSQLValid: String;
  public
    property BillId: Integer read FBillId write SetBillId;
    property AuthTradeCode: String read GetAuthTradeCode;
    property First: T_PosSaleAuth read FFirst write FFirst;
    property Last: T_PosSaleAuth read FLast write FLast;
    property SQL: String read GetSQL;
    property SQLValid: String read GetSQLValid;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
    function Append(): T_PosSaleAuth;
  end;

  {TTrade}
  TTrade = Class(TComponent)
  private
    FBillId: Integer;
    FPosSale: TPosSale;
    FPosSaleM: TPosSaleM;
    FPosSaleAuth: TPosSaleAuth;
    FPosSaleC: TPosSaleC;
    FComCode: String;
    procedure SetBillId(const Value: Integer);
    function GetBillId: Integer;
    function GetSQL: String;
    function GetSQLValid: String;
  public
    Next: TTrade;
    property ComCode: String read FComCode write FComCode;
    property BillId: Integer read GetBillId write SetBillId;
    property PosSale: TPosSale read FPosSale write FPosSale;
    property PosSaleM: TPosSaleM read FPosSaleM write FPosSaleM;
    property PosSaleC: TPosSaleC read FPosSaleC write FPosSaleC;
    property PosSaleAuth: TPosSaleAuth read FPosSaleAuth write FPosSaleAuth;
    property SQL: String read GetSQL;
    property SQLValid: String read GetSQLValid;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
  end;

  {TTradeList}
  TTradeList = Class(TComponent)
  private
    FFirst: TTrade;
    FLast: TTrade;
    function GetSQL: TStringList;
    function GetSQLValid: TStringList;
  public
    property First: TTrade read FFirst write FFirst;
    property Last: TTrade read FLast write FLast;
    property SQL: TStringList read GetSQL;
    property SQLValid: TStringList read GetSQLValid;
    Constructor Create(AOwner: TComponent); override;
    //Destructor Destroy; override;
    function Append(ComCode: String = ''; PosNo: String = ''): TTrade;
  end;


implementation

uses uDM;
{ TTradeList }

function TTradeList.Append(ComCode: String; PosNo: String): TTrade;
var
  con: TConnPool;
begin
  if FFirst = nil then
  begin
    FFirst := TTrade.Create(Self);
    FLast := FFirst;
  end
  else
  begin
    FLast.Next := TTrade.Create(Self);
    FLast := FLast.Next;
  end;
  if ComCode <> '' then
  begin
    FLast.ComCode := ComCode;
    if PosNo <> '' then
    begin
      con := DM.LsConnPool.GetConnByComCode(ComCode);
      if con <> nil then
      begin
        FLast.BillId := con.GetMaxBillId(PosNo);
      end;
    end;
  end;
  Result := FLast;
end;

constructor TTradeList.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFirst := nil;
  FLast := nil;
end;

function TTradeList.GetSQL: TStringList;
var
  p: TTrade;
begin
  Result := TStringList.Create;
  p := FFirst;
  while p <> nil do
  begin
    if p.BillId <> -1 then
    begin
      Result.Values[p.ComCode] := Result.Values[p.ComCode] + p.SQL;
    end;
    p := p.Next;
  end;
end;

function TTradeList.GetSQLValid: TStringList;
var
  p: TTrade;
begin
  Result := TStringList.Create;
  p := FFirst;
  while p <> nil do
  begin
    if p.BillId <> -1 then
    begin
      Result.Values[p.ComCode] := Result.Values[p.ComCode] + p.SQLValid;
      if p.Next <> nil then
        Result.Values[p.ComCode] := Result.Values[p.ComCode] + 'UNION'#13#10;
    end;
    p := p.Next;
  end;
end;

{ TTrade }

constructor TTrade.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPosSale := TPosSale.Create(AOwner);
  FPosSaleM := TPosSaleM.Create(AOwner);
  FPosSaleC := TPosSaleC.Create(AOwner);
  FPosSaleAuth := TPosSaleAuth.Create(AOwner);
end;


function TTrade.GetBillId: Integer;
begin
  Result := FBillId;
end;

procedure TTrade.SetBillId(const Value: Integer);
begin
  FBillId := Value;
  FPosSale.BillId := Value;
  FPosSaleM.BillId := Value;
  FPosSaleC.BillId := Value;
  FPosSaleAuth.BillId := Value;
end;

function TTrade.GetSQL: String;
begin
  Result := '';
  if FPosSaleAuth <> nil then
  begin
    if FPosSale <> nil then
    begin
      FPosSale.AuthTradeCode := FPosSaleAuth.AuthTradeCode;
      Result := Result + FPosSale.SQL;
    end;
    if FPosSaleM <> nil then
    begin
      FPosSaleM.AuthTradeCode := FPosSaleAuth.AuthTradeCode;
      Result := Result + FPosSaleM.SQL;
    end;
    if FPosSaleC <> nil then
    begin
      FPosSaleC.AuthTradeCode := FPosSaleAuth.AuthTradeCode;
      Result := Result + FPosSaleC.SQL;
    end;
    Result := Result + FPosSaleAuth.SQL;
  end;
end;

function TTrade.GetSQLValid: String;
begin
  Result := '';
  if FPosSaleAuth <> nil then
  begin
    Result := Result + FPosSaleAuth.SQLValid;
  end;
end;

{ T_PosSale }

constructor T_PosSale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FBillId := -1;
  FRecId := -1;
  Next := nil;
end;

function T_PosSale.GetSQL: String;
begin
  if FBillId = -1 then
    Result := ''
  else
    Result := 'INSERT INTO Pos_Sale(POS_NO, BILL_ID, BILL_TIME, BOOK_DATE, REC_ID)'#13#10
      + 'SELECT ''' + FPosNo + ''', ' + IntToStr(FBillId) + ', '''
      + FormatDateTime('yyyy-mm-dd hh:nn:ss', FBillTime) + ''', '''
      + FormatDateTime('yyyy-mm-dd', FBookDate) + ''', ' + IntToStr(FRecId) + ''#13#10
      + SQLWhere;
end;

function T_PosSale.GetSQLWhere: String;
begin
  Result := 'WHERE NOT EXISTS (SELECT 1  FROM Pos_Sale_Auth WHERE AUTH_TRADE_CODE=''' + FAuthTradeCode + ''')'#13#10;
end;

{ TPosSale }

function TPosSale.Append: T_PosSale;
begin
  if FFirst = nil then
  begin
    FFirst := T_PosSale.Create(Self);
    FLast := FFirst;
  end
  else
  begin
    FLast.Next := T_PosSale.Create(Self);
    FLast := FLast.Next;
  end;
  FLast.BillId := FBillId;
  Result := FLast;
end;

constructor TPosSale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFirst := nil;
  FLast := nil;
end;

function TPosSale.GetSQL: String;
var
  p: T_PosSale;
begin
  Result := '';
  p := FFirst;
  while p <> nil do
  begin
    Result := Result + p.SQL;
    p := p.Next;
  end;
end;

procedure TPosSale.SetAuthTradeCode(const Value: String);
var
  p: T_PosSale;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.AuthTradeCode := Value;
    p := p.Next;
  end;
  FAuthTradeCode := Value;
end;

procedure TPosSale.SetBillId(const Value: Integer);
var
  p: T_PosSale;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.BillId := Value;
    p := p.Next;
  end;
  FBillId := Value;
end;


{ T_PosSaleM }

constructor T_PosSaleM.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Next := nil;
end;

function T_PosSaleM.GetSQL: String;
begin
  if FBillId = -1 then
    Result := ''
  else
    Result := 'INSERT INTO Pos_Salem(POS_NO, BILL_ID, GATH_ID, GET_AMT, ODD_AMT)'#13#10
      + 'SELECT ''' + FPosNo + ''', ' + IntToStr(FBillId) + ', ' + IntToStr(FGathId) + ', ' + FGetAmt + ', ' + FOddAmt + ''#13#10
      + SQLWhere;
end;

function T_PosSaleM.GetSQLWhere: String;
begin
  Result := 'WHERE NOT EXISTS (SELECT 1  FROM Pos_Sale_Auth WHERE AUTH_TRADE_CODE=''' + FAuthTradeCode + ''')'#13#10;
end;

{ TPosSaleM }

function TPosSaleM.Append: T_PosSaleM;
begin
  if FFirst = nil then
  begin
    FFirst := T_PosSaleM.Create(Self);
    FLast := FFirst;
  end
  else
  begin
    FLast.Next := T_PosSaleM.Create(Self);
    FLast := FLast.Next;
  end;
  FLast.BillId := FBillId;
  Result := FLast;
end;

constructor TPosSaleM.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFirst := nil;
  FLast := nil;
end;

function TPosSaleM.GetSQL: String;
var
  p: T_PosSaleM;
begin
  Result := '';
  p := FFirst;
  while p <> nil do
  begin
    Result := Result + p.SQL;
    p := p.Next;
  end;
end;

procedure TPosSaleM.SetAuthTradeCode(const Value: String);
var
  p: T_PosSaleM;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.AuthTradeCode := Value;
    p := p.Next;
  end;
  FAuthTradeCode := Value;
end;

procedure TPosSaleM.SetBillId(const Value: Integer);
var
  p: T_PosSaleM;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.BillId := Value;
    p := p.Next;
  end;
  FBillId := Value;
end;

{ T_PosSaleC }

constructor T_PosSaleC.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Next := nil;
end;

function T_PosSaleC.GetSQL: String;
begin
  if FBillId = -1 then
    Result := ''
  else
    Result := 'INSERT INTO Pos_Salec(POS_NO, BILL_ID, INX, DEPT_ID, SP_CODE, SP_ID, SALE_PRICE, SALE_QTY, SALE_AMT, DISC_AMT, DISC_TYPE, DISC_BILL, FAV_AMT, SALES_ID)'#13#10
      + 'SELECT ''' + FPosNo + ''', ' + IntToStr(FBillId) + ', ' + FInx + ', ' + FDeptId + ', ''' + FSpCode + ''', ' + FSpId + ', ' + FSalePrice + ', ' + FSaleQty + ', ' + FSaleAmt + ', ' + FDiscAmt
      + ', ' + FDiscType + ', ' + FDiscBill + ', ' + FFavAmt + ', ' + FSalesId + ''#13#10
      + SQLWhere;
end;

function T_PosSaleC.GetSQLWhere: String;
begin
  Result := 'WHERE NOT EXISTS (SELECT 1  FROM Pos_Sale_Auth WHERE AUTH_TRADE_CODE=''' + FAuthTradeCode + ''')'#13#10;
end;

{ TPosSaleC }

function TPosSaleC.Append: T_PosSaleC;
begin
  if FFirst = nil then
  begin
    FFirst := T_PosSaleC.Create(Self);
    FLast := FFirst;
  end
  else
  begin
    FLast.Next := T_PosSaleC.Create(Self);
    FLast := FLast.Next;
  end;
  FLast.BillId := FBillId;
  Result := FLast;
end;

constructor TPosSaleC.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFirst := nil;
  FLast := nil;
end;

function TPosSaleC.GetSQL: String;
var
  p: T_PosSaleC;
begin
  Result := '';
  p := FFirst;
  while p <> nil do
  begin
    Result := Result + p.SQL;
    p := p.Next;
  end;
end;

procedure TPosSaleC.SetAuthTradeCode(const Value: String);
var
  p: T_PosSaleC;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.AuthTradeCode := Value;
    p := p.Next;
  end;
  FAuthTradeCode := Value;
end;

procedure TPosSaleC.SetBillId(const Value: Integer);
var
  p: T_PosSaleC;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.BillId := Value;
    p := p.Next;
  end;
  FBillId := Value;
end;

{ T_PosSaleAuth }

constructor T_PosSaleAuth.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Next := nil;
end;

function T_PosSaleAuth.GetSQL: String;
begin
  if FBillId = -1 then
    Result := ''
  else
    Result := 'INSERT INTO Pos_Sale_Auth(ID, POS_NO, BILL_ID, IS_VIP, AUTH_TYPE, AUTH_TRADE_CODE, BILL_DATE, BILL_TIME)'#13#10
      + 'SELECT ''' + FId + ''', ''' + FPosNo + ''', ' + IntToStr(FBillId) + ', ' + IntToStr(FIsVip) + ', ''' + FAuthType + ''', ''' + FAuthTradeCode + ''', '''
      + FormatDateTime('yyyy-mm-dd', FBillDate) + ''', '''
      + FormatDateTime('yyyy-mm-dd hh:nn:ss', FBillTime) + ''''#13#10
      + SQLWhere;
end;

function T_PosSaleAuth.GetSQLValid: String;
begin
  Result := 'SELECT POS_NO, BILL_ID, AUTH_TRADE_CODE FROM Pos_Sale_Auth WHERE AUTH_TRADE_CODE=''' + FAuthTradeCode + ''''#13#10;
end;

function T_PosSaleAuth.GetSQLWhere: String;
begin
  Result := 'WHERE NOT EXISTS (SELECT 1  FROM Pos_Sale_Auth WHERE AUTH_TRADE_CODE=''' + FAuthTradeCode + ''')'#13#10;
end;

{ TPosSaleAuth }

function TPosSaleAuth.Append: T_PosSaleAuth;
begin
  if FFirst = nil then
  begin
    FFirst := T_PosSaleAuth.Create(Self);
    FLast := FFirst;
  end
  else
  begin
    FLast.Next := T_PosSaleAuth.Create(Self);
    FLast := FLast.Next;
  end;
  FLast.BillId := FBillId;
  Result := FLast;
end;

constructor TPosSaleAuth.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFirst := nil;
  FLast := nil;
end;

function TPosSaleAuth.GetAuthTradeCode: String;
var
  p: T_PosSaleAuth;
begin
  Result := '';
  p := FFirst;
  if p <> nil then
  begin
    Result := p.AuthTradeCode;
  end;
end;

function TPosSaleAuth.GetSQL: String;
var
  p: T_PosSaleAuth;
begin
  Result := '';
  p := FFirst;
  while p <> nil do
  begin
    Result := Result + p.SQL;
    p := p.Next;
  end;
end;

function TPosSaleAuth.GetSQLValid: String;
var
  p: T_PosSaleAuth;
begin
  Result := '';
  p := FFirst;
  if p <> nil then
  begin
    Result := 'SELECT POS_NO, BILL_ID, AUTH_TRADE_CODE FROM Pos_Sale_Auth WHERE AUTH_TRADE_CODE=''' + p.AuthTradeCode + ''''#13#10;
  end;
end;

procedure TPosSaleAuth.SetBillId(const Value: Integer);
var
  p: T_PosSaleAuth;
begin
  p := FFirst;
  while p <> nil do
  begin
    p.BillId := Value;
    p := p.Next;
  end;
  FBillId := Value;
end;

end.
