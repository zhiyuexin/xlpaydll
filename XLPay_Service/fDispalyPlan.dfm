object frmDispalyPlan: TfrmDispalyPlan
  Left = 501
  Top = 293
  Width = 718
  Height = 417
  Caption = 'frmDispalyPlan'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TLMDSimplePanel
    Left = 0
    Top = 0
    Width = 710
    Height = 49
    Align = alTop
    Bevel.Mode = bmCustom
    TabOrder = 0
    object LMDButton3: TLMDButton
      Left = 192
      Top = 8
      Width = 75
      Height = 25
      Action = actEdit
      TabOrder = 0
    end
    object LMDButton2: TLMDButton
      Left = 104
      Top = 8
      Width = 75
      Height = 25
      Action = actDelete
      TabOrder = 1
    end
    object LMDButton1: TLMDButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Action = actAdd
      TabOrder = 2
    end
    object LMDButton4: TLMDButton
      Left = 280
      Top = 8
      Width = 75
      Height = 25
      Action = actActive
      TabOrder = 3
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 49
    Width = 710
    Height = 334
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object btnTest: TLMDButton
    Left = 536
    Top = 8
    Width = 75
    Height = 25
    Caption = 'test'#32534#36753
    TabOrder = 2
  end
  object ActionList: TActionList
    Left = 200
    Top = 128
    object actAdd: TAction
      Caption = #22686#21152
    end
    object actEdit: TAction
      Caption = #32534#36753
    end
    object actDelete: TAction
      Caption = #21024#38500
    end
    object actActive: TAction
      Caption = #21551'/'#31105#29992
    end
  end
end
