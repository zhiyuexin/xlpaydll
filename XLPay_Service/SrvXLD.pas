unit SrvXLD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  SvcMgr, MidasLib, ExtCtrls, IniFiles,
  // DB, DBClient, acClasses, acThread,


  Forms,

  rtcSyncObjs,
  rtcInfo, rtcConn,
  rtcThrPool,
  rtcConnProv,

  rtcDataSrv, rtcHttpSrv, rtcTcpSrv, rtcTcpCli,


  //MemUtils, MemData, CRAccess, MemDS,

  //DBAccess, Uni, UniProvider

  uCommon, DB, DBClient;

const
  HOST_DIRECTORY = 'WebContent';
  INDEX_PAGE = 'index.html';
  APP_VERSION = '1.0';

type

  TXLPay = class(TService)
    ServerHTTPS: TRtcHttpServer;
    ServerHTTP: TRtcHttpServer;
    TcpServer: TRtcTcpServer;
    Client: TRtcTcpClient;
    procedure ServerHTTPException(Sender: TRtcConnection; E: Exception);
    procedure ServerHTTPInvalidRequest(Sender: TRtcConnection);
    procedure ServerHTTPListenError(Sender: TRtcConnection; E: Exception);
    procedure ServerHTTPListenLost(Sender: TRtcConnection);
    procedure ServerHTTPListenStart(Sender: TRtcConnection);
    procedure ServerHTTPListenStop(Sender: TRtcConnection);
    procedure ServerHTTPRequestNotAccepted(Sender: TRtcConnection);
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure TcpServerConnect(Sender: TRtcConnection);
    procedure TcpServerDataReceived(Sender: TRtcConnection);
  private
    doServicing, doInitLsDbms: Boolean;
    RunTimeDesign: Boolean;

    function StartService(): Boolean;
    function StopService(): Boolean;
    function PauseService(): Boolean;
    function ContinueService(): Boolean;

    procedure Delay(dwMilliseconds: DWORD);

  public
    function GetServiceController: TServiceController; override;
  end;

var
  XLPay: TXLPay;

implementation

uses ActiveX, ShellAPI, uDMFileProvider, uDMXLDProvider;

{$R *.dfm}

{ TMDS }

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  XLPay.Controller(CtrlCode);
end;

procedure TXLPay.ServiceCreate(Sender: TObject);
begin
  //Name := SERVICE_NAME;
  //DisplayName := SERVICE_NAME;
end;

function TXLPay.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TXLPay.ServiceAfterInstall(Sender: TService);
begin
  InitServiceRegInfo();
  ShellExecute(0, 'open', PChar(ParamStr(0)), nil, nil, SW_SHOWNORMAL);
end;

procedure TXLPay.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Started := StartService;
end;

procedure TXLPay.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Stopped := StopService;
end;

procedure TXLPay.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Paused := PauseService;
end;

procedure TXLPay.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  Continued := ContinueService;
end;

function TXLPay.ContinueService: Boolean;
begin
  Result := StartService;
end;

function TXLPay.PauseService: Boolean;
begin
  Result := StopService;
end;

function TXLPay.StartService: Boolean;
var
  i: Integer;
  ls: TStringList;
begin
  Result := False;
  Exit;
  try
    //创建目录
    CreateAppDirectory;

    Log('--------- XLPay Service VER:' + APP_VERSION + ' ---------');
    Log('[100]启动服务...');
    (*
//    启动RO服务
//    try
//      ROHTTPServer.Port := GetDataPort;
//      Log('启动数据服务(端口：' + IntToStr(ROHTTPServer.Port) + ')...');
//      ROHTTPServer.Active := True;
//    except
//      on e: Exception do
//      begin
//        Log('[191]数据服务启动时发生错误:' + e.Message);
//        Exit;
//      end;
//    end;

    //启动HTTP服务
    try
      ServerHTTP.ServerPort := IntToStr(GetHttpPort);
      Log('启动HTTP服务(端口：' + ServerHTTP.ServerPort + ')...');
      with GetFileProvider do
      begin
        ClearIndexPages;
        AddIndexPage(INDEX_PAGE);
        ClearHosts;
        AddHost('* = ' + ExtractFilePath(ParamStr(0)) + HOST_DIRECTORY);
        AddHost('LOG = ' + ExtractFilePath(ParamStr(0)) + LOG_FOLDER);
        AddHost('ROOT = ' + ExtractFilePath(ParamStr(0)));

        ls := TStringList.Create;
        try
          with TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini') do
            try
              ReadSectionValues('HOSTS', ls);
            finally
              Free;
            end;
          for i := 0 to ls.Count - 1 do
          begin
            Log('加载虚拟目录' + ls.Strings[i]);
            AddHost(ls.Strings[i]);
          end;
        finally
          ls.Free;
        end;

        ClearContentTypes;
        AddContentType('html,zip');
      end;
      //Log(GetDocRoot('*'));
      GetFileProvider.ServerLink.Server := ServerHTTP;
      GetFileProvider.ServerLink.Server := ServerHTTP;
      //GetFileProvider.ServerLink.Server := ServerHTTPS;
      ServerHTTP.Listen;
    except
      on e: Exception do
      begin
        Log('HTTP服务启动时发生错误:' + e.Message);
        Exit;
      end;
    end;

    // 启动TCP通讯服务
    try
      TcpServer.ServerPort := IntToStr(GetCommPort);
      Log('启动通讯服务(端口：' + TCPServer.ServerPort + ')...');
      TcpServer.Listen();
    except
      on e: Exception do
      begin
        Log('[192]通讯服务启动时发生错误:' + e.Message);
        Exit;
      end;
    end;

//    doServicing := False;
//    InitLsDbms;

    RunTimeDesign := GetRunTimeDesign = 1;
    *)
    Result := True;
    Log('[111]服务已启动。');
  except
    on e: Exception do
    begin
      Log('[199]服务启动时发生错误:' + e.Message);
      Exit;
    end;
  end;

end;

function TXLPay.StopService: Boolean;
begin
  Result := False;
  //关闭RO服务
  try
    Log('[121]停止服务...');
    //TCPServer.Active := False;
    TcpServer.Disconnect;
    TCPServer.StopListen;
    Result := True;
    Log('服务已停止。');
  except
    on e: Exception do
    begin
      Log('[129]服务停止时发生错误:' + e.Message);
      Exit;
    end;
  end;
  Log('----------------------------------------------------');
  Log('');
end;

procedure TXLPay.TcpServerConnect(Sender: TRtcConnection);
begin
  if Sender.inMainThread then
  begin
    //DM.RegClient(Sender.PeerAddr);
    //Sender.Write('Hello client ' + Sender.PeerAddr + ':' + Sender.PeerPort);
  end
  else
    Sender.Sync(TcpServerConnect);
end;

procedure TXLPay.TcpServerDataReceived(Sender: TRtcConnection);
var
  RemoteCmd, CmdHead, CmdBody, CmdResult: string;
begin
  if Sender.inMainThread then
  begin
    RemoteCmd := Sender.Read;
    if RemoteCmd = '' then
    begin
      Sender.Write('Welcome to ' + SERVICE_NAME + #13#10 + SERVICE_INFO);
      Exit;
    end;
    if (RemoteCmd <> '') or (Sender is TRtcTcpServer) then
    begin
      CmdHead := '';
      CmdBody := '';
      With Split(RemoteCmd, '->') do
        try
          CmdHead := UpperCase(Strings[0]);
          if Count > 1 then CmdBody := Strings[1];
        finally
          Free;
        end;
      if CmdHead = 'CMD' then
      begin
        log('Execute remote command:' + CmdBody);
        CmdResult := ExecCommand(CmdBody);
        Sender.Write(CmdResult);
        log(CmdResult);
      end
      else if CmdHead = 'DATE' then
      begin
        Sender.Write(FormatDateTime('yyyy-mm-dd', date) + #13#10);
      end
      else if CmdHead = 'TIME' then
      begin
        Sender.Write(FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', now) + #13#10);
      end
      else if (CmdHead = 'Q') OR (CmdHead = 'QUIT') OR (CmdHead = 'EXIT') then
      begin
        Sender.Write('BYE'#13#10);
        Sender.Disconnect;
        Exit;
      end
      else
      begin
        Sender.Write('Command "' + RemoteCmd + '" received, but server can not resolve it.'#13#10);
      end;
    end;
  end
  else
    Sender.Sync(TcpServerDataReceived);
end;

procedure TXLPay.Delay(dwMilliseconds: DWORD);//Longint
var
  iStart, iStop: DWORD;
begin
  iStart := GetTickCount;
  repeat
    iStop := GetTickCount;
    Application.ProcessMessages;
  until (iStop - iStart) >= dwMilliseconds;
end;


procedure TXLPay.ServerHTTPException(Sender: TRtcConnection; E: Exception);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPException, E)
  else
    Log('Unhandled Exception ' + E.ClassName + ':' + #13#10 + E.Message);
end;

procedure TXLPay.ServerHTTPInvalidRequest(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPInvalidRequest)
  else
    Log('Received an invalid Request.');
end;

procedure TXLPay.ServerHTTPListenError(Sender: TRtcConnection; E: Exception);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenError, E)
  else
    Log('Can not Start the Server on Port ' + Sender.ServerPort + #13#10 + E.Message);
end;

procedure TXLPay.ServerHTTPListenLost(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenLost)
  else
  begin
    Log('Server stopped unexpectedly.');
    // todo... restart
  end;
end;

procedure TXLPay.ServerHTTPListenStart(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenStart)
  else
  begin
    Log('Server is running on Port ' + Sender.LocalPort + ' .');
  end;
end;

procedure TXLPay.ServerHTTPListenStop(Sender: TRtcConnection);
begin
  if not Sender.inMainThread then
    Sender.Sync(ServerHTTPListenStop)
  else
  begin
    Log('Server stopped by user.');
  end;
end;

procedure TXLPay.ServerHTTPRequestNotAccepted(Sender: TRtcConnection);
var
  Srv: TRtcDataServer absolute Sender;
begin
  Srv.Write('Bad Request: ' + Srv.Request.FileName);
end;

end.
