unit SrvMDS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  uDM, uCommon, MidasLib, IniFiles,
  uROClient, uROClientIntf, uROServer, uROBinMessage, uROBPDXTCPServer,
  uROBPDXHTTPServer, uROSOAPMessage, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient,
  IdTCPServer, DB, DBClient, acClasses, acThread,
  TCPClient,

  Forms,

  rtcSyncObjs,
  rtcInfo, rtcConn,
  rtcThrPool,
  rtcConnProv,

  rtcDataSrv, rtcHttpSrv, rtcTcpSrv, rtcTcpCli, ExtCtrls;

const
  HOST_DIRECTORY = 'Layout';
  INDEX_PAGE = 'index.html';
  APP_VERSION = '1.1.4';

type

  TMDS = class(TService)
    MSG_BIN_HTTP: TROBinMessage;
    MSG_SOAP_HTTP: TROSOAPMessage;
    ROHTTPServer: TROBPDXHTTPServer;
    ROTCPServer: TROBPDXTCPServer;
    MSG_BIN_TCP: TROBinMessage;
    cdsPlaySourceDetail: TClientDataSet;
    cdsPlaySourceDetailID: TIntegerField;
    cdsPlaySourceDetailPSID: TIntegerField;
    cdsPlaySourceDetailPropertyID: TIntegerField;
    cdsPlaySourceDetailPropertyName: TStringField;
    cdsPlaySourceDetailPropertyValue: TStringField;
    cdsPlaySourceDetailSortID: TIntegerField;
    cdsPlaySourceDetailRMK: TStringField;
    ClientDataSet1: TClientDataSet;
    ClientDataSet1ID: TIntegerField;
    ClientDataSet1PSID: TIntegerField;
    ClientDataSet1PropertyID: TIntegerField;
    ClientDataSet1PropertyName: TStringField;
    ClientDataSet1PropertyValue: TStringField;
    ClientDataSet1SortID: TIntegerField;
    ClientDataSet1RMK: TStringField;
    ClientDataSet2: TClientDataSet;
    ClientDataSet2ID: TIntegerField;
    ClientDataSet2PSID: TIntegerField;
    ClientDataSet2PropertyID: TIntegerField;
    ClientDataSet2PropertyName: TStringField;
    ClientDataSet2PropertyValue: TStringField;
    ClientDataSet2SortID: TIntegerField;
    ClientDataSet2RMK: TStringField;
    ServerHTTPS: TRtcHttpServer;
    ServerHTTP: TRtcHttpServer;
    TcpServer: TRtcTcpServer;
    Client: TRtcTcpClient;
    Timer: TTimer;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure TCPServer2Connect(AThread: TIdPeerThread);
    procedure TCPServer2Disconnect(AThread: TIdPeerThread);
    procedure TCPServer2Exception(AThread: TIdPeerThread; AException: Exception);
    procedure TCPServer2Execute(AThread: TIdPeerThread);
    procedure TcpServerConnect(Sender: TRtcConnection);
    procedure TcpServerDataReceived(Sender: TRtcConnection);
    procedure TcpServerDisconnecting(Sender: TRtcConnection);
    procedure TcpServerException(Sender: TRtcConnection; E: Exception);
    procedure TimerTimer(Sender: TObject);
  private
    Clients: TTCPClient;
    RunTimeDesign: Boolean;
    DataVersion: string;

    function Start(): Boolean;
    function Stop(): Boolean;
    function Pause(): Boolean;
    function Continue(): Boolean;

    procedure BroadCast(const Msg: string); overload;
    procedure BroadCast(const Msg, CastIP, Port: string); overload;
    procedure BroadCast(const Msg: string; const CastIPList: TStringList); overload;
    procedure Delay(dwMilliseconds: DWORD);

  public
    function GetServiceController: TServiceController; override;
  end;

var
  MDS: TMDS;

implementation

uses ActiveX, ShellAPI, uDMFileProvider;

{$R *.dfm}

{ TMDS }

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  MDS.Controller(CtrlCode);
end;

procedure TMDS.ServiceCreate(Sender: TObject);
begin
  Name := SERVICE_NAME;
  DisplayName := SERVICE_NAME;
end;

function TMDS.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TMDS.ServiceAfterInstall(Sender: TService);
begin
  InitServiceRegInfo();
  ShellExecute(0, 'open', PChar(ParamStr(0)), nil, nil, SW_SHOWNORMAL);
end;

procedure TMDS.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Started := Start;
end;

procedure TMDS.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Stopped := Stop;
end;

procedure TMDS.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Paused := Pause;
end;

procedure TMDS.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  Continued := Continue;
end;

function TMDS.Continue: Boolean;
begin
  Result := Start;
end;

function TMDS.Pause: Boolean;
begin
  Result := Stop;
end;

function TMDS.Start: Boolean;
var
  MaxUpdate, i: Integer;
  ls: TStringList;
begin
  Result := False;
  try
    Log('--------- 兴隆POS双屏显示系统   VER:' + APP_VERSION + ' ---------');
    Log('启动服务...');
    //创建目录
    CreateAppDirectory;

    //启动RO服务
    try
      ROHTTPServer.Port := GetDataPort;
      Log('启动数据服务(端口：' + IntToStr(ROHTTPServer.Port) + ')...');
      ROHTTPServer.Active := True;
    except
      on e: Exception do
      begin
        Log('数据服务启动时发生错误:' + e.Message);
        Exit;
      end;
    end;

    //启动HTTP服务
    try
      ServerHTTP.ServerPort := IntToStr(GetHttpPort);
      Log('启动HTTP服务(端口：' + ServerHTTP.ServerPort + ')...');
      with GetFileProvider do
      begin
        ClearIndexPages;
        AddIndexPage(INDEX_PAGE);
        ClearHosts;
        AddHost('* = ' + ExtractFilePath(ParamStr(0)) + HOST_DIRECTORY);
        AddHost('LOG = ' + ExtractFilePath(ParamStr(0)) + LOG_FOLDER);
        AddHost('ROOT = ' + ExtractFilePath(ParamStr(0)));

        ls := TStringList.Create;
        try
          with TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini') do
            try
              ReadSectionValues('HOSTS', ls);
            finally
              Free;
            end;
          for i := 0 to ls.Count - 1 do
          begin
            Log('加载虚拟目录' + ls.Strings[i]);
            AddHost(ls.Strings[i]);
          end;
        finally
          ls.Free;
        end;

        ClearContentTypes;
        AddContentType('html,zip');
      end;
      //Log(GetDocRoot('*'));
      GetFileProvider.ServerLink.Server := ServerHTTP;
      //GetFileProvider.ServerLink.Server := ServerHTTPS;
      ServerHTTP.Listen;
    except
      on e: Exception do
      begin
        Log('HTTP服务启动时发生错误:' + e.Message);
        Exit;
      end;
    end;

    //启动TCP通讯服务
    try
      TcpServer.ServerPort := IntToStr(GetCommPort);
      Log('启动通讯服务(端口：' + TCPServer.ServerPort + ')...');
      TcpServer.Listen();
    except
      on e: Exception do
      begin
        Log('通讯服务启动时发生错误:' + e.Message);
        Exit;
      end;
    end;

    //初始化客户端列表
    try
      MaxUpdate := GetMaxUpdateCount;
      Log('初始化客户端列表...');
      Clients := TTCPClient.Create(MaxUpdate);
      Clients.GetClientsFormDB;
      if Clients.MaxUpdateClientCount > 0 then
        Log('最大同时更新数：' + IntToStr(Clients.MaxUpdateClientCount))
      else
        Log('最大同时更新数：无限制');
    except
      on e: Exception do
      begin
        Log('初始化客户端列表时发生错误:' + e.Message);
        Exit;
      end;
    end;

    RunTimeDesign := GetRunTimeDesign = 1;
    DataVersion := DM.GetDataVersion;

    Result := True;
    Log('服务已启动。');
  except
    on e: Exception do
    begin
      Log('服务启动时发生错误:' + e.Message);
      Exit;
    end;
  end;
end;

function TMDS.Stop: Boolean;
begin
  Result := False;
  //关闭RO服务
  try
    Log('停止服务...');
    //TCPServer.Active := False;
    TcpServer.Disconnect;
    TCPServer.StopListen;
    ROTCPServer.Active := False;
    ROHTTPServer.Active := False;
    DM.Conn.Disconnect;
    Result := True;
    Log('服务已停止。');
  except
    on e: Exception do
    begin
      Log('服务停止时发生错误:' + e.Message);
      Exit;
    end;
  end;
end;

procedure TMDS.TCPServer2Connect(AThread: TIdPeerThread);
begin
  AThread.Connection.WriteLn('Welcome to ' + SERVICE_NAME +
    #13#10 + SERVICE_INFO +
    #13#10'ClientIP:' + AThread.Connection.Socket.Binding.PeerIP +
    #13#10'Port:' + IntToStr(AThread.Connection.Socket.Binding.PeerPort));
  DM.RegClient(AThread.Connection.Socket.Binding.PeerIP);
end;

procedure TMDS.TCPServer2Disconnect(AThread: TIdPeerThread);
begin
  DM.UnRegClient(AThread.Connection.Socket.Binding.PeerIP);
end;

procedure TMDS.TCPServer2Exception(AThread: TIdPeerThread; AException: Exception);
begin
  DM.UnRegClient(AThread.Connection.Socket.Binding.PeerIP);
end;

procedure TMDS.TCPServer2Execute(AThread: TIdPeerThread);
var
  RemoteCmd, CmdHead, CmdBody, CmdResult: string;
begin
  RemoteCmd := AThread.Connection.ReadLn();
  if RemoteCmd = '' then
  begin
    AThread.Connection.WriteLn('Welcome to ' + SERVICE_NAME + #13#10 + SERVICE_INFO);
    Exit;
  end;
end;

procedure TMDS.TcpServerConnect(Sender: TRtcConnection);
begin
  if Sender.inMainThread then
  begin
    //DM.RegClient(Sender.PeerAddr);
    //Sender.Write('Hello client ' + Sender.PeerAddr + ':' + Sender.PeerPort);
  end
  else
    Sender.Sync(TcpServerConnect);
end;

procedure TMDS.TcpServerDataReceived(Sender: TRtcConnection);
var
  s: string;
  RemoteCmd, CmdHead, CmdBody, CmdResult, ClientIP, str1, str2, str3: string;
  WaitTime, ClientPort: Integer;
begin
  if Sender.inMainThread then
  begin
    RemoteCmd := Sender.Read;
    if RemoteCmd = '' then
    begin
      Sender.Write('Welcome to ' + SERVICE_NAME + #13#10 + SERVICE_INFO);
      Exit;
    end;
    if (RemoteCmd <> '') or (Sender is TRtcTcpServer) then
    begin
      CmdHead := '';
      CmdBody := '';
      With Split(RemoteCmd, '->') do
        try
          CmdHead := UpperCase(Strings[0]);
          if Count > 1 then CmdBody := Strings[1];
        finally
          Free;
        end;
      if CmdHead = 'REG' then
      begin
        With Split(CmdBody, '#') do
          try
            if Count = 5 then
            begin
              Clients.Reg(
                Sender.PeerAddr,       //ClientIP
                Strings[0],            //ClientNo
                Strings[1],            //ClientType
                Strings[2],            //ComputerName
                StrToInt(Strings[3]),  //ClientPort
                StrToInt(Strings[4])   //LayoutID
                );
              Sender.Write('VER->' + DataVersion);
            end
            else
            if Count = 6 then
            begin
              Clients.Reg(
                Sender.PeerAddr,       //ClientIP
                Strings[0],            //ClientNo
                Strings[1],            //ClientType
                Strings[2],            //ComputerName
                StrToInt(Strings[3]),  //ClientPort
                StrToInt(Strings[4]),  //LayoutID
                StrToInt(Strings[5])   //WebPort
                );
              Sender.Write('VER->' + DataVersion);
            end
            else
            begin
              Sender.Write('Command Error(' + CmdBody + ')');
            end;
          finally
            Free;
          end;
      end
      else if CmdHead = '[MGR]UPD PLAYSOURCE' then
      begin
        if RunTimeDesign then
          BroadCast('UPD PLAYSOURCE->' + CmdBody);
      end
      else if CmdHead = '[MGR]UPD LAYOUT' then
      begin
        if RunTimeDesign then
          BroadCast('UPD LAYOUT->' + CmdBody);
      end
      else if CmdHead = '[MGR]VER' then   //更新所有
      begin
        DataVersion := DM.GetDataVersion;
        BroadCast('VER->' + DataVersion);
        log('BroadCast>>VER->' + DataVersion);
      end
      else if CmdHead = '[MGR]RESTART' then  //强制重启
      begin
        log('[RESTART][' + CmdBody + ']');
        try
          With Split(CmdBody, ':') do
          begin
            ClientIP := Strings[0];
            ClientPort := StrToInt(Strings[1]);
          end;

          with TIdTCPClient.Create(nil) do
          begin
            try
              Host := ClientIP;
              Port := ClientPort;
              try
                Connect();
                Delay(50);
                if Connected then Write('RESTART')
                else Log('[RESTART]Connect to ' + ClientIP + ':' + IntToStr(ClientPort) + ' failed.');;
              except
                on e: Exception do
                begin
                  log('[RESTART][ERR]' + e.Message);
                end;
              end;
              Disconnect;
            finally
              Free;
            end;
          end;
        except
          on e: Exception do
          begin
            log('[RESTART][ERR]' + e.Message);
          end;
        end;
      end
      else if CmdHead = '[MGR]UPDCFG' then  //修改客户度config.ini
      begin
        log('[UPDCFG][' + CmdBody + ']');
        try
          With Split(CmdBody, ':') do
          begin
            ClientIP := Strings[0];
            ClientPort := StrToInt(Strings[1]);
            str1 := Strings[2];
            str2 := Strings[3];
            str3 := Strings[4];
          end;

          with TIdTCPClient.Create(nil) do
          begin
            try
              Host := ClientIP;
              Port := ClientPort;
              try
                Connect();
                Delay(50);
                if Connected then Write('UPDCFG->' + str1 + ',' + str2 + ',' + str3)
                else Log('[UPDCFG]Connect to ' + ClientIP + ':' + IntToStr(ClientPort) + ' failed.');;
              except
                on e: Exception do
                begin
                  log('[UPDCFG][ERR]' + e.Message);
                end;
              end;
              Disconnect;
            finally
              Free;
            end;
          end;
        except
          on e: Exception do
          begin
            log('[UPDCFG][ERR]' + e.Message);
          end;
        end;
      end
      else if CmdHead = '[MGR]FORCEUPDATE' then  //强制更新
      begin
        DataVersion := DM.GetDataVersion;
        log('[FORCEUPDATE][' + CmdBody + ']UPDALL->' + DataVersion);
        try
          With Split(CmdBody, ':') do
          begin
            ClientIP := Strings[0];
            ClientPort := StrToInt(Strings[1]);
          end;

          with TIdTCPClient.Create(nil) do
          begin
            try
              Host := ClientIP;
              Port := ClientPort;
              try
                Connect();
                Delay(50);
                if Connected then Write('UPDALL->' + DataVersion)
                else Log('[FORCEUPDATE]Connect to ' + ClientIP + ':' + IntToStr(ClientPort) + ' failed.');;
              except
                on e: Exception do
                begin
                  log('[FORCEUPDATE][ERR]' + e.Message);
                end;
              end;
              Disconnect;
            finally
              Free;
            end;
          end;
        except
          on e: Exception do
          begin
            log('[FORCEUPDATE][ERR]' + e.Message);
          end;
        end;
      end
      else if ((CmdHead = 'GET DATA') OR (CmdHead = 'GET')) then   //请求更新
      begin
        //获取最新版本    效率
        DataVersion := DM.GetDataVersion;
        //比较版本
        if DataVersion <> CmdBody then
        begin
          if Clients.MaxUpdateClientCount <= 0 then  //不限制，允许更新
          begin
            Log('[SND][' + Sender.PeerAddr + ']UPD->' + DataVersion);
            Sender.Write('UPD->' + DataVersion);
          end
          else
          begin
            if Clients.CanUpdate(Sender.PeerAddr) then  //允许更新
            begin //Sender.Write('UPD ALL->' + DataVersion + '#' + Clients.ClientDataFromIP[Sender.PeerAddr].SessionID);
              Log('[SND][' + Sender.PeerAddr + ']UPD->' + DataVersion);
              Sender.Write('UPD->' + DataVersion);
            end
            else
            begin      //等待1分多钟
              randomize;
              WaitTime := 3000 + random(60000);
              Sender.Write('WAIT->' + IntToStr(WaitTime));
              Log('[SND][' + Sender.PeerAddr + ']WAIT->' + IntToStr(WaitTime));
            end;
          end;
        end
        else
        begin
          Log('[LASTEST VERSION][' + Sender.PeerAddr + ']' + DataVersion);
          Sender.Write('LASTEST VERSION');
        end;
      end
      else if CmdHead = 'UPD OVER' then   //答复更新结束
      begin
        Log('[UPD OVER][' + Sender.PeerAddr + ']' + CmdBody);
        Clients.ClientDataFromIP[Sender.PeerAddr].Updating := False;
      end
      else if (CmdHead = 'BROADCAST') or (CmdHead = 'BST') then  //广播
      begin
        Sender.Write('BroadCast message:' + CmdBody);
        BroadCast(CmdBody);
      end
      else if CmdHead = 'CMD' then
      begin
        log('Execute remote command:' + CmdBody);
        CmdResult := ExecCommand(CmdBody);
        Sender.Write(CmdResult);
        log(CmdResult);
      end
      else if CmdHead = 'DATE' then
      begin
        Sender.Write(FormatDateTime('yyyy-mm-dd', date));
      end
      else if CmdHead = 'TIME' then
      begin
        Sender.Write(FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', now));
      end
      else if (CmdHead = 'QUIT') OR (CmdHead = 'EXIT') then
      begin
        DM.UnRegClient(Sender.PeerAddr);
        Sender.Write('BYE');
        Sender.Disconnect;
        Exit;
      end
      else
      begin
        Sender.Write('Command "' + RemoteCmd + '" received, but server can not resolve it.');
      end;
    end;
  end
  else
    Sender.Sync(TcpServerDataReceived);
end;

procedure TMDS.TcpServerDisconnecting(Sender: TRtcConnection);
begin
  if Sender.inMainThread then
  begin
    DM.UnRegClient(Sender.PeerAddr);
  end
  else
    Sender.Sync(TcpServerDisconnecting);
end;

procedure TMDS.TcpServerException(Sender: TRtcConnection; E: Exception);
begin
  if Sender.inMainThread then
  begin
    DM.UnRegClient(Sender.PeerAddr);
  end
  else
    Sender.Sync(TcpServerException, E);
end;


procedure TMDS.BroadCast(const Msg: string);
begin
  //Clients.Add(DM.ClientList(False));
  Clients.BroadCast(Msg);
end;

procedure TMDS.BroadCast(const Msg, CastIP, Port: string);
begin
  Clients.BroadCast(Msg, CastIP, Port);
end;

procedure TMDS.BroadCast(const Msg: string; const CastIPList: TStringList);
begin
  Clients.BroadCast(Msg, CastIPList);
end;

procedure TMDS.TimerTimer(Sender: TObject);
begin
  DataVersion := DM.GetDataVersion;
  Clients.Save;
end;

procedure TMDS.Delay(dwMilliseconds: DWORD);//Longint
var
  iStart, iStop: DWORD;
begin
  iStart := GetTickCount;
  repeat
    iStop := GetTickCount;
    Application.ProcessMessages;
  until (iStop - iStart) >= dwMilliseconds;
end;

end.
