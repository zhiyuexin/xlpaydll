unit uCommon;

interface

uses
  SysUtils, Windows, Classes,
  UniProvider, SQLiteUniProvider, AccessUniProvider,
  SQLServerUniProvider, OracleUniProvider, MySQLUniProvider,
  InterBaseUniProvider, DB2UniProvider, ASEUniProvider,
  AdvantageUniProvider, ODBCUniProvider,
  DBAccess, Uni, IniFiles;

const
  OUTPUT_DEBUG_INFO = True;
  SERVICE_NAME = 'XLPayService';
  SERVICE_INFO = SERVICE_NAME + ' '#10#13'Version: 1.0 (20150422) '#10#13'';
  DB_FILE_NAME = 'XLPay.db';
  CONFIG_FILE = 'config.ini';
  LISTEN_PORT = 8099;

procedure CreateAppDirectory;
function GetLogFolder: string;

procedure Log(const text: string; const debug: Boolean = False);
procedure WriteToLog(const ext: string; const text: string);
function LOGS_LIVE_DAYS: Integer;
function LOG_FOLDER: string;
function GetLogFile(dt: TDateTime): String;

procedure InitServiceRegInfo();
procedure SetServiceDescription();

function InstallService(des: string = SERVICE_INFO; svr: string = SERVICE_NAME): Boolean;
function UnInstallService(svr: string = SERVICE_NAME): Boolean;
function ServiceStatus(svr: string = SERVICE_NAME): LongWord;
function IsServiceInstalled(svr: string = SERVICE_NAME): Boolean;
function IsServiceInstalled2(svr: string = SERVICE_NAME): Boolean;
function IsServiceStopped(svr: string = SERVICE_NAME): Boolean;
function IsServiceRunning(svr: string = SERVICE_NAME): Boolean;
function IsServicePaused(svr: string = SERVICE_NAME): Boolean;

function RunService(svr: string = SERVICE_NAME): Boolean;
function RunService2(svr: string = SERVICE_NAME): Boolean;
function StopService(svr: string = SERVICE_NAME): Boolean;

function GetDataPort: Integer;
procedure SetDataPort(value: Integer);
function GetHttpPort: Integer;
procedure SetHttpPort(value: Integer);
function GetCommPort: Integer;
procedure SetCommPort(value: Integer);
function GetSrvInterval: Integer;
procedure SetSrvInterval(value: Integer);
function GetMaxUpdateCount: Integer;

function GetRunTimeDesign: Integer;

function LayoutPath(): string;
function GetAppPath: string;
function GetServerURL(): String;
function GetServerIP: String;
procedure SetServerIP(IP: String);
function GetServerPort: Integer;
procedure SetServerPort(Port: Integer);
function GetListenPort: Integer;
procedure SetListenPort(Port: Integer);
function GetUploadInterval: Integer;
procedure SetUploadInterval(UploadInterval: Integer);
procedure SetConnectionConfig(conn: TUniConnection);
procedure GetConnectionConfig(conn: TUniConnection);


function GetCorpID: Integer;
procedure SetCorpID(CorpID: Integer);
function GetCorpName: String;
procedure SetCorpName(CorpName: String);


procedure WFile(const fname: string; const Data: string);
function GetKeyValue(Key: WPARAM): string;

procedure MakeAppAutoRunNT(AppName: string; StartName: string = '');
procedure Wait(t: Integer);

procedure MakeLink(Foldername: string; proName, lnkName, Description: string);
function GetSpecialFolder: string;
function GetSystemPath: string;
function GetWindowsPath: string;


function WinExecAndWait32(Command: String; Visibility: Integer; var mOutputs: string): Cardinal; overload;
function WinExecAndWait32(Command: String; var mOutputs: string): Cardinal; overload;
function WinExecAndWait32(Command: String): Cardinal; overload;
function WinExecAndWait32WithHide(Command: String; var mOutputs: string): Cardinal;


function ExecCommand(ACommandLine: string; AInputStream: TStream; AOutputStream: TStream): Boolean; overload;
function ExecCommand(ACommandLine: string; AInputString: string): string; overload;
function ExecCommand(ACommandLine: string; AInputs: array of string): string; overload;
function ExecCommand(AInputs: array of string): string; overload;
function ExecCommand(AInput: string): string; overload;

function CreateResFile(FileName, FilePath: String; ForceCreate: Boolean = False): Boolean; overload;
function CreateResFile(): Integer; overload;

function Split(const Source, SplitStr: string): TStringList;
function IpToInt(ip: string): Integer;
function IPInValue(ip, val1, val2: string): Boolean;

procedure SearchFile(const path: string; var FileList: TStringList);

implementation
uses
  Messages, Forms, Winsvc, Registry, NTServiceMan, ShellAPI,
  ActiveX, ComObj, StdCtrls, ShlObj, FileCtrl;


function CreateResFile(FileName, FilePath: String; ForceCreate: Boolean = False): Boolean;
var
  Res: TResourceStream;
  Source: TMemoryStream;
begin
  Result := False;
  if not ForceCreate then
  begin
    if FileExists(FilePath) then
    begin
      Exit;
    end;
  end;

  try
    Res := TResourceStream.Create(Hinstance, FileName, RT_RCDATA);
    source := TMemoryStream.Create;//创建内存流
    source.LoadFromStream(res);//将资源流复制给内存流
    source.SaveToFile(FilePath);
    Result := True;
    Log('Create file:' + FileName);
  finally
    source.Free;
    Res.Free;
  end;
end;

function CreateResFile: Integer;
var
  SystemPath, ResFile: string;
begin
  Result := 0;
  try
    //SystemPath := GetSystemPath;
    SystemPath := GetAppPath;

    ResFile := SystemPath + 'midas.dll';
    CreateResFile('midas_dll', ResFile);
    ResFile := SystemPath + 'sqlite3.dll';
    CreateResFile('sqlite3_dll', ResFile);
    ResFile := SystemPath + DB_FILE_NAME;
    CreateResFile('KeyInit_db', ResFile);
    Result := 1;
  except
    on E: Exception do
    begin
      WFile('err.txt', e.message);
      //ShowMessage(E.Message);
      Result := -1;
    end;
  end;
end;

//在指定文件夹建立当前应用程序才的快捷方式
procedure MakeLink(Foldername: string; proName, lnkName, Description: string);
var
  WorkDir, DestName: string;
  aObj: IUnknown;
  MyLink: IShellLink;
  MyPFile: IPersistFile;
  WFileName: WideString;
  curdir: array[0..100] of Char;
begin
  DestName := Foldername + lnkName;
  if FileExists(DestName) then Exit;
  aObj := CreateComObject(CLSID_ShellLink);
  MyLink := aObj as IShellLink;
  MyPFile := aObj as IPersistFile;
  GetCurrentDirectory(sizeof(curdir), curdir);
  workdir := curdir;
  proname := trim(WorkDir) + '\' + proname;
  with MyLink do
  begin
    SetShowCmd(SW_NORMAL);
    SetArguments('');
    SetDescription(pChar(Description));
    SetPath(pChar(proName));
    SetWorkingDirectory(pChar(WorkDir));
  end;
  CreateDir(ExtractFilePath(DestName));
  WFileName := DestName;
  MyPFile.Save(PWChar(WFileName), False);
end;

//get starup Floder获得开始-程序-的启动组 的路径
function GetSpecialFolder: string;
var
  Pidl: PItemIDList;
  handle: THandle;
  LinkDir: string;
begin
  result := '';
  handle := Application.Handle;
  if SUCCEEDED(SHGetSpecialFolderLocation(handle, CSIDL_COMMON_STARTUP, Pidl)) then //别的特殊路径也可以用CSIDL_COMMON或的，在delphi中查一下就可以了
  begin
    SetLength(LinkDir, MAX_PATH);
    SHGetPathFromIDList(Pidl, PChar(LinkDir));
    SetLength(LinkDir, StrLen(PChar(LinkDir)));
    result := linkdir + '\';
  end;
end;
function GetSystemPath: string;
var
  SysDir: array[0..MAX_PATH] of Char;
begin
  GetSystemDirectory(SysDir, MAX_PATH);
  Result := SysDir;
  Result := Result + '\';
end;
function GetWindowsPath: string;
var
  SysDir: array[0..MAX_PATH] of Char;
begin
  GetWindowsDirectory(SysDir, MAX_PATH);
  Result := SysDir;
  Result := Result + '\';
end;

procedure Wait(t: Integer);
var
  TickCount: LongInt;
begin
  TickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
  until GetTickCount - TickCount > t;
end;

procedure MakeAppAutoRunNT(AppName: string; StartName: string = '');
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\Run', True) then
      begin
        if StartName = '' then   StartName := ExtractFileName(AppName);
        WriteString(StartName, AppName);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

procedure CreateAppDirectory;
begin
  if not DirectoryExists(GetLogFolder) then
  begin
    CreateDir(GetLogFolder);
  end;
//  if not DirectoryExists(GetAppPath + HOST_DIRECTORY) then
//  begin
//    CreateDir(GetAppPath + HOST_DIRECTORY);
//  end;
end;

function GetLogFolder: string;
begin
  Result := GetAppPath + LOG_FOLDER;
end;

procedure Log(const text: string; const debug: Boolean = False);
begin
  if (not debug) or (OUTPUT_DEBUG_INFO and debug) then
    WriteToLog('log', '[' + FormatDateTime('YYYY-MM-DD HH:NN:SS.ZZZ', Now) + '] ' + text + ''#13#10);
end;

procedure WriteToLog(const ext: string; const text: string);
var
  MyLogFolder: string;
  procedure Delete_old_logs;
  var
    vdate: TDatetime;
    sr: TSearchRec;
    intFileAge: LongInt;
    myfileage: TDatetime;
  begin
    try
      vdate := Now - LOGS_LIVE_DAYS;
      if FindFirst(MyLogFolder + '\*.' + ext, faAnyFile - faDirectory, sr) = 0 then
        repeat
          intFileAge := FileAge(MyLogFolder + '\' + sr.name);
          if intFileAge > -1 then
          begin
            myfileage := FileDateToDateTime(intFileAge);
            if myfileage < vdate then
            begin
              DeleteFile(pchar(mylogfolder + '\' + sr.name));
            end;
          end;
        until (FindNext(sr) <> 0);
    finally
      SysUtils.FindClose(sr);
    end;
  end;
  procedure File_Append(const fname: string; const Data: string);
  var
    f: integer;
  begin
    f := FileOpen(fname, fmOpenReadWrite + fmShareDenyNone);
    if f < 0 then
    begin
      try
        if LOGS_LIVE_DAYS > 0 then
        begin
          Delete_old_logs;
        end;
      except
        // ignore problems with file deletion
      end;
      f := FileCreate(fname);
      //FileWrite(f, SERVICE_NAME + ' log file'#13#10, Length(SERVICE_NAME + ' log file'#13#10));
    end;
    if f >= 0 then
      try
        if FileSeek(f, 0, 2) >= 0 then
          FileWrite(f, data[1], length(data));
      finally
        FileClose(f);
      end;
  end;
  function GetTempDirectory: String;
  var
    tempFolder: array[0..MAX_PATH] of Char;
  begin
    GetTempPath(MAX_PATH, @tempFolder);
    result := StrPas(tempFolder);
  end;
begin
  if MyLogFolder = '' then
  begin
    MyLogFolder := ExtractFilePath(ParamStr(0));
    if Copy(MyLogFolder, length(MyLogFolder), 1) <> '\' then
      MyLogFolder := MyLogFolder + '\';

    if LOG_FOLDER <> '' then
    begin
      MyLogFolder := MyLogFolder + LOG_FOLDER;

      if not DirectoryExists(MyLogFolder) then
        if not CreateDir(MyLogFolder) then
        begin
          MyLogFolder := GetTempDirectory;
          if Copy(myLogFolder, length(MyLogFolder), 1) <> '\' then
            MyLogFolder := MyLogFolder + '\';
          MyLogFolder := MyLogFolder + LOG_FOLDER;
          if not DirectoryExists(MyLogFolder) then
            CreateDir(MyLogFolder);
        end;
    end;
  end;
  File_Append(myLogFolder + '\' + copy(ExtractFileName(ParamStr(0)), 1, length(ExtractFileName(ParamStr(0))) - 3) + FormatDateTime('YYYYMMDD.', Now) + ext, text);
end;
function LOG_FOLDER: string;
begin
  Result := 'LOG';
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('LogFolder') then
          Result := ReadString('LogFolder');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
function GetLogFile(dt: TDateTime): String;
begin
  Result := ExtractFilePath(ParamStr(0)) + LOG_FOLDER + '\' + copy(ExtractFileName(ParamStr(0)), 1, length(ExtractFileName(ParamStr(0))) - 3) + FormatDateTime('YYYYMMDD', dt) + '.log';
end;

function LOGS_LIVE_DAYS: Integer;
begin
  Result := 0;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('LogLiveDays') then
          Result := ReadInteger('LogLiveDays');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

procedure InitServiceRegInfo();
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        //服务描述信息
        WriteString('ServiceInfo', SERVICE_INFO);
        //服务器地址
        WriteString('ServerIP', '');
        //服务端口号
        //WriteInteger('ServerPort', 9099);
        //服务监听口号
        WriteInteger('ListenPort', LISTEN_PORT);

        if OpenKey('DBConnection', True) then
        begin
          WriteString('ProviderName', 'Oracle');
          WriteString('Server', '');
          WriteString('Database', '');
          WriteString('Username', '');
          WriteString('Password', '');
          WriteInteger('Port', 0);
        end;
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  SetServiceDescription;
end;
//服务描述信息
procedure SetServiceDescription();
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteString('Description', ReadString('ServiceInfo') +
          #10#13'服务监听端口:' + IntToStr(ReadInteger('ListenPort')) +
          //#10#13'上级服务器地址:' + ReadString('ServerIP') +
          //#10#13'上级服务器端口:' + IntToStr(ReadInteger('ServerPort')) +
          '');
      end;
    finally
      Free;
    end;
  end;
end;

function InstallService(des: string = SERVICE_INFO; svr: string = SERVICE_NAME): Boolean;
begin
  Result := False;
  with TNTServiceManager.Create(nil) do
  begin
    try
      ManagerAccess := 3;  // [M_CONNECT, M_CREATE_SERVICE];
      ServiceType := [WIN32_OWN_PROCESS];
      ErrorControl := ERROR_NORMAL;
      ServiceName := svr;
      DisplayName := svr;
      BinaryPathName := ParamStr(0);
      StartType := AUTO_START;
      try
        ActiveManager := true;
        Createservice;
        ActiveService := true;
        Description := des;
        Result := True;
      finally
        ActiveService := false;
        ActiveManager := false;
      end;
    finally
      Free;
    end;
  end;
end;

function UnInstallService(svr: string = SERVICE_NAME): Boolean;
begin
  Result := False;
  if StopService(svr) then
  begin
    with TNTServiceManager.Create(nil) do
    begin
      try
        ManagerAccess := 983099;//[S_ALL_ACCESS];
        ServiceName := svr;
        ActiveManager := true;
        try
          ActiveManager := true;
          DeleteService;
          Result := True;
        finally
          ActiveManager := false;
        end;
      finally
        Free;
      end;
    end;
  end;
end;


function RunService2(svr: string = SERVICE_NAME): Boolean;
begin
  Result := False;
  with TNTServiceManager.Create(nil) do
  begin
    try
      ManagerAccess := 983099;//[S_ALL_ACCESS];
      ServiceName := SERVICE_NAME;
      ActiveManager := true;
      try
        ActiveManager := true;
        ActiveService := true;
        Startservice(nil);
        Result := True;
      finally
        ActiveManager := false;
      end;
    finally
      Free;
    end;
  end;
end;


function ServiceStatus(svr: string = SERVICE_NAME): LongWord;
var
  schService: SC_HANDLE;
  schSCManager: SC_HANDLE;
  ssStatus: TServiceStatus;
  Argv: PChar;
begin
  Result := 0;
  try
    schSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
    schService := OpenService(schSCManager, PChar(svr), SERVICE_ALL_ACCESS);
    if schService > 0 then
    begin
      QueryServiceStatus(schService, ssStatus);
      Result := ssStatus.dwCurrentState;
//      SERVICE_STOPPED          = $00000001;
//      SERVICE_START_PENDING    = $00000002;
//      SERVICE_STOP_PENDING     = $00000003;
//      SERVICE_RUNNING          = $00000004;
//      SERVICE_CONTINUE_PENDING = $00000005;
//      SERVICE_PAUSE_PENDING    = $00000006;
//      SERVICE_PAUSED           = $00000007;
    end;
  finally
    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
  end;
end;
function IsServiceInstalled(svr: string = SERVICE_NAME): Boolean;
begin
  Result := ServiceStatus > 0;
end;
function IsServiceStopped(svr: string = SERVICE_NAME): Boolean;
begin
  Result := ServiceStatus = SERVICE_STOPPED;
end;
function IsServiceRunning(svr: string = SERVICE_NAME): Boolean;
begin
  Result := ServiceStatus = SERVICE_RUNNING;
end;
function IsServicePaused(svr: string = SERVICE_NAME): Boolean;
begin
  Result := ServiceStatus = SERVICE_PAUSED;
end;

function IsServiceInstalled2(svr: string = SERVICE_NAME): Boolean;
var
  schService: SC_HANDLE;
  schSCManager: SC_HANDLE;
  ssStatus: TServiceStatus;
  Argv: PChar;
begin
  Result := False;
  try
    schSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
    schService := OpenService(schSCManager, PChar(svr), SERVICE_ALL_ACCESS);
    Result := schService > 0;
  finally
    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
  end;
end;

function RunService(svr: string = SERVICE_NAME): Boolean;
var
  schService: SC_HANDLE;
  schSCManager: SC_HANDLE;
  ssStatus: TServiceStatus;
  Argv: PChar;
begin
  Result := False;
  try
    schSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
    schService := OpenService(schSCManager, PChar(svr), SERVICE_ALL_ACCESS);

    QueryServiceStatus(schService, ssStatus);
    //if ssStatus.dwCurrentState = SERVICE_PAUSED then
    //if ssStatus.dwCurrentState = SERVICE_STOPPED then
    //if ssStatus.dwCurrentState = SERVICE_STOPPED then
    if StartService(schService, 0, Argv) then
    begin
      while (QueryServiceStatus(schService, ssStatus)) do
      begin
        Sleep(500);
        Application.ProcessMessages;
        if ssStatus.dwCurrentState = SERVICE_START_PENDING then
          Sleep(500)
        else
          Break;
      end;//while
      if ssStatus.dwCurrentState = SERVICE_RUNNING then
        Result := True
      else
        Result := False;
    end
    else
      Result := False;
  finally
    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
  end;
end;

function StopService(svr: string = SERVICE_NAME): Boolean;
var
  schService: SC_HANDLE;
  schSCManager: SC_HANDLE;
  ssStatus: TServiceStatus;
begin
  Result := False;
  try
    schSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
    schService := OpenService(schSCManager, PChar(svr), SERVICE_ALL_ACCESS);
    while (QueryServiceStatus(schService, ssStatus)) do
    begin
      Application.ProcessMessages;
      if ssStatus.dwCurrentState = SERVICE_STOP_PENDING then
        Sleep(500)
      else
        Break;
    end;//while
    if ssStatus.dwCurrentState = SERVICE_STOPPED then
    begin
      Result := True;
    end
    else
    begin
      if ControlService(schService, SERVICE_CONTROL_STOP, ssStatus) then
      begin
        Sleep(500);
        while (QueryServiceStatus(schService, ssStatus)) do
        begin
          Application.ProcessMessages;
          if ssStatus.dwCurrentState = SERVICE_STOP_PENDING then
            Sleep(500)
          else
            Break;
        end;//while
        if ssStatus.dwCurrentState = SERVICE_STOPPED then
          Result := True
        else
          Result := False;
      end
      else
        Result := False;
    end;
  finally
    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
  end;
end;


function GetAppPath: string;
begin
  Result := ExtractFilePath(ParamStr(0));
end;
function LayoutPath(): string;
begin
  Result := GetAppPath + '\Layout\';
end;
function GetDataPort: Integer;
begin
  Result := 8091;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('DataPort') then
          Result := ReadInteger('DataPort');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      Result := ReadInteger('PORT', 'DATA', 8090);
    finally
      Free;
    end;
  end;
end;
procedure SetDataPort(value: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('DataPort', value);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      WriteInteger('PORT', 'DATA', value);
    finally
      Free;
    end;
  end;
end;
function GetHttpPort: Integer;
begin
  Result := 8093;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('HttpPort') then
          Result := ReadInteger('HttpPort');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      Result := ReadInteger('PORT', 'HTTP', 8093);
    finally
      Free;
    end;
  end;
end;
procedure SetHttpPort(value: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('HttpPort', value);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      WriteInteger('PORT', 'HTTP', value);
    finally
      Free;
    end;
  end;
end;

function GetSrvInterval: Integer;
begin
  Result := 1000 * 60 * 5;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('SrvInterval') then
          Result := ReadInteger('SrvInterval');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
procedure SetSrvInterval(value: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('SrvInterval', value);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

function GetCommPort: Integer;
begin
  Result := 8092;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('CommPort') then
          Result := ReadInteger('CommPort');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      Result := ReadInteger('PORT', 'COMM', 8092);
    finally
      Free;
    end;
  end;
end;
procedure SetCommPort(value: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('CommPort', value);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
  Exit;
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      WriteInteger('PORT', 'COMM', value);
    finally
      Free;
    end;
  end;
end;
function GetMaxUpdateCount: Integer;
begin
  Result := 10;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('MaxUpdateCount') then
          Result := ReadInteger('MaxUpdateCount');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
function GetRunTimeDesign: Integer;
begin
  with TIniFile.Create(GetAppPath + CONFIG_FILE) do
  begin
    try
      Result := ReadInteger('DESIGN', 'RUNTIME', 0);
    finally
      Free;
    end;
  end;
end;


function GetServerURL(): String;
begin
  Result := 'http://' + GetServerIP + ':' + IntToStr(GetServerPort) + '/BIN';
end;

function GetServerIP: String;
begin
  Result := '';
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        Result := ReadString('ServerIP');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
procedure SetServerIP(IP: String);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteString('ServerIP', IP);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

function GetCorpID: Integer;
begin
  Result := -1;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('CorpID') then
          Result := ReadInteger('CorpID');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
procedure SetCorpID(CorpID: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('CorpID', CorpID);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
function GetCorpName: String;
begin
  Result := '';
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        Result := ReadString('CorpName');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
procedure SetCorpName(CorpName: String);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteString('CorpName', CorpName);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

function GetServerPort: Integer;
begin
  Result := 0;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('ServerPort') then
          Result := ReadInteger('ServerPort');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

procedure SetServerPort(Port: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('ServerPort', Port);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
function GetListenPort: Integer;
begin
  Result := LISTEN_PORT;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('ListenPort') then
          Result := ReadInteger('ListenPort');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

procedure SetListenPort(Port: Integer);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('ListenPort', Port);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

function GetUploadInterval: Integer;
begin
  Result := 5;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        if ValueExists('UploadInterval') then
          Result := ReadInteger('UploadInterval');
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;
procedure SetUploadInterval(UploadInterval: Integer);
begin
  if UploadInterval = 0 then UploadInterval := 5;
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME, False) then
      begin
        WriteInteger('UploadInterval', UploadInterval);
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;


procedure SetConnectionConfig(conn: TUniConnection);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME + '\DBConnection', True) then
      begin
        with conn do
        begin
          WriteString('ProviderName', ProviderName);
          WriteString('Server', Server);
          WriteString('Database', Database);
          WriteString('Username', Username);
          WriteString('Password', Password);
          WriteInteger('Port', Port);
        end;
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

procedure GetConnectionConfig(conn: TUniConnection);
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('SYSTEM\CurrentControlSet\Services\' + SERVICE_NAME + '\DBConnection', False) then
      begin
        with conn do
        begin
          ProviderName := 'Oracle';
          Server := '';
          Database := '';
          Username := '';
          Password := '';
          Port := 0;
          //Options := '';
          if ValueExists('ProviderName') then
            ProviderName := ReadString('ProviderName');
          if ValueExists('Server') then
            Server := ReadString('Server');
          if ValueExists('Database') then
            Database := ReadString('Database');
          if ValueExists('Username') then
            Username := ReadString('Username');
          if ValueExists('Password') then
            Password := ReadString('Password');
          if ValueExists('Port') then
            Port := ReadInteger('Port')
          else
            Port := 0;
        end;
      end;
      CloseKey;
    finally
      Free;
    end;
  end;
end;

function GetKeyValue(Key: WPARAM): string;
begin
  Result := '';
  case Key of
    VK_NUMPAD0:
      Result := '0';
    VK_NUMPAD1:
      Result := '1';
    VK_NUMPAD2:
      Result := '2';
    VK_NUMPAD3:
      Result := '3';
    VK_NUMPAD4:
      Result := '4';
    VK_NUMPAD5:
      Result := '5';
    VK_NUMPAD6:
      Result := '6';
    VK_NUMPAD7:
      Result := '7';
    VK_NUMPAD8:
      Result := '8';
    VK_NUMPAD9:
      Result := '9';

    VK_F1:
      Result := 'F1';
    VK_F2:
      Result := 'F2';
    VK_F3:
      Result := 'F3';
    VK_F4:
      Result := 'F4';
    VK_F5:
      Result := 'F5';
    VK_F6:
      Result := 'F6';
    VK_F7:
      Result := 'F7';
    VK_F8:
      Result := 'F8';
    VK_F9:
      Result := 'F9';
    VK_F10:
      Result := 'F10';
    VK_F11:
      Result := 'F11';
    VK_F12:
      Result := 'F12';

    VK_BACK:
      Result := 'BAk';
    VK_TAB:
      Result := 'KT';
    VK_SPACE:
      Result := 'SB';
    VK_SHIFT, VK_LSHIFT, VK_RSHIFT:
      Result := 'SF';
    VK_CONTROL, VK_LCONTROL, VK_RCONTROL:
      Result := 'CTRL';
    VK_RETURN:
      Result := 'ENT';
    VK_MENU:
      Result := 'ALT';
    VK_PAUSE:
      Result := 'PS';
    VK_CAPITAL:
      Result := 'CL';
    VK_ESCAPE:
      Result := 'ESC';
    VK_PRINT:   //!!!
      Result := 'PS';
    VK_INSERT:
      Result := 'INS';
    VK_DELETE:
      Result := 'DEL';
    VK_PRIOR:
      Result := 'PUP';
    VK_NEXT:
      Result := 'PDOWN';
    VK_END:
      Result := 'END';
    VK_HOME:
      Result := 'HOME';
    VK_LEFT:
      Result := 'LA';
    VK_UP:
      Result := 'UA';
    VK_RIGHT:
      Result := 'RA';
    VK_DOWN:
      Result := 'DA';
    VK_NUMLOCK:
      Result := 'NL';
    VK_SCROLL:
      Result := 'SL';

    VK_DIVIDE:
      Result := '/';
    VK_DECIMAL:
      Result := '.';
    VK_MULTIPLY:
      Result := '*';
    VK_SUBTRACT:
      Result := '-';
    VK_ADD:
      Result := '+';

    186:
      Result := ';';
    187:
      Result := '=';
    189:
      Result := '_';
    188:
      Result := ',';
    190:
      Result := '>';
    191:
      Result := '?';
    192:
      Result := '`';
    219:
      Result := '[';
    220:
      Result := '\';
    221:
      Result := ']';
    222:
      Result := '''';
  else
    Result := Char(Key and $00FF);  //IntToStr(Key);  //IntToStr(Key);  //
  end;
end;

procedure WFile(const fname: string; const Data: string);
var
  f: integer;
begin
  f := FileOpen(fname, fmOpenReadWrite + fmShareDenyNone);
  if f < 0 then
  begin
    f := FileCreate(fname);
  end;
  if f >= 0 then
    try
      if FileSeek(f, 0, 2) >= 0 then
        FileWrite(f, data[1], length(data));
    finally
      FileClose(f);
    end;
end;






function WinExecAndWait32(Command: String): Cardinal; overload;
var
  mOutputs: string;
begin
  Result := WinExecAndWait32(Command, SW_HIDE, mOutputs);
end;
function WinExecAndWait32(Command: String; var mOutputs: string): Cardinal;
begin
  Result := WinExecAndWait32(Command, SW_HIDE, mOutputs);
end;
function WinExecAndWait32WithHide(Command: String; var mOutputs: string): Cardinal;
begin
  Result := WinExecAndWait32(Command, SW_HIDE, mOutputs);
end;
function WinExecAndWait32(Command: String; Visibility: Integer; var mOutputs: string): Cardinal;
var
  sa: TSecurityAttributes;
  hReadPipe, hWritePipe: THandle;
  ret: BOOL;
  strBuff: array[0..255] of char;
  lngBytesread: DWORD;

  WorkDir: String;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  FillChar(sa, Sizeof(sa), #0);
  sa.nLength := Sizeof(sa);
  sa.bInheritHandle := True;
  sa.lpSecurityDescriptor := nil;
  ret := CreatePipe(hReadPipe, hWritePipe, @sa, 0);

  WorkDir := ExtractFileDir(Application.ExeName);
  FillChar(StartupInfo, Sizeof(StartupInfo), #0);
  StartupInfo.cb := Sizeof(StartupInfo);
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
  StartupInfo.wShowWindow := Visibility;

  StartupInfo.hStdOutput := hWritePipe;
  StartupInfo.hStdError := hWritePipe;

  if not CreateProcess(nil,
    PChar(Command),               { pointer to command line string }
    @sa,                           { pointer to process security attributes }
    @sa,                           { pointer to thread security attributes }
    True,                          { handle inheritance flag }
    //CREATE_NEW_CONSOLE or          { creation flags }
    NORMAL_PRIORITY_CLASS,
    nil,                           { pointer to new environment block }
    PChar(WorkDir),                { pointer to current directory name, PChar}
    StartupInfo,                   { pointer to STARTUPINFO }
    ProcessInfo)                   { pointer to PROCESS_INF }
  then
    Result := INFINITE {-1}
  else
  begin
    ret := CloseHandle(hWritePipe);
    mOutputs := '';
    while ret do
    begin
      FillChar(strBuff, Sizeof(strBuff), #0);
      ret := ReadFile(hReadPipe, strBuff, 256, lngBytesread, nil);
      mOutputs := mOutputs + strBuff;
    end;
    Application.ProcessMessages;
    WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
    GetExitCodeProcess(ProcessInfo.hProcess, Result);
    CloseHandle(ProcessInfo.hProcess);  { to prevent memory leaks }
    CloseHandle(ProcessInfo.hThread);
    ret := CloseHandle(hReadPipe);
  end;
end;



function ExecCommand(ACommandLine: string; AInputStream: TStream; AOutputStream: TStream): Boolean; overload;
const
  cBufferSize = $2000;
var
  vReadPipe, vWritePipe: THandle;
  vStdInReadPipe, vStdInWritePipe, vStdInWritePipeDup: THandle;
  vErrReadPipe, vErrWritePipe: THandle;
  vSecurityAttributes: TSecurityAttributes;
  vStartupInfo: TStartupInfo;
  vProcessInfo: TProcessInformation;
  vBufferStdOut, vBufferErrOut, vBufferStdIn: array[0..cBufferSize] of Byte;
  vStdOut, vErrOut: Longword;
  vBufferSize: DWord;
begin
  Result := False;
  vSecurityAttributes.nLength := SizeOf(TSecurityAttributes);
  vSecurityAttributes.lpSecurityDescriptor := nil;
  vSecurityAttributes.bInheritHandle := True;

  vReadPipe := 0;
  vWritePipe := 0;
  vErrReadPipe := 0;
  vErrWritePipe := 0;
  CreatePipe(vStdInReadPipe, vStdInWritePipe, @vSecurityAttributes, cBufferSize);
  DuplicateHandle(GetCurrentProcess(), vStdInWritePipe, GetCurrentProcess(),
    @vStdInWritePipeDup, 0, false, DUPLICATE_SAME_ACCESS);
  CloseHandle(vStdInWritePipe);
  CreatePipe(vReadPipe, vWritePipe, @vSecurityAttributes, cBufferSize);
  try
    CreatePipe(vErrReadPipe, vErrWritePipe, @vSecurityAttributes, cBufferSize);
    try
      FillChar(vStartupInfo, SizeOf(TStartupInfo), 0);
      vStartupInfo.cb := sizeof(TStartupInfo);
      vStartupInfo.dwFlags := STARTF_USESTDHANDLES;
      vStartupInfo.wShowWindow := SW_HIDE;
      vStartupInfo.hStdInput := vStdInReadPipe;
      vStartupInfo.hStdOutput := vWritePipe;
      vStartupInfo.hStdError := vErrWritePipe;
      if CreateProcess(nil, PChar(ACommandLine), @vSecurityAttributes, nil, true, DETACHED_PROCESS,
        nil, nil, vStartupInfo, vProcessInfo) = true then
      begin
        WaitForInputIdle(vProcessInfo.hProcess, 1000);
        vBufferSize := cBufferSize;
        while vBufferSize = cBufferSize do
        begin
          vBufferSize := AInputStream.Read(vBufferStdIn, cBufferSize);
          WriteFile(vStdInWritePipeDup, vBufferStdIn, vBufferSize, vBufferSize, nil);
        end;
        CloseHandle(vStdInWritePipeDup);
        try
          repeat
            PeekNamedPipe(vReadPipe, nil, 0, nil, @vStdOut, nil);
            if vStdOut > 0 then
            begin
              ReadFile(vReadPipe, vBufferStdOut, Length(vBufferStdOut) - 1, vStdOut, nil);
              AOutputStream.Write(vBufferStdOut, vStdOut);
            end;
            PeekNamedPipe(vErrReadPipe, nil, 0, nil, @vErrOut, nil);
            if vErrOut > 0 then
              ReadFile(vErrReadPipe, vBufferErrOut, Length(vBufferErrOut) - 1, vErrOut, nil);
          until WaitForSingleObject(vProcessInfo.hProcess, 0) = WAIT_OBJECT_0;
        finally
          CloseHandle(vProcessInfo.hProcess);
          CloseHandle(vProcessInfo.hThread);
          CloseHandle(vStdInReadPipe);
        end;
        Result := True;
      end;
    finally
      CloseHandle(vErrReadPipe);
      CloseHandle(vErrWritePipe);
    end;
  finally
    CloseHandle(vReadPipe);
    CloseHandle(vWritePipe);
  end;
end;
function ExecCommand(ACommandLine: string; AInputString: string): string; overload;
var
  vInputStream, vOutputStream: TStringStream;
begin
  vInputStream := TStringStream.Create(AInputString);
  vOutputStream := TStringStream.Create('');
  Result := '';
  try
    vInputStream.Position := 0;
    if ExecCommand(ACommandLine, vInputStream, vOutputStream) then
    begin
      vOutputStream.Position := 0;
      Result := vOutputStream.DataString;
    end;
  finally
    vOutputStream.Free;
    vInputStream.Free;
  end;
end;
function ExecCommand(AInput: string): string; overload;
begin
  //Result := ExecCommand('cmd.exe', AInput);
  Result := ExecCommand(AInput, '');
end;
function ExecCommand(AInputs: array of string): string; overload;
var
  vInputs: string;
  I: Integer;
begin
  for I := Low(AInputs) to High(AInputs) do
    vInputs := vInputs + AInputs[I] + #13#10;
  Result := ExecCommand('cmd.exe', vInputs);
end;
function ExecCommand(ACommandLine: string; AInputs: array of string): string; overload;
var
  vInputs: string;
  I: Integer;
begin
  for I := Low(AInputs) to High(AInputs) do
    vInputs := vInputs + AInputs[I] + #13#10;
  Result := ExecCommand(ACommandLine, vInputs);
end;


function Split(const Source, SplitStr: string): TStringList;
var
  Temp: String;
  I: Integer;
  SplitStrLength: Integer;
begin
  Result := TStringList.Create;
  //如果是空自符串则返回空列表
  if Source = '' then Exit;
  Temp := Source;
  I := Pos(SplitStr, Source);
  SplitStrLength := Length(SplitStr);
  while I <> 0 do
  begin
    Result.Add(Copy(Temp, 0, I - 1));
    Delete(Temp, 1, I - 1 + SplitStrLength);
    I := pos(SplitStr, Temp);
  end;
  Result.Add(Temp);
end;

function IpToInt(ip: string): Integer;
var
  Temp: TStringList;
begin
  Result := 0;
  try
    Temp := Split(ip, '.');
    Result := StrToInt(Temp.Strings[0]) * 255 * 255 * 255 + StrToInt(Temp.Strings[1]) * 255 * 255 + StrToInt(Temp.Strings[2]) * 255 + StrToInt(Temp.Strings[3]);
  finally
    Temp.Free;
  end;
end;

function IPInValue(ip, val1, val2: string): Boolean;
begin
  Result := (IpToInt(ip) >= IpToInt(val1)) and (IpToInt(ip) <= IpToInt(val2));
end;


procedure SearchFile(const path: string; var FileList: TStringList);
var
  SearchRec: TSearchRec;
  found: integer;
begin
  found := SysUtils.FindFirst(path + '*.*', faAnyFile, SearchRec);
  while found = 0 do
  begin
    if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') and (SearchRec.Attr <> faDirectory) then
    begin
      FileList.Add(path + SearchRec.Name);
    end
    else if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') and (SearchRec.Attr = faDirectory) then
    begin
      SearchFile(path + SearchRec.Name + '\', FileList);
    end;
    found := SysUtils.FindNext(SearchRec);
  end;
  SysUtils.FindClose(SearchRec);
end;

end.
