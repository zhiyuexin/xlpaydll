unit FrmServiceSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uCommon, uDM, MidasLib,
  {DB}
  DB, Uni, DBAccess, UniDacVcl,
  UniProvider, OracleUniProvider, ASEUniProvider,
  SQLServerUniProvider, SQLiteUniProvider, MySQLUniProvider,
  InterBaseUniProvider, DB2UniProvider, AccessUniProvider,
  {Control}
  LMDCustomButton, LMDButton,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseLabel,
  LMDCustomSimpleLabel, LMDSimpleLabel, ExtCtrls, ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, dxPageControl, MemDS;

type
  TfServiceSetup = class(TForm)
    Conn: TUniConnection;
    ConnectDialog: TUniConnectDialog;
    pnlSetting: TPanel;
    LMDSimpleLabel1: TLMDSimpleLabel;
    btnInstallService: TLMDButton;
    btnUnInstallService: TLMDButton;
    btnStartService: TLMDButton;
    btnStopService: TLMDButton;
    btnDBConnConfig: TLMDButton;
    btnListenPortConfig: TLMDButton;
    btnQuit: TLMDButton;
    btnSrvIntervalConfig: TLMDButton;
    GroupBox1: TGroupBox;
    btnShowLog: TLMDButton;
    edtLogDate: TDateTimePicker;
    procedure FormCreate(Sender: TObject);
    procedure btnDBConnConfigClick(Sender: TObject);
    procedure btnInstallServiceClick(Sender: TObject);
    procedure btnListenPortConfigClick(Sender: TObject);
    procedure btnQuitClick(Sender: TObject);
    procedure btnShowLogClick(Sender: TObject);
    procedure btnSrvIntervalConfigClick(Sender: TObject);
    procedure btnStartServiceClick(Sender: TObject);
    procedure btnStopServiceClick(Sender: TObject);
    procedure btnUnInstallServiceClick(Sender: TObject);
  private
    procedure SetBtnStatus;
  public
    { Public declarations }
  end;

var
  fServiceSetup: TfServiceSetup;

implementation
uses ShellApi;

{$R *.dfm}

procedure TfServiceSetup.FormCreate(Sender: TObject);
begin
  edtLogDate.date := date();
  SetBtnStatus;
end;

procedure TfServiceSetup.btnDBConnConfigClick(Sender: TObject);
begin
  try
    Conn.Disconnect;
    uCommon.GetConnectionConfig(Conn);
    if ConnectDialog.Execute then
    begin
      uCommon.SetConnectionConfig(Conn);
    end;
  finally
    Conn.Connected := False;
  end;
end;

procedure TfServiceSetup.btnInstallServiceClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    If uCommon.InstallService then
    begin
      InitServiceRegInfo();
      ShowMessage('安装服务成功！');
    end
    else
      ShowMessage('安装服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfServiceSetup.btnListenPortConfigClick(Sender: TObject);
var
  s: String;
begin
  s := IntToStr(GetDataPort);
  if InputQuery('设置数据服务监听端口', '数据服务监听端口:', s) then
  begin
    try
      SetDataPort(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;

  s := IntToStr(GetCommPort);
  if InputQuery('设置通讯服务监听端口', '通讯服务监听端口:', s) then
  begin
    try
      SetCommPort(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;

  s := IntToStr(GetHttpPort);
  if InputQuery('设置HTTP服务监听端口', 'HTTP服务监听端口:', s) then
  begin
    try
      SetHttpPort(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;

  SetServiceDescription;
end;

procedure TfServiceSetup.btnQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TfServiceSetup.btnShowLogClick(Sender: TObject);
var
  fileName: String;
begin
  fileName := GetLogFile(edtLogDate.Date);
  if FileExists(fileName) then
    ShellExecute(handle, 'open', PChar(fileName), nil, nil, SW_SHOWNORMAL)
  else
    ShowMessage('未找到日志文件');
end;

procedure TfServiceSetup.btnSrvIntervalConfigClick(Sender: TObject);
var
  s: String;
begin
  s := IntToStr(GetSrvInterval);
  if InputQuery('设置服务轮询时间间隔(单位:毫秒)', '轮询时间间隔(毫秒):', s) then
  begin
    try
      SetSrvInterval(StrToInt(s));
    except
      on e: Exception do
      begin
        ShowMessage('错误:' + e.Message);
      end;
    end;
  end;
end;

procedure TfServiceSetup.btnStartServiceClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    If uCommon.RunService then
      ShowMessage('启动服务成功！')
    else
      ShowMessage('启动服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfServiceSetup.btnStopServiceClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    If uCommon.StopService then
      ShowMessage('停止服务成功！')
    else
      ShowMessage('停止服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfServiceSetup.btnUnInstallServiceClick(Sender: TObject);
begin
  if not IsServiceStopped then
  begin
    ShowMessage('请先停止服务！');
    exit;
  end;
  try
    Screen.Cursor := crHourGlass;
    If uCommon.UnInstallService then
      ShowMessage('卸载服务成功！')
    else
      ShowMessage('卸载服务失败！');
  finally
    SetBtnStatus;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfServiceSetup.SetBtnStatus;
var
  Enabled: Boolean;
begin
  Enabled := IsServiceInstalled;
  btnInstallService.Enabled := not Enabled;
  btnUnInstallService.Enabled := Enabled;
  btnDBConnConfig.Enabled := Enabled;
  btnListenPortConfig.Enabled := Enabled;
  btnSrvIntervalConfig.Enabled := Enabled;
  btnStartService.Enabled := Enabled and not IsServiceRunning;
  btnStopService.Enabled := Enabled and not btnStartService.Enabled;
end;

end.
