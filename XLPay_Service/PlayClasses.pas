unit PlayClasses;

interface

uses
  SysUtils, Variants, Classes;

type


  TPlayControl = class(TObject)
  private
    FX, FY, FWidth, FHeight: Integer;
    FPlaySourceType: Integer; //播放源类型
    FPlaySourceName: string;  //播放源名称
    FName: string;            //控件名称
  public
    property X: Integer read FX write FX;
    property Y: Integer read FY write FY;
    property Left: Integer read FX write FX;
    property Top: Integer read FY write FY;
    property Width: Integer read FWidth write FWidth;
    property Height: Integer read FHeight write FHeight;

    property PlaySourceType: Integer read FPlaySourceType write FPlaySourceType;
    property PlaySourceName: string read FPlaySourceName write FPlaySourceName;
    property Name: string read FName write FName;

    constructor Create(); overload;
    constructor Create(PSType, PSID: Integer; L, T, W, H: Integer); overload;
  end;

implementation


{ TPlayControl }

constructor TPlayControl.Create;
begin
  inherited;
end;

constructor TPlayControl.Create(PSType, PSID, L, T, W, H: Integer);
begin
  Create;
end;

end.
 