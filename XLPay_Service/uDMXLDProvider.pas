unit uDMXLDProvider;

interface

uses
  SysUtils, Classes, rtcDataSrv, rtcInfo, rtcConn;

type
  TXLD_Provider = class(TDataModule)
    ServerLink: TRtcDataServerLink;
    DataProvider1: TRtcDataProvider;
    procedure DataProvider1CheckRequest(Sender: TRtcConnection);
    procedure DataProvider1DataReceived(Sender: TRtcConnection);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function GetXLDProvider: TXLD_Provider;
var
  XLD_Provider: TXLD_Provider;

implementation

{$R *.dfm}

function GetXLDProvider: TXLD_Provider;
begin
  if not assigned(XLD_Provider) then
    XLD_Provider := TXLD_Provider.Create(nil);
  Result := XLD_Provider;
end;

procedure TXLD_Provider.DataProvider1CheckRequest(Sender: TRtcConnection);
var
  Srv: TRtcDataServer absolute Sender;
begin
  if Srv.Request.FileName = 'test' then
    Srv.Accept;
end;

procedure TXLD_Provider.DataProvider1DataReceived(Sender: TRtcConnection);
var
  Srv: TRtcDataServer absolute Sender;
  Result: TRtcRecord;
begin
  if Srv.Request.Complete then
  begin
    { If content body is expected, use "Srv.Read" or "Srv.ReadEx" to read it. }
    // MyContent := Srv.Read;

    { Then, return the appropriate response based on the request headers and/or content body. }
    begin
      // Some Headers might be required by the 3rd-party Client
      // Set them before you start to write the response content out
      Srv.Response['Content-Type'] := 'application/json; charset=UTF-8';
      Srv.Response['Server'] := 'Simple RTC Test Server';
      Srv.Response['X-Powered-By'] := 'RTC SDK ' + ServerLink.Server.Version_SDK;
      Srv.Response['Date'] := DateTime2Str(Now);
    end;

    Srv.Write('{result:true}');
  end;
end;

end.
