unit PlaySource;

interface

uses
  SysUtils, Variants, Classes;

type

  TPlaySourceType = class(TObject)
  private
  public
    class function GetPlaySourceType(PSID: Integer): Integer;
  end;

  TPlaySource = class(TObject)
  private
    //播放源类型
    FPlaySourceType: Integer;
    //播放源ID
    FPlaySourceID: Integer;
    //播放源名称
    FName: string;
    //是否启用
    FEnabled: boolean;
    //备注
    FRemark: string;
    //属性
    FOptions: TStringList;

  public
    property PlaySourceType: Integer read FPlaySourceType write FPlaySourceType;
    property PlaySourceID: Integer read FPlaySourceID write FPlaySourceID;
    property Name: string read FName write FName;
    property Enabled: boolean read FEnabled write FEnabled;
    property Remark: string read FRemark write FRemark;
    property Options: TStringList read FOptions write FOptions;

    constructor Create(); overload;
    constructor Create(PSName: string; PSID: Integer); overload;
  end;


implementation

{ TPlaySourceType }

class function TPlaySourceType.GetPlaySourceType(PSID: Integer): Integer;
begin
  Result := 1;
end;

{ TPlaySource }

constructor TPlaySource.Create;
begin
  inherited;
end;

constructor TPlaySource.Create(PSName: string; PSID: Integer);
begin
  Create;
  FName := PSName;
  FPlaySourceType := TPlaySourceType.GetPlaySourceType(PSID);
  FPlaySourceID := PSID;
end;


end.
